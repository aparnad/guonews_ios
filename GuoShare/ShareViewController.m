//
//  ShareViewController.m
//  GuoShare
//
//  Created by Amol Hirkane on 03/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ShareViewController.h"
#import "ShareHandler.h"
@interface ShareViewController ()

@end

@implementation ShareViewController

- (BOOL)isContentValid {
    // Do validation of contentText and/or NSExtensionContext attachments here
    return YES;
}

- (void)didSelectPost {
    // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    
  //  [ShareHandler shareExtension];
    for (NSItemProvider* itemProvider in ((NSExtensionItem*)self.extensionContext.inputItems[0]).attachments ) {
        
        if([itemProvider hasItemConformingToTypeIdentifier:@"public.jpeg"]) {
            NSLog(@"itemprovider = %@", itemProvider);
            
            [itemProvider loadItemForTypeIdentifier:@"public.jpeg" options:nil completionHandler: ^(id<NSSecureCoding> item, NSError *error) {
                
                NSData *imgData;
                NSString *imgPath = nil ;
                if([(NSObject*)item isKindOfClass:[NSURL class]]) {
                    imgData = [NSData dataWithContentsOfURL:(NSURL*)item];
                    
                    imgPath = ((NSURL*)item).path;
                }
                if([(NSObject*)item isKindOfClass:[UIImage class]]) {
                    imgData = UIImagePNGRepresentation((UIImage*)item);
                }
                
                if (imgData ) {
                    NSDictionary *dict = @{
                                           @"imgData" : imgData,
                                           @"message" : self.contentText,
                                           @"imagePath": imgPath? imgPath: @""
                                
                                           };
                    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.message.extension1"];
                    [defaults setObject:dict forKey:@"img"];
                    [defaults synchronize];
                }
                
            }];
        }
    }
    
//    [self.extensionContext openURL:[NSURL URLWithString:@"guomedia://"]
//                 completionHandler:^(BOOL success) {
//                     NSLog(@"MAIN APP OPENED %d",success);
//                 }];
//
    UIResponder* responder = self;
    while ((responder = [responder nextResponder]) != nil)
    {
        NSLog(@"responder = %@", responder);
        if([responder respondsToSelector:@selector(openURL:)] == YES)
        {
            [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:@"guomedia://"]];
        }
    }
    
    // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
    [self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
}



- (NSArray *)configurationItems {
    // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
    return @[];
}

@end
