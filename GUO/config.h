//
//  config.h
//  GUO
//
//  Created by mac on 04/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#ifndef config_h
#define config_h



#pragma mark - API URL

//Live
#define BASE_URL                @"https://www.guo.media/"
#define API_QUERY_KEY           @"2y10goLM7YVrc.T2G6AvkMdfS.XUF9Wej5O5XPLwL/H2S59z9yvM6W.AG"

//Test
//#define BASE_URL                @"https://stage.guo.media"
//#define API_QUERY_KEY                @"1234"

//#define WEB_DOMAIN                @"https://stage.guo.media/"
#define WEB_DOMAIN                @"https://www.guo.media/"

//#define COPY_BASE_URL @"https://stage.guo.media/posts/"
#define COPY_BASE_URL @"https://guo.media/posts/"


#define API_LOGIN                   @"login"
#define API_SIGNUP                  @"signup"
#define API_GET_LIKED_POST          @"sign_up"
#define API_FORGOT_PASSWORD         @"forget_password"
#define API_CHANGE_PASSWORD         @"change_password"
#define API_FOLLOW                  @"follow"
#define API_UNFOLLOW                @"unfollow"
#define API_FOLLOWERS_LIST          @"get_followers"
#define API_FOLLOWINGS_LIST         @"get_followings"
#define API_GET_COUNT               @"get_counts"
#define API_WHO_CAN_SEND_MSG        @"privacy"
#define API_EDIT_PROFILE_FIELD      @"edit_info"
#define API_EDIT_PROFILE_PIC        @"change_picture"
#define API_EDIT_PROFILE_PIC_TYPE   @"profile_picture"
#define API_EDIT_COVER_PIC_TYPE     @"profile_cover"

#define API_EDIT_PROFILE_COVER_PIC  @"edit_info"
#define API_BLOCK_USER_LIST         @"get_blocked_users"
#define API_UNBLOCK_USER            @"unblock_user"
#define API_BLOCK_USER              @"block_user"
#define API_GET_USER_PROFILE        @"get_user_profile"
#define API_GET_POST                @"get_posts"
#define API_SEARCH                  @"search"
#define API_SEARCH_TYPE             @"mymedia"
#define API_SEARCH_KEYWORD          @"all"
#define API_LIKED_POST              @"get_likes_posts"
#define API_GLOBAL_SEARCH           @"search"
#define API_REPORT_POST             @"report_post"
#define API_GLOBAL_SEARCH_PEOPLE    @"search_people"
#define API_VERIFIED_PROFILE        @"change_email"
#define API_DELETE_USER             @"delete_user"
#define API_LOGOUT                  @"logout"
#define API_UPDATE_PUSH_TOKEN       @"update_push_token"
#define API_SET_LANGUAGE           @"set_language"
#define API_MUTE_POST              @"mute_post"

#define API_SUPPORT                 @"support_email"

#define global_search_type_post @"posts"
#define global_search_type_people @"users"
#define global_search_type_media @"media"


#define API_GET_NOTIFICATION        @"notification"

#define API_LIKE_POST               @"like_post"
#define API_UNLIKE_POST             @"unlike_post"
#define API_SHARE_POST              @"share"

#define API_LIKE_COMMENT            @"like_comment"
#define API_UNLIKE_COMMENT          @"unlike_comment"

#define API_COMMENT_IDENTIFICATION  @"comments"
#define API_REPLY_TO_POST           @"post"
#define API_REPLY_TO_COMMENT        @"comment"
#define API_REPLY_TO_PHOTO          @"photo"

#define API_ADD_POST                @"add_post"

#define API_DELETE_POST   @"delete_post"
#define API_DELETE_COMMENT          @"delete_comment"

#define API_POST_DETAILS            @"get_post"
#define API_POST_DETAILS_WITH_POST_MODEL            @"get_post_post"

#define API_NOTIFICATION_UNREAD_COUNT   @"notification_unread_count"
#define API_SET_NOTIFICATION_READ       @"set_notification_read"
#define API_SET_MAX_NOTIFICATION_ID     @"set_max_notification_id"

#define API_TRANSLATE_TEXT              @"ms_translation"

#define API_NOTIFICATION_ACTIVATE       @"notification_activate"

#define p_name @"profile_img"
#define w_name @"wall_img"

#define IMAGE_COMPRESSION   0.4
#define CREATE_POST_CHARACTER_LIMIT 400
#define CREATE_POST_CHARACTER_LIMIT_BOSS 600



#define REMOTE_API     [[RemoteAPI alloc] init]
#define UTILS      Utils.getSharedInstance
#define defaultUserImg [UIImage imageNamed:@"default_user"]
#define defaultPostImg [UIImage imageNamed:@"default-postImg"]
#define defaultWallImg [UIImage imageNamed:@"img_user_wall"]

#define tempImg [UIImage imageNamed:@"1"]



#define AWS_POOL_ID     @"us-east-1:bee3678d-42ac-43da-89de-1ef7907458c4"
//#define S3_bUCKET_NAME   @"content.guo.media/uploads/"
#define S3_bUCKET_NAME   @"test-guo-media1"


#define CLOUDFRONT_URL  @"https://d57iplyuvntm7.cloudfront.net/uploads/"


//#define HTTPImgURL @"https://" S3_bUCKET_NAME
//#define HTTPImgThumbURL @"https://" S3_bUCKET_NAME @"photos/thumb/"


#define HTTPImgURL  CLOUDFRONT_URL
#define HTTPImgThumbURL CLOUDFRONT_URL @"photos/thumb/"


#define APP_AWS_ACCELERATION_KEY @"transfer-acceleration"

#define MAX_IMAGE_SELECTION_IN_CREATE_POST 9




//#define HTTPImgURL @"https://content.guo.media/uploads/"
//#define HTTPImgThumbURL @"https://content.guo.media/uploads/photos/thumb/"



//==================================================
//Screen resolutions
#define isIphone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define isIpad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define AppObj (AppDelegate *)[[UIApplication sharedApplication] delegate]

#define Screen_Height [[UIScreen mainScreen] bounds].size.height
#define Screen_Width [[UIScreen mainScreen] bounds].size.width

// check 3.5 inch devices
#define iPhone4 ([[UIScreen mainScreen] bounds].size.height == 480)

// check iPhone5 inch devices
#define iPhone5 ([[UIScreen mainScreen] bounds].size.height == 568)

// check iPhone6 inch devices
#define iPhone6 ([[UIScreen mainScreen] bounds].size.height == 667)

// check iPhone6Plus inch devices
#define iPhone6Plus ([[UIScreen mainScreen] bounds].size.height == 736)

//check iPad devices
#define iPad ([[UIScreen mainScreen] bounds].size.height == 1024)
#define iPadPro ([[UIScreen mainScreen] bounds].size.height == 1366)

#define Monika_storyboard @"Main_Monika"
#define PAWAN_storyboard @"Pawan_Storyboard"

#define LowerMenuCellIdentifier @"LowerMenuIdentifier"
#define UpperMenuCellIdentifier @"CellUpperMenuIdentifier"

#define followingscreen @"following_screen"
#define followersscreen @"followers_screen"
#define Select_Everyone @"public"
#define Select_Follow   @"follow"

#define NOTIFICATION_GLOBAL_SEARCH   @"NOTIFICATION_GLOBAL_SEARCH"


#define GUO_USERID   @"356"
#define DEVICE_TYPE  @"iPhone"

//==================================================
#define English_str @"English"
#define Chinese_str @"Chinese"

#define SetEnglish @"en"
#define SetChinese @"zh-Hant"
///=======

#define Please_Wait NSLocalizedString(@"Please Wait...",nil)
#define CheckInternetConnection NSLocalizedString(@"NoInternet",nil) 


#define NO_DATE_FOUND_FONT     [UIFont fontWithName:Font_regular size:18]
#define separator_height 2

//#define Font_regular @"Roboto-Regular"
//#define Font_Medium @"Roboto-Medium"
//#define Font_regular @"SFProDisplay-Regular"
//#define Font_Medium @"SFProDisplay-Bold"

//#define Font_regular @"HelveticaNeue-Light"

//#define Font_regular @ "HelveticaNeue"
//#define Font_Medium @"HelveticaNeue-Medium"
//#define Font_regular @"Gotham-Bold"
//#define Font_Medium @"Gotham-Bold"

#define Font_regular  @"OpenSans"
#define Font_Medium   @"OpenSans-Semibold"



#define CONST_PAGE_WIDTH      320.0



#define landing_page_button 22

#define slogan_font_size 20
#define txtfield_font_size 17
#define btn_font_size 18
#define header_lbl_font_size 18
#define user_name_lbl_font_size 15
#define drawer_Cell_title_font 15
#define drawer_User_title_font_size 17
#define username_font_size 13
#define userfull_name_font_size 17
#define post_desc 14//18
#define drawer_following_follower 15//18


#define date_time_font_size 11
#define You_follow_font_size 16
#define more_font_size 13
#define notification_text_font_size 16
#define search_textfield_font_size 15.0f


#define textfield_border_size 1
#define textfield_bg_alpha 0.5
#define textfield_font_size 18.0f
#define button_font_size 19.0f
#define segment_tab_font_size 16.0f


#define textfield_corner_radius 5
#define textfield_left_padding 15
#define header_font_size 18.0f

#define header_user_img_border 1.5

#define pullDownToRefreshSleepCount 1
#define RecordLimit @"10"


#pragma mark - color code

#define SlidingMenuBGColor ColorRGB(33, 47, 64)
#define SlidingMenuText_IconColor ColorRGB(255, 255, 255)
#define Sliding_HeaderBgColor    ColorRGB(237,237,237)
#define Sliding_SelectedCellBGColor    ColorRGB(52,72,92)




//#define red_color ColorRGB(223,0,11)
#define search_for_everything_color ColorRGB(12,50,112)
#define status_bar_color ColorRGB(43,85,161)

#define textfield_border_color ColorRGBA(255,255,255,0.5)
#define textfield_bg_color ColorRGBA(19,66,142,textfield_bg_alpha)
#define button_bg_color ColorRGB(255,255,255)
#define view_BGColor ColorRGB(255,255,255)
#define header_color ColorRGB(19,66,142)
#define header_lbl_text_color ColorRGB(255,255,255)
#define light_gray_bg_color ColorRGB(240,240,240)
#define medium_gray_bg_color ColorRGB(228,228,228)

#define floating_button_bg_color [UIColor colorWithRed:19/255.0 green:66/255.0 blue:142/255.0 alpha:0.8]

#define gray_icon_color ColorRGB(117,117,117)

#define tbl_separator_color ColorRGBA(170,170,170,0.5)

#define textfield_text_color ColorRGB(255,255,255)
#define popup_bg_color ColorRGB(255,255,255)

#define ColorRGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f]

#define ColorRGBA(r,g,b,alpha_val) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:alpha_val]
//Floated Textfield and TextView


#define kJVFieldHeight 44.0f
#define kJVFieldHMargin 10.0f

#define kJVFieldFontSize 17.0f

#define kJVFieldFloatingLabelFontSize 10



#define privacy_screen @"privacy"
#define tnc_screen @"tnc"

#define privacy_url @"https://www.guo.media/static/privacy"
#define tnc_url @"https://www.guo.media/static/privacy"
#define aboutus_url @"https://www.guo.media/static/privacy"
#define contactus_url @"https://www.guo.media/static/privacy"
#define GUO_Profile_url @"https://www.guo.media/milesguo"
#define Guo_name @"Guo Wengui"

#define RGB(r,g,b)          [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:1.0]

//#define t&c @""

#define UPLOAD_SUCCESS_NOTIFICATION     @"UPLOAD_SUCCESS_NOTIFICATION"
#define NEW_PUSH_NOTIFICATION           @"NEW_PUSH_NOTIFICATION"


#define GUO_DETAILS         @"Twitter: @KwokMiles\n\nYouTube: wenguiguo@gmail.com\n\nTelegram: +1(718)404-2488。\n\n蓝金黄：蓝 - 网络监控！信息窃盗！金 - 金钱买通！市场威胁！黄 - 女色诱惑！女色陷阱！蓝金黄将毁掉世界文明！是对每个人的威胁！\n\nBGY：Blue - Lies & Rumor, Cyber Attack, Media Control! Gold - Money, Kleptocracy Bribery! Yellow - Sex Seduction! BGY is destroying the culture and threatening human beings of whole world!"
#endif /* config_h */



//https://github.com/ElectricEasel/shindaiwacordless/blob/master/fonts/HelveticaNeue/HelveticaNeue-Medium.ttf
