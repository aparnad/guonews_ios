//
//  OneImageView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 10/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "OneImageView.h"

@implementation OneImageView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer *tapImgOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSinglePoster)];
    tapImgOne.numberOfTapsRequired = 1;
    [self.imgOnePost1 addGestureRecognizer:tapImgOne];

    [self.btnPlayVideo addTarget:self action:@selector(btnPlayVideoClicked) forControlEvents:UIControlEventTouchUpInside];

}

-(void)tapOnSinglePoster
{
    if (self.imgBlock) {
        self.imgBlock();
    }
}

-(void)btnPlayVideoClicked
{
    if (self.btnBlock) {
        self.btnBlock();
    }
}

-(void)onShowImage:(noArgBlock)blk
{
    self.imgBlock = blk;
}

-(void)onPlayButtonClicked:(noArgBlock)blk
{
    self.btnBlock = blk;
}

@end
