//
//  OneImageView.h
//  GUO Media
//
//  Created by Pawan Ramteke on 10/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OneImageView : UIView

@property (nonatomic,copy)noArgBlock imgBlock;
@property (nonatomic,copy)noArgBlock btnBlock;

@property (weak, nonatomic) IBOutlet UIImageView *imgOnePost1;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayVideo;

-(void)onShowImage:(noArgBlock)blk;
-(void)onPlayButtonClicked:(noArgBlock)blk;
@end
