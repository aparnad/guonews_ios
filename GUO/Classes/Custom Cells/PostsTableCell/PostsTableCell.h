//
//  PostsTableCell.h
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "OneImageView.h"
#import "TwoImageView.h"
#import "ThreeImageView.h"
typedef void(^btnBlock)(NSIndexPath *indexPath);

@interface PostsTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewDp;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDescription;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewPoster;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPoster2;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPoster3;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UIButton *btnMorePhotos;
@property (weak, nonatomic) IBOutlet UIButton *btnViewProfile,*btnNameViewProfile;
@property (strong, nonatomic)AVPlayerViewController *playerViewController;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIButton *btnSocialShare;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;

@property (weak, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgPostWdthConstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgPostHtConstr;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgPoster2HtConstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgPoster2WdthConstr;

@property (weak, nonatomic) IBOutlet UIView *shareBaseView;

@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *baseViewLeadingConstr;


@property (nonatomic,copy)btnBlock viewProfileBlock;
@property (nonatomic,copy)btnBlock moreBlock;
@property (nonatomic,copy)btnBlock likeBlock;
@property (nonatomic,copy)btnBlock commentBlock;
@property (nonatomic,copy)btnBlock shareBlock;
@property (nonatomic,copy)btnBlock playBlock;
@property (nonatomic,copy)btnBlock morePhotosBlock;
@property (nonatomic,copy)btnBlock bigPosterBlock;
@property (nonatomic,copy)btnBlock multiPosterBlock;
@property (nonatomic,copy)btnBlock socialShareBlock;
@property (nonatomic,copy)btnBlock chatBlock;

@property (nonatomic,copy)btnBlock translateBlock;
@property (nonatomic,copy)btnBlock textViewBlock;


@property (weak, nonatomic) IBOutlet UIButton *btnPlayVideo;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewShareDp;
@property (weak, nonatomic) IBOutlet UILabel *lblShareName;
@property (weak, nonatomic) IBOutlet UILabel *lblShareTIme;
@property (weak, nonatomic) IBOutlet UILabel *lblShareDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shareViewHtConstr;
@property (weak, nonatomic) IBOutlet UIButton *btnShareMore;
@property (weak, nonatomic) IBOutlet UIButton *btnTranslate;
@property (weak, nonatomic) IBOutlet UIButton *btnShareTranslate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnShareTranslateHtConst;
@property (weak, nonatomic) IBOutlet UILabel *lblShareTranslate;
@property (weak, nonatomic) IBOutlet UILabel *lblTranslate;
@property (weak, nonatomic) IBOutlet UITextView *txtViewTranslate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblShareDescTopConstr;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTranslatedByHtConstr;
@property (weak, nonatomic) IBOutlet UILabel *lblTranslatedBy;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewShareTranslatedByHtConstr;

@property (weak, nonatomic) IBOutlet UILabel *lblShareTranslatedBy;

@property (weak, nonatomic) IBOutlet UIView *viewPostContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewPostContainerHtConstr;

@property (weak, nonatomic) OneImageView *viewSingleImagePost;
@property (weak, nonatomic) TwoImageView *viewTwoImagePost;
@property (weak, nonatomic) ThreeImageView *viewThreeImagePost;


@property (strong, nonatomic) NSString *videoURL;
@property (nonatomic,strong)AVPlayerLayer *videoLayer;


-(void)viewProfile:(btnBlock)blk;

-(void)onMoreButtonClicked:(btnBlock)blk;
-(void)onLikeButtonClicked:(btnBlock)blk;
-(void)onCommenButtonClicked:(btnBlock)blk;
-(void)onShareButtonClicked:(btnBlock)blk;
-(void)onSocialShareButtonClicked:(btnBlock)blk;
-(void)onChatButtonClicked:(btnBlock)blk;

-(void)onTranslateButtonClicked:(btnBlock)blk;

-(void)onPlayButtonClicked:(btnBlock)blk;
-(void)onMorePhotosButtonClicked:(btnBlock)blk;

-(void)onSinglePosterClick:(btnBlock)blk;
-(void)onMultiPosterClick:(btnBlock)blk;

-(void)onTextViewTap:(btnBlock)blk;

-(void)configureCell:(NSString *)imageUrl videoURL:(NSString *)videoUrl;
-(CGFloat)visibleVideoHeight;
-(void)pauseVideo;
-(CMTime)getPlayerCurrentTime;
@end
