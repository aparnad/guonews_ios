//
//  ThreeImageView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 10/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ThreeImageView.h"

@implementation ThreeImageView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer *tapImgOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnFirstImage)];
    tapImgOne.numberOfTapsRequired = 1;
    [self.imgThreePost1 addGestureRecognizer:tapImgOne];
    
    UITapGestureRecognizer *tapImgTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSecondImage)];
    tapImgTwo.numberOfTapsRequired = 1;
    [self.imgThreePost2 addGestureRecognizer:tapImgTwo];
    
    UITapGestureRecognizer *tapImgThree = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnThirdImage)];
    tapImgThree.numberOfTapsRequired = 1;
    [self.imgThreePost3 addGestureRecognizer:tapImgThree];
    
    [self.btnMorePhotos addTarget:self action:@selector(btnMorePhotosClicked) forControlEvents:UIControlEventTouchUpInside];
}


-(void)tapOnFirstImage
{
    if (self.img1Block) {
        self.img1Block();
    }
}

-(void)tapOnSecondImage
{
    if (self.img2Block) {
        self.img2Block();
    }
}

-(void)tapOnThirdImage
{
    if (self.img3Block) {
        self.img3Block();
    }
}

-(void)btnMorePhotosClicked
{
    if (self.morePhotosBlock) {
        self.morePhotosBlock();
    }
}

-(void)onTapFirstImage:(noArgBlock)blk
{
    self.img1Block = blk;
}
-(void)onTapSecondImage:(noArgBlock)blk
{
    self.img2Block = blk;
}
-(void)onTapThirdImage:(noArgBlock)blk
{
    self.img3Block = blk;
}
-(void)onMorePhotoClicked:(noArgBlock)blk{
    self.morePhotosBlock = blk;
}

@end
