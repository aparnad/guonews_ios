//
//  ThreeImageView.h
//  GUO Media
//
//  Created by Pawan Ramteke on 10/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThreeImageView : UIView

@property (nonatomic,copy)noArgBlock img1Block;
@property (nonatomic,copy)noArgBlock img2Block;
@property (nonatomic,copy)noArgBlock img3Block;
@property (nonatomic,copy)noArgBlock morePhotosBlock;

@property (weak, nonatomic) IBOutlet UIImageView *imgThreePost1;
@property (weak, nonatomic) IBOutlet UIImageView *imgThreePost2;
@property (weak, nonatomic) IBOutlet UIImageView *imgThreePost3;
@property (weak, nonatomic) IBOutlet UIButton *btnMorePhotos;

-(void)onTapFirstImage:(noArgBlock)blk;
-(void)onTapSecondImage:(noArgBlock)blk;
-(void)onTapThirdImage:(noArgBlock)blk;
-(void)onMorePhotoClicked:(noArgBlock)blk;

@end
