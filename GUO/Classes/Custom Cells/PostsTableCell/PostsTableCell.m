//
//  PostsTableCell.m
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "PostsTableCell.h"
#import "UIImageView+AppImageView.h"
#import "VideoPlayerController.h"
@implementation PostsTableCell
@synthesize playerViewController;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [Utils SetRoundedCorner:self.imgViewDp];
    [Utils SetRoundedCorner:self.imgViewShareDp];
    [self.btnMore addTarget:self action:@selector(btnMoreClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnShareMore addTarget:self action:@selector(btnMoreClicked) forControlEvents:UIControlEventTouchUpInside];

    [self.btnLike addTarget:self action:@selector(btnLikeClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnComment addTarget:self action:@selector(btnCommentClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnShare addTarget:self action:@selector(btnShareClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnSocialShare addTarget:self action:@selector(btnSocialShareClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnChat addTarget:self action:@selector(btnChatClicked) forControlEvents:UIControlEventTouchUpInside];

    [self.btnViewProfile addTarget:self action:@selector(btnViewProfileClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNameViewProfile addTarget:self action:@selector(btnViewProfileClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [Utils makeHalfRoundedButton:self.btnTranslate];
    [self.btnTranslate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.btnTranslate.layer.masksToBounds = YES;
    [self.btnTranslate setTitle:NSLocalizedString(@"Translate", nil) forState:UIControlStateNormal];
    [self.btnTranslate addTarget:self action:@selector(btnTranslateClicked) forControlEvents:UIControlEventTouchUpInside];
    self.btnTranslate.titleLabel.font = [UIFont fontWithName:Font_Medium size:12];

    
    [Utils makeHalfRoundedButton:self.btnShareTranslate];
    [self.btnShareTranslate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.btnShareTranslate.layer.masksToBounds = YES;
    [self.btnShareTranslate setTitle:NSLocalizedString(@"Translate", nil) forState:UIControlStateNormal];
    self.btnShareTranslate.titleLabel.font = self.btnTranslate.titleLabel.font;
    
    [self.btnShareTranslate addTarget:self action:@selector(btnTranslateClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTextDescription)];
    tap.numberOfTapsRequired = 1;
    [self.txtViewDescription addGestureRecognizer:tap];

    self.viewPostContainer.layer.cornerRadius = 10;
    self.viewPostContainer.clipsToBounds = YES;
    
    [self setSingleImagePost];
    
    [self setTwoImagePost];

    [self setMultipleImagePost];

    self.lblName.font = [UIFont fontWithName:Font_Medium size:[UTILS getFlexibleHeight:userfull_name_font_size]];
    self.lblTime.font = [UIFont fontWithName:Font_regular size:[UTILS getFlexibleHeight:date_time_font_size]];
    self.lblDescription.font = self.txtViewDescription.font =   [UIFont fontWithName:Font_regular size:[UTILS getFlexibleHeight:post_desc]];
    self.lblDescription.textColor = self.txtViewDescription.textColor = [UIColor blackColor];
    
    self.lblShareDescription.font = self.lblDescription.font;
    self.lblShareTIme.font = self.lblTime.font;
    self.lblShareName.font = self.lblName.font;
    self.lblTranslate.font = self.txtViewTranslate.font =  self.lblDescription.font;
    self.lblShareTranslate.font = self.lblShareDescription.font;
    
    self.lblTranslatedBy.font = [UIFont fontWithName:Font_regular size:14];
    self.lblTranslatedBy.textColor = header_color;
    self.lblTranslatedBy.text = NSLocalizedString(@"Translated By", nil);
    
    self.lblShareTranslatedBy.font = self.lblTranslatedBy.font;
    self.lblShareTranslatedBy.textColor = self.lblTranslatedBy.textColor;
    self.lblShareTranslatedBy.text = NSLocalizedString(@"Translated By", nil);
    
    self.videoLayer = [[AVPlayerLayer alloc]init];
    self.videoLayer.backgroundColor = [UIColor clearColor].CGColor;
    self.videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.viewSingleImagePost.btnPlayVideo.layer addSublayer:self.videoLayer];
}

-(void)setSingleImagePost
{
    NSArray *viewSingleImage = [[NSBundle mainBundle] loadNibNamed:@"OneImageView" owner:self options:nil];
    
    self.viewSingleImagePost = viewSingleImage[0];
    [self.viewPostContainer addSubview:self.viewSingleImagePost];
    
    [self.viewSingleImagePost enableAutolayout];
    [self.viewSingleImagePost leadingMargin:0];
    [self.viewSingleImagePost trailingMargin:0];
    [self.viewSingleImagePost topMargin:0];
    [self.viewSingleImagePost bottomMargin:0];
    
    [self.viewSingleImagePost onShowImage:^{
        if (self.bigPosterBlock) {
            self.bigPosterBlock(self.indexPath);
        }
    }];
    
    [self.viewSingleImagePost onPlayButtonClicked:^{
        if (self.playBlock) {
            self.playBlock(self.indexPath);
        }
    }];
}

-(void)setTwoImagePost
{
    NSArray *viewTwoImage = [[NSBundle mainBundle] loadNibNamed:@"TwoImageView" owner:self options:nil];
    
    self.viewTwoImagePost = viewTwoImage[0];
    [self.viewPostContainer addSubview:self.viewTwoImagePost];
    
    [self.viewTwoImagePost enableAutolayout];
    [self.viewTwoImagePost leadingMargin:0];
    [self.viewTwoImagePost trailingMargin:0];
    [self.viewTwoImagePost topMargin:0];
    [self.viewTwoImagePost bottomMargin:0];
    
    [self.viewTwoImagePost onTapFirstImage:^{
        if (self.bigPosterBlock) {
            self.bigPosterBlock(self.indexPath);
        }
    }];
    
    [self.viewTwoImagePost onTapSecondImage:^{
        if (self.multiPosterBlock) {
            self.multiPosterBlock(self.indexPath);
        }
    }];
    
}

-(void)setMultipleImagePost
{
    NSArray *viewThreeImage = [[NSBundle mainBundle] loadNibNamed:@"ThreeImageView" owner:self options:nil];
    
    self.viewThreeImagePost = viewThreeImage[0];
    [self.viewPostContainer addSubview:self.viewThreeImagePost];
    
    [self.viewThreeImagePost enableAutolayout];
    [self.viewThreeImagePost leadingMargin:0];
    [self.viewThreeImagePost trailingMargin:0];
    [self.viewThreeImagePost topMargin:0];
    [self.viewThreeImagePost bottomMargin:0];
    
    [self.viewThreeImagePost onTapFirstImage:^{
        if (self.bigPosterBlock) {
            self.bigPosterBlock(self.indexPath);
        }
    }];
    
    [self.viewThreeImagePost onTapSecondImage:^{
        if (self.multiPosterBlock) {
            self.multiPosterBlock(self.indexPath);
        }
    }];
    
    [self.viewThreeImagePost onTapThirdImage:^{
        
    }];
    
    [self.viewThreeImagePost onMorePhotoClicked:^{
        if (self.morePhotosBlock) {
            self.morePhotosBlock(self.indexPath);
        }
    }];
}


-(void)btnMoreClicked
{
    if (self.moreBlock) {
        self.moreBlock(self.indexPath);
    }
}

-(void)tapOnTextDescription
{
    if (self.textViewBlock) {
        self.textViewBlock(self.indexPath);
    }
}

-(void)onMoreButtonClicked:(btnBlock)blk
{
    self.moreBlock = blk;
}
-(void)viewProfile:(btnBlock)blk
{
    self.viewProfileBlock = blk;
}
-(void)btnViewProfileClicked
{
    if (self.viewProfileBlock) {
        self.viewProfileBlock(self.indexPath);
    }
}
-(void)btnLikeClicked
{
    if (self.likeBlock) {
        self.likeBlock(self.indexPath);
    }
}

-(void)onLikeButtonClicked:(btnBlock)blk
{
    self.likeBlock = blk;
}

-(void)btnCommentClicked
{
    if (self.commentBlock) {
        self.commentBlock(self.indexPath);
    }
}

-(void)onCommenButtonClicked:(btnBlock)blk
{
    self.commentBlock = blk;
}

-(void)btnShareClicked
{
    if (self.shareBlock) {
        self.shareBlock(self.indexPath);
    }
}

-(void)btnTranslateClicked
{
    if (self.translateBlock) {
        self.translateBlock(self.indexPath);
    }
}
-(void)onShareButtonClicked:(btnBlock)blk
{
    self.shareBlock = blk;
}

-(void)btnSocialShareClicked
{
    if (self.socialShareBlock) {
        self.socialShareBlock(self.indexPath);
    }
}

-(void)btnChatClicked
{
    if (self.chatBlock) {
        self.chatBlock(self.indexPath);
    }
}

-(void)onSocialShareButtonClicked:(btnBlock)blk
{
    self.socialShareBlock = blk;
}

-(void)onChatButtonClicked:(btnBlock)blk
{
    self.chatBlock = blk;
}


-(void)btnPlayVideoClicked{
    if (self.playBlock) {
        self.playBlock(self.indexPath);
    }
}

-(void)onPlayButtonClicked:(btnBlock)blk
{
    self.playBlock = blk;
}

-(void)btnMorePhotosClicked
{
    self.morePhotosBlock(self.indexPath);
}

-(void)onMorePhotosButtonClicked:(btnBlock)blk
{
    self.morePhotosBlock = blk;
}

-(void)onSinglePosterClick:(btnBlock)blk
{
    self.bigPosterBlock = blk;
}

-(void)onMultiPosterClick:(btnBlock)blk
{
    self.multiPosterBlock = blk;
}

-(void)onTranslateButtonClicked:(btnBlock)blk
{
    self.translateBlock = blk;
}

-(void)onTextViewTap:(btnBlock)blk
{
    self.textViewBlock = blk;
}


-(void)setupPlayer:(NSString *)videoURL
{
    [[VideoPlayerController getSharedInstance]setupVideoFor:videoURL postCell:self];
    self.videoLayer.hidden = videoURL == nil;
}

-(void)configureCell:(NSString *)imageUrl videoURL:(NSString *)videoUrl
{
   // self.imgViewPoster.imageURL = imageUrl;
    self.videoURL = videoUrl;
    if (videoUrl != nil) {
        [self setupPlayer:videoUrl];
    }

}

-(void)pauseVideo
{
    [[VideoPlayerController getSharedInstance] pauseVideoforLayer:self.videoLayer url:self.videoURL];
}

-(CMTime)getPlayerCurrentTime
{
    return self.videoLayer.player.currentItem.currentTime;
}

-(CGFloat)visibleVideoHeight
{
    CGRect videoFrameInParentSuperView = [self.superview.superview convertRect:self.viewSingleImagePost.imgOnePost1.frame fromView:self.viewSingleImagePost.imgOnePost1];
    
    CGRect superViewFrame = self.superview.frame;
    
    CGRect visibleVideoFrame = CGRectIntersection(videoFrameInParentSuperView, superViewFrame);
    
    return visibleVideoFrame.size.height;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
  //  CGFloat horizontalMargin = 20;
  //  CGFloat width = self.bounds.size.width - horizontalMargin * 2;
   // CGFloat height = round(width * 0.9);
    self.videoLayer.frame = self.viewSingleImagePost.imgOnePost1.frame;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse
{
   // self.imgViewPoster.imageURL = nil;
    [super prepareForReuse];
}
@end
