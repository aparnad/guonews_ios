//
//  TwoImageView.h
//  GUO Media
//
//  Created by Pawan Ramteke on 10/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwoImageView : UIView

@property (nonatomic,copy)noArgBlock img1Block;
@property (nonatomic,copy)noArgBlock img2Block;

@property (weak, nonatomic) IBOutlet UIImageView *imgTwoPost1;
@property (weak, nonatomic) IBOutlet UIImageView *imgTwoPost2;

-(void)onTapFirstImage:(noArgBlock)blk;
-(void)onTapSecondImage:(noArgBlock)blk;
@end
