//
//  TwoImageView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 10/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "TwoImageView.h"

@implementation TwoImageView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer *tapImgOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnFirstImage)];
    tapImgOne.numberOfTapsRequired = 1;
    [self.imgTwoPost1 addGestureRecognizer:tapImgOne];
    
    UITapGestureRecognizer *tapImgTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSecondImage)];
    tapImgTwo.numberOfTapsRequired = 1;
    [self.imgTwoPost2 addGestureRecognizer:tapImgTwo];
    
}


-(void)tapOnFirstImage
{
    if (self.img1Block) {
        self.img1Block();
    }
}

-(void)tapOnSecondImage
{
    if (self.img2Block) {
        self.img2Block();
    }
}

-(void)onTapFirstImage:(noArgBlock)blk
{
    self.img1Block = blk;
}
-(void)onTapSecondImage:(noArgBlock)blk
{
    self.img2Block = blk;
}


@end
