//
//  GlobalSearchCollectionCell.h
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^FollowBlock)(NSIndexPath *indexPath);

@interface GlobalSearchCollectionCell : UICollectionViewCell
@property (nonatomic,strong)NSIndexPath *indexPath;
@property (nonatomic,strong)NSArray *arrData;

@property(nonatomic,copy)FollowBlock blk;
@property (nonatomic,copy)FollowBlock refreshBlk;
@property (nonatomic,copy)noArgBlock loadMoreblk;

-(void)onFollowClicked:(FollowBlock)block;
-(void)onRefreshList:(FollowBlock)blk;
-(void)onLoadMore:(noArgBlock)loadMoreBlock;

-(void)endRefreshing;
-(void)apiCallInitiated;
-(void)apiCallEndWithResultCount:(NSInteger)count  pageNo:(NSInteger)pageNo;
@end
