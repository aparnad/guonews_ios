//
//  GlobalSearchCollectionCell.m
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "GlobalSearchCollectionCell.h"
#import "PeopleHeaderView.h"
#import "PostsTableCell.h"
#import "TrendsTableCell.h"
#import "YiRefreshFooter.h"
@interface GlobalSearchCollectionCell()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSIndexPath *selIndexPath;
    NSArray *arrSectionTitle;
    NSString *searchType;
    //NSMutableArray *PostArr;
    int current_page;
    YiRefreshFooter *refreshFooter;
    UIRefreshControl *refresh;
    DGActivityIndicatorView *activityIndicatorView;
    UILabel *lblMessage;
    NSArray *tempData;


}
@property(nonatomic,strong) NSDictionary* searchParams;

@end

@implementation GlobalSearchCollectionCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.estimatedRowHeight = 200;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.backgroundColor = [UIColor controllerBGColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:_tableView];
        
        refresh =[[UIRefreshControl alloc]init];
        [refresh addTarget:self action:@selector(refreshList:) forControlEvents:UIControlEventValueChanged];
        [_tableView addSubview:refresh];

        _tableView.backgroundView = [self tableBGView];
        
        
        refreshFooter=[[YiRefreshFooter alloc] init];
        __weak typeof(self) weakSelf = self;
        refreshFooter.beginRefreshingBlock=^(){
            
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                sleep(pullDownToRefreshSleepCount);
                
                if (weakSelf.loadMoreblk) {
                    weakSelf.loadMoreblk();
                }
            });
        };
    }
    return self;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tempData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ConnectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ConnectionCell"];
    if(cell==nil)
    {
        cell=[[ConnectionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ConnectionCell"];
        
        NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"ConnectionCell" owner:self options:nil];
        
        cell=[menuarray objectAtIndex:0];
    }
    
    cell.btnFollow.tag = indexPath.row;
    cell.btnUserImg.tag = indexPath.row;
    
    FollowModel *model = [tempData objectAtIndex:indexPath.row];
    
    cell.btnMore.tag  = indexPath.row;
    cell.lblDesc.hidden = YES;
    cell.lblName.text = [Utils setFullname:@"" lname:@"" username:model.userName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.imgUser sd_setImageWithURL:[Utils getThumbUrl:model.userPicture] placeholderImage:defaultUserImg];
    
    [cell.btnMore addTarget:self action:@selector(onclick_more:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btnFollow addTarget:self action:@selector(onclick_follow:) forControlEvents:UIControlEventTouchUpInside];
    
   // [cell.btnFollow setTitle:model.isFollowing ? NSLocalizedString(@"btn_unfollow", nil) : NSLocalizedString(@"btn_follow", nil) forState:UIControlStateNormal];
    
    
    NSString *status_title = NSLocalizedString(@"btn_follow", nil);
    
    //default color
    cell.btnFollow.backgroundColor = [UIColor whiteColor];
    [cell.btnFollow setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    if(model.isFollowing)
    {
        status_title = NSLocalizedString(@"txt_following", nil);
        cell.btnFollow.backgroundColor = header_color;
        [cell.btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    [cell.btnFollow setTitle:status_title forState:UIControlStateNormal];
    
    
    //default color
   // cell.btnFollow.backgroundColor = [UIColor whiteColor];
   // [cell.btnFollow setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    return cell;

}

-(IBAction)onclick_follow:(UIButton*)sender
{
    NSIndexPath *nowIndex = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;
    if (self.blk) {
        self.blk(nowIndex);
        return;
    }
    
    ConnectionCell *cell = [_tableView cellForRowAtIndexPath:nowIndex];
    FollowModel *model = [[FollowModel alloc]init];
    model = [tempData objectAtIndex:nowIndex.row];
    if(model.isFollowing)
    {
        [cell follow_unfollow_API:API_UNFOLLOW model:model];
    }
    else
    {
        [cell follow_unfollow_API:API_FOLLOW model:model];
    }
    
    
    [cell updateCommentBlock:^(FollowModel *f_model) {
        //        NSLog(@"f_model:%@",f_model.userName);
        [_tableView reloadRowsAtIndexPaths:@[nowIndex] withRowAnimation:UITableViewRowAnimationNone];
    }];
    
    
    //    [self follow_unfollow_API:API_FOLLOW model:model];
    
    [_tableView reloadData];
}
-(void)setIndexPath:(NSIndexPath *)indexPath
{
    selIndexPath = indexPath;
//    if (indexPath.row == 0) {
//        _tableView.tableHeaderView = [self tableHeaderView];
//    }
//    else {
//        _tableView.tableHeaderView = [[UIView alloc]init];
//    }
    
    _tableView.tableHeaderView = [[UIView alloc]init];

    
    [_tableView reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    FollowModel *model = [[FollowModel alloc]init];
    model = [tempData objectAtIndex:indexPath.row];
    
  /*  UserProfileController *userProfileVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"UserProfileController"];
    userProfileVC.other_user_id = model.userId;
    [[UTILS topMostControllerNormal].navigationController pushViewController:userProfileVC animated:YES];*/
    
    
    [UTILS showUserProfileSingleController:model.userId];
}


-(void)refreshList:(UIRefreshControl *)refreshControl
{
    current_page = 1;
    if (self.refreshBlk) {
        self.refreshBlk(self.indexPath);
    }
}


#pragma mark- Remove pull down to refresh
-(void)RemovePullDownToRefresh
{
    //Remove Pull down to refresh when category_total_count match with array
    
    [refreshFooter remove_observer];
    
    refreshFooter.scrollView=nil;
    [refreshFooter.footerView removeFromSuperview];
   // self.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    
}
-(void)AddPullDowntoRefresh
{
    [self RemovePullDownToRefresh];
    refreshFooter.scrollView=_tableView;
    [refreshFooter footer];
}


-(void)setArrData:(NSArray *)arrData
{
    tempData = [NSMutableArray arrayWithArray:arrData];
    if([arrData count] == 0 )
    {
        [self RemovePullDownToRefresh];
    }
    else if(tempData.count >= [RecordLimit intValue])
    {
        [self AddPullDowntoRefresh];
    }
    
    
    
    if (arrData == nil) {
        return;
    }
    if (tempData != nil) {
        if (tempData.count == arrData.count) {
            [self RemovePullDownToRefresh];
        }
        else{
            tempData = arrData;
            [self AddPullDowntoRefresh];
        }
    }
    else{
        tempData = arrData;
        if([arrData count] == 0  || [arrData count] <= [RecordLimit intValue])
        {
            [self RemovePullDownToRefresh];
        }
        else if(arrData.count >= [RecordLimit intValue])
        {
            [self AddPullDowntoRefresh];
        }
    }
    
    
    [_tableView reloadData];
}

-(void)onFollowClicked:(FollowBlock)block
{
    self.blk = block;
}

-(void)onRefreshList:(FollowBlock)blk
{
    self.refreshBlk = blk;
}

-(void)onLoadMore:(noArgBlock)loadMoreBlock
{
    self.loadMoreblk = loadMoreBlock;
}

-(void)endRefreshing
{
    [refresh endRefreshing];
    
}

-(UIView *)tableBGView
{
    lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, _tableView.frame.size.width-20, _tableView.frame.size.height)];
    lblMessage.textAlignment = NSTextAlignmentCenter;
    lblMessage.backgroundColor = [UIColor clearColor];
    lblMessage.font = NO_DATE_FOUND_FONT;
    lblMessage.lineBreakMode = NSLineBreakByWordWrapping;
    lblMessage.numberOfLines = 0;
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
    activityIndicatorView.frame = lblMessage.bounds;//CGRectMake(0,0,100,100);
    
    [lblMessage addSubview:activityIndicatorView];
    // [activityIndicatorView startAnimating];
    return lblMessage;
}


-(void)apiCallInitiated
{
    [activityIndicatorView startAnimating];
    lblMessage.text = @"";
}
-(void)apiCallEndWithResultCount:(NSInteger)count  pageNo:(NSInteger)pageNo
{
    [activityIndicatorView stopAnimating];
    
    if (pageNo == 1 && count == 0) {
        lblMessage.text = NSLocalizedString(@"No data found", nil);
    }
    else
    {
        lblMessage.text = @"";
    }
    
   // [UTILS playSoundOnEndRefresh];
}
@end

