//
//  AboutCollectionCell.m
//  GUO Media
//
//  Created by Pawan Ramteke on 15/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "AboutCollectionCell.h"

@implementation AboutCollectionCell
@synthesize lblAbout;
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        lblAbout = [[UILabel alloc]init];
        lblAbout.font = [UIFont fontWithName:Font_regular size:18];
        lblAbout.numberOfLines = 0;
        lblAbout.text = GUO_DETAILS;
        [self addSubview:lblAbout];
        [lblAbout enableAutolayout];
        [lblAbout leadingMargin:20];
        [lblAbout trailingMargin:20];
        [lblAbout topMargin:20];
    }
    return self;
}
@end
