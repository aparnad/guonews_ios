//
//  ProfileTableCell.h
//  GUO Media
//
//  Created by Pawan Ramteke on 15/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ScrollBlock)(int pageNo);
typedef void(^loadMoreBlock)(NSInteger index);

@interface ProfileTableCell : UITableViewCell
@property (nonatomic,strong)NSDictionary *dataDict;
@property (nonatomic)NSString *userBiography;

@property (nonatomic,copy)ScrollBlock scrollBlk;
@property (nonatomic,copy)loadMoreBlock loadMoreBlock;
@property (nonatomic,copy)loadMoreBlock refreshBlock;
@property (nonatomic,copy)noArgBlock scrollToTopBlk;

-(void)onCollectionScroll:(ScrollBlock)block;
-(void)setCollectionContentOffSet:(NSInteger)page;

-(void)onLoadMore:(loadMoreBlock)loadMoreBlock;
-(void)onRefresh:(loadMoreBlock)refreshBlock;
-(void)onScrollToTop:(noArgBlock)blk;
@end
