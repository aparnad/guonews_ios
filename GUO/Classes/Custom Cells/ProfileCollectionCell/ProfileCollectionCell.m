//
//  ProfileCollectionCell.m
//  GUO
//
//  Created by Pawan Ramteke on 14/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ProfileCollectionCell.h"
#import "PostsTableCell.h"
#import "BroadcastCell.h"
@interface ProfileCollectionCell ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSIndexPath *selIndexPath;
    NSArray *arrSectionTitle;
}
@end

@implementation ProfileCollectionCell
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _tableView = [[UITableView alloc]initWithFrame:self.bounds style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.estimatedRowHeight = 200;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.backgroundColor = [UIColor controllerBGColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        
        [self addSubview:_tableView];
        
        
        
        PostArr = [[NSMutableArray alloc]init];
        
    }
    return self;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selIndexPath.row == 3) {
        BroadcastCell *cell=[tableView dequeueReusableCellWithIdentifier:@"BroadcastCell"];
        if(cell==nil)
        {
            cell=[[BroadcastCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BroadcastCell"];
            
            NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"BroadcastCell" owner:self options:nil];
            cell=[menuarray objectAtIndex:0];
        }
        

        return cell;
    }
    
    PostsTableCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PostsTableCell"];
    if(cell==nil)
    {
        cell=[[PostsTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PostsTableCell"];
        
        NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"PostsTableCell" owner:self options:nil];
        cell=[menuarray objectAtIndex:0];
    }
    if(!self.is_came_From_slide_menu)
    {
        cell.lblName.text =  Guo_name;
        cell.lblTime.text =  @"1 day ago";
        cell.lblDescription.text =  @"尊敬的战友们好：文贵将推动两个在美国国会的听证会．就是有关中国腾讯公司．和马云的阿里巴巴公司．利用大数据．以及或以及他们的网络获取在国外的以及在美国的各种个人信息．为盗国贼服务，或植入木马，软件获取客户的使用信息的证据！如果任何一个战友们有这方面的证据和信息，请发给文贵，万分感谢！一切都是刚刚开始。请登录，以喜欢，分享郭文和留言";
        cell.imgViewDp.image = [UIImage imageNamed:@"img_photo"];
        cell.imgViewPoster.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",@"a9"]];
        cell.imgViewPoster.contentMode = UIViewContentModeScaleToFill;
    }
    return cell;
}

-(void)setIndexPath:(NSIndexPath *)indexPath
{
    selIndexPath = indexPath;
    [_tableView reloadData];
}
@end
