//
//  AboutCollectionCell.h
//  GUO Media
//
//  Created by Pawan Ramteke on 15/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutCollectionCell : UICollectionViewCell
@property (nonatomic,strong)UILabel *lblAbout;
@end
