//
//  ProfilePhotosCollectionCell.h
//  GUO
//
//  Created by Pawan Ramteke on 14/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

typedef void(^OnSizeChangeBlock)(CGFloat height);
@interface ProfilePhotosCollectionCell : UICollectionViewCell

@property (nonatomic,copy)OnSizeChangeBlock block;
@property (nonatomic,copy)noArgBlock blk;
@property (nonatomic,copy)noArgBlock loadMoreblk;

-(void)onContentSizeChange:(OnSizeChangeBlock)blk;
-(void)onScrollToTop:(noArgBlock)block;
-(void)onLoadMore:(noArgBlock)loadMoreBlock;

@property BOOL is_came_From_slide_menu;
@property(strong,nonatomic) NSString *visited_profile_userId;
@property(strong,nonatomic) NSIndexPath *indexPath;
@property(strong,nonatomic) NSArray *arrData;

@end

typedef void(^videoBlock)(NSIndexPath *indexPath);
@interface ProfilePhotosCell:UICollectionViewCell
@property (nonatomic,strong)UIImageView *imgViewPhoto;
@property (nonatomic,strong)UIButton *videoBtn;
@property (strong, nonatomic)AVPlayerViewController *playerViewController;
@property (strong, nonatomic) UIView *videoView;
@property (nonatomic,copy)videoBlock block;
@property (nonatomic,strong)NSIndexPath *indexPath;
-(void)onPlayVideoClicked:(videoBlock)blk;
@end
