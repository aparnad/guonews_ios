//
//  ProfileTableCell.m
//  GUO Media
//
//  Created by Pawan Ramteke on 15/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ProfileTableCell.h"
#import "ProfilePhotosCollectionCell.h"
#import "ProfileCollectionCell.h"
#import "GlobalPostCollectionCell.h"
#import "AboutCollectionCell.h"
#define TAB_ABOUT   @"About"
#define TAB_PHOTOS  @"Photos"
#define TAB_POSTS   @"Posts"
#define TAB_LIKES   @"Likes"
@interface ProfileTableCell()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    UICollectionView *_collectionView;
    NSDictionary *mainDict;
    BOOL isGuo;
}
@end

@implementation ProfileTableCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor controllerBGColor];
        [self addSubview:_collectionView];
        
        [_collectionView enableAutolayout];
        [_collectionView leadingMargin:0];
        [_collectionView topMargin:0];
        [_collectionView trailingMargin:0];
        [_collectionView bottomMargin:0];
        
        _collectionView.pagingEnabled = YES;
        
        [_collectionView registerClass:[AboutCollectionCell class] forCellWithReuseIdentifier:@"AboutCell"];
        
        [_collectionView registerClass:[ProfilePhotosCollectionCell class] forCellWithReuseIdentifier:@"PhotosCell"];
        
        [_collectionView registerClass:[GlobalPostCollectionCell class] forCellWithReuseIdentifier:@"PostCell"];
    }
    return self;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        ProfilePhotosCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotosCell" forIndexPath:indexPath];
        [cell onContentSizeChange:^(CGFloat height) {
        }];
        cell.indexPath = indexPath;
        cell.arrData = [mainDict objectForKey:TAB_PHOTOS];
        [cell onScrollToTop:^{
            if (self.scrollToTopBlk) {
                self.scrollToTopBlk();
            }
        }];
        
        [cell onLoadMore:^{
            if (self.loadMoreBlock) {
                self.loadMoreBlock(indexPath.row);
            }
        }];
        return cell;
    }
    
    if (indexPath.row == 0) {
        //  UTILS.apiToCall = API_GET_POST;
        GlobalPostCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PostCell" forIndexPath:indexPath];
        
        cell.indexPath = indexPath;
        NSArray *arrPosts = [mainDict objectForKey:TAB_POSTS];
        if (arrPosts.count) {
            cell.arrData = arrPosts;
        }
        [cell onscrollToTop:^{
            if (self.scrollToTopBlk) {
                self.scrollToTopBlk();
            }
        }];
        
        [cell onLoadMore:^{
            if (self.loadMoreBlock) {
                self.loadMoreBlock(indexPath.row);
            }
        }];
        
        [cell onRefreshList:^(NSIndexPath *idxPath) {
            if (self.refreshBlock) {
                self.refreshBlock(idxPath.row);
            }
        }];
        return cell;
    }
    
    if (indexPath.row == 2) {
        GlobalPostCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PostCell" forIndexPath:indexPath];
        [cell onscrollToTop:^{
            if (self.scrollToTopBlk) {
                self.scrollToTopBlk();
            }
        }];
        
        cell.indexPath = indexPath;
        cell.arrData = [mainDict objectForKey:TAB_LIKES];
        
        [cell onLoadMore:^{
            if (self.loadMoreBlock) {
                self.loadMoreBlock(indexPath.row);
            }
        }];
        
        [cell onRefreshList:^(NSIndexPath *idxPath) {
            if (self.refreshBlock) {
                self.refreshBlock(idxPath.row);
            }
        }];
        
        return cell;
    }
    

    return nil;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width, collectionView.frame.size.height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

-(void)setDataDict:(NSDictionary *)dataDict
{
    mainDict = dataDict;
    [_collectionView reloadData];
}

-(void)setUserBiography:(NSString *)userBiography
{
    [mainDict setValue:userBiography forKey:TAB_ABOUT];
    isGuo = YES;
    [_collectionView reloadData];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    if (self.scrollBlk) {
        self.scrollBlk(page);
    }
}

-(void)onCollectionScroll:(ScrollBlock)block
{
    self.scrollBlk = block;
}

-(void)setCollectionContentOffSet:(NSInteger)page
{
    [_collectionView setContentOffset: CGPointMake(page * Screen_Width, 0) animated:YES];
}

-(void)onLoadMore:(loadMoreBlock)loadMoreBlock
{
    self.loadMoreBlock = loadMoreBlock;
}

-(void)onRefresh:(loadMoreBlock)refreshBlock
{
    self.refreshBlock = refreshBlock;
}

-(void)onScrollToTop:(noArgBlock)blk
{
    self.scrollToTopBlk = blk;
}
@end
