//
//  ProfilePhotosCollectionCell.m
//  GUO
//
//  Created by Pawan Ramteke on 14/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ProfilePhotosCollectionCell.h"
#import "ASJCollectionViewFillLayout.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "DGActivityIndicatorView.h"
#import "YiRefreshFooter.h"
@interface ProfilePhotosCollectionCell()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>
{
    UICollectionView *collectionViewPhotos;
    int current_page;
    YiRefreshFooter *refreshFooter;
    DGActivityIndicatorView *activityIndicatorView;
    NSMutableArray *arrPhotosVideos;
    UIRefreshControl *refresh;
}

@end
@implementation ProfilePhotosCollectionCell
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        arrPhotosVideos = [NSMutableArray new];
        
        //amoltricks
        collectionViewPhotos.scrollEnabled = NO;

        ASJCollectionViewFillLayout *layout = [[ASJCollectionViewFillLayout alloc]init];
        layout.numberOfItemsInSide = 3;
        layout.itemSpacing = 5;
        layout.stretchesLastItems = NO;
        layout.itemLength = (Screen_Width/3) - 7;
        collectionViewPhotos = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
        collectionViewPhotos.dataSource = self;
        collectionViewPhotos.delegate = self;
        [self addSubview:collectionViewPhotos];
        collectionViewPhotos.backgroundColor = [UIColor clearColor];
        [collectionViewPhotos registerClass:[ProfilePhotosCell class] forCellWithReuseIdentifier:@"Cell"];
        
        self.visited_profile_userId = UTILS.userIdOfVisitedProfile;
        
        refresh = [[UIRefreshControl alloc] init];
        [refresh addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
        [collectionViewPhotos addSubview:refresh];
        //[refresh beginRefreshing];
        
//        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
//        activityIndicatorView.frame = CGRectMake(0.0f, 0.0f, 100, 100);
//        self.backgroundView = activityIndicatorView;

     //   [self userPostListAPI];
        
        
        //        combineArr= [[NSMutableArray alloc]init];
        
        
        refreshFooter=[[YiRefreshFooter alloc] init];
        __weak typeof(self) weakSelf = self;
        refreshFooter.beginRefreshingBlock=^(){
            
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                sleep(pullDownToRefreshSleepCount);
                
                if (weakSelf.loadMoreblk) {
                    weakSelf.loadMoreblk();
                }
            });
        };
        
        
        collectionViewPhotos.scrollEnabled = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enable_child_scroll:) name:@"enable_child_scroll" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disable_child_scroll:) name:@"disable_child_scroll" object:nil];
        

    }
    return self;
}

-(void)enable_child_scroll:(NSNotification*)notiObj
{
    collectionViewPhotos.scrollEnabled = YES;
}

-(void)disable_child_scroll:(NSNotification*)notiObj
{
    collectionViewPhotos.scrollEnabled = NO;
}

-(void)refershControlAction{
   
}
-(void)RemovePullDownToRefresh
{
    //Remove Pull down to refresh when category_total_count match with array
    
    [refreshFooter remove_observer];
    
    refreshFooter.scrollView=nil;
    [refreshFooter.footerView removeFromSuperview];
   // collectionViewPhotos.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    
}
-(void)AddPullDowntoRefresh
{
    [self RemovePullDownToRefresh];
    refreshFooter.scrollView=collectionViewPhotos;
    [refreshFooter footer];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    CGRect frame = collectionViewPhotos.frame;
    frame.size.height = collectionViewPhotos.contentSize.height;
    collectionViewPhotos.frame = frame;
    
    if (self.block) {
        self.block(frame.size.height);
    }
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (arrPhotosVideos.count) {
        collectionView.backgroundView = nil;
    }
    else{
        collectionView.backgroundView = [self collectionBGView];
    }
    return arrPhotosVideos.count;
}

-(UIView *)collectionBGView
{
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
    activityIndicatorView.frame = CGRectMake(0.0f, 0.0f, 100, 100);
//    self.backgroundView = activityIndicatorView;
    return activityIndicatorView;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProfilePhotosCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];

    Photos *model = arrPhotosVideos[indexPath.row];
    cell.indexPath = indexPath;
    if([model.source containsString:@"photos"])
    {
       
     //   cell.imgViewPhoto.image = tempImg;
       [cell.imgViewPhoto sd_setImageWithURL:[Utils getThumbUrl:model.source] placeholderImage:defaultPostImg];
        cell.videoBtn.hidden = YES;
        cell.videoView.hidden = YES;
        cell.imgViewPhoto.backgroundColor = [UIColor clearColor];
    }
    else
    {
    
        
        NSString *thumbURL = model.source;
        thumbURL = [thumbURL stringByReplacingOccurrencesOfString:@"videos/" withString:@"photos/"];
        thumbURL = [[thumbURL stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
        
        [cell.imgViewPhoto sd_setImageWithURL:[Utils getThumbUrl:thumbURL] placeholderImage:nil];
        cell.imgViewPhoto.backgroundColor = [UIColor blackColor];
        cell.videoBtn.hidden = NO;
        cell.videoView.hidden = NO;
    }
    
    [cell onPlayVideoClicked:^(NSIndexPath *indexPath) {
        Photos *model = arrPhotosVideos[indexPath.row];
        [self playVideoWithURL:[Utils getProperContentUrl:model.source]];
    }];

    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Photos *photosModel = arrPhotosVideos[indexPath.row];
    
    NSMutableArray *arrPhotos = [NSMutableArray new];
    for (Photos *model in arrPhotosVideos) {
        if ([model.source containsString:@"photos"]) {
            [arrPhotos addObject:model];
        }
    }
    
    [UTILS showMultiImageViewer:arrPhotos currentPageIndex:[arrPhotos indexOfObject:photosModel]];
}


-(void)playVideoWithURL:(NSURL *)videoUrl
{
    AVPlayer *player = [AVPlayer playerWithURL:videoUrl];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];
    [[UTILS topMostController] presentViewController:playerViewController animated:YES completion:nil];
}

-(void)onContentSizeChange:(OnSizeChangeBlock)blk
{
    self.block = blk;
}

-(void)dealloc
{
    [collectionViewPhotos removeObserver:self forKeyPath:@"contentSize"];
}

-(void)setArrData:(NSArray *)arrData
{
    arrPhotosVideos = [NSMutableArray arrayWithArray:arrData];
    
    if([arrData count] == 0)
    {
        [self RemovePullDownToRefresh];
    }
    else if(arrPhotosVideos.count >= [RecordLimit intValue])
    {
        [self AddPullDowntoRefresh];
    }
    
    [collectionViewPhotos reloadData];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y <= 0) {
        if (self.blk) {
            self.blk();
        }
        //amoltricks
        //collectionViewPhotos.scrollEnabled = NO;
    }
}

-(void)onScrollToTop:(noArgBlock)block
{
    self.blk = block;
}

-(void)onLoadMore:(noArgBlock)loadMoreBlock
{
    self.loadMoreblk = loadMoreBlock;
}
@end



@implementation ProfilePhotosCell
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _imgViewPhoto = [[UIImageView alloc]initWithFrame:self.bounds];

        _imgViewPhoto.contentMode = UIViewContentModeScaleAspectFill;
        _imgViewPhoto.clipsToBounds = YES;
        [self addSubview:_imgViewPhoto];
        
        [_imgViewPhoto enableAutolayout];
        [_imgViewPhoto leadingMargin:0];
        [_imgViewPhoto trailingMargin:0];
        [_imgViewPhoto topMargin:0];
        [_imgViewPhoto bottomMargin:0];
        

        _videoView = [[UIView alloc]init];
        [self addSubview:_videoView];
        [_videoView enableAutolayout];
        [_videoView leadingMargin:0];
        [_videoView trailingMargin:0];
        [_videoView topMargin:0];
        [_videoView bottomMargin:0];
      
//        _playerViewController = [AVPlayerViewController new];
//        [self.videoView addSubview:_playerViewController.view];
//        _playerViewController.showsPlaybackControls = YES;
//        _playerViewController.view.userInteractionEnabled = YES;
//
//        [_playerViewController.view enableAutolayout];
//        [_playerViewController.view leadingMargin:0];
//        [_playerViewController.view trailingMargin:0];
//        [_playerViewController.view topMargin:0];
//        [_playerViewController.view bottomMargin:0];
        
        _videoBtn = [[UIButton alloc]initWithFrame:self.bounds];
        _videoBtn.hidden =YES;
        _videoBtn.backgroundColor = [UIColor clearColor];
       [_videoBtn setImage:[UIImage imageNamed:@"ic_play_video"] forState:UIControlStateNormal];
        _videoBtn.contentMode = UIViewContentModeScaleAspectFill;
        _videoBtn.clipsToBounds = YES;
        [self addSubview:_videoBtn];
        [_videoBtn addTarget:self action:@selector(btnVideoClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_videoBtn enableAutolayout];
        [_videoBtn leadingMargin:0];
        [_videoBtn trailingMargin:0];
        [_videoBtn topMargin:0];
        [_videoBtn bottomMargin:0];

    }
    return self;
}

-(void)onPlayVideoClicked:(videoBlock)blk
{
    self.block = blk;
}

-(void)btnVideoClicked
{
    self.block(self.indexPath);
}


@end
