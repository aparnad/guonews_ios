//
//  ProfileCollectionCell.h
//  GUO
//
//  Created by Pawan Ramteke on 14/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCollectionCell : UICollectionViewCell
{
    NSMutableArray *PostArr;
}
@property (nonatomic,strong)NSIndexPath *indexPath;
@property BOOL is_came_From_slide_menu;
@end
