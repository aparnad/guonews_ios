//
//  GlobalPostCollectionCell.m
//  GUO Media
//
//  Created by Pawan Ramteke on 27/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "GlobalPostCollectionCell.h"
#import "CustomPostTableView.h"
#import "YiRefreshFooter.h"
#import "PostDetailsController.h"
@interface GlobalPostCollectionCell()
{
    CustomPostTableView *tblView;
    int current_page;
    YiRefreshFooter *refreshFooter;
    NSMutableArray *arrPosts;
    NSArray *tempData;
}
@end

@implementation GlobalPostCollectionCell
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        arrPosts = [NSMutableArray new];
        tblView = [[CustomPostTableView alloc] initWithFrame:self.bounds fromProfile:YES];
        [self addSubview:tblView];
        
        
        //amol tricks
       // tblView.scrollEnabled = NO;
        
        [tblView onRefreshOnGlobalSearch:^{
            if (self.block) {
                self.block(self.indexPath);
            }
        }];
        
        [tblView onUserProfileClicked:^(NSString *value) {
           
            /*UserProfileController *userProfileVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"UserProfileController"];
            userProfileVC.other_user_id = value;
            [[UTILS topMostControllerNormal].navigationController pushViewController:userProfileVC animated:YES];*/
            
            [UTILS showUserProfileSingleController:value];

        }];
        
        [tblView onscrollToTop:^{
            //amoltricks
            //tblView.scrollEnabled = NO;
            if (self.blk) {
                self.blk();
            }

        }];
        
        [tblView didSelectRow:^(PostModel *selModel, NSIndexPath *indexPath) {
            
            PostDetailsController *postDetailsVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"PostDetailsController"];
            postDetailsVC.selPostModel = selModel;
            postDetailsVC.indexPath = indexPath;
            postDetailsVC.hidesBottomBarWhenPushed = NO;
            [[UTILS topMostControllerNormal].navigationController pushViewController:postDetailsVC animated:YES];
            
            [postDetailsVC onLikeCommentSuccess:^(PostModel *model, NSIndexPath *indexPath) {
                [tblView like_comments_onDetails:model indexPath:indexPath];
            }];
            
            [postDetailsVC onShareSuccess:^(PostModel *model, NSIndexPath *indexPath) {
                [tblView addSharedPost:model];
            }];
            
            
            [postDetailsVC onPostDelete:^() {
                [tblView deletePost:indexPath];
            }];
            
            
//            if (self.rowBlock) {
//                self.rowBlock(selModel, indexPath);
//            }
        }];
        
        refreshFooter=[[YiRefreshFooter alloc] init];
        __weak typeof(self) weakSelf = self;
        refreshFooter.beginRefreshingBlock=^(){
            
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                sleep(pullDownToRefreshSleepCount);
                
                if (weakSelf.loadMoreblk) {
                    weakSelf.loadMoreblk();
                }
            });
        };

        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enable_child_scroll:) name:@"enable_child_scroll" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disable_child_scroll:) name:@"disable_child_scroll" object:nil];
    }
    return self;
}

-(void)enable_child_scroll:(NSNotification*)notiObj
{
    tblView.scrollEnabled = YES;
}

-(void)disable_child_scroll:(NSNotification*)notiObj
{
    tblView.scrollEnabled = NO;
}

-(void)onLoadMore:(noArgBlock)loadMoreBlock
{
    self.loadMoreblk = loadMoreBlock;
}

-(void)RemovePullDownToRefresh
{
    //Remove Pull down to refresh when category_total_count match with array
    
    [refreshFooter remove_observer];
    
    refreshFooter.scrollView=nil;
    [refreshFooter.footerView removeFromSuperview];
   // tblView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    
}
-(void)AddPullDowntoRefresh
{
    [self RemovePullDownToRefresh];
    refreshFooter.scrollView=tblView;
    [refreshFooter footer];
}


-(void)setArrData:(NSArray *)arrData
{
    if (arrData == nil) {
        return;
    }
    if (tempData != nil) {
        if (tempData.count == arrData.count) {
            [self RemovePullDownToRefresh];
        }
        else{
            tempData = arrData;
            tblView.arrSearchData = arrData;
            [self AddPullDowntoRefresh];
        }
    }
    else{
        tempData = arrData;
        tblView.arrSearchData = arrData;
        if([arrData count] == 0  || [arrData count] <= [RecordLimit intValue])
        {
            [self RemovePullDownToRefresh];
        }
        else if(arrData.count >= [RecordLimit intValue])
        {
            [self AddPullDowntoRefresh];
        }
    }
  //  [arrPosts addObjectsFromArray:arrData];
}

-(void)onRefreshList:(RefreshDataBlock)blk
{
    self.block = blk;
}

-(void)onscrollToTop:(noArgBlock)block
{
    self.blk = block;
}

-(void)apiCallInitiated
{
    [tblView apiCallInitiated];
}
-(void)apiCallEndWithResultCount:(NSInteger)count  pageNo:(NSInteger)pageNo
{
    [tblView apiCallEndWithResultCount:count pageNo:pageNo];
}

-(void)didSelectRow:(SelectRowBlock)rowBlk
{
    self.rowBlock = rowBlk;
}
@end
