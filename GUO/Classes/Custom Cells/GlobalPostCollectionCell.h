//
//  GlobalPostCollectionCell.h
//  GUO Media
//
//  Created by Pawan Ramteke on 27/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SelectRowBlock)(PostModel *selModel,NSIndexPath *indexPath);
typedef void(^RefreshDataBlock)(NSIndexPath *idxPath);
@interface GlobalPostCollectionCell : UICollectionViewCell
@property (nonatomic,strong)NSIndexPath *indexPath;
@property (nonatomic,strong)NSArray *arrData;

@property (nonatomic,copy)RefreshDataBlock block;
@property (nonatomic,copy)noArgBlock blk;
@property (nonatomic,copy)noArgBlock loadMoreblk;
@property (nonatomic,copy)SelectRowBlock rowBlock;

-(void)onRefreshList:(RefreshDataBlock)blk;
-(void)onscrollToTop:(noArgBlock)block;
-(void)onLoadMore:(noArgBlock)loadMoreBlock;
-(void)apiCallInitiated;
-(void)apiCallEndWithResultCount:(NSInteger)count  pageNo:(NSInteger)pageNo;
-(void)didSelectRow:(SelectRowBlock)rowBlk;
@end
