//
//  NSBundle+Localize.h
//  Mahalati
//
//  Created by Chandra on 27/07/16.
//  Copyright © 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Localize)
+ (void)setLanguage:(NSString *)language;
+ (void)getLanguage;

@end
