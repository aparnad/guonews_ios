//
//  Photos.m
//
//  Created by Amol Hirkane on 28/06/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "Photos.h"


NSString *const kPhotosPhotoId = @"photo_id";
NSString *const kPhotosLikes = @"likes";
NSString *const kPhotosPostId = @"post_id";
NSString *const kPhotosSource = @"source";
NSString *const kPhotosAlbumId = @"album_id";
NSString *const kPhotosComments = @"comments";


@interface Photos ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Photos

@synthesize photoId = _photoId;
@synthesize likes = _likes;
@synthesize postId = _postId;
@synthesize source = _source;
@synthesize albumId = _albumId;
@synthesize comments = _comments;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.photoId = [self objectOrNilForKey:kPhotosPhotoId fromDictionary:dict];
            self.likes = [self objectOrNilForKey:kPhotosLikes fromDictionary:dict];
            self.postId = [self objectOrNilForKey:kPhotosPostId fromDictionary:dict];
            self.source = [self objectOrNilForKey:kPhotosSource fromDictionary:dict];
            self.albumId = [self objectOrNilForKey:kPhotosAlbumId fromDictionary:dict];
            self.comments = [self objectOrNilForKey:kPhotosComments fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.photoId forKey:kPhotosPhotoId];
    [mutableDict setValue:self.likes forKey:kPhotosLikes];
    [mutableDict setValue:self.postId forKey:kPhotosPostId];
    [mutableDict setValue:self.source forKey:kPhotosSource];
    [mutableDict setValue:self.albumId forKey:kPhotosAlbumId];
    [mutableDict setValue:self.comments forKey:kPhotosComments];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.photoId = [aDecoder decodeObjectForKey:kPhotosPhotoId];
    self.likes = [aDecoder decodeObjectForKey:kPhotosLikes];
    self.postId = [aDecoder decodeObjectForKey:kPhotosPostId];
    self.source = [aDecoder decodeObjectForKey:kPhotosSource];
    self.albumId = [aDecoder decodeObjectForKey:kPhotosAlbumId];
    self.comments = [aDecoder decodeObjectForKey:kPhotosComments];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_photoId forKey:kPhotosPhotoId];
    [aCoder encodeObject:_likes forKey:kPhotosLikes];
    [aCoder encodeObject:_postId forKey:kPhotosPostId];
    [aCoder encodeObject:_source forKey:kPhotosSource];
    [aCoder encodeObject:_albumId forKey:kPhotosAlbumId];
    [aCoder encodeObject:_comments forKey:kPhotosComments];
}

- (id)copyWithZone:(NSZone *)zone
{
    Photos *copy = [[Photos alloc] init];
    
    if (copy) {

        copy.photoId = [self.photoId copyWithZone:zone];
        copy.likes = [self.likes copyWithZone:zone];
        copy.postId = [self.postId copyWithZone:zone];
        copy.source = [self.source copyWithZone:zone];
        copy.albumId = [self.albumId copyWithZone:zone];
        copy.comments = [self.comments copyWithZone:zone];
    }
    
    return copy;
}


@end
