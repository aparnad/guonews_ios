//
//  DataModels.h
//
//  Created by Amol Hirkane on 23/06/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "Photos.h"
#import "PostDetailsModel.h"
#import "PostComments.h"
#import "CommentReplies.h"
#import "Video.h"
#import "UserModel.h"
#import "FollowModel.h"
#import "NotificationModel.h"
#import "CountModel.h"