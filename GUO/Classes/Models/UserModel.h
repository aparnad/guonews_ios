//
//  UserModel.h
//
//  Created by Amol Hirkane on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface UserModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *userRelationship;
@property (nonatomic, strong) NSString *emailPostComments;
@property (nonatomic, strong) NSString *featureStatus;
@property (nonatomic, strong) NSString *userBirthdate;
@property (nonatomic, strong) NSString *userLiveMessagesLastid;
@property (nonatomic, strong) NSString *instagramId;
@property (nonatomic, strong) NSString *userCoverId;
@property (nonatomic, strong) NSString *userPrivacyWork;
@property (nonatomic, strong) NSString *userAffiliateBalance;
@property (nonatomic, strong) NSString *broadcast;
@property (nonatomic, strong) NSString *userAlbumTimeline;
@property (nonatomic, strong) NSString *userDefaultLanguage;
@property (nonatomic, strong) NSString *vkontakteConnected;
@property (nonatomic, strong) NSString *userPictureId;

@property (nonatomic, strong) NSString *sendbirdAccessToken;
@property (nonatomic, strong) NSString *instagramConnected;
@property (nonatomic, strong) NSString *userLastname;
@property (nonatomic, strong) NSString *userEduMajor;
@property (nonatomic, strong) NSString *userLiveNotificationsLastid;
@property (nonatomic, strong) NSString *userWebsite;
@property (nonatomic, strong) NSString *periority;
@property (nonatomic, strong) NSString *userPrivacyPages;
@property (nonatomic, strong) NSString *userPrivacyMessage;
@property (nonatomic, strong) NSString *userSocialInstagram;
@property (nonatomic, strong) NSString *userSocialGoogle;
@property (nonatomic, strong) NSString *googleConnected;
@property (nonatomic, strong) NSString *userBoostedPages;
@property (nonatomic, strong) NSString *twitterConnected;
@property (nonatomic, strong) NSString *userPinnedPost;
@property (nonatomic, strong) NSString *userWorkPlace;
@property (nonatomic, strong) NSString *userPassword;
@property (nonatomic, strong) NSString *userVerified;
@property (nonatomic, strong) NSString *userStarted;
@property (nonatomic, strong) NSString *userPrivacyRelationship;
@property (nonatomic, strong) NSString *facebookId;
@property (nonatomic, strong) NSString *userEduSchool;
@property (nonatomic, strong) NSString *emailMentions;
@property (nonatomic, strong) NSString *userPrivacyWall;
@property (nonatomic, strong) NSString *userPrivacyBirthdate;
@property (nonatomic, strong) NSString *userLiveRequestsLastid;
@property (nonatomic, strong) NSString *userReferrerId;
@property (nonatomic, strong) NSString *vkontakteId;
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *userSocialLinkedin;
@property (nonatomic, strong) NSString *userPrivacyPhotos;
@property (nonatomic, strong) NSString *userWorkTitle;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *followersDate;
@property (nonatomic, strong) NSString *userPrivacyLocation;
@property (nonatomic, strong) NSString *twitterId;
@property (nonatomic, strong) NSString *userBiography;
@property (nonatomic, strong) NSString *userLastLogin;
@property (nonatomic, strong) NSString *userPrivacyOther;
@property (nonatomic, strong) NSString *userSocialVkontakte;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, strong) NSString *notificationActivated;
@property (nonatomic, strong) NSString *notificationsSound;
@property (nonatomic, strong) NSString *facebookConnected;
@property (nonatomic, strong) NSString *userPicture;
@property (nonatomic, strong) NSString *userFirstname;
@property (nonatomic, strong) NSString *userRegistered;
@property (nonatomic, strong) NSString *userChatEnabled;
@property (nonatomic, strong) NSString *userPrivacyFriends;
@property (nonatomic, strong) NSString *userPrivacyGroups;
@property (nonatomic, strong) NSString *userBoostedPosts;
@property (nonatomic, strong) NSString *emailPostShares;
@property (nonatomic, strong) NSString *userSubscribed;
@property (nonatomic, strong) NSString *userPackage;
@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *emailPostLikes;
@property (nonatomic, strong) NSString *linkedinId;
@property (nonatomic, strong) NSString *emailProfileVisits;
@property (nonatomic, strong) NSString *gaSecret;
@property (nonatomic, strong) NSString *userPrivacyBasic;
@property (nonatomic, strong) NSString *userEduClass;
@property (nonatomic, strong) NSString *userCover;
@property (nonatomic, strong) NSString *userSocialYoutube;
@property (nonatomic, strong) NSString *loginVerification;
@property (nonatomic, strong) NSString *userReseted;
@property (nonatomic, strong) NSString *emailWallPosts;
@property (nonatomic, strong) NSString *userPrivacyEvents;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *enableTwoFactor;
@property (nonatomic, strong) NSString *linkedinConnected;
@property (nonatomic, strong) NSString *userActivationKey;
@property (nonatomic, strong) NSString *userLiveNotificationsCounter;
@property (nonatomic, strong) NSString *userLiveMessagesCounter;
@property (nonatomic, strong) NSString *userSocialTwitter;
@property (nonatomic, strong) NSString *bossFollowers;
@property (nonatomic, strong) NSString *userPhone;
@property (nonatomic, strong) NSString *userPrivacyEducation;
@property (nonatomic, strong) NSString *userBanned;
@property (nonatomic, strong) NSString *userLiveRequestsCounter;
@property (nonatomic, strong) NSString *userAlbumCovers;
@property (nonatomic, strong) NSString *googleId;
@property (nonatomic, strong) NSString *userResetKey;
@property (nonatomic, strong) NSString *userCurrentCity;
@property (nonatomic, strong) NSString *userGroup;
@property (nonatomic, strong) NSString *emailFriendRequests;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userSocialFacebook;
@property (nonatomic, strong) NSString *userAlbumPictures;
@property (nonatomic, assign) BOOL isFollowing;
@property (nonatomic, strong) NSString *userActivated;
@property (nonatomic, strong) NSString *userHometown;
@property (nonatomic, strong) NSString *userSubscriptionDate;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
