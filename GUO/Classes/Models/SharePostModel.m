//
//  SharePostModel.m
//  GUO Media
//
//  Created by Pawan Ramteke on 04/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "SharePostModel.h"
#import "Photos.h"


NSString *const kSharePostModelUserPicture = @"user_picture";
NSString *const kSharePostModelPostAuthorName = @"post_author_name";
NSString *const kSharePostModelUserType = @"user_type";
NSString *const kSharePostModelPinned = @"pinned";
NSString *const kSharePostModelUserName = @"user_name";
NSString *const kSharePostModelChannelUrl = @"channel_url";
NSString *const kSharePostModelComments = @"comments";
NSString *const kSharePostModelLocation = @"location";
NSString *const kSharePostModelText = @"text";
NSString *const kSharePostModelUserPinnedPost = @"user_pinned_post";
NSString *const kSharePostModelIsEventAdmin = @"is_event_admin";
NSString *const kSharePostModelILike = @"i_like";
NSString *const kSharePostModelUserVerified = @"user_verified";
NSString *const kSharePostModelPostAuthorPicture = @"post_author_picture";
NSString *const kSharePostModelUserCoverId = @"user_cover_id";
NSString *const kSharePostModelUserFirstname = @"user_firstname";
NSString *const kSharePostModelIsGroupAdmin = @"is_group_admin";
NSString *const kSharePostModelPrivacy = @"privacy";
NSString *const kSharePostModelBroadcastUrl = @"broadcast_url";
NSString *const kSharePostModelUserLastname = @"user_lastname";
NSString *const kSharePostModelShares = @"shares";
NSString *const kSharePostModelPostUserName = @"post_user_name";
NSString *const kSharePostModelAuthorId = @"author_id";
NSString *const kSharePostModelEventId = @"event_id";
NSString *const kSharePostModelUserSubscribed = @"user_subscribed";
NSString *const kSharePostModelBroadcastName = @"broadcast_name";
NSString *const kSharePostModelOriginId = @"origin_id";
NSString *const kSharePostModelLikes = @"likes";
NSString *const kSharePostModelFeelingAction = @"feeling_action";
NSString *const kSharePostModelViews = @"views";
NSString *const kSharePostModelPostType = @"post_type";
NSString *const kSharePostModelPostAuthorUrl = @"post_author_url";
NSString *const kSharePostModelInWall = @"in_wall";
NSString *const kSharePostModelInEvent = @"in_event";
NSString *const kSharePostModelUserPictureId = @"user_picture_id";
NSString *const kSharePostModelISave = @"i_save";
NSString *const kSharePostModelInGroup = @"in_group";
NSString *const kSharePostModelFeelingValue = @"feeling_value";
NSString *const kSharePostModelUserGender = @"user_gender";
NSString *const kSharePostModelIsPageAdmin = @"is_page_admin";
NSString *const kSharePostModelUserId = @"user_id";
NSString *const kSharePostModelManagePost = @"manage_post";
NSString *const kSharePostModelTextPlain = @"text_plain";
NSString *const kSharePostModelGroupId = @"group_id";
NSString *const kSharePostModelWallId = @"wall_id";
NSString *const kSharePostModelBoosted = @"boosted";
NSString *const kSharePostModelPostAuthorVerified = @"post_author_verified";
NSString *const kSharePostModelPhotos = @"photos";
NSString *const kSharePostModelVideo = @"video";
NSString *const kSharePostModelPhotosNum = @"photos_num";
NSString *const kSharePostModelPostId = @"post_id";
NSString *const kSharePostModelTime = @"time";
NSString *const kSharePostModelFormattedTime = @"formatted_time";

NSString *const kSharePostModelIsBroadcast = @"is_broadcast";

@interface SharePostModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation SharePostModel
@synthesize userPicture = _userPicture;
@synthesize postAuthorName = _postAuthorName;
@synthesize userType = _userType;
@synthesize pinned = _pinned;
@synthesize userName = _userName;
@synthesize channelUrl = _channelUrl;
@synthesize comments = _comments;
@synthesize location = _location;
@synthesize text = _text;
@synthesize userPinnedPost = _userPinnedPost;
@synthesize isEventAdmin = _isEventAdmin;
@synthesize iLike = _iLike;
@synthesize userVerified = _userVerified;
@synthesize postAuthorPicture = _postAuthorPicture;
@synthesize userCoverId = _userCoverId;
@synthesize userFirstname = _userFirstname;
@synthesize isGroupAdmin = _isGroupAdmin;
@synthesize privacy = _privacy;
@synthesize broadcastUrl = _broadcastUrl;
@synthesize userLastname = _userLastname;
@synthesize shares = _shares;
@synthesize postUserName = _postUserName;
@synthesize authorId = _authorId;
@synthesize eventId = _eventId;
@synthesize userSubscribed = _userSubscribed;
@synthesize broadcastName = _broadcastName;
@synthesize originId = _originId;
@synthesize likes = _likes;
@synthesize feelingAction = _feelingAction;
@synthesize views = _views;
@synthesize postType = _postType;
@synthesize postAuthorUrl = _postAuthorUrl;
@synthesize inWall = _inWall;
@synthesize inEvent = _inEvent;
@synthesize userPictureId = _userPictureId;
@synthesize iSave = _iSave;
@synthesize inGroup = _inGroup;
@synthesize feelingValue = _feelingValue;
@synthesize userGender = _userGender;
@synthesize isPageAdmin = _isPageAdmin;
@synthesize userId = _userId;
@synthesize managePost = _managePost;
@synthesize textPlain = _textPlain;
@synthesize groupId = _groupId;
@synthesize wallId = _wallId;
@synthesize boosted = _boosted;
@synthesize postAuthorVerified = _postAuthorVerified;
@synthesize photos = _photos;
@synthesize videos = _videos;
@synthesize photosNum = _photosNum;
@synthesize postId = _postId;
@synthesize time = _time;
@synthesize formattedTime = _formattedTime;
@synthesize isBroadcast = _isBroadcast;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.userPicture = [self objectOrNilForKey:kSharePostModelUserPicture fromDictionary:dict];
        self.postAuthorName = [self objectOrNilForKey:kSharePostModelPostAuthorName fromDictionary:dict];
        self.userType = [self objectOrNilForKey:kSharePostModelUserType fromDictionary:dict];
        self.pinned = [[self objectOrNilForKey:kSharePostModelPinned fromDictionary:dict] boolValue];
        self.userName = [self objectOrNilForKey:kSharePostModelUserName fromDictionary:dict];
        self.channelUrl = [self objectOrNilForKey:kSharePostModelChannelUrl fromDictionary:dict];
        self.comments = [self objectOrNilForKey:kSharePostModelComments fromDictionary:dict];
        self.location = [self objectOrNilForKey:kSharePostModelLocation fromDictionary:dict];
        self.text = [self objectOrNilForKey:kSharePostModelText fromDictionary:dict];
        self.userPinnedPost = [self objectOrNilForKey:kSharePostModelUserPinnedPost fromDictionary:dict];
        self.isEventAdmin = [[self objectOrNilForKey:kSharePostModelIsEventAdmin fromDictionary:dict] boolValue];
        self.iLike = [[self objectOrNilForKey:kSharePostModelILike fromDictionary:dict] boolValue];
        self.userVerified = [self objectOrNilForKey:kSharePostModelUserVerified fromDictionary:dict];
        self.postAuthorPicture = [self objectOrNilForKey:kSharePostModelPostAuthorPicture fromDictionary:dict];
        self.userCoverId = [self objectOrNilForKey:kSharePostModelUserCoverId fromDictionary:dict];
        self.userFirstname = [self objectOrNilForKey:kSharePostModelUserFirstname fromDictionary:dict];
        self.isGroupAdmin = [[self objectOrNilForKey:kSharePostModelIsGroupAdmin fromDictionary:dict] boolValue];
        self.privacy = [self objectOrNilForKey:kSharePostModelPrivacy fromDictionary:dict];
        self.broadcastUrl = [self objectOrNilForKey:kSharePostModelBroadcastUrl fromDictionary:dict];
        self.userLastname = [self objectOrNilForKey:kSharePostModelUserLastname fromDictionary:dict];
        self.shares = [self objectOrNilForKey:kSharePostModelShares fromDictionary:dict];
        self.postUserName = [self objectOrNilForKey:kSharePostModelPostUserName fromDictionary:dict];
        self.authorId = [self objectOrNilForKey:kSharePostModelAuthorId fromDictionary:dict];
        self.eventId = [self objectOrNilForKey:kSharePostModelEventId fromDictionary:dict];
        self.userSubscribed = [self objectOrNilForKey:kSharePostModelUserSubscribed fromDictionary:dict];
        self.broadcastName = [self objectOrNilForKey:kSharePostModelBroadcastName fromDictionary:dict];
        self.originId = [self objectOrNilForKey:kSharePostModelOriginId fromDictionary:dict];
        self.likes = [self objectOrNilForKey:kSharePostModelLikes fromDictionary:dict];
        self.feelingAction = [self objectOrNilForKey:kSharePostModelFeelingAction fromDictionary:dict];
        self.views = [self objectOrNilForKey:kSharePostModelViews fromDictionary:dict];
        self.postType = [self objectOrNilForKey:kSharePostModelPostType fromDictionary:dict];
        self.postAuthorUrl = [self objectOrNilForKey:kSharePostModelPostAuthorUrl fromDictionary:dict];
        self.inWall = [self objectOrNilForKey:kSharePostModelInWall fromDictionary:dict];
        self.inEvent = [self objectOrNilForKey:kSharePostModelInEvent fromDictionary:dict];
        self.userPictureId = [self objectOrNilForKey:kSharePostModelUserPictureId fromDictionary:dict];
        self.iSave = [[self objectOrNilForKey:kSharePostModelISave fromDictionary:dict] boolValue];
        self.inGroup = [self objectOrNilForKey:kSharePostModelInGroup fromDictionary:dict];
        self.feelingValue = [self objectOrNilForKey:kSharePostModelFeelingValue fromDictionary:dict];
        self.userGender = [self objectOrNilForKey:kSharePostModelUserGender fromDictionary:dict];
        self.isPageAdmin = [[self objectOrNilForKey:kSharePostModelIsPageAdmin fromDictionary:dict] boolValue];
        self.userId = [self objectOrNilForKey:kSharePostModelUserId fromDictionary:dict];
        self.managePost = [[self objectOrNilForKey:kSharePostModelManagePost fromDictionary:dict] boolValue];
        self.textPlain = [self objectOrNilForKey:kSharePostModelTextPlain fromDictionary:dict];
        self.groupId = [self objectOrNilForKey:kSharePostModelGroupId fromDictionary:dict];
        self.wallId = [self objectOrNilForKey:kSharePostModelWallId fromDictionary:dict];
        self.boosted = [self objectOrNilForKey:kSharePostModelBoosted fromDictionary:dict];
        self.postAuthorVerified = [self objectOrNilForKey:kSharePostModelPostAuthorVerified fromDictionary:dict];
        NSObject *receivedPhotos = [dict objectForKey:kSharePostModelPhotos];
        NSMutableArray *parsedPhotos = [NSMutableArray array];
        if ([receivedPhotos isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedPhotos) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedPhotos addObject:[Photos modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedPhotos isKindOfClass:[NSDictionary class]]) {
            [parsedPhotos addObject:[Photos modelObjectWithDictionary:(NSDictionary *)receivedPhotos]];
        }
        
        self.photos = [NSArray arrayWithArray:parsedPhotos];
        
        NSObject *receivedVideo = [dict objectForKey:kSharePostModelVideo];
        NSMutableArray *parsedVideo = [NSMutableArray array];
        if ([receivedVideo isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedVideo) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedVideo addObject:[Photos modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedVideo isKindOfClass:[NSDictionary class]]) {
            [parsedVideo addObject:[Photos modelObjectWithDictionary:(NSDictionary *)receivedVideo]];
        }
        
        self.videos = [NSArray arrayWithArray:parsedVideo];
        
        self.photosNum = [[self objectOrNilForKey:kSharePostModelPhotosNum fromDictionary:dict] doubleValue];
        self.postId = [self objectOrNilForKey:kSharePostModelPostId fromDictionary:dict];
        self.time = [self objectOrNilForKey:kSharePostModelTime fromDictionary:dict];
        self.formattedTime = [self objectOrNilForKey:kSharePostModelFormattedTime fromDictionary:dict];

        self.isBroadcast = [self objectOrNilForKey:kSharePostModelIsBroadcast fromDictionary:dict];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.userPicture forKey:kSharePostModelUserPicture];
    [mutableDict setValue:self.postAuthorName forKey:kSharePostModelPostAuthorName];
    [mutableDict setValue:self.userType forKey:kSharePostModelUserType];
    [mutableDict setValue:[NSNumber numberWithBool:self.pinned] forKey:kSharePostModelPinned];
    [mutableDict setValue:self.userName forKey:kSharePostModelUserName];
    [mutableDict setValue:self.channelUrl forKey:kSharePostModelChannelUrl];
    [mutableDict setValue:self.comments forKey:kSharePostModelComments];
    [mutableDict setValue:self.location forKey:kSharePostModelLocation];
    [mutableDict setValue:self.text forKey:kSharePostModelText];
    [mutableDict setValue:self.userPinnedPost forKey:kSharePostModelUserPinnedPost];
    [mutableDict setValue:[NSNumber numberWithBool:self.isEventAdmin] forKey:kSharePostModelIsEventAdmin];
    [mutableDict setValue:[NSNumber numberWithBool:self.iLike] forKey:kSharePostModelILike];
    [mutableDict setValue:self.userVerified forKey:kSharePostModelUserVerified];
    [mutableDict setValue:self.postAuthorPicture forKey:kSharePostModelPostAuthorPicture];
    [mutableDict setValue:self.userCoverId forKey:kSharePostModelUserCoverId];
    [mutableDict setValue:self.userFirstname forKey:kSharePostModelUserFirstname];
    [mutableDict setValue:[NSNumber numberWithBool:self.isGroupAdmin] forKey:kSharePostModelIsGroupAdmin];
    [mutableDict setValue:self.privacy forKey:kSharePostModelPrivacy];
    [mutableDict setValue:self.broadcastUrl forKey:kSharePostModelBroadcastUrl];
    [mutableDict setValue:self.userLastname forKey:kSharePostModelUserLastname];
    [mutableDict setValue:self.shares forKey:kSharePostModelShares];
    [mutableDict setValue:self.postUserName forKey:kSharePostModelPostUserName];
    [mutableDict setValue:self.authorId forKey:kSharePostModelAuthorId];
    [mutableDict setValue:self.eventId forKey:kSharePostModelEventId];
    [mutableDict setValue:self.userSubscribed forKey:kSharePostModelUserSubscribed];
    [mutableDict setValue:self.broadcastName forKey:kSharePostModelBroadcastName];
    [mutableDict setValue:self.originId forKey:kSharePostModelOriginId];
    [mutableDict setValue:self.likes forKey:kSharePostModelLikes];
    [mutableDict setValue:self.feelingAction forKey:kSharePostModelFeelingAction];
    [mutableDict setValue:self.views forKey:kSharePostModelViews];
    [mutableDict setValue:self.postType forKey:kSharePostModelPostType];
    [mutableDict setValue:self.postAuthorUrl forKey:kSharePostModelPostAuthorUrl];
    [mutableDict setValue:self.inWall forKey:kSharePostModelInWall];
    [mutableDict setValue:self.inEvent forKey:kSharePostModelInEvent];
    [mutableDict setValue:self.userPictureId forKey:kSharePostModelUserPictureId];
    [mutableDict setValue:[NSNumber numberWithBool:self.iSave] forKey:kSharePostModelISave];
    [mutableDict setValue:self.inGroup forKey:kSharePostModelInGroup];
    [mutableDict setValue:self.feelingValue forKey:kSharePostModelFeelingValue];
    [mutableDict setValue:self.userGender forKey:kSharePostModelUserGender];
    [mutableDict setValue:[NSNumber numberWithBool:self.isPageAdmin] forKey:kSharePostModelIsPageAdmin];
    [mutableDict setValue:self.userId forKey:kSharePostModelUserId];
    [mutableDict setValue:[NSNumber numberWithBool:self.managePost] forKey:kSharePostModelManagePost];
    [mutableDict setValue:self.textPlain forKey:kSharePostModelTextPlain];
    [mutableDict setValue:self.groupId forKey:kSharePostModelGroupId];
    [mutableDict setValue:self.wallId forKey:kSharePostModelWallId];
    [mutableDict setValue:self.boosted forKey:kSharePostModelBoosted];
    [mutableDict setValue:self.postAuthorVerified forKey:kSharePostModelPostAuthorVerified];
    NSMutableArray *tempArrayForPhotos = [NSMutableArray array];
    for (NSObject *subArrayObject in self.photos) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPhotos addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPhotos addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPhotos] forKey:kSharePostModelPhotos];
    [mutableDict setValue:[NSNumber numberWithDouble:self.photosNum] forKey:kSharePostModelPhotosNum];
    [mutableDict setValue:self.postId forKey:kSharePostModelPostId];
    [mutableDict setValue:self.time forKey:kSharePostModelTime];
    [mutableDict setValue:self.isBroadcast forKey:kSharePostModelIsBroadcast];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.userPicture = [aDecoder decodeObjectForKey:kSharePostModelUserPicture];
    self.postAuthorName = [aDecoder decodeObjectForKey:kSharePostModelPostAuthorName];
    self.userType = [aDecoder decodeObjectForKey:kSharePostModelUserType];
    self.pinned = [aDecoder decodeBoolForKey:kSharePostModelPinned];
    self.userName = [aDecoder decodeObjectForKey:kSharePostModelUserName];
    self.channelUrl = [aDecoder decodeObjectForKey:kSharePostModelChannelUrl];
    self.comments = [aDecoder decodeObjectForKey:kSharePostModelComments];
    self.location = [aDecoder decodeObjectForKey:kSharePostModelLocation];
    self.text = [aDecoder decodeObjectForKey:kSharePostModelText];
    self.userPinnedPost = [aDecoder decodeObjectForKey:kSharePostModelUserPinnedPost];
    self.isEventAdmin = [aDecoder decodeBoolForKey:kSharePostModelIsEventAdmin];
    self.iLike = [aDecoder decodeBoolForKey:kSharePostModelILike];
    self.userVerified = [aDecoder decodeObjectForKey:kSharePostModelUserVerified];
    self.postAuthorPicture = [aDecoder decodeObjectForKey:kSharePostModelPostAuthorPicture];
    self.userCoverId = [aDecoder decodeObjectForKey:kSharePostModelUserCoverId];
    self.userFirstname = [aDecoder decodeObjectForKey:kSharePostModelUserFirstname];
    self.isGroupAdmin = [aDecoder decodeBoolForKey:kSharePostModelIsGroupAdmin];
    self.privacy = [aDecoder decodeObjectForKey:kSharePostModelPrivacy];
    self.broadcastUrl = [aDecoder decodeObjectForKey:kSharePostModelBroadcastUrl];
    self.userLastname = [aDecoder decodeObjectForKey:kSharePostModelUserLastname];
    self.shares = [aDecoder decodeObjectForKey:kSharePostModelShares];
    self.postUserName = [aDecoder decodeObjectForKey:kSharePostModelPostUserName];
    self.authorId = [aDecoder decodeObjectForKey:kSharePostModelAuthorId];
    self.eventId = [aDecoder decodeObjectForKey:kSharePostModelEventId];
    self.userSubscribed = [aDecoder decodeObjectForKey:kSharePostModelUserSubscribed];
    self.broadcastName = [aDecoder decodeObjectForKey:kSharePostModelBroadcastName];
    self.originId = [aDecoder decodeObjectForKey:kSharePostModelOriginId];
    self.likes = [aDecoder decodeObjectForKey:kSharePostModelLikes];
    self.feelingAction = [aDecoder decodeObjectForKey:kSharePostModelFeelingAction];
    self.views = [aDecoder decodeObjectForKey:kSharePostModelViews];
    self.postType = [aDecoder decodeObjectForKey:kSharePostModelPostType];
    self.postAuthorUrl = [aDecoder decodeObjectForKey:kSharePostModelPostAuthorUrl];
    self.inWall = [aDecoder decodeObjectForKey:kSharePostModelInWall];
    self.inEvent = [aDecoder decodeObjectForKey:kSharePostModelInEvent];
    self.userPictureId = [aDecoder decodeObjectForKey:kSharePostModelUserPictureId];
    self.iSave = [aDecoder decodeBoolForKey:kSharePostModelISave];
    self.inGroup = [aDecoder decodeObjectForKey:kSharePostModelInGroup];
    self.feelingValue = [aDecoder decodeObjectForKey:kSharePostModelFeelingValue];
    self.userGender = [aDecoder decodeObjectForKey:kSharePostModelUserGender];
    self.isPageAdmin = [aDecoder decodeBoolForKey:kSharePostModelIsPageAdmin];
    self.userId = [aDecoder decodeObjectForKey:kSharePostModelUserId];
    self.managePost = [aDecoder decodeBoolForKey:kSharePostModelManagePost];
    self.textPlain = [aDecoder decodeObjectForKey:kSharePostModelTextPlain];
    self.groupId = [aDecoder decodeObjectForKey:kSharePostModelGroupId];
    self.wallId = [aDecoder decodeObjectForKey:kSharePostModelWallId];
    self.boosted = [aDecoder decodeObjectForKey:kSharePostModelBoosted];
    self.postAuthorVerified = [aDecoder decodeObjectForKey:kSharePostModelPostAuthorVerified];
    self.photos = [aDecoder decodeObjectForKey:kSharePostModelPhotos];
    self.photosNum = [aDecoder decodeDoubleForKey:kSharePostModelPhotosNum];
    self.postId = [aDecoder decodeObjectForKey:kSharePostModelPostId];
    self.time = [aDecoder decodeObjectForKey:kSharePostModelTime];
    self.isBroadcast = [aDecoder decodeObjectForKey:kSharePostModelIsBroadcast];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_userPicture forKey:kSharePostModelUserPicture];
    [aCoder encodeObject:_postAuthorName forKey:kSharePostModelPostAuthorName];
    [aCoder encodeObject:_userType forKey:kSharePostModelUserType];
    [aCoder encodeBool:_pinned forKey:kSharePostModelPinned];
    [aCoder encodeObject:_userName forKey:kSharePostModelUserName];
    [aCoder encodeObject:_channelUrl forKey:kSharePostModelChannelUrl];
    [aCoder encodeObject:_comments forKey:kSharePostModelComments];
    [aCoder encodeObject:_location forKey:kSharePostModelLocation];
    [aCoder encodeObject:_text forKey:kSharePostModelText];
    [aCoder encodeObject:_userPinnedPost forKey:kSharePostModelUserPinnedPost];
    [aCoder encodeBool:_isEventAdmin forKey:kSharePostModelIsEventAdmin];
    [aCoder encodeBool:_iLike forKey:kSharePostModelILike];
    [aCoder encodeObject:_userVerified forKey:kSharePostModelUserVerified];
    [aCoder encodeObject:_postAuthorPicture forKey:kSharePostModelPostAuthorPicture];
    [aCoder encodeObject:_userCoverId forKey:kSharePostModelUserCoverId];
    [aCoder encodeObject:_userFirstname forKey:kSharePostModelUserFirstname];
    [aCoder encodeBool:_isGroupAdmin forKey:kSharePostModelIsGroupAdmin];
    [aCoder encodeObject:_privacy forKey:kSharePostModelPrivacy];
    [aCoder encodeObject:_broadcastUrl forKey:kSharePostModelBroadcastUrl];
    [aCoder encodeObject:_userLastname forKey:kSharePostModelUserLastname];
    [aCoder encodeObject:_shares forKey:kSharePostModelShares];
    [aCoder encodeObject:_postUserName forKey:kSharePostModelPostUserName];
    [aCoder encodeObject:_authorId forKey:kSharePostModelAuthorId];
    [aCoder encodeObject:_eventId forKey:kSharePostModelEventId];
    [aCoder encodeObject:_userSubscribed forKey:kSharePostModelUserSubscribed];
    [aCoder encodeObject:_broadcastName forKey:kSharePostModelBroadcastName];
    [aCoder encodeObject:_originId forKey:kSharePostModelOriginId];
    [aCoder encodeObject:_likes forKey:kSharePostModelLikes];
    [aCoder encodeObject:_feelingAction forKey:kSharePostModelFeelingAction];
    [aCoder encodeObject:_views forKey:kSharePostModelViews];
    [aCoder encodeObject:_postType forKey:kSharePostModelPostType];
    [aCoder encodeObject:_postAuthorUrl forKey:kSharePostModelPostAuthorUrl];
    [aCoder encodeObject:_inWall forKey:kSharePostModelInWall];
    [aCoder encodeObject:_inEvent forKey:kSharePostModelInEvent];
    [aCoder encodeObject:_userPictureId forKey:kSharePostModelUserPictureId];
    [aCoder encodeBool:_iSave forKey:kSharePostModelISave];
    [aCoder encodeObject:_inGroup forKey:kSharePostModelInGroup];
    [aCoder encodeObject:_feelingValue forKey:kSharePostModelFeelingValue];
    [aCoder encodeObject:_userGender forKey:kSharePostModelUserGender];
    [aCoder encodeBool:_isPageAdmin forKey:kSharePostModelIsPageAdmin];
    [aCoder encodeObject:_userId forKey:kSharePostModelUserId];
    [aCoder encodeBool:_managePost forKey:kSharePostModelManagePost];
    [aCoder encodeObject:_textPlain forKey:kSharePostModelTextPlain];
    [aCoder encodeObject:_groupId forKey:kSharePostModelGroupId];
    [aCoder encodeObject:_wallId forKey:kSharePostModelWallId];
    [aCoder encodeObject:_boosted forKey:kSharePostModelBoosted];
    [aCoder encodeObject:_postAuthorVerified forKey:kSharePostModelPostAuthorVerified];
    [aCoder encodeObject:_photos forKey:kSharePostModelPhotos];
    [aCoder encodeDouble:_photosNum forKey:kSharePostModelPhotosNum];
    [aCoder encodeObject:_postId forKey:kSharePostModelPostId];
    [aCoder encodeObject:_time forKey:kSharePostModelTime];
    [aCoder encodeObject:_isBroadcast forKey:kSharePostModelIsBroadcast];
}

- (id)copyWithZone:(NSZone *)zone
{
    SharePostModel *copy = [[SharePostModel alloc] init];
    
    if (copy) {
        
        copy.userPicture = [self.userPicture copyWithZone:zone];
        copy.postAuthorName = [self.postAuthorName copyWithZone:zone];
        copy.userType = [self.userType copyWithZone:zone];
        copy.pinned = self.pinned;
        copy.userName = [self.userName copyWithZone:zone];
        copy.channelUrl = [self.channelUrl copyWithZone:zone];
        copy.comments = [self.comments copyWithZone:zone];
        copy.location = [self.location copyWithZone:zone];
        copy.text = [self.text copyWithZone:zone];
        copy.userPinnedPost = [self.userPinnedPost copyWithZone:zone];
        copy.isEventAdmin = self.isEventAdmin;
        copy.iLike = self.iLike;
        copy.userVerified = [self.userVerified copyWithZone:zone];
        copy.postAuthorPicture = [self.postAuthorPicture copyWithZone:zone];
        copy.userCoverId = [self.userCoverId copyWithZone:zone];
        copy.userFirstname = [self.userFirstname copyWithZone:zone];
        copy.isGroupAdmin = self.isGroupAdmin;
        copy.privacy = [self.privacy copyWithZone:zone];
        copy.broadcastUrl = [self.broadcastUrl copyWithZone:zone];
        copy.userLastname = [self.userLastname copyWithZone:zone];
        copy.shares = [self.shares copyWithZone:zone];
        copy.postUserName = [self.postUserName copyWithZone:zone];
        copy.authorId = [self.authorId copyWithZone:zone];
        copy.eventId = [self.eventId copyWithZone:zone];
        copy.userSubscribed = [self.userSubscribed copyWithZone:zone];
        copy.broadcastName = [self.broadcastName copyWithZone:zone];
        copy.originId = [self.originId copyWithZone:zone];
        copy.likes = [self.likes copyWithZone:zone];
        copy.feelingAction = [self.feelingAction copyWithZone:zone];
        copy.views = [self.views copyWithZone:zone];
        copy.postType = [self.postType copyWithZone:zone];
        copy.postAuthorUrl = [self.postAuthorUrl copyWithZone:zone];
        copy.inWall = [self.inWall copyWithZone:zone];
        copy.inEvent = [self.inEvent copyWithZone:zone];
        copy.userPictureId = [self.userPictureId copyWithZone:zone];
        copy.iSave = self.iSave;
        copy.inGroup = [self.inGroup copyWithZone:zone];
        copy.feelingValue = [self.feelingValue copyWithZone:zone];
        copy.userGender = [self.userGender copyWithZone:zone];
        copy.isPageAdmin = self.isPageAdmin;
        copy.userId = [self.userId copyWithZone:zone];
        copy.managePost = self.managePost;
        copy.textPlain = [self.textPlain copyWithZone:zone];
        copy.groupId = [self.groupId copyWithZone:zone];
        copy.wallId = [self.wallId copyWithZone:zone];
        copy.boosted = [self.boosted copyWithZone:zone];
        copy.postAuthorVerified = [self.postAuthorVerified copyWithZone:zone];
        copy.photos = [self.photos copyWithZone:zone];
        copy.photosNum = self.photosNum;
        copy.postId = [self.postId copyWithZone:zone];
        copy.time = [self.time copyWithZone:zone];
        copy.isBroadcast = [self.isBroadcast copyWithZone:zone];
    }
    
    return copy;
}
@end
