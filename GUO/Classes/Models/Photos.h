//
//  Photos.h
//
//  Created by Amol Hirkane on 28/06/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Photos : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *photoId;
@property (nonatomic, strong) NSString *likes;
@property (nonatomic, strong) NSString *postId;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) NSString *albumId;
@property (nonatomic, strong) NSString *comments;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
