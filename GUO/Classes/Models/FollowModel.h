//
//  FollowModel.h
//
//  Created by Amol Hirkane on 29/06/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface FollowModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *connection;
@property (nonatomic, assign) BOOL isFollowing;
@property (nonatomic, strong) NSString *userLastname;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, strong) NSString *userFirstname;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userPicture;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
