//
//  PostModel.h
//
//  Created by Amol Hirkane on 28/06/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SharePostModel.h"


@interface PostModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *userPicture;
@property (nonatomic, strong) NSString *postAuthorName;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, assign) BOOL pinned;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *channelUrl;
@property (nonatomic, strong) NSString *comments;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *userPinnedPost;
@property (nonatomic, assign) BOOL isEventAdmin;
@property (nonatomic, assign) BOOL iLike;
@property (nonatomic, strong) NSString *userVerified;
@property (nonatomic, strong) NSString *postAuthorPicture;
@property (nonatomic, strong) NSString *userCoverId;
@property (nonatomic, strong) NSString *userFirstname;
@property (nonatomic, assign) BOOL isGroupAdmin;
@property (nonatomic, strong) NSString *privacy;
@property (nonatomic, strong) NSString *broadcastUrl;
@property (nonatomic, strong) NSString *userLastname;
@property (nonatomic, strong) NSString *shares;
@property (nonatomic, strong) NSString *postUserName;
@property (nonatomic, strong) NSString *authorId;
@property (nonatomic, strong) NSString *eventId;
@property (nonatomic, strong) NSString *userSubscribed;
@property (nonatomic, strong) NSString *broadcastName;
@property (nonatomic, strong) NSString *originId;
@property (nonatomic, strong) NSString *likes;
@property (nonatomic, strong) NSString *feelingAction;
@property (nonatomic, strong) NSString *views;
@property (nonatomic, strong) NSString *postType;
@property (nonatomic, strong) NSString *postAuthorUrl;
@property (nonatomic, strong) NSString *inWall;
@property (nonatomic, strong) NSString *inEvent;
@property (nonatomic, strong) NSString *userPictureId;
@property (nonatomic, assign) BOOL iSave;
@property (nonatomic, strong) NSString *inGroup;
@property (nonatomic, strong) NSString *feelingValue;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, assign) BOOL isPageAdmin;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, assign) BOOL managePost;
@property (nonatomic, strong) NSString *textPlain;
@property (nonatomic, strong) NSString *groupId;
@property (nonatomic, strong) NSString *wallId;
@property (nonatomic, strong) NSString *boosted;
@property (nonatomic, strong) NSString *postAuthorVerified;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSArray *videos;
@property (nonatomic, assign) double photosNum;
@property (nonatomic, strong) NSString *postId;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *formattedTime;
@property (nonatomic, strong) NSString *isBroadcast;
@property (nonatomic, strong) UIImage *videoThumbnail;
@property (nonatomic,strong) SharePostModel *origin;
@property (nonatomic, strong) NSString *translatedString;



////Monika model
//@property (nonatomic,strong)NSString *name;
////@property (nonatomic,strong)NSString *time;
//@property (nonatomic,strong)NSString *postDescr;
//@property (nonatomic,strong)NSString *postImg;
//@property (nonatomic,strong)NSString *commentCount;
//@property (nonatomic,strong)NSString *likeCount;
//@property (nonatomic,strong)NSString *shareCount;
//@property (nonatomic,strong)NSString *userImg;
//@property (nonatomic,assign)BOOL isSelected;
//@property (nonatomic,assign)BOOL isVip;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
