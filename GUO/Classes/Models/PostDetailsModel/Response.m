//
//  Response.m
//
//  Created by Pawan Ramteke on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "Response.h"
#import "Video.h"
#import "PostComments.h"
#import "Photos.h"


NSString *const kResponseShares = @"shares";
NSString *const kResponseVideo = @"video";
NSString *const kResponseFeelingValue = @"feeling_value";
NSString *const kResponseUserCoverId = @"user_cover_id";
NSString *const kResponseUserId = @"user_id";
NSString *const kResponsePostId = @"post_id";
NSString *const kResponseUserFirstname = @"user_firstname";
NSString *const kResponseUserType = @"user_type";
NSString *const kResponsePostComments = @"post_comments";
NSString *const kResponseGroupId = @"group_id";
NSString *const kResponseInWall = @"in_wall";
NSString *const kResponsePostAuthorUrl = @"post_author_url";
NSString *const kResponseOriginId = @"origin_id";
NSString *const kResponseFeelingAction = @"feeling_action";
NSString *const kResponseBroadcastUrl = @"broadcast_url";
NSString *const kResponsePostUserName = @"post_user_name";
NSString *const kResponseEventId = @"event_id";
NSString *const kResponsePostAuthorPicture = @"post_author_picture";
NSString *const kResponseUserLastname = @"user_lastname";
NSString *const kResponseChannelUrl = @"channel_url";
NSString *const kResponseLocation = @"location";
NSString *const kResponseInEvent = @"in_event";
NSString *const kResponsePostAuthorName = @"post_author_name";
NSString *const kResponseViews = @"views";
NSString *const kResponseInGroup = @"in_group";
NSString *const kResponseText = @"text";
NSString *const kResponseTextPlain = @"text_plain";
NSString *const kResponseIsGroupAdmin = @"is_group_admin";
NSString *const kResponseWallId = @"wall_id";
NSString *const kResponseIsEventAdmin = @"is_event_admin";
NSString *const kResponseISave = @"i_save";
NSString *const kResponseUserPictureId = @"user_picture_id";
NSString *const kResponseBoosted = @"boosted";
NSString *const kResponseUserPinnedPost = @"user_pinned_post";
NSString *const kResponseBroadcastName = @"broadcast_name";
NSString *const kResponseComments = @"comments";
NSString *const kResponseUserGender = @"user_gender";
NSString *const kResponseAuthorId = @"author_id";
NSString *const kResponseUserPicture = @"user_picture";
NSString *const kResponseILike = @"i_like";
NSString *const kResponsePostType = @"post_type";
NSString *const kResponsePostAuthorVerified = @"post_author_verified";
NSString *const kResponseTime = @"time";
NSString *const kResponseUserVerified = @"user_verified";
NSString *const kResponseUserSubscribed = @"user_subscribed";
NSString *const kResponsePinned = @"pinned";
NSString *const kResponseUserName = @"user_name";
NSString *const kResponseIsBroadcast = @"is_broadcast";
NSString *const kResponseManagePost = @"manage_post";
NSString *const kResponsePhotos = @"photos";
NSString *const kResponseLikes = @"likes";
NSString *const kResponsePrivacy = @"privacy";
NSString *const kResponseIsPageAdmin = @"is_page_admin";


@interface Response ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Response

@synthesize shares = _shares;
@synthesize video = _video;
@synthesize feelingValue = _feelingValue;
@synthesize userCoverId = _userCoverId;
@synthesize userId = _userId;
@synthesize postId = _postId;
@synthesize userFirstname = _userFirstname;
@synthesize userType = _userType;
@synthesize postComments = _postComments;
@synthesize groupId = _groupId;
@synthesize inWall = _inWall;
@synthesize postAuthorUrl = _postAuthorUrl;
@synthesize originId = _originId;
@synthesize feelingAction = _feelingAction;
@synthesize broadcastUrl = _broadcastUrl;
@synthesize postUserName = _postUserName;
@synthesize eventId = _eventId;
@synthesize postAuthorPicture = _postAuthorPicture;
@synthesize userLastname = _userLastname;
@synthesize channelUrl = _channelUrl;
@synthesize location = _location;
@synthesize inEvent = _inEvent;
@synthesize postAuthorName = _postAuthorName;
@synthesize views = _views;
@synthesize inGroup = _inGroup;
@synthesize text = _text;
@synthesize textPlain = _textPlain;
@synthesize isGroupAdmin = _isGroupAdmin;
@synthesize wallId = _wallId;
@synthesize isEventAdmin = _isEventAdmin;
@synthesize iSave = _iSave;
@synthesize userPictureId = _userPictureId;
@synthesize boosted = _boosted;
@synthesize userPinnedPost = _userPinnedPost;
@synthesize broadcastName = _broadcastName;
@synthesize comments = _comments;
@synthesize userGender = _userGender;
@synthesize authorId = _authorId;
@synthesize userPicture = _userPicture;
@synthesize iLike = _iLike;
@synthesize postType = _postType;
@synthesize postAuthorVerified = _postAuthorVerified;
@synthesize time = _time;
@synthesize userVerified = _userVerified;
@synthesize userSubscribed = _userSubscribed;
@synthesize pinned = _pinned;
@synthesize userName = _userName;
@synthesize isBroadcast = _isBroadcast;
@synthesize managePost = _managePost;
@synthesize photos = _photos;
@synthesize likes = _likes;
@synthesize privacy = _privacy;
@synthesize isPageAdmin = _isPageAdmin;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.shares = [self objectOrNilForKey:kResponseShares fromDictionary:dict];
            self.video = [Video modelObjectWithDictionary:[dict objectForKey:kResponseVideo]];
            self.feelingValue = [self objectOrNilForKey:kResponseFeelingValue fromDictionary:dict];
            self.userCoverId = [self objectOrNilForKey:kResponseUserCoverId fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kResponseUserId fromDictionary:dict];
            self.postId = [self objectOrNilForKey:kResponsePostId fromDictionary:dict];
            self.userFirstname = [self objectOrNilForKey:kResponseUserFirstname fromDictionary:dict];
            self.userType = [self objectOrNilForKey:kResponseUserType fromDictionary:dict];
    NSObject *receivedPostComments = [dict objectForKey:kResponsePostComments];
    NSMutableArray *parsedPostComments = [NSMutableArray array];
    if ([receivedPostComments isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedPostComments) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedPostComments addObject:[PostComments modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedPostComments isKindOfClass:[NSDictionary class]]) {
       [parsedPostComments addObject:[PostComments modelObjectWithDictionary:(NSDictionary *)receivedPostComments]];
    }

    self.postComments = [NSArray arrayWithArray:parsedPostComments];
            self.groupId = [self objectOrNilForKey:kResponseGroupId fromDictionary:dict];
            self.inWall = [self objectOrNilForKey:kResponseInWall fromDictionary:dict];
            self.postAuthorUrl = [self objectOrNilForKey:kResponsePostAuthorUrl fromDictionary:dict];
            self.originId = [self objectOrNilForKey:kResponseOriginId fromDictionary:dict];
            self.feelingAction = [self objectOrNilForKey:kResponseFeelingAction fromDictionary:dict];
            self.broadcastUrl = [self objectOrNilForKey:kResponseBroadcastUrl fromDictionary:dict];
            self.postUserName = [self objectOrNilForKey:kResponsePostUserName fromDictionary:dict];
            self.eventId = [self objectOrNilForKey:kResponseEventId fromDictionary:dict];
            self.postAuthorPicture = [self objectOrNilForKey:kResponsePostAuthorPicture fromDictionary:dict];
            self.userLastname = [self objectOrNilForKey:kResponseUserLastname fromDictionary:dict];
            self.channelUrl = [self objectOrNilForKey:kResponseChannelUrl fromDictionary:dict];
            self.location = [self objectOrNilForKey:kResponseLocation fromDictionary:dict];
            self.inEvent = [self objectOrNilForKey:kResponseInEvent fromDictionary:dict];
            self.postAuthorName = [self objectOrNilForKey:kResponsePostAuthorName fromDictionary:dict];
            self.views = [self objectOrNilForKey:kResponseViews fromDictionary:dict];
            self.inGroup = [self objectOrNilForKey:kResponseInGroup fromDictionary:dict];
            self.text = [self objectOrNilForKey:kResponseText fromDictionary:dict];
            self.textPlain = [self objectOrNilForKey:kResponseTextPlain fromDictionary:dict];
            self.isGroupAdmin = [[self objectOrNilForKey:kResponseIsGroupAdmin fromDictionary:dict] boolValue];
            self.wallId = [self objectOrNilForKey:kResponseWallId fromDictionary:dict];
            self.isEventAdmin = [[self objectOrNilForKey:kResponseIsEventAdmin fromDictionary:dict] boolValue];
            self.iSave = [[self objectOrNilForKey:kResponseISave fromDictionary:dict] boolValue];
            self.userPictureId = [self objectOrNilForKey:kResponseUserPictureId fromDictionary:dict];
            self.boosted = [self objectOrNilForKey:kResponseBoosted fromDictionary:dict];
            self.userPinnedPost = [self objectOrNilForKey:kResponseUserPinnedPost fromDictionary:dict];
            self.broadcastName = [self objectOrNilForKey:kResponseBroadcastName fromDictionary:dict];
            self.comments = [self objectOrNilForKey:kResponseComments fromDictionary:dict];
            self.userGender = [self objectOrNilForKey:kResponseUserGender fromDictionary:dict];
            self.authorId = [self objectOrNilForKey:kResponseAuthorId fromDictionary:dict];
            self.userPicture = [self objectOrNilForKey:kResponseUserPicture fromDictionary:dict];
            self.iLike = [[self objectOrNilForKey:kResponseILike fromDictionary:dict] boolValue];
            self.postType = [self objectOrNilForKey:kResponsePostType fromDictionary:dict];
            self.postAuthorVerified = [self objectOrNilForKey:kResponsePostAuthorVerified fromDictionary:dict];
            self.time = [self objectOrNilForKey:kResponseTime fromDictionary:dict];
            self.userVerified = [self objectOrNilForKey:kResponseUserVerified fromDictionary:dict];
            self.userSubscribed = [self objectOrNilForKey:kResponseUserSubscribed fromDictionary:dict];
            self.pinned = [[self objectOrNilForKey:kResponsePinned fromDictionary:dict] boolValue];
            self.userName = [self objectOrNilForKey:kResponseUserName fromDictionary:dict];
            self.isBroadcast = [self objectOrNilForKey:kResponseIsBroadcast fromDictionary:dict];
            self.managePost = [[self objectOrNilForKey:kResponseManagePost fromDictionary:dict] boolValue];
    NSObject *receivedPhotos = [dict objectForKey:kResponsePhotos];
    NSMutableArray *parsedPhotos = [NSMutableArray array];
    if ([receivedPhotos isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedPhotos) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedPhotos addObject:[Photos modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedPhotos isKindOfClass:[NSDictionary class]]) {
       [parsedPhotos addObject:[Photos modelObjectWithDictionary:(NSDictionary *)receivedPhotos]];
    }

    self.photos = [NSArray arrayWithArray:parsedPhotos];
            self.likes = [self objectOrNilForKey:kResponseLikes fromDictionary:dict];
            self.privacy = [self objectOrNilForKey:kResponsePrivacy fromDictionary:dict];
            self.isPageAdmin = [[self objectOrNilForKey:kResponseIsPageAdmin fromDictionary:dict] boolValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.shares forKey:kResponseShares];
    [mutableDict setValue:[self.video dictionaryRepresentation] forKey:kResponseVideo];
    [mutableDict setValue:self.feelingValue forKey:kResponseFeelingValue];
    [mutableDict setValue:self.userCoverId forKey:kResponseUserCoverId];
    [mutableDict setValue:self.userId forKey:kResponseUserId];
    [mutableDict setValue:self.postId forKey:kResponsePostId];
    [mutableDict setValue:self.userFirstname forKey:kResponseUserFirstname];
    [mutableDict setValue:self.userType forKey:kResponseUserType];
    NSMutableArray *tempArrayForPostComments = [NSMutableArray array];
    for (NSObject *subArrayObject in self.postComments) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPostComments addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPostComments addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPostComments] forKey:kResponsePostComments];
    [mutableDict setValue:self.groupId forKey:kResponseGroupId];
    [mutableDict setValue:self.inWall forKey:kResponseInWall];
    [mutableDict setValue:self.postAuthorUrl forKey:kResponsePostAuthorUrl];
    [mutableDict setValue:self.originId forKey:kResponseOriginId];
    [mutableDict setValue:self.feelingAction forKey:kResponseFeelingAction];
    [mutableDict setValue:self.broadcastUrl forKey:kResponseBroadcastUrl];
    [mutableDict setValue:self.postUserName forKey:kResponsePostUserName];
    [mutableDict setValue:self.eventId forKey:kResponseEventId];
    [mutableDict setValue:self.postAuthorPicture forKey:kResponsePostAuthorPicture];
    [mutableDict setValue:self.userLastname forKey:kResponseUserLastname];
    [mutableDict setValue:self.channelUrl forKey:kResponseChannelUrl];
    [mutableDict setValue:self.location forKey:kResponseLocation];
    [mutableDict setValue:self.inEvent forKey:kResponseInEvent];
    [mutableDict setValue:self.postAuthorName forKey:kResponsePostAuthorName];
    [mutableDict setValue:self.views forKey:kResponseViews];
    [mutableDict setValue:self.inGroup forKey:kResponseInGroup];
    [mutableDict setValue:self.text forKey:kResponseText];
    [mutableDict setValue:self.textPlain forKey:kResponseTextPlain];
    [mutableDict setValue:[NSNumber numberWithBool:self.isGroupAdmin] forKey:kResponseIsGroupAdmin];
    [mutableDict setValue:self.wallId forKey:kResponseWallId];
    [mutableDict setValue:[NSNumber numberWithBool:self.isEventAdmin] forKey:kResponseIsEventAdmin];
    [mutableDict setValue:[NSNumber numberWithBool:self.iSave] forKey:kResponseISave];
    [mutableDict setValue:self.userPictureId forKey:kResponseUserPictureId];
    [mutableDict setValue:self.boosted forKey:kResponseBoosted];
    [mutableDict setValue:self.userPinnedPost forKey:kResponseUserPinnedPost];
    [mutableDict setValue:self.broadcastName forKey:kResponseBroadcastName];
    [mutableDict setValue:self.comments forKey:kResponseComments];
    [mutableDict setValue:self.userGender forKey:kResponseUserGender];
    [mutableDict setValue:self.authorId forKey:kResponseAuthorId];
    [mutableDict setValue:self.userPicture forKey:kResponseUserPicture];
    [mutableDict setValue:[NSNumber numberWithBool:self.iLike] forKey:kResponseILike];
    [mutableDict setValue:self.postType forKey:kResponsePostType];
    [mutableDict setValue:self.postAuthorVerified forKey:kResponsePostAuthorVerified];
    [mutableDict setValue:self.time forKey:kResponseTime];
    [mutableDict setValue:self.userVerified forKey:kResponseUserVerified];
    [mutableDict setValue:self.userSubscribed forKey:kResponseUserSubscribed];
    [mutableDict setValue:[NSNumber numberWithBool:self.pinned] forKey:kResponsePinned];
    [mutableDict setValue:self.userName forKey:kResponseUserName];
    [mutableDict setValue:self.isBroadcast forKey:kResponseIsBroadcast];
    [mutableDict setValue:[NSNumber numberWithBool:self.managePost] forKey:kResponseManagePost];
    NSMutableArray *tempArrayForPhotos = [NSMutableArray array];
    for (NSObject *subArrayObject in self.photos) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPhotos addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPhotos addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPhotos] forKey:kResponsePhotos];
    [mutableDict setValue:self.likes forKey:kResponseLikes];
    [mutableDict setValue:self.privacy forKey:kResponsePrivacy];
    [mutableDict setValue:[NSNumber numberWithBool:self.isPageAdmin] forKey:kResponseIsPageAdmin];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.shares = [aDecoder decodeObjectForKey:kResponseShares];
    self.video = [aDecoder decodeObjectForKey:kResponseVideo];
    self.feelingValue = [aDecoder decodeObjectForKey:kResponseFeelingValue];
    self.userCoverId = [aDecoder decodeObjectForKey:kResponseUserCoverId];
    self.userId = [aDecoder decodeObjectForKey:kResponseUserId];
    self.postId = [aDecoder decodeObjectForKey:kResponsePostId];
    self.userFirstname = [aDecoder decodeObjectForKey:kResponseUserFirstname];
    self.userType = [aDecoder decodeObjectForKey:kResponseUserType];
    self.postComments = [aDecoder decodeObjectForKey:kResponsePostComments];
    self.groupId = [aDecoder decodeObjectForKey:kResponseGroupId];
    self.inWall = [aDecoder decodeObjectForKey:kResponseInWall];
    self.postAuthorUrl = [aDecoder decodeObjectForKey:kResponsePostAuthorUrl];
    self.originId = [aDecoder decodeObjectForKey:kResponseOriginId];
    self.feelingAction = [aDecoder decodeObjectForKey:kResponseFeelingAction];
    self.broadcastUrl = [aDecoder decodeObjectForKey:kResponseBroadcastUrl];
    self.postUserName = [aDecoder decodeObjectForKey:kResponsePostUserName];
    self.eventId = [aDecoder decodeObjectForKey:kResponseEventId];
    self.postAuthorPicture = [aDecoder decodeObjectForKey:kResponsePostAuthorPicture];
    self.userLastname = [aDecoder decodeObjectForKey:kResponseUserLastname];
    self.channelUrl = [aDecoder decodeObjectForKey:kResponseChannelUrl];
    self.location = [aDecoder decodeObjectForKey:kResponseLocation];
    self.inEvent = [aDecoder decodeObjectForKey:kResponseInEvent];
    self.postAuthorName = [aDecoder decodeObjectForKey:kResponsePostAuthorName];
    self.views = [aDecoder decodeObjectForKey:kResponseViews];
    self.inGroup = [aDecoder decodeObjectForKey:kResponseInGroup];
    self.text = [aDecoder decodeObjectForKey:kResponseText];
    self.textPlain = [aDecoder decodeObjectForKey:kResponseTextPlain];
    self.isGroupAdmin = [aDecoder decodeBoolForKey:kResponseIsGroupAdmin];
    self.wallId = [aDecoder decodeObjectForKey:kResponseWallId];
    self.isEventAdmin = [aDecoder decodeBoolForKey:kResponseIsEventAdmin];
    self.iSave = [aDecoder decodeBoolForKey:kResponseISave];
    self.userPictureId = [aDecoder decodeObjectForKey:kResponseUserPictureId];
    self.boosted = [aDecoder decodeObjectForKey:kResponseBoosted];
    self.userPinnedPost = [aDecoder decodeObjectForKey:kResponseUserPinnedPost];
    self.broadcastName = [aDecoder decodeObjectForKey:kResponseBroadcastName];
    self.comments = [aDecoder decodeObjectForKey:kResponseComments];
    self.userGender = [aDecoder decodeObjectForKey:kResponseUserGender];
    self.authorId = [aDecoder decodeObjectForKey:kResponseAuthorId];
    self.userPicture = [aDecoder decodeObjectForKey:kResponseUserPicture];
    self.iLike = [aDecoder decodeBoolForKey:kResponseILike];
    self.postType = [aDecoder decodeObjectForKey:kResponsePostType];
    self.postAuthorVerified = [aDecoder decodeObjectForKey:kResponsePostAuthorVerified];
    self.time = [aDecoder decodeObjectForKey:kResponseTime];
    self.userVerified = [aDecoder decodeObjectForKey:kResponseUserVerified];
    self.userSubscribed = [aDecoder decodeObjectForKey:kResponseUserSubscribed];
    self.pinned = [aDecoder decodeBoolForKey:kResponsePinned];
    self.userName = [aDecoder decodeObjectForKey:kResponseUserName];
    self.isBroadcast = [aDecoder decodeObjectForKey:kResponseIsBroadcast];
    self.managePost = [aDecoder decodeBoolForKey:kResponseManagePost];
    self.photos = [aDecoder decodeObjectForKey:kResponsePhotos];
    self.likes = [aDecoder decodeObjectForKey:kResponseLikes];
    self.privacy = [aDecoder decodeObjectForKey:kResponsePrivacy];
    self.isPageAdmin = [aDecoder decodeBoolForKey:kResponseIsPageAdmin];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_shares forKey:kResponseShares];
    [aCoder encodeObject:_video forKey:kResponseVideo];
    [aCoder encodeObject:_feelingValue forKey:kResponseFeelingValue];
    [aCoder encodeObject:_userCoverId forKey:kResponseUserCoverId];
    [aCoder encodeObject:_userId forKey:kResponseUserId];
    [aCoder encodeObject:_postId forKey:kResponsePostId];
    [aCoder encodeObject:_userFirstname forKey:kResponseUserFirstname];
    [aCoder encodeObject:_userType forKey:kResponseUserType];
    [aCoder encodeObject:_postComments forKey:kResponsePostComments];
    [aCoder encodeObject:_groupId forKey:kResponseGroupId];
    [aCoder encodeObject:_inWall forKey:kResponseInWall];
    [aCoder encodeObject:_postAuthorUrl forKey:kResponsePostAuthorUrl];
    [aCoder encodeObject:_originId forKey:kResponseOriginId];
    [aCoder encodeObject:_feelingAction forKey:kResponseFeelingAction];
    [aCoder encodeObject:_broadcastUrl forKey:kResponseBroadcastUrl];
    [aCoder encodeObject:_postUserName forKey:kResponsePostUserName];
    [aCoder encodeObject:_eventId forKey:kResponseEventId];
    [aCoder encodeObject:_postAuthorPicture forKey:kResponsePostAuthorPicture];
    [aCoder encodeObject:_userLastname forKey:kResponseUserLastname];
    [aCoder encodeObject:_channelUrl forKey:kResponseChannelUrl];
    [aCoder encodeObject:_location forKey:kResponseLocation];
    [aCoder encodeObject:_inEvent forKey:kResponseInEvent];
    [aCoder encodeObject:_postAuthorName forKey:kResponsePostAuthorName];
    [aCoder encodeObject:_views forKey:kResponseViews];
    [aCoder encodeObject:_inGroup forKey:kResponseInGroup];
    [aCoder encodeObject:_text forKey:kResponseText];
    [aCoder encodeObject:_textPlain forKey:kResponseTextPlain];
    [aCoder encodeBool:_isGroupAdmin forKey:kResponseIsGroupAdmin];
    [aCoder encodeObject:_wallId forKey:kResponseWallId];
    [aCoder encodeBool:_isEventAdmin forKey:kResponseIsEventAdmin];
    [aCoder encodeBool:_iSave forKey:kResponseISave];
    [aCoder encodeObject:_userPictureId forKey:kResponseUserPictureId];
    [aCoder encodeObject:_boosted forKey:kResponseBoosted];
    [aCoder encodeObject:_userPinnedPost forKey:kResponseUserPinnedPost];
    [aCoder encodeObject:_broadcastName forKey:kResponseBroadcastName];
    [aCoder encodeObject:_comments forKey:kResponseComments];
    [aCoder encodeObject:_userGender forKey:kResponseUserGender];
    [aCoder encodeObject:_authorId forKey:kResponseAuthorId];
    [aCoder encodeObject:_userPicture forKey:kResponseUserPicture];
    [aCoder encodeBool:_iLike forKey:kResponseILike];
    [aCoder encodeObject:_postType forKey:kResponsePostType];
    [aCoder encodeObject:_postAuthorVerified forKey:kResponsePostAuthorVerified];
    [aCoder encodeObject:_time forKey:kResponseTime];
    [aCoder encodeObject:_userVerified forKey:kResponseUserVerified];
    [aCoder encodeObject:_userSubscribed forKey:kResponseUserSubscribed];
    [aCoder encodeBool:_pinned forKey:kResponsePinned];
    [aCoder encodeObject:_userName forKey:kResponseUserName];
    [aCoder encodeObject:_isBroadcast forKey:kResponseIsBroadcast];
    [aCoder encodeBool:_managePost forKey:kResponseManagePost];
    [aCoder encodeObject:_photos forKey:kResponsePhotos];
    [aCoder encodeObject:_likes forKey:kResponseLikes];
    [aCoder encodeObject:_privacy forKey:kResponsePrivacy];
    [aCoder encodeBool:_isPageAdmin forKey:kResponseIsPageAdmin];
}

- (id)copyWithZone:(NSZone *)zone
{
    Response *copy = [[Response alloc] init];
    
    if (copy) {

        copy.shares = [self.shares copyWithZone:zone];
        copy.video = [self.video copyWithZone:zone];
        copy.feelingValue = [self.feelingValue copyWithZone:zone];
        copy.userCoverId = [self.userCoverId copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.postId = [self.postId copyWithZone:zone];
        copy.userFirstname = [self.userFirstname copyWithZone:zone];
        copy.userType = [self.userType copyWithZone:zone];
        copy.postComments = [self.postComments copyWithZone:zone];
        copy.groupId = [self.groupId copyWithZone:zone];
        copy.inWall = [self.inWall copyWithZone:zone];
        copy.postAuthorUrl = [self.postAuthorUrl copyWithZone:zone];
        copy.originId = [self.originId copyWithZone:zone];
        copy.feelingAction = [self.feelingAction copyWithZone:zone];
        copy.broadcastUrl = [self.broadcastUrl copyWithZone:zone];
        copy.postUserName = [self.postUserName copyWithZone:zone];
        copy.eventId = [self.eventId copyWithZone:zone];
        copy.postAuthorPicture = [self.postAuthorPicture copyWithZone:zone];
        copy.userLastname = [self.userLastname copyWithZone:zone];
        copy.channelUrl = [self.channelUrl copyWithZone:zone];
        copy.location = [self.location copyWithZone:zone];
        copy.inEvent = [self.inEvent copyWithZone:zone];
        copy.postAuthorName = [self.postAuthorName copyWithZone:zone];
        copy.views = [self.views copyWithZone:zone];
        copy.inGroup = [self.inGroup copyWithZone:zone];
        copy.text = [self.text copyWithZone:zone];
        copy.textPlain = [self.textPlain copyWithZone:zone];
        copy.isGroupAdmin = self.isGroupAdmin;
        copy.wallId = [self.wallId copyWithZone:zone];
        copy.isEventAdmin = self.isEventAdmin;
        copy.iSave = self.iSave;
        copy.userPictureId = [self.userPictureId copyWithZone:zone];
        copy.boosted = [self.boosted copyWithZone:zone];
        copy.userPinnedPost = [self.userPinnedPost copyWithZone:zone];
        copy.broadcastName = [self.broadcastName copyWithZone:zone];
        copy.comments = [self.comments copyWithZone:zone];
        copy.userGender = [self.userGender copyWithZone:zone];
        copy.authorId = [self.authorId copyWithZone:zone];
        copy.userPicture = [self.userPicture copyWithZone:zone];
        copy.iLike = self.iLike;
        copy.postType = [self.postType copyWithZone:zone];
        copy.postAuthorVerified = [self.postAuthorVerified copyWithZone:zone];
        copy.time = [self.time copyWithZone:zone];
        copy.userVerified = [self.userVerified copyWithZone:zone];
        copy.userSubscribed = [self.userSubscribed copyWithZone:zone];
        copy.pinned = self.pinned;
        copy.userName = [self.userName copyWithZone:zone];
        copy.isBroadcast = [self.isBroadcast copyWithZone:zone];
        copy.managePost = self.managePost;
        copy.photos = [self.photos copyWithZone:zone];
        copy.likes = [self.likes copyWithZone:zone];
        copy.privacy = [self.privacy copyWithZone:zone];
        copy.isPageAdmin = self.isPageAdmin;
    }
    
    return copy;
}


@end
