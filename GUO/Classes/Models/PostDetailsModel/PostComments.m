//
//  PostComments.m
//
//  Created by Pawan Ramteke on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "PostComments.h"
#import "CommentReplies.h"


NSString *const kPostCommentsPageAdmin = @"page_admin";
NSString *const kPostCommentsUserPicture = @"user_picture";
NSString *const kPostCommentsUserType = @"user_type";
NSString *const kPostCommentsPageCover = @"page_cover";
NSString *const kPostCommentsPagePinnedPost = @"page_pinned_post";
NSString *const kPostCommentsReplies = @"replies";
NSString *const kPostCommentsUserName = @"user_name";
NSString *const kPostCommentsAuthorVerified = @"author_verified";
NSString *const kPostCommentsDeleteComment = @"delete_comment";
NSString *const kPostCommentsText = @"text";
NSString *const kPostCommentsNodeId = @"node_id";
NSString *const kPostCommentsUserVerified = @"user_verified";
NSString *const kPostCommentsAuthorName = @"author_name";
NSString *const kPostCommentsUserFirstname = @"user_firstname";
NSString *const kPostCommentsAuthorPicture = @"author_picture";
NSString *const kPostCommentsUserLastname = @"user_lastname";
NSString *const kPostCommentsPageCoverId = @"page_cover_id";
NSString *const kPostCommentsEditComment = @"edit_comment";
NSString *const kPostCommentsPageDescription = @"page_description";
NSString *const kPostCommentsAuthorId = @"author_id";
NSString *const kPostCommentsImage = @"image";
NSString *const kPostCommentsVideo = @"video";
NSString *const kPostCommentsLikes = @"likes";
NSString *const kPostCommentsPageVerified = @"page_verified";
NSString *const kPostCommentsPageAlbumPictures = @"page_album_pictures";
NSString *const kPostCommentsPageAlbumTimeline = @"page_album_timeline";
NSString *const kPostCommentsPageName = @"page_name";
NSString *const kPostCommentsCommentReplies = @"comment_replies";
NSString *const kPostCommentsPagePictureId = @"page_picture_id";
NSString *const kPostCommentsPageTitle = @"page_title";
NSString *const kPostCommentsPageLikes = @"page_likes";
NSString *const kPostCommentsUserGender = @"user_gender";
NSString *const kPostCommentsNodeType = @"node_type";
NSString *const kPostCommentsUserId = @"user_id";
NSString *const kPostCommentsPagePicture = @"page_picture";
NSString *const kPostCommentsPageAlbumCovers = @"page_album_covers";
NSString *const kPostCommentsTextPlain = @"text_plain";
NSString *const kPostCommentsCommentId = @"comment_id";
NSString *const kPostCommentsPageId = @"page_id";
NSString *const kPostCommentsTime = @"time";
NSString *const kPostCommentsFormattedTime = @"formatted_time";
NSString *const kPostCommentsPageBoosted = @"page_boosted";
NSString *const kPostCommentsPageCategory = @"page_category";
NSString *const kPostCommentsAuthorUrl = @"author_url";
NSString *const kPostCommentsPageDate = @"page_date";
NSString *const kPostCommentsILike = @"i_like";


@interface PostComments ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PostComments

@synthesize pageAdmin = _pageAdmin;
@synthesize userPicture = _userPicture;
@synthesize userType = _userType;
@synthesize pageCover = _pageCover;
@synthesize pagePinnedPost = _pagePinnedPost;
@synthesize replies = _replies;
@synthesize userName = _userName;
@synthesize authorVerified = _authorVerified;
@synthesize deleteComment = _deleteComment;
@synthesize text = _text;
@synthesize nodeId = _nodeId;
@synthesize userVerified = _userVerified;
@synthesize authorName = _authorName;
@synthesize userFirstname = _userFirstname;
@synthesize authorPicture = _authorPicture;
@synthesize userLastname = _userLastname;
@synthesize pageCoverId = _pageCoverId;
@synthesize editComment = _editComment;
@synthesize pageDescription = _pageDescription;
@synthesize authorId = _authorId;
@synthesize image = _image;
@synthesize video = _video;
@synthesize likes = _likes;
@synthesize pageVerified = _pageVerified;
@synthesize pageAlbumPictures = _pageAlbumPictures;
@synthesize pageAlbumTimeline = _pageAlbumTimeline;
@synthesize pageName = _pageName;
@synthesize commentReplies = _commentReplies;
@synthesize pagePictureId = _pagePictureId;
@synthesize pageTitle = _pageTitle;
@synthesize pageLikes = _pageLikes;
@synthesize userGender = _userGender;
@synthesize nodeType = _nodeType;
@synthesize userId = _userId;
@synthesize pagePicture = _pagePicture;
@synthesize pageAlbumCovers = _pageAlbumCovers;
@synthesize textPlain = _textPlain;
@synthesize commentId = _commentId;
@synthesize pageId = _pageId;
@synthesize time = _time;
@synthesize formattedTime = _formattedTime;
@synthesize pageBoosted = _pageBoosted;
@synthesize pageCategory = _pageCategory;
@synthesize authorUrl = _authorUrl;
@synthesize pageDate = _pageDate;
@synthesize iLike = _iLike;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.pageAdmin = [self objectOrNilForKey:kPostCommentsPageAdmin fromDictionary:dict];
            self.userPicture = [self objectOrNilForKey:kPostCommentsUserPicture fromDictionary:dict];
            self.userType = [self objectOrNilForKey:kPostCommentsUserType fromDictionary:dict];
            self.pageCover = [self objectOrNilForKey:kPostCommentsPageCover fromDictionary:dict];
            self.pagePinnedPost = [self objectOrNilForKey:kPostCommentsPagePinnedPost fromDictionary:dict];
            self.replies = [self objectOrNilForKey:kPostCommentsReplies fromDictionary:dict];
            self.userName = [self objectOrNilForKey:kPostCommentsUserName fromDictionary:dict];
            self.authorVerified = [self objectOrNilForKey:kPostCommentsAuthorVerified fromDictionary:dict];
            self.deleteComment = [[self objectOrNilForKey:kPostCommentsDeleteComment fromDictionary:dict] boolValue];
            self.text = [self objectOrNilForKey:kPostCommentsText fromDictionary:dict];
            self.nodeId = [self objectOrNilForKey:kPostCommentsNodeId fromDictionary:dict];
            self.userVerified = [self objectOrNilForKey:kPostCommentsUserVerified fromDictionary:dict];
            self.authorName = [self objectOrNilForKey:kPostCommentsAuthorName fromDictionary:dict];
            self.userFirstname = [self objectOrNilForKey:kPostCommentsUserFirstname fromDictionary:dict];
            self.authorPicture = [self objectOrNilForKey:kPostCommentsAuthorPicture fromDictionary:dict];
            self.userLastname = [self objectOrNilForKey:kPostCommentsUserLastname fromDictionary:dict];
            self.pageCoverId = [self objectOrNilForKey:kPostCommentsPageCoverId fromDictionary:dict];
            self.iLike = [[self objectOrNilForKey:kPostCommentsILike fromDictionary:dict] boolValue];

            self.editComment = [[self objectOrNilForKey:kPostCommentsEditComment fromDictionary:dict] boolValue];
            self.pageDescription = [self objectOrNilForKey:kPostCommentsPageDescription fromDictionary:dict];
            self.authorId = [self objectOrNilForKey:kPostCommentsAuthorId fromDictionary:dict];
            self.image = [self objectOrNilForKey:kPostCommentsImage fromDictionary:dict];
        self.video = [self objectOrNilForKey:kPostCommentsVideo fromDictionary:dict];

            self.likes = [self objectOrNilForKey:kPostCommentsLikes fromDictionary:dict];
            self.pageVerified = [self objectOrNilForKey:kPostCommentsPageVerified fromDictionary:dict];
            self.pageAlbumPictures = [self objectOrNilForKey:kPostCommentsPageAlbumPictures fromDictionary:dict];
            self.pageAlbumTimeline = [self objectOrNilForKey:kPostCommentsPageAlbumTimeline fromDictionary:dict];
            self.pageName = [self objectOrNilForKey:kPostCommentsPageName fromDictionary:dict];
    NSObject *receivedCommentReplies = [dict objectForKey:kPostCommentsCommentReplies];
    NSMutableArray *parsedCommentReplies = [NSMutableArray array];
    if ([receivedCommentReplies isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedCommentReplies) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedCommentReplies addObject:[CommentReplies modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedCommentReplies isKindOfClass:[NSDictionary class]]) {
       [parsedCommentReplies addObject:[CommentReplies modelObjectWithDictionary:(NSDictionary *)receivedCommentReplies]];
    }

    self.commentReplies = [NSArray arrayWithArray:parsedCommentReplies];
            self.pagePictureId = [self objectOrNilForKey:kPostCommentsPagePictureId fromDictionary:dict];
            self.pageTitle = [self objectOrNilForKey:kPostCommentsPageTitle fromDictionary:dict];
            self.pageLikes = [self objectOrNilForKey:kPostCommentsPageLikes fromDictionary:dict];
            self.userGender = [self objectOrNilForKey:kPostCommentsUserGender fromDictionary:dict];
            self.nodeType = [self objectOrNilForKey:kPostCommentsNodeType fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kPostCommentsUserId fromDictionary:dict];
            self.pagePicture = [self objectOrNilForKey:kPostCommentsPagePicture fromDictionary:dict];
            self.pageAlbumCovers = [self objectOrNilForKey:kPostCommentsPageAlbumCovers fromDictionary:dict];
            self.textPlain = [self objectOrNilForKey:kPostCommentsTextPlain fromDictionary:dict];
            self.commentId = [self objectOrNilForKey:kPostCommentsCommentId fromDictionary:dict];
            self.pageId = [self objectOrNilForKey:kPostCommentsPageId fromDictionary:dict];
            self.time = [self objectOrNilForKey:kPostCommentsTime fromDictionary:dict];
        self.formattedTime = [self objectOrNilForKey:kPostCommentsFormattedTime fromDictionary:dict];

            self.pageBoosted = [self objectOrNilForKey:kPostCommentsPageBoosted fromDictionary:dict];
            self.pageCategory = [self objectOrNilForKey:kPostCommentsPageCategory fromDictionary:dict];
            self.authorUrl = [self objectOrNilForKey:kPostCommentsAuthorUrl fromDictionary:dict];
            self.pageDate = [self objectOrNilForKey:kPostCommentsPageDate fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.pageAdmin forKey:kPostCommentsPageAdmin];
    [mutableDict setValue:self.userPicture forKey:kPostCommentsUserPicture];
    [mutableDict setValue:self.userType forKey:kPostCommentsUserType];
    [mutableDict setValue:self.pageCover forKey:kPostCommentsPageCover];
    [mutableDict setValue:self.pagePinnedPost forKey:kPostCommentsPagePinnedPost];
    [mutableDict setValue:self.replies forKey:kPostCommentsReplies];
    [mutableDict setValue:self.userName forKey:kPostCommentsUserName];
    [mutableDict setValue:self.authorVerified forKey:kPostCommentsAuthorVerified];
    [mutableDict setValue:[NSNumber numberWithBool:self.deleteComment] forKey:kPostCommentsDeleteComment];
    [mutableDict setValue:self.text forKey:kPostCommentsText];
    [mutableDict setValue:self.nodeId forKey:kPostCommentsNodeId];
    [mutableDict setValue:self.userVerified forKey:kPostCommentsUserVerified];
    [mutableDict setValue:self.authorName forKey:kPostCommentsAuthorName];
    [mutableDict setValue:self.userFirstname forKey:kPostCommentsUserFirstname];
    [mutableDict setValue:self.authorPicture forKey:kPostCommentsAuthorPicture];
    [mutableDict setValue:self.userLastname forKey:kPostCommentsUserLastname];
    [mutableDict setValue:self.pageCoverId forKey:kPostCommentsPageCoverId];
    [mutableDict setValue:[NSNumber numberWithBool:self.iLike] forKey:kPostCommentsILike];

    [mutableDict setValue:[NSNumber numberWithBool:self.editComment] forKey:kPostCommentsEditComment];
    [mutableDict setValue:self.pageDescription forKey:kPostCommentsPageDescription];
    [mutableDict setValue:self.authorId forKey:kPostCommentsAuthorId];
    [mutableDict setValue:self.image forKey:kPostCommentsImage];
    [mutableDict setValue:self.video forKey:kPostCommentsVideo];
    [mutableDict setValue:self.likes forKey:kPostCommentsLikes];
    [mutableDict setValue:self.pageVerified forKey:kPostCommentsPageVerified];
    [mutableDict setValue:self.pageAlbumPictures forKey:kPostCommentsPageAlbumPictures];
    [mutableDict setValue:self.pageAlbumTimeline forKey:kPostCommentsPageAlbumTimeline];
    [mutableDict setValue:self.pageName forKey:kPostCommentsPageName];
    NSMutableArray *tempArrayForCommentReplies = [NSMutableArray array];
    for (NSObject *subArrayObject in self.commentReplies) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForCommentReplies addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForCommentReplies addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForCommentReplies] forKey:kPostCommentsCommentReplies];
    [mutableDict setValue:self.pagePictureId forKey:kPostCommentsPagePictureId];
    [mutableDict setValue:self.pageTitle forKey:kPostCommentsPageTitle];
    [mutableDict setValue:self.pageLikes forKey:kPostCommentsPageLikes];
    [mutableDict setValue:self.userGender forKey:kPostCommentsUserGender];
    [mutableDict setValue:self.nodeType forKey:kPostCommentsNodeType];
    [mutableDict setValue:self.userId forKey:kPostCommentsUserId];
    [mutableDict setValue:self.pagePicture forKey:kPostCommentsPagePicture];
    [mutableDict setValue:self.pageAlbumCovers forKey:kPostCommentsPageAlbumCovers];
    [mutableDict setValue:self.textPlain forKey:kPostCommentsTextPlain];
    [mutableDict setValue:self.commentId forKey:kPostCommentsCommentId];
    [mutableDict setValue:self.pageId forKey:kPostCommentsPageId];
    [mutableDict setValue:self.time forKey:kPostCommentsTime];
    [mutableDict setValue:self.formattedTime forKey:kPostCommentsFormattedTime];
    [mutableDict setValue:self.pageBoosted forKey:kPostCommentsPageBoosted];
    [mutableDict setValue:self.pageCategory forKey:kPostCommentsPageCategory];
    [mutableDict setValue:self.authorUrl forKey:kPostCommentsAuthorUrl];
    [mutableDict setValue:self.pageDate forKey:kPostCommentsPageDate];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.pageAdmin = [aDecoder decodeObjectForKey:kPostCommentsPageAdmin];
    self.userPicture = [aDecoder decodeObjectForKey:kPostCommentsUserPicture];
    self.userType = [aDecoder decodeObjectForKey:kPostCommentsUserType];
    self.pageCover = [aDecoder decodeObjectForKey:kPostCommentsPageCover];
    self.pagePinnedPost = [aDecoder decodeObjectForKey:kPostCommentsPagePinnedPost];
    self.replies = [aDecoder decodeObjectForKey:kPostCommentsReplies];
    self.userName = [aDecoder decodeObjectForKey:kPostCommentsUserName];
    self.authorVerified = [aDecoder decodeObjectForKey:kPostCommentsAuthorVerified];
    self.deleteComment = [aDecoder decodeBoolForKey:kPostCommentsDeleteComment];
    self.text = [aDecoder decodeObjectForKey:kPostCommentsText];
    self.nodeId = [aDecoder decodeObjectForKey:kPostCommentsNodeId];
    self.userVerified = [aDecoder decodeObjectForKey:kPostCommentsUserVerified];
    self.authorName = [aDecoder decodeObjectForKey:kPostCommentsAuthorName];
    self.userFirstname = [aDecoder decodeObjectForKey:kPostCommentsUserFirstname];
    self.authorPicture = [aDecoder decodeObjectForKey:kPostCommentsAuthorPicture];
    self.userLastname = [aDecoder decodeObjectForKey:kPostCommentsUserLastname];
    self.pageCoverId = [aDecoder decodeObjectForKey:kPostCommentsPageCoverId];
    self.editComment = [aDecoder decodeBoolForKey:kPostCommentsEditComment];
    self.pageDescription = [aDecoder decodeObjectForKey:kPostCommentsPageDescription];
    self.authorId = [aDecoder decodeObjectForKey:kPostCommentsAuthorId];
    self.image = [aDecoder decodeObjectForKey:kPostCommentsImage];
    self.video = [aDecoder decodeObjectForKey:kPostCommentsVideo];
    self.likes = [aDecoder decodeObjectForKey:kPostCommentsLikes];
    self.pageVerified = [aDecoder decodeObjectForKey:kPostCommentsPageVerified];
    self.pageAlbumPictures = [aDecoder decodeObjectForKey:kPostCommentsPageAlbumPictures];
    self.pageAlbumTimeline = [aDecoder decodeObjectForKey:kPostCommentsPageAlbumTimeline];
    self.pageName = [aDecoder decodeObjectForKey:kPostCommentsPageName];
    self.commentReplies = [aDecoder decodeObjectForKey:kPostCommentsCommentReplies];
    self.pagePictureId = [aDecoder decodeObjectForKey:kPostCommentsPagePictureId];
    self.pageTitle = [aDecoder decodeObjectForKey:kPostCommentsPageTitle];
    self.pageLikes = [aDecoder decodeObjectForKey:kPostCommentsPageLikes];
    self.userGender = [aDecoder decodeObjectForKey:kPostCommentsUserGender];
    self.nodeType = [aDecoder decodeObjectForKey:kPostCommentsNodeType];
    self.userId = [aDecoder decodeObjectForKey:kPostCommentsUserId];
    self.pagePicture = [aDecoder decodeObjectForKey:kPostCommentsPagePicture];
    self.pageAlbumCovers = [aDecoder decodeObjectForKey:kPostCommentsPageAlbumCovers];
    self.textPlain = [aDecoder decodeObjectForKey:kPostCommentsTextPlain];
    self.commentId = [aDecoder decodeObjectForKey:kPostCommentsCommentId];
    self.pageId = [aDecoder decodeObjectForKey:kPostCommentsPageId];
    self.time = [aDecoder decodeObjectForKey:kPostCommentsTime];
    self.pageBoosted = [aDecoder decodeObjectForKey:kPostCommentsPageBoosted];
    self.pageCategory = [aDecoder decodeObjectForKey:kPostCommentsPageCategory];
    self.authorUrl = [aDecoder decodeObjectForKey:kPostCommentsAuthorUrl];
    self.pageDate = [aDecoder decodeObjectForKey:kPostCommentsPageDate];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_pageAdmin forKey:kPostCommentsPageAdmin];
    [aCoder encodeObject:_userPicture forKey:kPostCommentsUserPicture];
    [aCoder encodeObject:_userType forKey:kPostCommentsUserType];
    [aCoder encodeObject:_pageCover forKey:kPostCommentsPageCover];
    [aCoder encodeObject:_pagePinnedPost forKey:kPostCommentsPagePinnedPost];
    [aCoder encodeObject:_replies forKey:kPostCommentsReplies];
    [aCoder encodeObject:_userName forKey:kPostCommentsUserName];
    [aCoder encodeObject:_authorVerified forKey:kPostCommentsAuthorVerified];
    [aCoder encodeBool:_deleteComment forKey:kPostCommentsDeleteComment];
    [aCoder encodeObject:_text forKey:kPostCommentsText];
    [aCoder encodeObject:_nodeId forKey:kPostCommentsNodeId];
    [aCoder encodeObject:_userVerified forKey:kPostCommentsUserVerified];
    [aCoder encodeObject:_authorName forKey:kPostCommentsAuthorName];
    [aCoder encodeObject:_userFirstname forKey:kPostCommentsUserFirstname];
    [aCoder encodeObject:_authorPicture forKey:kPostCommentsAuthorPicture];
    [aCoder encodeObject:_userLastname forKey:kPostCommentsUserLastname];
    [aCoder encodeObject:_pageCoverId forKey:kPostCommentsPageCoverId];
    [aCoder encodeBool:_editComment forKey:kPostCommentsEditComment];
    [aCoder encodeObject:_pageDescription forKey:kPostCommentsPageDescription];
    [aCoder encodeObject:_authorId forKey:kPostCommentsAuthorId];
    [aCoder encodeObject:_image forKey:kPostCommentsImage];
    [aCoder encodeObject:_image forKey:kPostCommentsVideo];
    [aCoder encodeObject:_likes forKey:kPostCommentsLikes];
    [aCoder encodeObject:_pageVerified forKey:kPostCommentsPageVerified];
    [aCoder encodeObject:_pageAlbumPictures forKey:kPostCommentsPageAlbumPictures];
    [aCoder encodeObject:_pageAlbumTimeline forKey:kPostCommentsPageAlbumTimeline];
    [aCoder encodeObject:_pageName forKey:kPostCommentsPageName];
    [aCoder encodeObject:_commentReplies forKey:kPostCommentsCommentReplies];
    [aCoder encodeObject:_pagePictureId forKey:kPostCommentsPagePictureId];
    [aCoder encodeObject:_pageTitle forKey:kPostCommentsPageTitle];
    [aCoder encodeObject:_pageLikes forKey:kPostCommentsPageLikes];
    [aCoder encodeObject:_userGender forKey:kPostCommentsUserGender];
    [aCoder encodeObject:_nodeType forKey:kPostCommentsNodeType];
    [aCoder encodeObject:_userId forKey:kPostCommentsUserId];
    [aCoder encodeObject:_pagePicture forKey:kPostCommentsPagePicture];
    [aCoder encodeObject:_pageAlbumCovers forKey:kPostCommentsPageAlbumCovers];
    [aCoder encodeObject:_textPlain forKey:kPostCommentsTextPlain];
    [aCoder encodeObject:_commentId forKey:kPostCommentsCommentId];
    [aCoder encodeObject:_pageId forKey:kPostCommentsPageId];
    [aCoder encodeObject:_time forKey:kPostCommentsTime];
    [aCoder encodeObject:_pageBoosted forKey:kPostCommentsPageBoosted];
    [aCoder encodeObject:_pageCategory forKey:kPostCommentsPageCategory];
    [aCoder encodeObject:_authorUrl forKey:kPostCommentsAuthorUrl];
    [aCoder encodeObject:_pageDate forKey:kPostCommentsPageDate];
}

- (id)copyWithZone:(NSZone *)zone
{
    PostComments *copy = [[PostComments alloc] init];
    
    if (copy) {

        copy.pageAdmin = [self.pageAdmin copyWithZone:zone];
        copy.userPicture = [self.userPicture copyWithZone:zone];
        copy.userType = [self.userType copyWithZone:zone];
        copy.pageCover = [self.pageCover copyWithZone:zone];
        copy.pagePinnedPost = [self.pagePinnedPost copyWithZone:zone];
        copy.replies = [self.replies copyWithZone:zone];
        copy.userName = [self.userName copyWithZone:zone];
        copy.authorVerified = [self.authorVerified copyWithZone:zone];
        copy.deleteComment = self.deleteComment;
        copy.text = [self.text copyWithZone:zone];
        copy.nodeId = [self.nodeId copyWithZone:zone];
        copy.userVerified = [self.userVerified copyWithZone:zone];
        copy.authorName = [self.authorName copyWithZone:zone];
        copy.userFirstname = [self.userFirstname copyWithZone:zone];
        copy.authorPicture = [self.authorPicture copyWithZone:zone];
        copy.userLastname = [self.userLastname copyWithZone:zone];
        copy.pageCoverId = [self.pageCoverId copyWithZone:zone];
        copy.editComment = self.editComment;
        copy.pageDescription = [self.pageDescription copyWithZone:zone];
        copy.authorId = [self.authorId copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
        copy.video = [self.video copyWithZone:zone];
        copy.likes = [self.likes copyWithZone:zone];
        copy.pageVerified = [self.pageVerified copyWithZone:zone];
        copy.pageAlbumPictures = [self.pageAlbumPictures copyWithZone:zone];
        copy.pageAlbumTimeline = [self.pageAlbumTimeline copyWithZone:zone];
        copy.pageName = [self.pageName copyWithZone:zone];
        copy.commentReplies = [self.commentReplies copyWithZone:zone];
        copy.pagePictureId = [self.pagePictureId copyWithZone:zone];
        copy.pageTitle = [self.pageTitle copyWithZone:zone];
        copy.pageLikes = [self.pageLikes copyWithZone:zone];
        copy.userGender = [self.userGender copyWithZone:zone];
        copy.nodeType = [self.nodeType copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.pagePicture = [self.pagePicture copyWithZone:zone];
        copy.pageAlbumCovers = [self.pageAlbumCovers copyWithZone:zone];
        copy.textPlain = [self.textPlain copyWithZone:zone];
        copy.commentId = [self.commentId copyWithZone:zone];
        copy.pageId = [self.pageId copyWithZone:zone];
        copy.time = [self.time copyWithZone:zone];
        copy.pageBoosted = [self.pageBoosted copyWithZone:zone];
        copy.pageCategory = [self.pageCategory copyWithZone:zone];
        copy.authorUrl = [self.authorUrl copyWithZone:zone];
        copy.pageDate = [self.pageDate copyWithZone:zone];
    }
    
    return copy;
}


@end
