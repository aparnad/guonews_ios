//
//  PostDetailsModel.h
//
//  Created by Pawan Ramteke on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SharePostModel.h"

@class Video;

@interface PostDetailsModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *userPicture;
@property (nonatomic, strong) NSArray *postComments;
@property (nonatomic, strong) NSString *postAuthorName;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, assign) BOOL pinned;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, assign) id channelUrl;
@property (nonatomic, strong) NSString *comments;
@property (nonatomic, assign) id location;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, assign) id userPinnedPost;
@property (nonatomic, assign) BOOL isEventAdmin;
@property (nonatomic, assign) BOOL iLike;
@property (nonatomic, strong) NSString *userVerified;
@property (nonatomic, strong) NSString *postAuthorPicture;
@property (nonatomic, strong) NSString *userCoverId;
@property (nonatomic, strong) NSString *userFirstname;
@property (nonatomic, assign) BOOL isGroupAdmin;
@property (nonatomic, strong) NSString *privacy;
@property (nonatomic, assign) id broadcastUrl;
@property (nonatomic, strong) NSString *userLastname;
@property (nonatomic, strong) NSString *shares;
@property (nonatomic, strong) NSString *postUserName;
@property (nonatomic, strong) NSString *authorId;
@property (nonatomic, assign) id eventId;
@property (nonatomic, strong) NSString *userSubscribed;
@property (nonatomic, assign) id broadcastName;
@property (nonatomic, assign) id originId;
@property (nonatomic, strong) NSString *likes;
@property (nonatomic, assign) id feelingAction;
@property (nonatomic, strong) NSString *views;
@property (nonatomic, strong) NSString *postType;
@property (nonatomic, strong) NSString *postAuthorUrl;
@property (nonatomic, strong) NSString *inWall;
@property (nonatomic, strong) NSString *inEvent;
@property (nonatomic, strong) NSString *userPictureId;
@property (nonatomic, assign) BOOL iSave;
@property (nonatomic, strong) NSString *inGroup;
@property (nonatomic, assign) id feelingValue;
@property (nonatomic, strong) Video *video;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, assign) BOOL isPageAdmin;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, assign) BOOL managePost;
@property (nonatomic, strong) NSString *textPlain;
@property (nonatomic, assign) id groupId;
@property (nonatomic, assign) id wallId;
@property (nonatomic, strong) NSString *boosted;
@property (nonatomic, strong) NSString *postAuthorVerified;
@property (nonatomic, strong) NSString *postId;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *formattedTime;
@property (nonatomic, strong) NSString *isBroadcast;
@property (nonatomic,strong) SharePostModel *origin;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSArray *videos;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
