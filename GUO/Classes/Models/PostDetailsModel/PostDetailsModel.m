//
//  PostDetailsModel.m
//
//  Created by Pawan Ramteke on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "PostDetailsModel.h"
#import "PostComments.h"
#import "Video.h"
#import "Photos.h"

NSString *const kPostDetailsModelUserPicture = @"user_picture";
NSString *const kPostDetailsModelPostComments = @"post_comments";
NSString *const kPostDetailsModelPostAuthorName = @"post_author_name";
NSString *const kPostDetailsModelUserType = @"user_type";
NSString *const kPostDetailsModelPinned = @"pinned";
NSString *const kPostDetailsModelUserName = @"user_name";
NSString *const kPostDetailsModelChannelUrl = @"channel_url";
NSString *const kPostDetailsModelComments = @"comments";
NSString *const kPostDetailsModelLocation = @"location";
NSString *const kPostDetailsModelText = @"text";
NSString *const kPostDetailsModelUserPinnedPost = @"user_pinned_post";
NSString *const kPostDetailsModelIsEventAdmin = @"is_event_admin";
NSString *const kPostDetailsModelILike = @"i_like";
NSString *const kPostDetailsModelUserVerified = @"user_verified";
NSString *const kPostDetailsModelPostAuthorPicture = @"post_author_picture";
NSString *const kPostDetailsModelUserCoverId = @"user_cover_id";
NSString *const kPostDetailsModelUserFirstname = @"user_firstname";
NSString *const kPostDetailsModelIsGroupAdmin = @"is_group_admin";
NSString *const kPostDetailsModelPrivacy = @"privacy";
NSString *const kPostDetailsModelBroadcastUrl = @"broadcast_url";
NSString *const kPostDetailsModelUserLastname = @"user_lastname";
NSString *const kPostDetailsModelShares = @"shares";
NSString *const kPostDetailsModelPostUserName = @"post_user_name";
NSString *const kPostDetailsModelAuthorId = @"author_id";
NSString *const kPostDetailsModelEventId = @"event_id";
NSString *const kPostDetailsModelUserSubscribed = @"user_subscribed";
NSString *const kPostDetailsModelBroadcastName = @"broadcast_name";
NSString *const kPostDetailsModelOriginId = @"origin_id";
NSString *const kPostDetailsModelLikes = @"likes";
NSString *const kPostDetailsModelFeelingAction = @"feeling_action";
NSString *const kPostDetailsModelViews = @"views";
NSString *const kPostDetailsModelPostType = @"post_type";
NSString *const kPostDetailsModelPostAuthorUrl = @"post_author_url";
NSString *const kPostDetailsModelInWall = @"in_wall";
NSString *const kPostDetailsModelInEvent = @"in_event";
NSString *const kPostDetailsModelUserPictureId = @"user_picture_id";
NSString *const kPostDetailsModelISave = @"i_save";
NSString *const kPostDetailsModelInGroup = @"in_group";
NSString *const kPostDetailsModelFeelingValue = @"feeling_value";
NSString *const kPostDetailsModelVideo = @"video";
NSString *const kPostDetailsModelUserGender = @"user_gender";
NSString *const kPostDetailsModelIsPageAdmin = @"is_page_admin";
NSString *const kPostDetailsModelUserId = @"user_id";
NSString *const kPostDetailsModelManagePost = @"manage_post";
NSString *const kPostDetailsModelTextPlain = @"text_plain";
NSString *const kPostDetailsModelGroupId = @"group_id";
NSString *const kPostDetailsModelWallId = @"wall_id";
NSString *const kPostDetailsModelBoosted = @"boosted";
NSString *const kPostDetailsModelPostAuthorVerified = @"post_author_verified";
NSString *const kPostDetailsModelPostId = @"post_id";
NSString *const kPostDetailsModelTime = @"time";
NSString *const kPostDetailsModelFormattedTime = @"formatted_time";

NSString *const kPostDetailsModelIsBroadcast = @"is_broadcast";
NSString *const kPostDetailsModelOrigin = @"origin";
NSString *const kPostDetailsModelPhotos = @"photos";

@interface PostDetailsModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PostDetailsModel

@synthesize userPicture = _userPicture;
@synthesize postComments = _postComments;
@synthesize postAuthorName = _postAuthorName;
@synthesize userType = _userType;
@synthesize pinned = _pinned;
@synthesize userName = _userName;
@synthesize channelUrl = _channelUrl;
@synthesize comments = _comments;
@synthesize location = _location;
@synthesize text = _text;
@synthesize userPinnedPost = _userPinnedPost;
@synthesize isEventAdmin = _isEventAdmin;
@synthesize iLike = _iLike;
@synthesize userVerified = _userVerified;
@synthesize postAuthorPicture = _postAuthorPicture;
@synthesize userCoverId = _userCoverId;
@synthesize userFirstname = _userFirstname;
@synthesize isGroupAdmin = _isGroupAdmin;
@synthesize privacy = _privacy;
@synthesize broadcastUrl = _broadcastUrl;
@synthesize userLastname = _userLastname;
@synthesize shares = _shares;
@synthesize postUserName = _postUserName;
@synthesize authorId = _authorId;
@synthesize eventId = _eventId;
@synthesize userSubscribed = _userSubscribed;
@synthesize broadcastName = _broadcastName;
@synthesize originId = _originId;
@synthesize likes = _likes;
@synthesize feelingAction = _feelingAction;
@synthesize views = _views;
@synthesize postType = _postType;
@synthesize postAuthorUrl = _postAuthorUrl;
@synthesize inWall = _inWall;
@synthesize inEvent = _inEvent;
@synthesize userPictureId = _userPictureId;
@synthesize iSave = _iSave;
@synthesize inGroup = _inGroup;
@synthesize feelingValue = _feelingValue;
@synthesize video = _video;
@synthesize userGender = _userGender;
@synthesize isPageAdmin = _isPageAdmin;
@synthesize userId = _userId;
@synthesize managePost = _managePost;
@synthesize textPlain = _textPlain;
@synthesize groupId = _groupId;
@synthesize wallId = _wallId;
@synthesize boosted = _boosted;
@synthesize postAuthorVerified = _postAuthorVerified;
@synthesize postId = _postId;
@synthesize time = _time;
@synthesize formattedTime = _formattedTime;
@synthesize isBroadcast = _isBroadcast;
@synthesize origin = _origin;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userPicture = [self objectOrNilForKey:kPostDetailsModelUserPicture fromDictionary:dict];
    NSObject *receivedPostComments = [dict objectForKey:kPostDetailsModelPostComments];
    NSMutableArray *parsedPostComments = [NSMutableArray array];
    if ([receivedPostComments isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedPostComments) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedPostComments addObject:[PostComments modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedPostComments isKindOfClass:[NSDictionary class]]) {
       [parsedPostComments addObject:[PostComments modelObjectWithDictionary:(NSDictionary *)receivedPostComments]];
    }

    self.postComments = [NSArray arrayWithArray:parsedPostComments];
            self.postAuthorName = [self objectOrNilForKey:kPostDetailsModelPostAuthorName fromDictionary:dict];
            self.userType = [self objectOrNilForKey:kPostDetailsModelUserType fromDictionary:dict];
            self.pinned = [[self objectOrNilForKey:kPostDetailsModelPinned fromDictionary:dict] boolValue];
            self.userName = [self objectOrNilForKey:kPostDetailsModelUserName fromDictionary:dict];
            self.channelUrl = [self objectOrNilForKey:kPostDetailsModelChannelUrl fromDictionary:dict];
            self.comments = [self objectOrNilForKey:kPostDetailsModelComments fromDictionary:dict];
            self.location = [self objectOrNilForKey:kPostDetailsModelLocation fromDictionary:dict];
            self.text = [self objectOrNilForKey:kPostDetailsModelText fromDictionary:dict];
            self.userPinnedPost = [self objectOrNilForKey:kPostDetailsModelUserPinnedPost fromDictionary:dict];
            self.isEventAdmin = [[self objectOrNilForKey:kPostDetailsModelIsEventAdmin fromDictionary:dict] boolValue];
            self.iLike = [[self objectOrNilForKey:kPostDetailsModelILike fromDictionary:dict] boolValue];
            self.userVerified = [self objectOrNilForKey:kPostDetailsModelUserVerified fromDictionary:dict];
            self.postAuthorPicture = [self objectOrNilForKey:kPostDetailsModelPostAuthorPicture fromDictionary:dict];
            self.userCoverId = [self objectOrNilForKey:kPostDetailsModelUserCoverId fromDictionary:dict];
            self.userFirstname = [self objectOrNilForKey:kPostDetailsModelUserFirstname fromDictionary:dict];
            self.isGroupAdmin = [[self objectOrNilForKey:kPostDetailsModelIsGroupAdmin fromDictionary:dict] boolValue];
            self.privacy = [self objectOrNilForKey:kPostDetailsModelPrivacy fromDictionary:dict];
            self.broadcastUrl = [self objectOrNilForKey:kPostDetailsModelBroadcastUrl fromDictionary:dict];
            self.userLastname = [self objectOrNilForKey:kPostDetailsModelUserLastname fromDictionary:dict];
            self.shares = [self objectOrNilForKey:kPostDetailsModelShares fromDictionary:dict];
            self.postUserName = [self objectOrNilForKey:kPostDetailsModelPostUserName fromDictionary:dict];
            self.authorId = [self objectOrNilForKey:kPostDetailsModelAuthorId fromDictionary:dict];
            self.eventId = [self objectOrNilForKey:kPostDetailsModelEventId fromDictionary:dict];
            self.userSubscribed = [self objectOrNilForKey:kPostDetailsModelUserSubscribed fromDictionary:dict];
            self.broadcastName = [self objectOrNilForKey:kPostDetailsModelBroadcastName fromDictionary:dict];
            self.originId = [self objectOrNilForKey:kPostDetailsModelOriginId fromDictionary:dict];
            self.likes = [self objectOrNilForKey:kPostDetailsModelLikes fromDictionary:dict];
            self.feelingAction = [self objectOrNilForKey:kPostDetailsModelFeelingAction fromDictionary:dict];
            self.views = [self objectOrNilForKey:kPostDetailsModelViews fromDictionary:dict];
            self.postType = [self objectOrNilForKey:kPostDetailsModelPostType fromDictionary:dict];
            self.postAuthorUrl = [self objectOrNilForKey:kPostDetailsModelPostAuthorUrl fromDictionary:dict];
            self.inWall = [self objectOrNilForKey:kPostDetailsModelInWall fromDictionary:dict];
            self.inEvent = [self objectOrNilForKey:kPostDetailsModelInEvent fromDictionary:dict];
            self.userPictureId = [self objectOrNilForKey:kPostDetailsModelUserPictureId fromDictionary:dict];
            self.iSave = [[self objectOrNilForKey:kPostDetailsModelISave fromDictionary:dict] boolValue];
            self.inGroup = [self objectOrNilForKey:kPostDetailsModelInGroup fromDictionary:dict];
            self.feelingValue = [self objectOrNilForKey:kPostDetailsModelFeelingValue fromDictionary:dict];
            self.video = [Video modelObjectWithDictionary:[dict objectForKey:kPostDetailsModelVideo]];
            self.userGender = [self objectOrNilForKey:kPostDetailsModelUserGender fromDictionary:dict];
            self.isPageAdmin = [[self objectOrNilForKey:kPostDetailsModelIsPageAdmin fromDictionary:dict] boolValue];
            self.userId = [self objectOrNilForKey:kPostDetailsModelUserId fromDictionary:dict];
            self.managePost = [[self objectOrNilForKey:kPostDetailsModelManagePost fromDictionary:dict] boolValue];
            self.textPlain = [self objectOrNilForKey:kPostDetailsModelTextPlain fromDictionary:dict];
            self.groupId = [self objectOrNilForKey:kPostDetailsModelGroupId fromDictionary:dict];
            self.wallId = [self objectOrNilForKey:kPostDetailsModelWallId fromDictionary:dict];
            self.boosted = [self objectOrNilForKey:kPostDetailsModelBoosted fromDictionary:dict];
            self.postAuthorVerified = [self objectOrNilForKey:kPostDetailsModelPostAuthorVerified fromDictionary:dict];
            self.postId = [self objectOrNilForKey:kPostDetailsModelPostId fromDictionary:dict];
            self.time = [self objectOrNilForKey:kPostDetailsModelTime fromDictionary:dict];
        self.formattedTime = [self objectOrNilForKey:kPostDetailsModelFormattedTime fromDictionary:dict];

            self.isBroadcast = [self objectOrNilForKey:kPostDetailsModelIsBroadcast fromDictionary:dict];
        
        if (dict[kPostDetailsModelOrigin] != nil) {
            self.origin = [SharePostModel modelObjectWithDictionary:dict[kPostDetailsModelOrigin]];
        }
        
        NSObject *receivedPhotos = [dict objectForKey:kPostDetailsModelPhotos];
        NSMutableArray *parsedPhotos = [NSMutableArray array];
        if ([receivedPhotos isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedPhotos) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedPhotos addObject:[Photos modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedPhotos isKindOfClass:[NSDictionary class]]) {
            [parsedPhotos addObject:[Photos modelObjectWithDictionary:(NSDictionary *)receivedPhotos]];
        }
        
        self.photos = [NSArray arrayWithArray:parsedPhotos];
        
        NSObject *receivedVideo = [dict objectForKey:kPostDetailsModelVideo];
        NSMutableArray *parsedVideo = [NSMutableArray array];
        if ([receivedVideo isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedVideo) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedVideo addObject:[Photos modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedVideo isKindOfClass:[NSDictionary class]]) {
            [parsedVideo addObject:[Photos modelObjectWithDictionary:(NSDictionary *)receivedVideo]];
        }
        
        self.videos = [NSArray arrayWithArray:parsedVideo];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.userPicture forKey:kPostDetailsModelUserPicture];
    NSMutableArray *tempArrayForPostComments = [NSMutableArray array];
    for (NSObject *subArrayObject in self.postComments) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPostComments addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPostComments addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPostComments] forKey:kPostDetailsModelPostComments];
    [mutableDict setValue:self.postAuthorName forKey:kPostDetailsModelPostAuthorName];
    [mutableDict setValue:self.userType forKey:kPostDetailsModelUserType];
    [mutableDict setValue:[NSNumber numberWithBool:self.pinned] forKey:kPostDetailsModelPinned];
    [mutableDict setValue:self.userName forKey:kPostDetailsModelUserName];
    [mutableDict setValue:self.channelUrl forKey:kPostDetailsModelChannelUrl];
    [mutableDict setValue:self.comments forKey:kPostDetailsModelComments];
    [mutableDict setValue:self.location forKey:kPostDetailsModelLocation];
    [mutableDict setValue:self.text forKey:kPostDetailsModelText];
    [mutableDict setValue:self.userPinnedPost forKey:kPostDetailsModelUserPinnedPost];
    [mutableDict setValue:[NSNumber numberWithBool:self.isEventAdmin] forKey:kPostDetailsModelIsEventAdmin];
    [mutableDict setValue:[NSNumber numberWithBool:self.iLike] forKey:kPostDetailsModelILike];
    [mutableDict setValue:self.userVerified forKey:kPostDetailsModelUserVerified];
    [mutableDict setValue:self.postAuthorPicture forKey:kPostDetailsModelPostAuthorPicture];
    [mutableDict setValue:self.userCoverId forKey:kPostDetailsModelUserCoverId];
    [mutableDict setValue:self.userFirstname forKey:kPostDetailsModelUserFirstname];
    [mutableDict setValue:[NSNumber numberWithBool:self.isGroupAdmin] forKey:kPostDetailsModelIsGroupAdmin];
    [mutableDict setValue:self.privacy forKey:kPostDetailsModelPrivacy];
    [mutableDict setValue:self.broadcastUrl forKey:kPostDetailsModelBroadcastUrl];
    [mutableDict setValue:self.userLastname forKey:kPostDetailsModelUserLastname];
    [mutableDict setValue:self.shares forKey:kPostDetailsModelShares];
    [mutableDict setValue:self.postUserName forKey:kPostDetailsModelPostUserName];
    [mutableDict setValue:self.authorId forKey:kPostDetailsModelAuthorId];
    [mutableDict setValue:self.eventId forKey:kPostDetailsModelEventId];
    [mutableDict setValue:self.userSubscribed forKey:kPostDetailsModelUserSubscribed];
    [mutableDict setValue:self.broadcastName forKey:kPostDetailsModelBroadcastName];
    [mutableDict setValue:self.originId forKey:kPostDetailsModelOriginId];
    [mutableDict setValue:self.likes forKey:kPostDetailsModelLikes];
    [mutableDict setValue:self.feelingAction forKey:kPostDetailsModelFeelingAction];
    [mutableDict setValue:self.views forKey:kPostDetailsModelViews];
    [mutableDict setValue:self.postType forKey:kPostDetailsModelPostType];
    [mutableDict setValue:self.postAuthorUrl forKey:kPostDetailsModelPostAuthorUrl];
    [mutableDict setValue:self.inWall forKey:kPostDetailsModelInWall];
    [mutableDict setValue:self.inEvent forKey:kPostDetailsModelInEvent];
    [mutableDict setValue:self.userPictureId forKey:kPostDetailsModelUserPictureId];
    [mutableDict setValue:[NSNumber numberWithBool:self.iSave] forKey:kPostDetailsModelISave];
    [mutableDict setValue:self.inGroup forKey:kPostDetailsModelInGroup];
    [mutableDict setValue:self.feelingValue forKey:kPostDetailsModelFeelingValue];
    [mutableDict setValue:[self.video dictionaryRepresentation] forKey:kPostDetailsModelVideo];
    [mutableDict setValue:self.userGender forKey:kPostDetailsModelUserGender];
    [mutableDict setValue:[NSNumber numberWithBool:self.isPageAdmin] forKey:kPostDetailsModelIsPageAdmin];
    [mutableDict setValue:self.userId forKey:kPostDetailsModelUserId];
    [mutableDict setValue:[NSNumber numberWithBool:self.managePost] forKey:kPostDetailsModelManagePost];
    [mutableDict setValue:self.textPlain forKey:kPostDetailsModelTextPlain];
    [mutableDict setValue:self.groupId forKey:kPostDetailsModelGroupId];
    [mutableDict setValue:self.wallId forKey:kPostDetailsModelWallId];
    [mutableDict setValue:self.boosted forKey:kPostDetailsModelBoosted];
    [mutableDict setValue:self.postAuthorVerified forKey:kPostDetailsModelPostAuthorVerified];
    [mutableDict setValue:self.postId forKey:kPostDetailsModelPostId];
    [mutableDict setValue:self.time forKey:kPostDetailsModelTime];
    [mutableDict setValue:self.isBroadcast forKey:kPostDetailsModelIsBroadcast];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userPicture = [aDecoder decodeObjectForKey:kPostDetailsModelUserPicture];
    self.postComments = [aDecoder decodeObjectForKey:kPostDetailsModelPostComments];
    self.postAuthorName = [aDecoder decodeObjectForKey:kPostDetailsModelPostAuthorName];
    self.userType = [aDecoder decodeObjectForKey:kPostDetailsModelUserType];
    self.pinned = [aDecoder decodeBoolForKey:kPostDetailsModelPinned];
    self.userName = [aDecoder decodeObjectForKey:kPostDetailsModelUserName];
    self.channelUrl = [aDecoder decodeObjectForKey:kPostDetailsModelChannelUrl];
    self.comments = [aDecoder decodeObjectForKey:kPostDetailsModelComments];
    self.location = [aDecoder decodeObjectForKey:kPostDetailsModelLocation];
    self.text = [aDecoder decodeObjectForKey:kPostDetailsModelText];
    self.userPinnedPost = [aDecoder decodeObjectForKey:kPostDetailsModelUserPinnedPost];
    self.isEventAdmin = [aDecoder decodeBoolForKey:kPostDetailsModelIsEventAdmin];
    self.iLike = [aDecoder decodeBoolForKey:kPostDetailsModelILike];
    self.userVerified = [aDecoder decodeObjectForKey:kPostDetailsModelUserVerified];
    self.postAuthorPicture = [aDecoder decodeObjectForKey:kPostDetailsModelPostAuthorPicture];
    self.userCoverId = [aDecoder decodeObjectForKey:kPostDetailsModelUserCoverId];
    self.userFirstname = [aDecoder decodeObjectForKey:kPostDetailsModelUserFirstname];
    self.isGroupAdmin = [aDecoder decodeBoolForKey:kPostDetailsModelIsGroupAdmin];
    self.privacy = [aDecoder decodeObjectForKey:kPostDetailsModelPrivacy];
    self.broadcastUrl = [aDecoder decodeObjectForKey:kPostDetailsModelBroadcastUrl];
    self.userLastname = [aDecoder decodeObjectForKey:kPostDetailsModelUserLastname];
    self.shares = [aDecoder decodeObjectForKey:kPostDetailsModelShares];
    self.postUserName = [aDecoder decodeObjectForKey:kPostDetailsModelPostUserName];
    self.authorId = [aDecoder decodeObjectForKey:kPostDetailsModelAuthorId];
    self.eventId = [aDecoder decodeObjectForKey:kPostDetailsModelEventId];
    self.userSubscribed = [aDecoder decodeObjectForKey:kPostDetailsModelUserSubscribed];
    self.broadcastName = [aDecoder decodeObjectForKey:kPostDetailsModelBroadcastName];
    self.originId = [aDecoder decodeObjectForKey:kPostDetailsModelOriginId];
    self.likes = [aDecoder decodeObjectForKey:kPostDetailsModelLikes];
    self.feelingAction = [aDecoder decodeObjectForKey:kPostDetailsModelFeelingAction];
    self.views = [aDecoder decodeObjectForKey:kPostDetailsModelViews];
    self.postType = [aDecoder decodeObjectForKey:kPostDetailsModelPostType];
    self.postAuthorUrl = [aDecoder decodeObjectForKey:kPostDetailsModelPostAuthorUrl];
    self.inWall = [aDecoder decodeObjectForKey:kPostDetailsModelInWall];
    self.inEvent = [aDecoder decodeObjectForKey:kPostDetailsModelInEvent];
    self.userPictureId = [aDecoder decodeObjectForKey:kPostDetailsModelUserPictureId];
    self.iSave = [aDecoder decodeBoolForKey:kPostDetailsModelISave];
    self.inGroup = [aDecoder decodeObjectForKey:kPostDetailsModelInGroup];
    self.feelingValue = [aDecoder decodeObjectForKey:kPostDetailsModelFeelingValue];
    self.video = [aDecoder decodeObjectForKey:kPostDetailsModelVideo];
    self.userGender = [aDecoder decodeObjectForKey:kPostDetailsModelUserGender];
    self.isPageAdmin = [aDecoder decodeBoolForKey:kPostDetailsModelIsPageAdmin];
    self.userId = [aDecoder decodeObjectForKey:kPostDetailsModelUserId];
    self.managePost = [aDecoder decodeBoolForKey:kPostDetailsModelManagePost];
    self.textPlain = [aDecoder decodeObjectForKey:kPostDetailsModelTextPlain];
    self.groupId = [aDecoder decodeObjectForKey:kPostDetailsModelGroupId];
    self.wallId = [aDecoder decodeObjectForKey:kPostDetailsModelWallId];
    self.boosted = [aDecoder decodeObjectForKey:kPostDetailsModelBoosted];
    self.postAuthorVerified = [aDecoder decodeObjectForKey:kPostDetailsModelPostAuthorVerified];
    self.postId = [aDecoder decodeObjectForKey:kPostDetailsModelPostId];
    self.time = [aDecoder decodeObjectForKey:kPostDetailsModelTime];
    self.isBroadcast = [aDecoder decodeObjectForKey:kPostDetailsModelIsBroadcast];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_userPicture forKey:kPostDetailsModelUserPicture];
    [aCoder encodeObject:_postComments forKey:kPostDetailsModelPostComments];
    [aCoder encodeObject:_postAuthorName forKey:kPostDetailsModelPostAuthorName];
    [aCoder encodeObject:_userType forKey:kPostDetailsModelUserType];
    [aCoder encodeBool:_pinned forKey:kPostDetailsModelPinned];
    [aCoder encodeObject:_userName forKey:kPostDetailsModelUserName];
    [aCoder encodeObject:_channelUrl forKey:kPostDetailsModelChannelUrl];
    [aCoder encodeObject:_comments forKey:kPostDetailsModelComments];
    [aCoder encodeObject:_location forKey:kPostDetailsModelLocation];
    [aCoder encodeObject:_text forKey:kPostDetailsModelText];
    [aCoder encodeObject:_userPinnedPost forKey:kPostDetailsModelUserPinnedPost];
    [aCoder encodeBool:_isEventAdmin forKey:kPostDetailsModelIsEventAdmin];
    [aCoder encodeBool:_iLike forKey:kPostDetailsModelILike];
    [aCoder encodeObject:_userVerified forKey:kPostDetailsModelUserVerified];
    [aCoder encodeObject:_postAuthorPicture forKey:kPostDetailsModelPostAuthorPicture];
    [aCoder encodeObject:_userCoverId forKey:kPostDetailsModelUserCoverId];
    [aCoder encodeObject:_userFirstname forKey:kPostDetailsModelUserFirstname];
    [aCoder encodeBool:_isGroupAdmin forKey:kPostDetailsModelIsGroupAdmin];
    [aCoder encodeObject:_privacy forKey:kPostDetailsModelPrivacy];
    [aCoder encodeObject:_broadcastUrl forKey:kPostDetailsModelBroadcastUrl];
    [aCoder encodeObject:_userLastname forKey:kPostDetailsModelUserLastname];
    [aCoder encodeObject:_shares forKey:kPostDetailsModelShares];
    [aCoder encodeObject:_postUserName forKey:kPostDetailsModelPostUserName];
    [aCoder encodeObject:_authorId forKey:kPostDetailsModelAuthorId];
    [aCoder encodeObject:_eventId forKey:kPostDetailsModelEventId];
    [aCoder encodeObject:_userSubscribed forKey:kPostDetailsModelUserSubscribed];
    [aCoder encodeObject:_broadcastName forKey:kPostDetailsModelBroadcastName];
    [aCoder encodeObject:_originId forKey:kPostDetailsModelOriginId];
    [aCoder encodeObject:_likes forKey:kPostDetailsModelLikes];
    [aCoder encodeObject:_feelingAction forKey:kPostDetailsModelFeelingAction];
    [aCoder encodeObject:_views forKey:kPostDetailsModelViews];
    [aCoder encodeObject:_postType forKey:kPostDetailsModelPostType];
    [aCoder encodeObject:_postAuthorUrl forKey:kPostDetailsModelPostAuthorUrl];
    [aCoder encodeObject:_inWall forKey:kPostDetailsModelInWall];
    [aCoder encodeObject:_inEvent forKey:kPostDetailsModelInEvent];
    [aCoder encodeObject:_userPictureId forKey:kPostDetailsModelUserPictureId];
    [aCoder encodeBool:_iSave forKey:kPostDetailsModelISave];
    [aCoder encodeObject:_inGroup forKey:kPostDetailsModelInGroup];
    [aCoder encodeObject:_feelingValue forKey:kPostDetailsModelFeelingValue];
    [aCoder encodeObject:_video forKey:kPostDetailsModelVideo];
    [aCoder encodeObject:_userGender forKey:kPostDetailsModelUserGender];
    [aCoder encodeBool:_isPageAdmin forKey:kPostDetailsModelIsPageAdmin];
    [aCoder encodeObject:_userId forKey:kPostDetailsModelUserId];
    [aCoder encodeBool:_managePost forKey:kPostDetailsModelManagePost];
    [aCoder encodeObject:_textPlain forKey:kPostDetailsModelTextPlain];
    [aCoder encodeObject:_groupId forKey:kPostDetailsModelGroupId];
    [aCoder encodeObject:_wallId forKey:kPostDetailsModelWallId];
    [aCoder encodeObject:_boosted forKey:kPostDetailsModelBoosted];
    [aCoder encodeObject:_postAuthorVerified forKey:kPostDetailsModelPostAuthorVerified];
    [aCoder encodeObject:_postId forKey:kPostDetailsModelPostId];
    [aCoder encodeObject:_time forKey:kPostDetailsModelTime];
    [aCoder encodeObject:_isBroadcast forKey:kPostDetailsModelIsBroadcast];
}

- (id)copyWithZone:(NSZone *)zone
{
    PostDetailsModel *copy = [[PostDetailsModel alloc] init];
    
    if (copy) {

        copy.userPicture = [self.userPicture copyWithZone:zone];
        copy.postComments = [self.postComments copyWithZone:zone];
        copy.postAuthorName = [self.postAuthorName copyWithZone:zone];
        copy.userType = [self.userType copyWithZone:zone];
        copy.pinned = self.pinned;
        copy.userName = [self.userName copyWithZone:zone];
        copy.channelUrl = [self.channelUrl copyWithZone:zone];
        copy.comments = [self.comments copyWithZone:zone];
        copy.location = [self.location copyWithZone:zone];
        copy.text = [self.text copyWithZone:zone];
        copy.userPinnedPost = [self.userPinnedPost copyWithZone:zone];
        copy.isEventAdmin = self.isEventAdmin;
        copy.iLike = self.iLike;
        copy.userVerified = [self.userVerified copyWithZone:zone];
        copy.postAuthorPicture = [self.postAuthorPicture copyWithZone:zone];
        copy.userCoverId = [self.userCoverId copyWithZone:zone];
        copy.userFirstname = [self.userFirstname copyWithZone:zone];
        copy.isGroupAdmin = self.isGroupAdmin;
        copy.privacy = [self.privacy copyWithZone:zone];
        copy.broadcastUrl = [self.broadcastUrl copyWithZone:zone];
        copy.userLastname = [self.userLastname copyWithZone:zone];
        copy.shares = [self.shares copyWithZone:zone];
        copy.postUserName = [self.postUserName copyWithZone:zone];
        copy.authorId = [self.authorId copyWithZone:zone];
        copy.eventId = [self.eventId copyWithZone:zone];
        copy.userSubscribed = [self.userSubscribed copyWithZone:zone];
        copy.broadcastName = [self.broadcastName copyWithZone:zone];
        copy.originId = [self.originId copyWithZone:zone];
        copy.likes = [self.likes copyWithZone:zone];
        copy.feelingAction = [self.feelingAction copyWithZone:zone];
        copy.views = [self.views copyWithZone:zone];
        copy.postType = [self.postType copyWithZone:zone];
        copy.postAuthorUrl = [self.postAuthorUrl copyWithZone:zone];
        copy.inWall = [self.inWall copyWithZone:zone];
        copy.inEvent = [self.inEvent copyWithZone:zone];
        copy.userPictureId = [self.userPictureId copyWithZone:zone];
        copy.iSave = self.iSave;
        copy.inGroup = [self.inGroup copyWithZone:zone];
        copy.feelingValue = [self.feelingValue copyWithZone:zone];
        copy.video = [self.video copyWithZone:zone];
        copy.userGender = [self.userGender copyWithZone:zone];
        copy.isPageAdmin = self.isPageAdmin;
        copy.userId = [self.userId copyWithZone:zone];
        copy.managePost = self.managePost;
        copy.textPlain = [self.textPlain copyWithZone:zone];
        copy.groupId = [self.groupId copyWithZone:zone];
        copy.wallId = [self.wallId copyWithZone:zone];
        copy.boosted = [self.boosted copyWithZone:zone];
        copy.postAuthorVerified = [self.postAuthorVerified copyWithZone:zone];
        copy.postId = [self.postId copyWithZone:zone];
        copy.time = [self.time copyWithZone:zone];
        copy.isBroadcast = [self.isBroadcast copyWithZone:zone];
    }
    
    return copy;
}


@end
