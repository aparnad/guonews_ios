//
//  CommentReplies.h
//
//  Created by Pawan Ramteke on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CommentReplies : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) id pageAdmin;
@property (nonatomic, strong) NSString *userPicture;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, assign) id pageCover;
@property (nonatomic, assign) id pagePinnedPost;
@property (nonatomic, strong) NSString *replies;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *authorVerified;
@property (nonatomic, assign) BOOL deleteComment;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *nodeId;
@property (nonatomic, strong) NSString *userVerified;
@property (nonatomic, strong) NSString *authorName;
@property (nonatomic, strong) NSString *authorUserName;
@property (nonatomic, strong) NSString *userFirstname;
@property (nonatomic, strong) NSString *authorPicture;
@property (nonatomic, strong) NSString *userLastname;
@property (nonatomic, assign) id pageCoverId;
@property (nonatomic, assign) BOOL editComment;
@property (nonatomic, assign) id pageDescription;
@property (nonatomic, strong) NSString *authorId;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *video;
@property (nonatomic, strong) NSString *likes;
@property (nonatomic, assign) id pageVerified;
@property (nonatomic, assign) id pageAlbumPictures;
@property (nonatomic, assign) id pageAlbumTimeline;
@property (nonatomic, assign) id pageName;
@property (nonatomic, assign) id pagePictureId;
@property (nonatomic, assign) id pageTitle;
@property (nonatomic, assign) id pageLikes;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, strong) NSString *nodeType;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, assign) id pagePicture;
@property (nonatomic, assign) id pageAlbumCovers;
@property (nonatomic, strong) NSString *textPlain;
@property (nonatomic, strong) NSString *commentId;
@property (nonatomic, assign) id pageId;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, assign) id pageBoosted;
@property (nonatomic, assign) id pageCategory;
@property (nonatomic, strong) NSString *authorUrl;
@property (nonatomic, assign) id pageDate;
@property (nonatomic, assign) BOOL iLike;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
