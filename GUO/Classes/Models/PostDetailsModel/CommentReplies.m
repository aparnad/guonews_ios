//
//  CommentReplies.m
//
//  Created by Pawan Ramteke on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "CommentReplies.h"


NSString *const kCommentRepliesPageAdmin = @"page_admin";
NSString *const kCommentRepliesUserPicture = @"user_picture";
NSString *const kCommentRepliesUserType = @"user_type";
NSString *const kCommentRepliesPageCover = @"page_cover";
NSString *const kCommentRepliesPagePinnedPost = @"page_pinned_post";
NSString *const kCommentRepliesReplies = @"replies";
NSString *const kCommentRepliesUserName = @"user_name";
NSString *const kCommentRepliesAuthorVerified = @"author_verified";
NSString *const kCommentRepliesDeleteComment = @"delete_comment";
NSString *const kCommentRepliesText = @"text";
NSString *const kCommentRepliesNodeId = @"node_id";
NSString *const kCommentRepliesUserVerified = @"user_verified";
NSString *const kCommentRepliesAuthorName = @"author_name";
NSString *const kCommentRepliesAuthorUserName = @"author_user_name";
NSString *const kCommentRepliesUserFirstname = @"user_firstname";
NSString *const kCommentRepliesAuthorPicture = @"author_picture";
NSString *const kCommentRepliesUserLastname = @"user_lastname";
NSString *const kCommentRepliesPageCoverId = @"page_cover_id";
NSString *const kCommentRepliesEditComment = @"edit_comment";
NSString *const kCommentRepliesPageDescription = @"page_description";
NSString *const kCommentRepliesAuthorId = @"author_id";
NSString *const kCommentRepliesImage = @"image";
NSString *const kCommentRepliesVideo = @"video";
NSString *const kCommentRepliesLikes = @"likes";
NSString *const kCommentRepliesPageVerified = @"page_verified";
NSString *const kCommentRepliesPageAlbumPictures = @"page_album_pictures";
NSString *const kCommentRepliesPageAlbumTimeline = @"page_album_timeline";
NSString *const kCommentRepliesPageName = @"page_name";
NSString *const kCommentRepliesPagePictureId = @"page_picture_id";
NSString *const kCommentRepliesPageTitle = @"page_title";
NSString *const kCommentRepliesPageLikes = @"page_likes";
NSString *const kCommentRepliesUserGender = @"user_gender";
NSString *const kCommentRepliesNodeType = @"node_type";
NSString *const kCommentRepliesUserId = @"user_id";
NSString *const kCommentRepliesPagePicture = @"page_picture";
NSString *const kCommentRepliesPageAlbumCovers = @"page_album_covers";
NSString *const kCommentRepliesTextPlain = @"text_plain";
NSString *const kCommentRepliesCommentId = @"comment_id";
NSString *const kCommentRepliesPageId = @"page_id";
NSString *const kCommentRepliesTime = @"time";
NSString *const kCommentRepliesPageBoosted = @"page_boosted";
NSString *const kCommentRepliesPageCategory = @"page_category";
NSString *const kCommentRepliesAuthorUrl = @"author_url";
NSString *const kCommentRepliesPageDate = @"page_date";
NSString *const kCommentRepliesILike = @"i_like";


@interface CommentReplies ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CommentReplies

@synthesize pageAdmin = _pageAdmin;
@synthesize userPicture = _userPicture;
@synthesize userType = _userType;
@synthesize pageCover = _pageCover;
@synthesize pagePinnedPost = _pagePinnedPost;
@synthesize replies = _replies;
@synthesize userName = _userName;
@synthesize authorVerified = _authorVerified;
@synthesize deleteComment = _deleteComment;
@synthesize text = _text;
@synthesize nodeId = _nodeId;
@synthesize userVerified = _userVerified;
@synthesize authorName = _authorName;
@synthesize authorUserName = _authorUserName;
@synthesize userFirstname = _userFirstname;
@synthesize authorPicture = _authorPicture;
@synthesize userLastname = _userLastname;
@synthesize pageCoverId = _pageCoverId;
@synthesize editComment = _editComment;
@synthesize pageDescription = _pageDescription;
@synthesize authorId = _authorId;
@synthesize image = _image;
@synthesize video = _video;
@synthesize likes = _likes;
@synthesize pageVerified = _pageVerified;
@synthesize pageAlbumPictures = _pageAlbumPictures;
@synthesize pageAlbumTimeline = _pageAlbumTimeline;
@synthesize pageName = _pageName;
@synthesize pagePictureId = _pagePictureId;
@synthesize pageTitle = _pageTitle;
@synthesize pageLikes = _pageLikes;
@synthesize userGender = _userGender;
@synthesize nodeType = _nodeType;
@synthesize userId = _userId;
@synthesize pagePicture = _pagePicture;
@synthesize pageAlbumCovers = _pageAlbumCovers;
@synthesize textPlain = _textPlain;
@synthesize commentId = _commentId;
@synthesize pageId = _pageId;
@synthesize time = _time;
@synthesize pageBoosted = _pageBoosted;
@synthesize pageCategory = _pageCategory;
@synthesize authorUrl = _authorUrl;
@synthesize pageDate = _pageDate;
@synthesize iLike = _iLike;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.pageAdmin = [self objectOrNilForKey:kCommentRepliesPageAdmin fromDictionary:dict];
            self.userPicture = [self objectOrNilForKey:kCommentRepliesUserPicture fromDictionary:dict];
            self.userType = [self objectOrNilForKey:kCommentRepliesUserType fromDictionary:dict];
            self.pageCover = [self objectOrNilForKey:kCommentRepliesPageCover fromDictionary:dict];
            self.pagePinnedPost = [self objectOrNilForKey:kCommentRepliesPagePinnedPost fromDictionary:dict];
            self.replies = [self objectOrNilForKey:kCommentRepliesReplies fromDictionary:dict];
            self.userName = [self objectOrNilForKey:kCommentRepliesUserName fromDictionary:dict];
            self.authorVerified = [self objectOrNilForKey:kCommentRepliesAuthorVerified fromDictionary:dict];
            self.deleteComment = [[self objectOrNilForKey:kCommentRepliesDeleteComment fromDictionary:dict] boolValue];
            self.text = [self objectOrNilForKey:kCommentRepliesText fromDictionary:dict];
            self.nodeId = [self objectOrNilForKey:kCommentRepliesNodeId fromDictionary:dict];
            self.userVerified = [self objectOrNilForKey:kCommentRepliesUserVerified fromDictionary:dict];
            self.authorName = [self objectOrNilForKey:kCommentRepliesAuthorName fromDictionary:dict];
            self.authorUserName = [self objectOrNilForKey:kCommentRepliesAuthorUserName fromDictionary:dict];
            self.userFirstname = [self objectOrNilForKey:kCommentRepliesUserFirstname fromDictionary:dict];
            self.authorPicture = [self objectOrNilForKey:kCommentRepliesAuthorPicture fromDictionary:dict];
            self.userLastname = [self objectOrNilForKey:kCommentRepliesUserLastname fromDictionary:dict];
            self.pageCoverId = [self objectOrNilForKey:kCommentRepliesPageCoverId fromDictionary:dict];
            self.editComment = [[self objectOrNilForKey:kCommentRepliesEditComment fromDictionary:dict] boolValue];
            self.pageDescription = [self objectOrNilForKey:kCommentRepliesPageDescription fromDictionary:dict];
            self.authorId = [self objectOrNilForKey:kCommentRepliesAuthorId fromDictionary:dict];
            self.image = [self objectOrNilForKey:kCommentRepliesImage fromDictionary:dict];
        self.video = [self objectOrNilForKey:kCommentRepliesVideo fromDictionary:dict];
            self.likes = [self objectOrNilForKey:kCommentRepliesLikes fromDictionary:dict];
            self.pageVerified = [self objectOrNilForKey:kCommentRepliesPageVerified fromDictionary:dict];
            self.pageAlbumPictures = [self objectOrNilForKey:kCommentRepliesPageAlbumPictures fromDictionary:dict];
            self.pageAlbumTimeline = [self objectOrNilForKey:kCommentRepliesPageAlbumTimeline fromDictionary:dict];
            self.pageName = [self objectOrNilForKey:kCommentRepliesPageName fromDictionary:dict];
            self.pagePictureId = [self objectOrNilForKey:kCommentRepliesPagePictureId fromDictionary:dict];
            self.pageTitle = [self objectOrNilForKey:kCommentRepliesPageTitle fromDictionary:dict];
            self.pageLikes = [self objectOrNilForKey:kCommentRepliesPageLikes fromDictionary:dict];
            self.userGender = [self objectOrNilForKey:kCommentRepliesUserGender fromDictionary:dict];
            self.nodeType = [self objectOrNilForKey:kCommentRepliesNodeType fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kCommentRepliesUserId fromDictionary:dict];
            self.pagePicture = [self objectOrNilForKey:kCommentRepliesPagePicture fromDictionary:dict];
            self.pageAlbumCovers = [self objectOrNilForKey:kCommentRepliesPageAlbumCovers fromDictionary:dict];
            self.textPlain = [self objectOrNilForKey:kCommentRepliesTextPlain fromDictionary:dict];
            self.commentId = [self objectOrNilForKey:kCommentRepliesCommentId fromDictionary:dict];
            self.pageId = [self objectOrNilForKey:kCommentRepliesPageId fromDictionary:dict];
            self.time = [self objectOrNilForKey:kCommentRepliesTime fromDictionary:dict];
            self.pageBoosted = [self objectOrNilForKey:kCommentRepliesPageBoosted fromDictionary:dict];
            self.pageCategory = [self objectOrNilForKey:kCommentRepliesPageCategory fromDictionary:dict];
            self.authorUrl = [self objectOrNilForKey:kCommentRepliesAuthorUrl fromDictionary:dict];
            self.pageDate = [self objectOrNilForKey:kCommentRepliesPageDate fromDictionary:dict];
        self.iLike = [[self objectOrNilForKey:kCommentRepliesILike fromDictionary:dict] boolValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.pageAdmin forKey:kCommentRepliesPageAdmin];
    [mutableDict setValue:self.userPicture forKey:kCommentRepliesUserPicture];
    [mutableDict setValue:self.userType forKey:kCommentRepliesUserType];
    [mutableDict setValue:self.pageCover forKey:kCommentRepliesPageCover];
    [mutableDict setValue:self.pagePinnedPost forKey:kCommentRepliesPagePinnedPost];
    [mutableDict setValue:self.replies forKey:kCommentRepliesReplies];
    [mutableDict setValue:self.userName forKey:kCommentRepliesUserName];
    [mutableDict setValue:self.authorVerified forKey:kCommentRepliesAuthorVerified];
    [mutableDict setValue:[NSNumber numberWithBool:self.deleteComment] forKey:kCommentRepliesDeleteComment];
    [mutableDict setValue:self.text forKey:kCommentRepliesText];
    [mutableDict setValue:self.nodeId forKey:kCommentRepliesNodeId];
    [mutableDict setValue:self.userVerified forKey:kCommentRepliesUserVerified];
    [mutableDict setValue:self.authorName forKey:kCommentRepliesAuthorName];
    [mutableDict setValue:self.authorUserName forKey:kCommentRepliesAuthorUserName];
    [mutableDict setValue:self.userFirstname forKey:kCommentRepliesUserFirstname];
    [mutableDict setValue:self.authorPicture forKey:kCommentRepliesAuthorPicture];
    [mutableDict setValue:self.userLastname forKey:kCommentRepliesUserLastname];
    [mutableDict setValue:self.pageCoverId forKey:kCommentRepliesPageCoverId];
    [mutableDict setValue:[NSNumber numberWithBool:self.editComment] forKey:kCommentRepliesEditComment];
    [mutableDict setValue:self.pageDescription forKey:kCommentRepliesPageDescription];
    [mutableDict setValue:self.authorId forKey:kCommentRepliesAuthorId];
    [mutableDict setValue:self.image forKey:kCommentRepliesImage];
    [mutableDict setValue:self.video forKey:kCommentRepliesVideo];
    [mutableDict setValue:self.likes forKey:kCommentRepliesLikes];
    [mutableDict setValue:self.pageVerified forKey:kCommentRepliesPageVerified];
    [mutableDict setValue:self.pageAlbumPictures forKey:kCommentRepliesPageAlbumPictures];
    [mutableDict setValue:self.pageAlbumTimeline forKey:kCommentRepliesPageAlbumTimeline];
    [mutableDict setValue:self.pageName forKey:kCommentRepliesPageName];
    [mutableDict setValue:self.pagePictureId forKey:kCommentRepliesPagePictureId];
    [mutableDict setValue:self.pageTitle forKey:kCommentRepliesPageTitle];
    [mutableDict setValue:self.pageLikes forKey:kCommentRepliesPageLikes];
    [mutableDict setValue:self.userGender forKey:kCommentRepliesUserGender];
    [mutableDict setValue:self.nodeType forKey:kCommentRepliesNodeType];
    [mutableDict setValue:self.userId forKey:kCommentRepliesUserId];
    [mutableDict setValue:self.pagePicture forKey:kCommentRepliesPagePicture];
    [mutableDict setValue:self.pageAlbumCovers forKey:kCommentRepliesPageAlbumCovers];
    [mutableDict setValue:self.textPlain forKey:kCommentRepliesTextPlain];
    [mutableDict setValue:self.commentId forKey:kCommentRepliesCommentId];
    [mutableDict setValue:self.pageId forKey:kCommentRepliesPageId];
    [mutableDict setValue:self.time forKey:kCommentRepliesTime];
    [mutableDict setValue:self.pageBoosted forKey:kCommentRepliesPageBoosted];
    [mutableDict setValue:self.pageCategory forKey:kCommentRepliesPageCategory];
    [mutableDict setValue:self.authorUrl forKey:kCommentRepliesAuthorUrl];
    [mutableDict setValue:self.pageDate forKey:kCommentRepliesPageDate];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.pageAdmin = [aDecoder decodeObjectForKey:kCommentRepliesPageAdmin];
    self.userPicture = [aDecoder decodeObjectForKey:kCommentRepliesUserPicture];
    self.userType = [aDecoder decodeObjectForKey:kCommentRepliesUserType];
    self.pageCover = [aDecoder decodeObjectForKey:kCommentRepliesPageCover];
    self.pagePinnedPost = [aDecoder decodeObjectForKey:kCommentRepliesPagePinnedPost];
    self.replies = [aDecoder decodeObjectForKey:kCommentRepliesReplies];
    self.userName = [aDecoder decodeObjectForKey:kCommentRepliesUserName];
    self.authorVerified = [aDecoder decodeObjectForKey:kCommentRepliesAuthorVerified];
    self.deleteComment = [aDecoder decodeBoolForKey:kCommentRepliesDeleteComment];
    self.text = [aDecoder decodeObjectForKey:kCommentRepliesText];
    self.nodeId = [aDecoder decodeObjectForKey:kCommentRepliesNodeId];
    self.userVerified = [aDecoder decodeObjectForKey:kCommentRepliesUserVerified];
    self.authorName = [aDecoder decodeObjectForKey:kCommentRepliesAuthorName];
    self.authorUserName = [aDecoder decodeObjectForKey:kCommentRepliesAuthorUserName];
    self.userFirstname = [aDecoder decodeObjectForKey:kCommentRepliesUserFirstname];
    self.authorPicture = [aDecoder decodeObjectForKey:kCommentRepliesAuthorPicture];
    self.userLastname = [aDecoder decodeObjectForKey:kCommentRepliesUserLastname];
    self.pageCoverId = [aDecoder decodeObjectForKey:kCommentRepliesPageCoverId];
    self.editComment = [aDecoder decodeBoolForKey:kCommentRepliesEditComment];
    self.pageDescription = [aDecoder decodeObjectForKey:kCommentRepliesPageDescription];
    self.authorId = [aDecoder decodeObjectForKey:kCommentRepliesAuthorId];
    self.image = [aDecoder decodeObjectForKey:kCommentRepliesImage];
    self.video = [aDecoder decodeObjectForKey:kCommentRepliesVideo];
    self.likes = [aDecoder decodeObjectForKey:kCommentRepliesLikes];
    self.pageVerified = [aDecoder decodeObjectForKey:kCommentRepliesPageVerified];
    self.pageAlbumPictures = [aDecoder decodeObjectForKey:kCommentRepliesPageAlbumPictures];
    self.pageAlbumTimeline = [aDecoder decodeObjectForKey:kCommentRepliesPageAlbumTimeline];
    self.pageName = [aDecoder decodeObjectForKey:kCommentRepliesPageName];
    self.pagePictureId = [aDecoder decodeObjectForKey:kCommentRepliesPagePictureId];
    self.pageTitle = [aDecoder decodeObjectForKey:kCommentRepliesPageTitle];
    self.pageLikes = [aDecoder decodeObjectForKey:kCommentRepliesPageLikes];
    self.userGender = [aDecoder decodeObjectForKey:kCommentRepliesUserGender];
    self.nodeType = [aDecoder decodeObjectForKey:kCommentRepliesNodeType];
    self.userId = [aDecoder decodeObjectForKey:kCommentRepliesUserId];
    self.pagePicture = [aDecoder decodeObjectForKey:kCommentRepliesPagePicture];
    self.pageAlbumCovers = [aDecoder decodeObjectForKey:kCommentRepliesPageAlbumCovers];
    self.textPlain = [aDecoder decodeObjectForKey:kCommentRepliesTextPlain];
    self.commentId = [aDecoder decodeObjectForKey:kCommentRepliesCommentId];
    self.pageId = [aDecoder decodeObjectForKey:kCommentRepliesPageId];
    self.time = [aDecoder decodeObjectForKey:kCommentRepliesTime];
    self.pageBoosted = [aDecoder decodeObjectForKey:kCommentRepliesPageBoosted];
    self.pageCategory = [aDecoder decodeObjectForKey:kCommentRepliesPageCategory];
    self.authorUrl = [aDecoder decodeObjectForKey:kCommentRepliesAuthorUrl];
    self.pageDate = [aDecoder decodeObjectForKey:kCommentRepliesPageDate];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_pageAdmin forKey:kCommentRepliesPageAdmin];
    [aCoder encodeObject:_userPicture forKey:kCommentRepliesUserPicture];
    [aCoder encodeObject:_userType forKey:kCommentRepliesUserType];
    [aCoder encodeObject:_pageCover forKey:kCommentRepliesPageCover];
    [aCoder encodeObject:_pagePinnedPost forKey:kCommentRepliesPagePinnedPost];
    [aCoder encodeObject:_replies forKey:kCommentRepliesReplies];
    [aCoder encodeObject:_userName forKey:kCommentRepliesUserName];
    [aCoder encodeObject:_authorVerified forKey:kCommentRepliesAuthorVerified];
    [aCoder encodeBool:_deleteComment forKey:kCommentRepliesDeleteComment];
    [aCoder encodeObject:_text forKey:kCommentRepliesText];
    [aCoder encodeObject:_nodeId forKey:kCommentRepliesNodeId];
    [aCoder encodeObject:_userVerified forKey:kCommentRepliesUserVerified];
    [aCoder encodeObject:_authorName forKey:kCommentRepliesAuthorName];
    [aCoder encodeObject:_authorUserName forKey:kCommentRepliesAuthorUserName];
    [aCoder encodeObject:_userFirstname forKey:kCommentRepliesUserFirstname];
    [aCoder encodeObject:_authorPicture forKey:kCommentRepliesAuthorPicture];
    [aCoder encodeObject:_userLastname forKey:kCommentRepliesUserLastname];
    [aCoder encodeObject:_pageCoverId forKey:kCommentRepliesPageCoverId];
    [aCoder encodeBool:_editComment forKey:kCommentRepliesEditComment];
    [aCoder encodeObject:_pageDescription forKey:kCommentRepliesPageDescription];
    [aCoder encodeObject:_authorId forKey:kCommentRepliesAuthorId];
    [aCoder encodeObject:_image forKey:kCommentRepliesImage];
    [aCoder encodeObject:_video forKey:kCommentRepliesVideo];
    [aCoder encodeObject:_likes forKey:kCommentRepliesLikes];
    [aCoder encodeObject:_pageVerified forKey:kCommentRepliesPageVerified];
    [aCoder encodeObject:_pageAlbumPictures forKey:kCommentRepliesPageAlbumPictures];
    [aCoder encodeObject:_pageAlbumTimeline forKey:kCommentRepliesPageAlbumTimeline];
    [aCoder encodeObject:_pageName forKey:kCommentRepliesPageName];
    [aCoder encodeObject:_pagePictureId forKey:kCommentRepliesPagePictureId];
    [aCoder encodeObject:_pageTitle forKey:kCommentRepliesPageTitle];
    [aCoder encodeObject:_pageLikes forKey:kCommentRepliesPageLikes];
    [aCoder encodeObject:_userGender forKey:kCommentRepliesUserGender];
    [aCoder encodeObject:_nodeType forKey:kCommentRepliesNodeType];
    [aCoder encodeObject:_userId forKey:kCommentRepliesUserId];
    [aCoder encodeObject:_pagePicture forKey:kCommentRepliesPagePicture];
    [aCoder encodeObject:_pageAlbumCovers forKey:kCommentRepliesPageAlbumCovers];
    [aCoder encodeObject:_textPlain forKey:kCommentRepliesTextPlain];
    [aCoder encodeObject:_commentId forKey:kCommentRepliesCommentId];
    [aCoder encodeObject:_pageId forKey:kCommentRepliesPageId];
    [aCoder encodeObject:_time forKey:kCommentRepliesTime];
    [aCoder encodeObject:_pageBoosted forKey:kCommentRepliesPageBoosted];
    [aCoder encodeObject:_pageCategory forKey:kCommentRepliesPageCategory];
    [aCoder encodeObject:_authorUrl forKey:kCommentRepliesAuthorUrl];
    [aCoder encodeObject:_pageDate forKey:kCommentRepliesPageDate];
}

- (id)copyWithZone:(NSZone *)zone
{
    CommentReplies *copy = [[CommentReplies alloc] init];
    
    if (copy) {

        copy.pageAdmin = [self.pageAdmin copyWithZone:zone];
        copy.userPicture = [self.userPicture copyWithZone:zone];
        copy.userType = [self.userType copyWithZone:zone];
        copy.pageCover = [self.pageCover copyWithZone:zone];
        copy.pagePinnedPost = [self.pagePinnedPost copyWithZone:zone];
        copy.replies = [self.replies copyWithZone:zone];
        copy.userName = [self.userName copyWithZone:zone];
        copy.authorVerified = [self.authorVerified copyWithZone:zone];
        copy.deleteComment = self.deleteComment;
        copy.text = [self.text copyWithZone:zone];
        copy.nodeId = [self.nodeId copyWithZone:zone];
        copy.userVerified = [self.userVerified copyWithZone:zone];
        copy.authorName = [self.authorName copyWithZone:zone];
        copy.authorUserName = [self.authorUserName copyWithZone:zone];
        copy.userFirstname = [self.userFirstname copyWithZone:zone];
        copy.authorPicture = [self.authorPicture copyWithZone:zone];
        copy.userLastname = [self.userLastname copyWithZone:zone];
        copy.pageCoverId = [self.pageCoverId copyWithZone:zone];
        copy.editComment = self.editComment;
        copy.pageDescription = [self.pageDescription copyWithZone:zone];
        copy.authorId = [self.authorId copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
        copy.video = [self.video copyWithZone:zone];
        copy.likes = [self.likes copyWithZone:zone];
        copy.pageVerified = [self.pageVerified copyWithZone:zone];
        copy.pageAlbumPictures = [self.pageAlbumPictures copyWithZone:zone];
        copy.pageAlbumTimeline = [self.pageAlbumTimeline copyWithZone:zone];
        copy.pageName = [self.pageName copyWithZone:zone];
        copy.pagePictureId = [self.pagePictureId copyWithZone:zone];
        copy.pageTitle = [self.pageTitle copyWithZone:zone];
        copy.pageLikes = [self.pageLikes copyWithZone:zone];
        copy.userGender = [self.userGender copyWithZone:zone];
        copy.nodeType = [self.nodeType copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.pagePicture = [self.pagePicture copyWithZone:zone];
        copy.pageAlbumCovers = [self.pageAlbumCovers copyWithZone:zone];
        copy.textPlain = [self.textPlain copyWithZone:zone];
        copy.commentId = [self.commentId copyWithZone:zone];
        copy.pageId = [self.pageId copyWithZone:zone];
        copy.time = [self.time copyWithZone:zone];
        copy.pageBoosted = [self.pageBoosted copyWithZone:zone];
        copy.pageCategory = [self.pageCategory copyWithZone:zone];
        copy.authorUrl = [self.authorUrl copyWithZone:zone];
        copy.pageDate = [self.pageDate copyWithZone:zone];
    }
    
    return copy;
}


@end
