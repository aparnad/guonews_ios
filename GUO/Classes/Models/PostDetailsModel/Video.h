//
//  Video.h
//
//  Created by Pawan Ramteke on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Video : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *postId;
@property (nonatomic, strong) NSString *videoId;
@property (nonatomic, strong) NSString *source;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
