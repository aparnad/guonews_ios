//
//  Video.m
//
//  Created by Pawan Ramteke on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "Video.h"


NSString *const kVideoPostId = @"post_id";
NSString *const kVideoVideoId = @"video_id";
NSString *const kVideoSource = @"source";


@interface Video ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Video

@synthesize postId = _postId;
@synthesize videoId = _videoId;
@synthesize source = _source;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.postId = [self objectOrNilForKey:kVideoPostId fromDictionary:dict];
            self.videoId = [self objectOrNilForKey:kVideoVideoId fromDictionary:dict];
            self.source = [self objectOrNilForKey:kVideoSource fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.postId forKey:kVideoPostId];
    [mutableDict setValue:self.videoId forKey:kVideoVideoId];
    [mutableDict setValue:self.source forKey:kVideoSource];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.postId = [aDecoder decodeObjectForKey:kVideoPostId];
    self.videoId = [aDecoder decodeObjectForKey:kVideoVideoId];
    self.source = [aDecoder decodeObjectForKey:kVideoSource];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_postId forKey:kVideoPostId];
    [aCoder encodeObject:_videoId forKey:kVideoVideoId];
    [aCoder encodeObject:_source forKey:kVideoSource];
}

- (id)copyWithZone:(NSZone *)zone
{
    Video *copy = [[Video alloc] init];
    
    if (copy) {

        copy.postId = [self.postId copyWithZone:zone];
        copy.videoId = [self.videoId copyWithZone:zone];
        copy.source = [self.source copyWithZone:zone];
    }
    
    return copy;
}


@end
