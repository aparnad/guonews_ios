//
//  CountModel.m
//
//  Created by Amol Hirkane on 25/06/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "CountModel.h"


NSString *const kCountModelPosts = @"posts";
NSString *const kCountModelFollowings = @"followings";
NSString *const kCountModelFollowers = @"followers";
NSString *const kCountModelLikes = @"likes";


@interface CountModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CountModel

@synthesize posts = _posts;
@synthesize followings = _followings;
@synthesize followers = _followers;
@synthesize likes = _likes;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.posts = [[self objectOrNilForKey:kCountModelPosts fromDictionary:dict] doubleValue];
            self.followings = [[self objectOrNilForKey:kCountModelFollowings fromDictionary:dict] doubleValue];
            self.followers = [[self objectOrNilForKey:kCountModelFollowers fromDictionary:dict] doubleValue];
            self.likes = [[self objectOrNilForKey:kCountModelLikes fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.posts] forKey:kCountModelPosts];
    [mutableDict setValue:[NSNumber numberWithDouble:self.followings] forKey:kCountModelFollowings];
    [mutableDict setValue:[NSNumber numberWithDouble:self.followers] forKey:kCountModelFollowers];
    [mutableDict setValue:[NSNumber numberWithDouble:self.likes] forKey:kCountModelLikes];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.posts = [aDecoder decodeDoubleForKey:kCountModelPosts];
    self.followings = [aDecoder decodeDoubleForKey:kCountModelFollowings];
    self.followers = [aDecoder decodeDoubleForKey:kCountModelFollowers];
    self.likes = [aDecoder decodeDoubleForKey:kCountModelLikes];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_posts forKey:kCountModelPosts];
    [aCoder encodeDouble:_followings forKey:kCountModelFollowings];
    [aCoder encodeDouble:_followers forKey:kCountModelFollowers];
    [aCoder encodeDouble:_likes forKey:kCountModelLikes];
}

- (id)copyWithZone:(NSZone *)zone
{
    CountModel *copy = [[CountModel alloc] init];
    
    if (copy) {

        copy.posts = self.posts;
        copy.followings = self.followings;
        copy.followers = self.followers;
        copy.likes = self.likes;
    }
    
    return copy;
}


@end
