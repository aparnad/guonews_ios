//
//  CreatePostModel.h
//  GUO Media
//
//  Created by Pawan Ramteke on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AWSS3.h"
@interface CreatePostModel : NSObject
@property(nonatomic,strong)UIImage *attachment;
@property (nonatomic,strong)AWSS3TransferManagerUploadRequest *uploadRequest;
@property (nonatomic,assign)CGFloat progress;
@property (nonatomic,strong)NSString *type;

@end
