//
//  NotificationModel.h
//
//  Created by Amol Hirkane on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostModel.h"
@interface NotificationModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *toUserId;
@property (nonatomic, strong) NSString *nodeUrl;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *notifyId;
@property (nonatomic, strong) NSString *userFirstname;
@property (nonatomic, strong) NSString *userLastname;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *formattedTime;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *fromUserId;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *action;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *userPicture;
@property (nonatomic, strong) NSString *seen;
@property (nonatomic, strong) NSString *fullMessage;
@property (nonatomic, strong) NSString *nodeType;
@property (nonatomic, strong) NSString *notificationId;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, strong) PostModel *notificationDetailsModel;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
