//
//  NotificationModel.m
//
//  Created by Amol Hirkane on 01/07/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "NotificationModel.h"


NSString *const kNotificationModelToUserId = @"to_user_id";
NSString *const kNotificationModelNodeUrl = @"node_url";
NSString *const kNotificationModelMessage = @"message";
NSString *const kNotificationModelNotifyId = @"notify_id";
NSString *const kNotificationModelUserFirstname = @"user_firstname";
NSString *const kNotificationModelUserLastname = @"user_lastname";
NSString *const kNotificationModelTime = @"time";
NSString *const kNotificationModelFormattedTime = @"formatted_time";
NSString *const kNotificationModelUserId = @"user_id";
NSString *const kNotificationModelFromUserId = @"from_user_id";
NSString *const kNotificationModelUrl = @"url";
NSString *const kNotificationModelAction = @"action";
NSString *const kNotificationModelUserName = @"user_name";
NSString *const kNotificationModelIcon = @"icon";
NSString *const kNotificationModelUserPicture = @"user_picture";
NSString *const kNotificationModelSeen = @"seen";
NSString *const kNotificationModelFullMessage = @"full_message";
NSString *const kNotificationModelNodeType = @"node_type";
NSString *const kNotificationModelNotificationId = @"notification_id";
NSString *const kNotificationModelUserGender = @"user_gender";
NSString *const kNotificationModelNotificationObj = @"obj";


@interface NotificationModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation NotificationModel

@synthesize toUserId = _toUserId;
@synthesize nodeUrl = _nodeUrl;
@synthesize message = _message;
@synthesize notifyId = _notifyId;
@synthesize userFirstname = _userFirstname;
@synthesize userLastname = _userLastname;
@synthesize time = _time;
@synthesize formattedTime = _formattedTime;
@synthesize userId = _userId;
@synthesize fromUserId = _fromUserId;
@synthesize url = _url;
@synthesize action = _action;
@synthesize userName = _userName;
@synthesize icon = _icon;
@synthesize userPicture = _userPicture;
@synthesize seen = _seen;
@synthesize fullMessage = _fullMessage;
@synthesize nodeType = _nodeType;
@synthesize notificationId = _notificationId;
@synthesize userGender = _userGender;
@synthesize notificationDetailsModel = _notificationDetailsModel;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.toUserId = [self objectOrNilForKey:kNotificationModelToUserId fromDictionary:dict];
            self.nodeUrl = [self objectOrNilForKey:kNotificationModelNodeUrl fromDictionary:dict];
            self.message = [self objectOrNilForKey:kNotificationModelMessage fromDictionary:dict];
            self.notifyId = [self objectOrNilForKey:kNotificationModelNotifyId fromDictionary:dict];
            self.userFirstname = [self objectOrNilForKey:kNotificationModelUserFirstname fromDictionary:dict];
            self.userLastname = [self objectOrNilForKey:kNotificationModelUserLastname fromDictionary:dict];
            self.time = [self objectOrNilForKey:kNotificationModelTime fromDictionary:dict];
        self.formattedTime = [self objectOrNilForKey:kNotificationModelFormattedTime fromDictionary:dict];

            self.userId = [self objectOrNilForKey:kNotificationModelUserId fromDictionary:dict];
            self.fromUserId = [self objectOrNilForKey:kNotificationModelFromUserId fromDictionary:dict];
            self.url = [self objectOrNilForKey:kNotificationModelUrl fromDictionary:dict];
            self.action = [self objectOrNilForKey:kNotificationModelAction fromDictionary:dict];
            self.userName = [self objectOrNilForKey:kNotificationModelUserName fromDictionary:dict];
            self.icon = [self objectOrNilForKey:kNotificationModelIcon fromDictionary:dict];
            self.userPicture = [self objectOrNilForKey:kNotificationModelUserPicture fromDictionary:dict];
            self.seen = [self objectOrNilForKey:kNotificationModelSeen fromDictionary:dict];
            self.fullMessage = [self objectOrNilForKey:kNotificationModelFullMessage fromDictionary:dict];
            self.nodeType = [self objectOrNilForKey:kNotificationModelNodeType fromDictionary:dict];
            self.notificationId = [self objectOrNilForKey:kNotificationModelNotificationId fromDictionary:dict];
            self.userGender = [self objectOrNilForKey:kNotificationModelUserGender fromDictionary:dict];
        
        self.notificationDetailsModel = [PostModel modelObjectWithDictionary:dict[kNotificationModelNotificationObj]];
        

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.toUserId forKey:kNotificationModelToUserId];
    [mutableDict setValue:self.nodeUrl forKey:kNotificationModelNodeUrl];
    [mutableDict setValue:self.message forKey:kNotificationModelMessage];
    [mutableDict setValue:self.notifyId forKey:kNotificationModelNotifyId];
    [mutableDict setValue:self.userFirstname forKey:kNotificationModelUserFirstname];
    [mutableDict setValue:self.userLastname forKey:kNotificationModelUserLastname];
    [mutableDict setValue:self.time forKey:kNotificationModelTime];
    [mutableDict setValue:self.formattedTime forKey:kNotificationModelFormattedTime];
    [mutableDict setValue:self.userId forKey:kNotificationModelUserId];
    [mutableDict setValue:self.fromUserId forKey:kNotificationModelFromUserId];
    [mutableDict setValue:self.url forKey:kNotificationModelUrl];
    [mutableDict setValue:self.action forKey:kNotificationModelAction];
    [mutableDict setValue:self.userName forKey:kNotificationModelUserName];
    [mutableDict setValue:self.icon forKey:kNotificationModelIcon];
    [mutableDict setValue:self.userPicture forKey:kNotificationModelUserPicture];
    [mutableDict setValue:self.seen forKey:kNotificationModelSeen];
    [mutableDict setValue:self.fullMessage forKey:kNotificationModelFullMessage];
    [mutableDict setValue:self.nodeType forKey:kNotificationModelNodeType];
    [mutableDict setValue:self.notificationId forKey:kNotificationModelNotificationId];
    [mutableDict setValue:self.userGender forKey:kNotificationModelUserGender];
    [mutableDict setObject:[self.notificationDetailsModel dictionaryRepresentation] forKey:kNotificationModelNotificationObj];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.toUserId = [aDecoder decodeObjectForKey:kNotificationModelToUserId];
    self.nodeUrl = [aDecoder decodeObjectForKey:kNotificationModelNodeUrl];
    self.message = [aDecoder decodeObjectForKey:kNotificationModelMessage];
    self.notifyId = [aDecoder decodeObjectForKey:kNotificationModelNotifyId];
    self.userFirstname = [aDecoder decodeObjectForKey:kNotificationModelUserFirstname];
    self.userLastname = [aDecoder decodeObjectForKey:kNotificationModelUserLastname];
    self.time = [aDecoder decodeObjectForKey:kNotificationModelTime];
    self.userId = [aDecoder decodeObjectForKey:kNotificationModelUserId];
    self.fromUserId = [aDecoder decodeObjectForKey:kNotificationModelFromUserId];
    self.url = [aDecoder decodeObjectForKey:kNotificationModelUrl];
    self.action = [aDecoder decodeObjectForKey:kNotificationModelAction];
    self.userName = [aDecoder decodeObjectForKey:kNotificationModelUserName];
    self.icon = [aDecoder decodeObjectForKey:kNotificationModelIcon];
    self.userPicture = [aDecoder decodeObjectForKey:kNotificationModelUserPicture];
    self.seen = [aDecoder decodeObjectForKey:kNotificationModelSeen];
    self.fullMessage = [aDecoder decodeObjectForKey:kNotificationModelFullMessage];
    self.nodeType = [aDecoder decodeObjectForKey:kNotificationModelNodeType];
    self.notificationId = [aDecoder decodeObjectForKey:kNotificationModelNotificationId];
    self.userGender = [aDecoder decodeObjectForKey:kNotificationModelUserGender];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_toUserId forKey:kNotificationModelToUserId];
    [aCoder encodeObject:_nodeUrl forKey:kNotificationModelNodeUrl];
    [aCoder encodeObject:_message forKey:kNotificationModelMessage];
    [aCoder encodeObject:_notifyId forKey:kNotificationModelNotifyId];
    [aCoder encodeObject:_userFirstname forKey:kNotificationModelUserFirstname];
    [aCoder encodeObject:_userLastname forKey:kNotificationModelUserLastname];
    [aCoder encodeObject:_time forKey:kNotificationModelTime];
    [aCoder encodeObject:_userId forKey:kNotificationModelUserId];
    [aCoder encodeObject:_fromUserId forKey:kNotificationModelFromUserId];
    [aCoder encodeObject:_url forKey:kNotificationModelUrl];
    [aCoder encodeObject:_action forKey:kNotificationModelAction];
    [aCoder encodeObject:_userName forKey:kNotificationModelUserName];
    [aCoder encodeObject:_icon forKey:kNotificationModelIcon];
    [aCoder encodeObject:_userPicture forKey:kNotificationModelUserPicture];
    [aCoder encodeObject:_seen forKey:kNotificationModelSeen];
    [aCoder encodeObject:_fullMessage forKey:kNotificationModelFullMessage];
    [aCoder encodeObject:_nodeType forKey:kNotificationModelNodeType];
    [aCoder encodeObject:_notificationId forKey:kNotificationModelNotificationId];
    [aCoder encodeObject:_userGender forKey:kNotificationModelUserGender];
}

- (id)copyWithZone:(NSZone *)zone
{
    NotificationModel *copy = [[NotificationModel alloc] init];
    
    if (copy) {

        copy.toUserId = [self.toUserId copyWithZone:zone];
        copy.nodeUrl = [self.nodeUrl copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
        copy.notifyId = [self.notifyId copyWithZone:zone];
        copy.userFirstname = [self.userFirstname copyWithZone:zone];
        copy.userLastname = [self.userLastname copyWithZone:zone];
        copy.time = [self.time copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.fromUserId = [self.fromUserId copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.action = [self.action copyWithZone:zone];
        copy.userName = [self.userName copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
        copy.userPicture = [self.userPicture copyWithZone:zone];
        copy.seen = [self.seen copyWithZone:zone];
        copy.fullMessage = [self.fullMessage copyWithZone:zone];
        copy.nodeType = [self.nodeType copyWithZone:zone];
        copy.notificationId = [self.notificationId copyWithZone:zone];
        copy.userGender = [self.userGender copyWithZone:zone];
    }
    
    return copy;
}


@end
