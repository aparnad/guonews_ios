//
//  PostModel.m
//
//  Created by Amol Hirkane on 28/06/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "PostModel.h"
#import "Photos.h"


NSString *const kPostModelUserPicture = @"user_picture";
NSString *const kPostModelPostAuthorName = @"post_author_name";
NSString *const kPostModelUserType = @"user_type";
NSString *const kPostModelPinned = @"pinned";
NSString *const kPostModelUserName = @"user_name";
NSString *const kPostModelChannelUrl = @"channel_url";
NSString *const kPostModelComments = @"comments";
NSString *const kPostModelLocation = @"location";
NSString *const kPostModelText = @"text";
NSString *const kPostModelUserPinnedPost = @"user_pinned_post";
NSString *const kPostModelIsEventAdmin = @"is_event_admin";
NSString *const kPostModelILike = @"i_like";
NSString *const kPostModelUserVerified = @"user_verified";
NSString *const kPostModelPostAuthorPicture = @"post_author_picture";
NSString *const kPostModelUserCoverId = @"user_cover_id";
NSString *const kPostModelUserFirstname = @"user_firstname";
NSString *const kPostModelIsGroupAdmin = @"is_group_admin";
NSString *const kPostModelPrivacy = @"privacy";
NSString *const kPostModelBroadcastUrl = @"broadcast_url";
NSString *const kPostModelUserLastname = @"user_lastname";
NSString *const kPostModelShares = @"shares";
NSString *const kPostModelPostUserName = @"post_user_name";
NSString *const kPostModelAuthorId = @"author_id";
NSString *const kPostModelEventId = @"event_id";
NSString *const kPostModelUserSubscribed = @"user_subscribed";
NSString *const kPostModelBroadcastName = @"broadcast_name";
NSString *const kPostModelOriginId = @"origin_id";
NSString *const kPostModelLikes = @"likes";
NSString *const kPostModelFeelingAction = @"feeling_action";
NSString *const kPostModelViews = @"views";
NSString *const kPostModelPostType = @"post_type";
NSString *const kPostModelPostAuthorUrl = @"post_author_url";
NSString *const kPostModelInWall = @"in_wall";
NSString *const kPostModelInEvent = @"in_event";
NSString *const kPostModelUserPictureId = @"user_picture_id";
NSString *const kPostModelISave = @"i_save";
NSString *const kPostModelInGroup = @"in_group";
NSString *const kPostModelFeelingValue = @"feeling_value";
NSString *const kPostModelUserGender = @"user_gender";
NSString *const kPostModelIsPageAdmin = @"is_page_admin";
NSString *const kPostModelUserId = @"user_id";
NSString *const kPostModelManagePost = @"manage_post";
NSString *const kPostModelTextPlain = @"text_plain";
NSString *const kPostModelGroupId = @"group_id";
NSString *const kPostModelWallId = @"wall_id";
NSString *const kPostModelBoosted = @"boosted";
NSString *const kPostModelPostAuthorVerified = @"post_author_verified";
NSString *const kPostModelPhotos = @"photos";
NSString *const kPostModelVideo = @"video";
NSString *const kPostModelPhotosNum = @"photos_num";
NSString *const kPostModelPostId = @"post_id";
NSString *const kPostModelTime = @"time";
NSString *const kPostModelFormattedTime = @"formatted_time";
NSString *const kPostModelIsBroadcast = @"is_broadcast";
NSString *const kPostModelOrigin = @"origin";


@interface PostModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PostModel

@synthesize userPicture = _userPicture;
@synthesize postAuthorName = _postAuthorName;
@synthesize userType = _userType;
@synthesize pinned = _pinned;
@synthesize userName = _userName;
@synthesize channelUrl = _channelUrl;
@synthesize comments = _comments;
@synthesize location = _location;
@synthesize text = _text;
@synthesize userPinnedPost = _userPinnedPost;
@synthesize isEventAdmin = _isEventAdmin;
@synthesize iLike = _iLike;
@synthesize userVerified = _userVerified;
@synthesize postAuthorPicture = _postAuthorPicture;
@synthesize userCoverId = _userCoverId;
@synthesize userFirstname = _userFirstname;
@synthesize isGroupAdmin = _isGroupAdmin;
@synthesize privacy = _privacy;
@synthesize broadcastUrl = _broadcastUrl;
@synthesize userLastname = _userLastname;
@synthesize shares = _shares;
@synthesize postUserName = _postUserName;
@synthesize authorId = _authorId;
@synthesize eventId = _eventId;
@synthesize userSubscribed = _userSubscribed;
@synthesize broadcastName = _broadcastName;
@synthesize originId = _originId;
@synthesize likes = _likes;
@synthesize feelingAction = _feelingAction;
@synthesize views = _views;
@synthesize postType = _postType;
@synthesize postAuthorUrl = _postAuthorUrl;
@synthesize inWall = _inWall;
@synthesize inEvent = _inEvent;
@synthesize userPictureId = _userPictureId;
@synthesize iSave = _iSave;
@synthesize inGroup = _inGroup;
@synthesize feelingValue = _feelingValue;
@synthesize userGender = _userGender;
@synthesize isPageAdmin = _isPageAdmin;
@synthesize userId = _userId;
@synthesize managePost = _managePost;
@synthesize textPlain = _textPlain;
@synthesize groupId = _groupId;
@synthesize wallId = _wallId;
@synthesize boosted = _boosted;
@synthesize postAuthorVerified = _postAuthorVerified;
@synthesize photos = _photos;
@synthesize videos = _videos;
@synthesize photosNum = _photosNum;
@synthesize postId = _postId;
@synthesize time = _time;
@synthesize formattedTime = _formattedTime;
@synthesize isBroadcast = _isBroadcast;
@synthesize origin = _origin;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    
    
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userPicture = [self objectOrNilForKey:kPostModelUserPicture fromDictionary:dict];
            self.postAuthorName = [self objectOrNilForKey:kPostModelPostAuthorName fromDictionary:dict];
            self.userType = [self objectOrNilForKey:kPostModelUserType fromDictionary:dict];
            self.pinned = [[self objectOrNilForKey:kPostModelPinned fromDictionary:dict] boolValue];
            self.userName = [self objectOrNilForKey:kPostModelUserName fromDictionary:dict];
            self.channelUrl = [self objectOrNilForKey:kPostModelChannelUrl fromDictionary:dict];
            self.comments = [self objectOrNilForKey:kPostModelComments fromDictionary:dict];
            self.location = [self objectOrNilForKey:kPostModelLocation fromDictionary:dict];
            self.text = [self objectOrNilForKey:kPostModelText fromDictionary:dict];
            self.userPinnedPost = [self objectOrNilForKey:kPostModelUserPinnedPost fromDictionary:dict];
            self.isEventAdmin = [[self objectOrNilForKey:kPostModelIsEventAdmin fromDictionary:dict] boolValue];
            self.iLike = [[self objectOrNilForKey:kPostModelILike fromDictionary:dict] boolValue];
            self.userVerified = [self objectOrNilForKey:kPostModelUserVerified fromDictionary:dict];
            self.postAuthorPicture = [self objectOrNilForKey:kPostModelPostAuthorPicture fromDictionary:dict];
            self.userCoverId = [self objectOrNilForKey:kPostModelUserCoverId fromDictionary:dict];
            self.userFirstname = [self objectOrNilForKey:kPostModelUserFirstname fromDictionary:dict];
            self.isGroupAdmin = [[self objectOrNilForKey:kPostModelIsGroupAdmin fromDictionary:dict] boolValue];
            self.privacy = [self objectOrNilForKey:kPostModelPrivacy fromDictionary:dict];
            self.broadcastUrl = [self objectOrNilForKey:kPostModelBroadcastUrl fromDictionary:dict];
            self.userLastname = [self objectOrNilForKey:kPostModelUserLastname fromDictionary:dict];
            self.shares = [self objectOrNilForKey:kPostModelShares fromDictionary:dict];
            self.postUserName = [self objectOrNilForKey:kPostModelPostUserName fromDictionary:dict];
            self.authorId = [self objectOrNilForKey:kPostModelAuthorId fromDictionary:dict];
            self.eventId = [self objectOrNilForKey:kPostModelEventId fromDictionary:dict];
            self.userSubscribed = [self objectOrNilForKey:kPostModelUserSubscribed fromDictionary:dict];
            self.broadcastName = [self objectOrNilForKey:kPostModelBroadcastName fromDictionary:dict];
            self.originId = [self objectOrNilForKey:kPostModelOriginId fromDictionary:dict];
            self.likes = [self objectOrNilForKey:kPostModelLikes fromDictionary:dict];
            self.feelingAction = [self objectOrNilForKey:kPostModelFeelingAction fromDictionary:dict];
            self.views = [self objectOrNilForKey:kPostModelViews fromDictionary:dict];
            self.postType = [self objectOrNilForKey:kPostModelPostType fromDictionary:dict];
            self.postAuthorUrl = [self objectOrNilForKey:kPostModelPostAuthorUrl fromDictionary:dict];
            self.inWall = [self objectOrNilForKey:kPostModelInWall fromDictionary:dict];
            self.inEvent = [self objectOrNilForKey:kPostModelInEvent fromDictionary:dict];
            self.userPictureId = [self objectOrNilForKey:kPostModelUserPictureId fromDictionary:dict];
            self.iSave = [[self objectOrNilForKey:kPostModelISave fromDictionary:dict] boolValue];
            self.inGroup = [self objectOrNilForKey:kPostModelInGroup fromDictionary:dict];
            self.feelingValue = [self objectOrNilForKey:kPostModelFeelingValue fromDictionary:dict];
            self.userGender = [self objectOrNilForKey:kPostModelUserGender fromDictionary:dict];
            self.isPageAdmin = [[self objectOrNilForKey:kPostModelIsPageAdmin fromDictionary:dict] boolValue];
            self.userId = [self objectOrNilForKey:kPostModelUserId fromDictionary:dict];
            self.managePost = [[self objectOrNilForKey:kPostModelManagePost fromDictionary:dict] boolValue];
            self.textPlain = [self objectOrNilForKey:kPostModelTextPlain fromDictionary:dict];
            self.groupId = [self objectOrNilForKey:kPostModelGroupId fromDictionary:dict];
            self.wallId = [self objectOrNilForKey:kPostModelWallId fromDictionary:dict];
            self.boosted = [self objectOrNilForKey:kPostModelBoosted fromDictionary:dict];
            self.postAuthorVerified = [self objectOrNilForKey:kPostModelPostAuthorVerified fromDictionary:dict];
            NSObject *receivedPhotos = [dict objectForKey:kPostModelPhotos];
            NSMutableArray *parsedPhotos = [NSMutableArray array];
            if ([receivedPhotos isKindOfClass:[NSArray class]]) {
                for (NSDictionary *item in (NSArray *)receivedPhotos) {
                    if ([item isKindOfClass:[NSDictionary class]]) {
                        [parsedPhotos addObject:[Photos modelObjectWithDictionary:item]];
                    }
               }
            } else if ([receivedPhotos isKindOfClass:[NSDictionary class]]) {
               [parsedPhotos addObject:[Photos modelObjectWithDictionary:(NSDictionary *)receivedPhotos]];
            }

            self.photos = [NSArray arrayWithArray:parsedPhotos];
        
            NSObject *receivedVideo = [dict objectForKey:kPostModelVideo];
            NSMutableArray *parsedVideo = [NSMutableArray array];
            if ([receivedVideo isKindOfClass:[NSArray class]]) {
                for (NSDictionary *item in (NSArray *)receivedVideo) {
                    if ([item isKindOfClass:[NSDictionary class]]) {
                        [parsedVideo addObject:[Photos modelObjectWithDictionary:item]];
                    }
                }
            } else if ([receivedVideo isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict = (NSDictionary *)receivedVideo;
                if ([dict allKeys].count) {
                    [parsedVideo addObject:[Photos modelObjectWithDictionary:(NSDictionary *)receivedVideo]];
                }
            }
        
            self.videos = [NSArray arrayWithArray:parsedVideo];
    
    self.photosNum = [[self objectOrNilForKey:kPostModelPhotosNum fromDictionary:dict] doubleValue];
    self.postId = [self objectOrNilForKey:kPostModelPostId fromDictionary:dict];
    self.time = [self objectOrNilForKey:kPostModelTime fromDictionary:dict];
    self.formattedTime = [self objectOrNilForKey:kPostModelFormattedTime fromDictionary:dict];

    self.isBroadcast = [self objectOrNilForKey:kPostModelIsBroadcast fromDictionary:dict];

        if (dict[kPostModelOrigin] != nil) {
            self.origin = [SharePostModel modelObjectWithDictionary:dict[kPostModelOrigin]];
        }
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.userPicture forKey:kPostModelUserPicture];
    [mutableDict setValue:self.postAuthorName forKey:kPostModelPostAuthorName];
    [mutableDict setValue:self.userType forKey:kPostModelUserType];
    [mutableDict setValue:[NSNumber numberWithBool:self.pinned] forKey:kPostModelPinned];
    [mutableDict setValue:self.userName forKey:kPostModelUserName];
    [mutableDict setValue:self.channelUrl forKey:kPostModelChannelUrl];
    [mutableDict setValue:self.comments forKey:kPostModelComments];
    [mutableDict setValue:self.location forKey:kPostModelLocation];
    [mutableDict setValue:self.text forKey:kPostModelText];
    [mutableDict setValue:self.userPinnedPost forKey:kPostModelUserPinnedPost];
    [mutableDict setValue:[NSNumber numberWithBool:self.isEventAdmin] forKey:kPostModelIsEventAdmin];
    [mutableDict setValue:[NSNumber numberWithBool:self.iLike] forKey:kPostModelILike];
    [mutableDict setValue:self.userVerified forKey:kPostModelUserVerified];
    [mutableDict setValue:self.postAuthorPicture forKey:kPostModelPostAuthorPicture];
    [mutableDict setValue:self.userCoverId forKey:kPostModelUserCoverId];
    [mutableDict setValue:self.userFirstname forKey:kPostModelUserFirstname];
    [mutableDict setValue:[NSNumber numberWithBool:self.isGroupAdmin] forKey:kPostModelIsGroupAdmin];
    [mutableDict setValue:self.privacy forKey:kPostModelPrivacy];
    [mutableDict setValue:self.broadcastUrl forKey:kPostModelBroadcastUrl];
    [mutableDict setValue:self.userLastname forKey:kPostModelUserLastname];
    [mutableDict setValue:self.shares forKey:kPostModelShares];
    [mutableDict setValue:self.postUserName forKey:kPostModelPostUserName];
    [mutableDict setValue:self.authorId forKey:kPostModelAuthorId];
    [mutableDict setValue:self.eventId forKey:kPostModelEventId];
    [mutableDict setValue:self.userSubscribed forKey:kPostModelUserSubscribed];
    [mutableDict setValue:self.broadcastName forKey:kPostModelBroadcastName];
    [mutableDict setValue:self.originId forKey:kPostModelOriginId];
    [mutableDict setValue:self.likes forKey:kPostModelLikes];
    [mutableDict setValue:self.feelingAction forKey:kPostModelFeelingAction];
    [mutableDict setValue:self.views forKey:kPostModelViews];
    [mutableDict setValue:self.postType forKey:kPostModelPostType];
    [mutableDict setValue:self.postAuthorUrl forKey:kPostModelPostAuthorUrl];
    [mutableDict setValue:self.inWall forKey:kPostModelInWall];
    [mutableDict setValue:self.inEvent forKey:kPostModelInEvent];
    [mutableDict setValue:self.userPictureId forKey:kPostModelUserPictureId];
    [mutableDict setValue:[NSNumber numberWithBool:self.iSave] forKey:kPostModelISave];
    [mutableDict setValue:self.inGroup forKey:kPostModelInGroup];
    [mutableDict setValue:self.feelingValue forKey:kPostModelFeelingValue];
    [mutableDict setValue:self.userGender forKey:kPostModelUserGender];
    [mutableDict setValue:[NSNumber numberWithBool:self.isPageAdmin] forKey:kPostModelIsPageAdmin];
    [mutableDict setValue:self.userId forKey:kPostModelUserId];
    [mutableDict setValue:[NSNumber numberWithBool:self.managePost] forKey:kPostModelManagePost];
    [mutableDict setValue:self.textPlain forKey:kPostModelTextPlain];
    [mutableDict setValue:self.groupId forKey:kPostModelGroupId];
    [mutableDict setValue:self.wallId forKey:kPostModelWallId];
    [mutableDict setValue:self.boosted forKey:kPostModelBoosted];
    [mutableDict setValue:self.postAuthorVerified forKey:kPostModelPostAuthorVerified];
    NSMutableArray *tempArrayForPhotos = [NSMutableArray array];
    for (NSObject *subArrayObject in self.photos) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPhotos addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPhotos addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPhotos] forKey:kPostModelPhotos];
    [mutableDict setValue:[NSNumber numberWithDouble:self.photosNum] forKey:kPostModelPhotosNum];
    [mutableDict setValue:self.postId forKey:kPostModelPostId];
    [mutableDict setValue:self.time forKey:kPostModelTime];
    [mutableDict setValue:self.formattedTime forKey:kPostModelFormattedTime];

    [mutableDict setValue:self.isBroadcast forKey:kPostModelIsBroadcast];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.userPicture = [aDecoder decodeObjectForKey:kPostModelUserPicture];
    self.postAuthorName = [aDecoder decodeObjectForKey:kPostModelPostAuthorName];
    self.userType = [aDecoder decodeObjectForKey:kPostModelUserType];
    self.pinned = [aDecoder decodeBoolForKey:kPostModelPinned];
    self.userName = [aDecoder decodeObjectForKey:kPostModelUserName];
    self.channelUrl = [aDecoder decodeObjectForKey:kPostModelChannelUrl];
    self.comments = [aDecoder decodeObjectForKey:kPostModelComments];
    self.location = [aDecoder decodeObjectForKey:kPostModelLocation];
    self.text = [aDecoder decodeObjectForKey:kPostModelText];
    self.userPinnedPost = [aDecoder decodeObjectForKey:kPostModelUserPinnedPost];
    self.isEventAdmin = [aDecoder decodeBoolForKey:kPostModelIsEventAdmin];
    self.iLike = [aDecoder decodeBoolForKey:kPostModelILike];
    self.userVerified = [aDecoder decodeObjectForKey:kPostModelUserVerified];
    self.postAuthorPicture = [aDecoder decodeObjectForKey:kPostModelPostAuthorPicture];
    self.userCoverId = [aDecoder decodeObjectForKey:kPostModelUserCoverId];
    self.userFirstname = [aDecoder decodeObjectForKey:kPostModelUserFirstname];
    self.isGroupAdmin = [aDecoder decodeBoolForKey:kPostModelIsGroupAdmin];
    self.privacy = [aDecoder decodeObjectForKey:kPostModelPrivacy];
    self.broadcastUrl = [aDecoder decodeObjectForKey:kPostModelBroadcastUrl];
    self.userLastname = [aDecoder decodeObjectForKey:kPostModelUserLastname];
    self.shares = [aDecoder decodeObjectForKey:kPostModelShares];
    self.postUserName = [aDecoder decodeObjectForKey:kPostModelPostUserName];
    self.authorId = [aDecoder decodeObjectForKey:kPostModelAuthorId];
    self.eventId = [aDecoder decodeObjectForKey:kPostModelEventId];
    self.userSubscribed = [aDecoder decodeObjectForKey:kPostModelUserSubscribed];
    self.broadcastName = [aDecoder decodeObjectForKey:kPostModelBroadcastName];
    self.originId = [aDecoder decodeObjectForKey:kPostModelOriginId];
    self.likes = [aDecoder decodeObjectForKey:kPostModelLikes];
    self.feelingAction = [aDecoder decodeObjectForKey:kPostModelFeelingAction];
    self.views = [aDecoder decodeObjectForKey:kPostModelViews];
    self.postType = [aDecoder decodeObjectForKey:kPostModelPostType];
    self.postAuthorUrl = [aDecoder decodeObjectForKey:kPostModelPostAuthorUrl];
    self.inWall = [aDecoder decodeObjectForKey:kPostModelInWall];
    self.inEvent = [aDecoder decodeObjectForKey:kPostModelInEvent];
    self.userPictureId = [aDecoder decodeObjectForKey:kPostModelUserPictureId];
    self.iSave = [aDecoder decodeBoolForKey:kPostModelISave];
    self.inGroup = [aDecoder decodeObjectForKey:kPostModelInGroup];
    self.feelingValue = [aDecoder decodeObjectForKey:kPostModelFeelingValue];
    self.userGender = [aDecoder decodeObjectForKey:kPostModelUserGender];
    self.isPageAdmin = [aDecoder decodeBoolForKey:kPostModelIsPageAdmin];
    self.userId = [aDecoder decodeObjectForKey:kPostModelUserId];
    self.managePost = [aDecoder decodeBoolForKey:kPostModelManagePost];
    self.textPlain = [aDecoder decodeObjectForKey:kPostModelTextPlain];
    self.groupId = [aDecoder decodeObjectForKey:kPostModelGroupId];
    self.wallId = [aDecoder decodeObjectForKey:kPostModelWallId];
    self.boosted = [aDecoder decodeObjectForKey:kPostModelBoosted];
    self.postAuthorVerified = [aDecoder decodeObjectForKey:kPostModelPostAuthorVerified];
    self.photos = [aDecoder decodeObjectForKey:kPostModelPhotos];
    self.photosNum = [aDecoder decodeDoubleForKey:kPostModelPhotosNum];
    self.postId = [aDecoder decodeObjectForKey:kPostModelPostId];
    self.time = [aDecoder decodeObjectForKey:kPostModelTime];
    self.formattedTime = [aDecoder decodeObjectForKey:kPostModelFormattedTime];

    self.isBroadcast = [aDecoder decodeObjectForKey:kPostModelIsBroadcast];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_userPicture forKey:kPostModelUserPicture];
    [aCoder encodeObject:_postAuthorName forKey:kPostModelPostAuthorName];
    [aCoder encodeObject:_userType forKey:kPostModelUserType];
    [aCoder encodeBool:_pinned forKey:kPostModelPinned];
    [aCoder encodeObject:_userName forKey:kPostModelUserName];
    [aCoder encodeObject:_channelUrl forKey:kPostModelChannelUrl];
    [aCoder encodeObject:_comments forKey:kPostModelComments];
    [aCoder encodeObject:_location forKey:kPostModelLocation];
    [aCoder encodeObject:_text forKey:kPostModelText];
    [aCoder encodeObject:_userPinnedPost forKey:kPostModelUserPinnedPost];
    [aCoder encodeBool:_isEventAdmin forKey:kPostModelIsEventAdmin];
    [aCoder encodeBool:_iLike forKey:kPostModelILike];
    [aCoder encodeObject:_userVerified forKey:kPostModelUserVerified];
    [aCoder encodeObject:_postAuthorPicture forKey:kPostModelPostAuthorPicture];
    [aCoder encodeObject:_userCoverId forKey:kPostModelUserCoverId];
    [aCoder encodeObject:_userFirstname forKey:kPostModelUserFirstname];
    [aCoder encodeBool:_isGroupAdmin forKey:kPostModelIsGroupAdmin];
    [aCoder encodeObject:_privacy forKey:kPostModelPrivacy];
    [aCoder encodeObject:_broadcastUrl forKey:kPostModelBroadcastUrl];
    [aCoder encodeObject:_userLastname forKey:kPostModelUserLastname];
    [aCoder encodeObject:_shares forKey:kPostModelShares];
    [aCoder encodeObject:_postUserName forKey:kPostModelPostUserName];
    [aCoder encodeObject:_authorId forKey:kPostModelAuthorId];
    [aCoder encodeObject:_eventId forKey:kPostModelEventId];
    [aCoder encodeObject:_userSubscribed forKey:kPostModelUserSubscribed];
    [aCoder encodeObject:_broadcastName forKey:kPostModelBroadcastName];
    [aCoder encodeObject:_originId forKey:kPostModelOriginId];
    [aCoder encodeObject:_likes forKey:kPostModelLikes];
    [aCoder encodeObject:_feelingAction forKey:kPostModelFeelingAction];
    [aCoder encodeObject:_views forKey:kPostModelViews];
    [aCoder encodeObject:_postType forKey:kPostModelPostType];
    [aCoder encodeObject:_postAuthorUrl forKey:kPostModelPostAuthorUrl];
    [aCoder encodeObject:_inWall forKey:kPostModelInWall];
    [aCoder encodeObject:_inEvent forKey:kPostModelInEvent];
    [aCoder encodeObject:_userPictureId forKey:kPostModelUserPictureId];
    [aCoder encodeBool:_iSave forKey:kPostModelISave];
    [aCoder encodeObject:_inGroup forKey:kPostModelInGroup];
    [aCoder encodeObject:_feelingValue forKey:kPostModelFeelingValue];
    [aCoder encodeObject:_userGender forKey:kPostModelUserGender];
    [aCoder encodeBool:_isPageAdmin forKey:kPostModelIsPageAdmin];
    [aCoder encodeObject:_userId forKey:kPostModelUserId];
    [aCoder encodeBool:_managePost forKey:kPostModelManagePost];
    [aCoder encodeObject:_textPlain forKey:kPostModelTextPlain];
    [aCoder encodeObject:_groupId forKey:kPostModelGroupId];
    [aCoder encodeObject:_wallId forKey:kPostModelWallId];
    [aCoder encodeObject:_boosted forKey:kPostModelBoosted];
    [aCoder encodeObject:_postAuthorVerified forKey:kPostModelPostAuthorVerified];
    [aCoder encodeObject:_photos forKey:kPostModelPhotos];
    [aCoder encodeDouble:_photosNum forKey:kPostModelPhotosNum];
    [aCoder encodeObject:_postId forKey:kPostModelPostId];
    [aCoder encodeObject:_time forKey:kPostModelTime];
    [aCoder encodeObject:_formattedTime forKey:kPostModelFormattedTime];

    [aCoder encodeObject:_isBroadcast forKey:kPostModelIsBroadcast];
}

- (id)copyWithZone:(NSZone *)zone
{
    PostModel *copy = [[PostModel alloc] init];
    
    if (copy) {

        copy.userPicture = [self.userPicture copyWithZone:zone];
        copy.postAuthorName = [self.postAuthorName copyWithZone:zone];
        copy.userType = [self.userType copyWithZone:zone];
        copy.pinned = self.pinned;
        copy.userName = [self.userName copyWithZone:zone];
        copy.channelUrl = [self.channelUrl copyWithZone:zone];
        copy.comments = [self.comments copyWithZone:zone];
        copy.location = [self.location copyWithZone:zone];
        copy.text = [self.text copyWithZone:zone];
        copy.userPinnedPost = [self.userPinnedPost copyWithZone:zone];
        copy.isEventAdmin = self.isEventAdmin;
        copy.iLike = self.iLike;
        copy.userVerified = [self.userVerified copyWithZone:zone];
        copy.postAuthorPicture = [self.postAuthorPicture copyWithZone:zone];
        copy.userCoverId = [self.userCoverId copyWithZone:zone];
        copy.userFirstname = [self.userFirstname copyWithZone:zone];
        copy.isGroupAdmin = self.isGroupAdmin;
        copy.privacy = [self.privacy copyWithZone:zone];
        copy.broadcastUrl = [self.broadcastUrl copyWithZone:zone];
        copy.userLastname = [self.userLastname copyWithZone:zone];
        copy.shares = [self.shares copyWithZone:zone];
        copy.postUserName = [self.postUserName copyWithZone:zone];
        copy.authorId = [self.authorId copyWithZone:zone];
        copy.eventId = [self.eventId copyWithZone:zone];
        copy.userSubscribed = [self.userSubscribed copyWithZone:zone];
        copy.broadcastName = [self.broadcastName copyWithZone:zone];
        copy.originId = [self.originId copyWithZone:zone];
        copy.likes = [self.likes copyWithZone:zone];
        copy.feelingAction = [self.feelingAction copyWithZone:zone];
        copy.views = [self.views copyWithZone:zone];
        copy.postType = [self.postType copyWithZone:zone];
        copy.postAuthorUrl = [self.postAuthorUrl copyWithZone:zone];
        copy.inWall = [self.inWall copyWithZone:zone];
        copy.inEvent = [self.inEvent copyWithZone:zone];
        copy.userPictureId = [self.userPictureId copyWithZone:zone];
        copy.iSave = self.iSave;
        copy.inGroup = [self.inGroup copyWithZone:zone];
        copy.feelingValue = [self.feelingValue copyWithZone:zone];
        copy.userGender = [self.userGender copyWithZone:zone];
        copy.isPageAdmin = self.isPageAdmin;
        copy.userId = [self.userId copyWithZone:zone];
        copy.managePost = self.managePost;
        copy.textPlain = [self.textPlain copyWithZone:zone];
        copy.groupId = [self.groupId copyWithZone:zone];
        copy.wallId = [self.wallId copyWithZone:zone];
        copy.boosted = [self.boosted copyWithZone:zone];
        copy.postAuthorVerified = [self.postAuthorVerified copyWithZone:zone];
        copy.photos = [self.photos copyWithZone:zone];
        copy.photosNum = self.photosNum;
        copy.postId = [self.postId copyWithZone:zone];
        copy.time = [self.time copyWithZone:zone];
        copy.formattedTime = [self.formattedTime copyWithZone:zone];

        copy.isBroadcast = [self.isBroadcast copyWithZone:zone];
    }
    
    return copy;
}


@end
