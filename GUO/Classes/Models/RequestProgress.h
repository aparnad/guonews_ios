//
//  RequestProgress.h
//  GUO Media
//
//  Created by Amol Hirkane on 27/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestProgress : NSObject
@property(nonatomic,assign) CGFloat progress;
@end
