//
//  FollowModel.m
//
//  Created by Amol Hirkane on 29/06/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "FollowModel.h"


NSString *const kFollowModelConnection = @"connection";
NSString *const kFollowModelIsFollowing = @"is_following";
NSString *const kFollowModelUserLastname = @"user_lastname";
NSString *const kFollowModelUserGender = @"user_gender";
NSString *const kFollowModelUserFirstname = @"user_firstname";
NSString *const kFollowModelUserId = @"user_id";
NSString *const kFollowModelUserName = @"user_name";
NSString *const kFollowModelUserPicture = @"user_picture";


@interface FollowModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FollowModel

@synthesize connection = _connection;
@synthesize isFollowing = _isFollowing;
@synthesize userLastname = _userLastname;
@synthesize userGender = _userGender;
@synthesize userFirstname = _userFirstname;
@synthesize userId = _userId;
@synthesize userName = _userName;
@synthesize userPicture = _userPicture;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.connection = [self objectOrNilForKey:kFollowModelConnection fromDictionary:dict];
            self.isFollowing = [[self objectOrNilForKey:kFollowModelIsFollowing fromDictionary:dict] boolValue];
            self.userLastname = [self objectOrNilForKey:kFollowModelUserLastname fromDictionary:dict];
            self.userGender = [self objectOrNilForKey:kFollowModelUserGender fromDictionary:dict];
            self.userFirstname = [self objectOrNilForKey:kFollowModelUserFirstname fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kFollowModelUserId fromDictionary:dict];
            self.userName = [self objectOrNilForKey:kFollowModelUserName fromDictionary:dict];
            self.userPicture = [self objectOrNilForKey:kFollowModelUserPicture fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.connection forKey:kFollowModelConnection];
    [mutableDict setValue:[NSNumber numberWithBool:self.isFollowing] forKey:kFollowModelIsFollowing];
    [mutableDict setValue:self.userLastname forKey:kFollowModelUserLastname];
    [mutableDict setValue:self.userGender forKey:kFollowModelUserGender];
    [mutableDict setValue:self.userFirstname forKey:kFollowModelUserFirstname];
    [mutableDict setValue:self.userId forKey:kFollowModelUserId];
    [mutableDict setValue:self.userName forKey:kFollowModelUserName];
    [mutableDict setValue:self.userPicture forKey:kFollowModelUserPicture];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.connection = [aDecoder decodeObjectForKey:kFollowModelConnection];
    self.isFollowing = [aDecoder decodeBoolForKey:kFollowModelIsFollowing];
    self.userLastname = [aDecoder decodeObjectForKey:kFollowModelUserLastname];
    self.userGender = [aDecoder decodeObjectForKey:kFollowModelUserGender];
    self.userFirstname = [aDecoder decodeObjectForKey:kFollowModelUserFirstname];
    self.userId = [aDecoder decodeObjectForKey:kFollowModelUserId];
    self.userName = [aDecoder decodeObjectForKey:kFollowModelUserName];
    self.userPicture = [aDecoder decodeObjectForKey:kFollowModelUserPicture];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_connection forKey:kFollowModelConnection];
    [aCoder encodeBool:_isFollowing forKey:kFollowModelIsFollowing];
    [aCoder encodeObject:_userLastname forKey:kFollowModelUserLastname];
    [aCoder encodeObject:_userGender forKey:kFollowModelUserGender];
    [aCoder encodeObject:_userFirstname forKey:kFollowModelUserFirstname];
    [aCoder encodeObject:_userId forKey:kFollowModelUserId];
    [aCoder encodeObject:_userName forKey:kFollowModelUserName];
    [aCoder encodeObject:_userPicture forKey:kFollowModelUserPicture];
}

- (id)copyWithZone:(NSZone *)zone
{
    FollowModel *copy = [[FollowModel alloc] init];
    
    if (copy) {

        copy.connection = [self.connection copyWithZone:zone];
        copy.isFollowing = self.isFollowing;
        copy.userLastname = [self.userLastname copyWithZone:zone];
        copy.userGender = [self.userGender copyWithZone:zone];
        copy.userFirstname = [self.userFirstname copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.userName = [self.userName copyWithZone:zone];
        copy.userPicture = [self.userPicture copyWithZone:zone];
    }
    
    return copy;
}


@end
