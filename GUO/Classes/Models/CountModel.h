//
//  CountModel.h
//
//  Created by Amol Hirkane on 25/06/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CountModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double posts;
@property (nonatomic, assign) double followings;
@property (nonatomic, assign) double followers;
@property (nonatomic, assign) double likes;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
