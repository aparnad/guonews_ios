//
//  GUOSettings.h
//  GUO
//
//  Created by mac on 04/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GUOSettings : NSObject
{
    
}
+(void)SetLanguage:(NSString*)str;
+(NSString*)GetLanguage;
+(void)setIsLogin:(UserModel*)model;
+(UserModel*)getIsLogin;

+(void)saveDeviceToken:(NSString *)deviceToken;
+(NSString *)getDeviceToken;
@end
