//
//  GUOSettings.m
//  GUO
//
//  Created by mac on 04/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "GUOSettings.h"

#define USER_DEFAULTS  [NSUserDefaults standardUserDefaults]
@implementation GUOSettings

+(void)SetLanguage:(NSString*)str
{
    [USER_DEFAULTS setValue:str forKey:@"AppleLanguages1"];
    [USER_DEFAULTS synchronize];
}
+(NSString*)GetLanguage
{
    return [USER_DEFAULTS objectForKey:@"AppleLanguages1"];
//    NSArray *arr=[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages1"];
//    NSLog(@"********:%@",arr);
//    if(arr.count>0)
//    {
//        return [arr objectAtIndex:0];
//    }
//    else
//    {
//        return  @"en";
//    }
//    return [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
}

+(void)setIsLogin:(UserModel*)model
{
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:model];
    [USER_DEFAULTS setObject:myEncodedObject forKey:@"user_model"];

}
+(UserModel*)getIsLogin
{
    NSData *myEncodedObject = [USER_DEFAULTS objectForKey:@"user_model"];
    UserModel *obj = (UserModel *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
    return obj;
}

+(void)saveDeviceToken:(NSString *)deviceToken
{
    [USER_DEFAULTS setValue:deviceToken forKey:@"DeviceToken"];
    [USER_DEFAULTS synchronize];
}

+(NSString *)getDeviceToken
{
    if([[USER_DEFAULTS valueForKey:@"DeviceToken"] length]>0)
    {
        return [USER_DEFAULTS valueForKey:@"DeviceToken"];
    }
    else
    {
        return @"";
    }
}

@end
