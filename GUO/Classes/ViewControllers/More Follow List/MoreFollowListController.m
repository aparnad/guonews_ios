//
//  MoreFollowListController.m
//  GUO
//
//  Created by mac on 07/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "MoreFollowListController.h"

@interface MoreFollowListController ()
{
    DGActivityIndicatorView *activityIndicatorView;
    UILabel *lblMessage;
    UIRefreshControl *refreshControl;
}

@end

@implementation MoreFollowListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = light_gray_bg_color;
    self.aTableView.backgroundColor = light_gray_bg_color;
    self.viewFollowers.backgroundColor = light_gray_bg_color;
    
    
    self.aTableView.backgroundView = [self tableBGView];
    
 //   [Utils RemoveTableViewHeaderFooterSpace:self.aTableView];
  //  [Utils SearchContainerDesign:self.txtSearch ContainerView:self.view];
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];

    self.lblHeader.text =self.header_str;
    ArrFollow = [[NSMutableArray alloc]init];
    
    refreshControl=[[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshList:) forControlEvents:UIControlEventValueChanged];
    [self.aTableView addSubview:refreshControl];
  
    if([self.screen_identifier isEqualToString:followingscreen])
    {
        [self getFollowers_Following_ListAPI:API_FOLLOWINGS_LIST];
        refreshFooter=[[YiRefreshFooter alloc] init];
        
        __weak typeof(self) weakSelf = self;
        refreshFooter.beginRefreshingBlock=^(){
            
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                sleep(pullDownToRefreshSleepCount);
                [weakSelf getFollowers_Following_ListAPI:API_FOLLOWINGS_LIST];
            });
        };
    }
    else if([self.screen_identifier isEqualToString:followersscreen])
    {
        [self getFollowers_Following_ListAPI:API_FOLLOWERS_LIST];
        refreshFooter=[[YiRefreshFooter alloc] init];
        
        __weak typeof(self) weakSelf = self;
        refreshFooter.beginRefreshingBlock=^(){
            
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                sleep(pullDownToRefreshSleepCount);
                [weakSelf getFollowers_Following_ListAPI:API_FOLLOWINGS_LIST];
            });
        };
    }
    else
    {
        
    }

    
    if(!(AppObj).is_first_time_add_observer_people)
    {
        (AppObj).is_first_time_add_observer_people = true;

        
    }
    
   // [self.txtSearch addTarget:self action:@selector(textField1Active:) forControlEvents:UIControlEventEditingDidBegin];
//    [self.txtSearch addTarget:self
//                  action:@selector(myTextFieldDidChange:)
//        forControlEvents:UIControlEventEditingChanged];
    // Do any additional setup after loading the view.
}
-(void)globalSearchTextChanged1:(NSNotification*)noti
{
    
    if(noti!=nil)
    {
        NSDictionary *detailDic= noti.object;
        _api_name  = API_GLOBAL_SEARCH;
        NSString *searchText=[detailDic objectForKey:@"search_text"];
        if (searchText != nil)
        {
            current_page = 1;
            self.searchParam = @{@"get":_api_name,
                       @"type":[detailDic objectForKey:@"type"],
                       @"keyword":searchText,
                       @"limit":RecordLimit,
                       @"page":[NSString stringWithFormat:@"%d",current_page],
                       @"user_id":UTILS.currentUser.userId};

            [self getFollowers_Following_ListAPI:_api_name];
        }
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)refreshList:(UIRefreshControl *)refreshControl
{
    
    current_page = 1;
//    [UTILS ShowProgress];
    
    if([self.screen_identifier isEqualToString:followingscreen])
    {
        [self getFollowers_Following_ListAPI:API_FOLLOWINGS_LIST];
    }
    else if([self.screen_identifier isEqualToString:followersscreen])
    {
        [self getFollowers_Following_ListAPI:API_FOLLOWERS_LIST];
    }
    else
    {
        
    }
}
#pragma mark- API calling

-(void)getFollowers_Following_ListAPI:(NSString*)API_NAME
{
    
    [self apiCallInitiated];
    if(ArrFollow.count == 0)
    {
       // [UTILS ShowProgress];
        current_page = 1;
    }
    NSDictionary *params;
    if(is_search_open)
    {
        
        params = @{
                   @"get":API_NAME,
                   @"user_id":self.get_user_id,
                   @"keyword":@"",
                   @"limit":RecordLimit,
                   @"page":[NSString stringWithFormat:@"%d",current_page],
                   @"me_id":UTILS.currentUser.userId
                   };

    }
    else
    {
        params = @{
                     @"get":API_NAME,
                     @"user_id":self.get_user_id,
                     @"me_id":UTILS.currentUser.userId,
                     @"limit":RecordLimit,
                     @"page":[NSString stringWithFormat:@"%d",current_page]
                    };
       
    }
    if(self.api_name.length)
    {
        params = [[NSDictionary alloc]initWithDictionary:self.searchParam];
    }
    NSLog(@"params:%@",params);
    [REMOTE_API CallPOSTWebServiceWithParam:API_NAME params:params sBlock:^(id responseObject) {
       
       
        if(current_page == 1)
        {
            [ArrFollow removeAllObjects];
            [self RemovePullDownToRefresh];
        }
        
        if(is_api_call)
        {
            NSLog(@"**********************");

            [ArrFollow removeAllObjects];
            [self RemovePullDownToRefresh];
        }
        [ArrFollow addObjectsFromArray:responseObject];
        [self.aTableView reloadData];
        [Utils HideProgress];
        [refreshControl endRefreshing];

        
        if([responseObject count] == 0)
        {
            [self RemovePullDownToRefresh];
        }
       else if([ArrFollow  count] >= [RecordLimit intValue])
       {
           [self AddPullDowntoRefresh];
       }
        
        [self apiCallEndWithResultCount:[ArrFollow count] pageNo:current_page];
        
        if([responseObject count] >0)
        {
            is_api_call =false;
            current_page += 1;
        }

    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
    //    [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        [self RemovePullDownToRefresh];
        [refreshControl endRefreshing];
       
        [self apiCallEndWithResultCount:0 pageNo:1];

    }];
    
}

#pragma mark - Tableview Delegate and Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
//-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return UITableViewAutomaticDimension;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
//{
//    return self.viewFollowers.frame.size.height;
//}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    return self.viewFollowers;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrFollow.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ConnectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ConnectionCell"];
    if(cell==nil)
    {
        cell=[[ConnectionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ConnectionCell"];
        
        NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"ConnectionCell" owner:self options:nil];
        cell=[menuarray objectAtIndex:0];
    }
    
    FollowModel *model = [[FollowModel alloc]init];
    model = [ArrFollow objectAtIndex:indexPath.row];
 //  [cell.imgUser setImageWithURL:[NSURL URLWithString:model.userPicture] placeholderImage:defaultUserImg];
    
    
    [cell.imgUser sd_setImageWithURL:[Utils getThumbUrl:model.userPicture] placeholderImage:defaultUserImg];

    
    cell.lblName.text = [Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName];
    cell.lblDesc.text = nil;
    
    cell.selectionStyle = NO;
    cell.btnFollow.tag = indexPath.row;
    cell.btnUserImg.tag = indexPath.row;
    
    [cell.btnUserImg addTarget:self action:@selector(onclick_view_user_profile:) forControlEvents:UIControlEventTouchUpInside];

    [cell.btnFollow addTarget:self action:@selector(onclick_follow:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnMore addTarget:self action:@selector(onclick_more:) forControlEvents:UIControlEventTouchUpInside];

    cell.selectionStyle = NO;

    NSString *status_title = NSLocalizedString(@"btn_follow", nil);
    
    //default color
    cell.btnFollow.backgroundColor = [UIColor whiteColor];
    [cell.btnFollow setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  
    
    if(model.isFollowing)
    {
        status_title = NSLocalizedString(@"txt_following", nil);
        cell.btnFollow.backgroundColor = header_color;
        [cell.btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    [cell.btnFollow setTitle:status_title forState:UIControlStateNormal];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{

    
    FollowModel *model = [[FollowModel alloc]init];
    model = [ArrFollow objectAtIndex:indexPath.row];
    
    [(AppObj).postView showUserProfile:model.userId];
}
#pragma mark- Button click`
-(IBAction)onclick_view_user_profile:(UIButton*)sender
{
    NSIndexPath *nowIndex = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;

    FollowModel *model = [[FollowModel alloc]init];
    model = [ArrFollow objectAtIndex:nowIndex.row];

    

}
-(IBAction)onclick_follow:(UIButton*)sender
{
    NSIndexPath *nowIndex = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;
    ConnectionCell *cell = [self.aTableView cellForRowAtIndexPath:nowIndex];
    FollowModel *model = [[FollowModel alloc]init];
    model = [ArrFollow objectAtIndex:nowIndex.row];
    if(model.isFollowing)
    {
        [cell follow_unfollow_API:API_UNFOLLOW model:model];
    }
    else
    {
        [cell follow_unfollow_API:API_FOLLOW model:model];
    }
    
   
    [cell updateCommentBlock:^(FollowModel *f_model) {
//        NSLog(@"f_model:%@",f_model.userName);
        [self.aTableView reloadRowsAtIndexPaths:@[nowIndex] withRowAnimation:UITableViewRowAnimationNone];
    }];
    

//    [self follow_unfollow_API:API_FOLLOW model:model];
    
    [self.aTableView reloadData];
}
-(IBAction)onclick_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)onclick_more:(UIButton*)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"message_dialog_cancel", nil).capitalizedString style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"block_connection", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

#pragma mark- Textfield delegate method
-(void)myTextFieldDidChange:(UITextField *)textField{
    oldStr = textField.text;
    if([[Utils RemoveWhiteSpaceFromText:textField.text] length]>0)
    {
        is_api_call = true;
        is_search_open = true;
        current_page = 1;
        if([self.screen_identifier isEqualToString:followingscreen])
        {
            [self getFollowers_Following_ListAPI:API_FOLLOWINGS_LIST];
        }
        else if([self.screen_identifier isEqualToString:followersscreen])
        {
            [self getFollowers_Following_ListAPI:API_FOLLOWERS_LIST];
        }
    }
    else
    {
        current_page = 1;
        [ArrFollow removeAllObjects];
        [self RemovePullDownToRefresh];
        [self.aTableView reloadData];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
   
    return YES;
}
-(void)textField1Active:(UITextField *)textField {
    if(!is_search_open)
    {
        storePrevArr = [[NSMutableArray alloc]initWithArray:ArrFollow];
        is_search_open = true;
        storeCurrentPage = current_page;
//        ArrFollow = [[NSMutableArray alloc]init];
        current_page = 1;
        
        [self.aTableView reloadData];
    }
    
    

}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = nil;
    is_search_open = false;
    ArrFollow = [[NSMutableArray alloc]initWithArray:storePrevArr];
    current_page = storeCurrentPage;

    [self.aTableView reloadData];
    [textField resignFirstResponder];
    return NO;
}
#pragma mark- Remove pull down to refresh
-(void)RemovePullDownToRefresh
{
    //Remove Pull down to refresh when category_total_count match with array
    
    [refreshFooter remove_observer];
    
    refreshFooter.scrollView=nil;
    [refreshFooter.footerView removeFromSuperview];
   // self.aTableView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    
}
-(void)AddPullDowntoRefresh
{
    [self RemovePullDownToRefresh];
    refreshFooter.scrollView=self.aTableView;
    [refreshFooter footer];
}

-(UIView *)tableBGView
{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64)];
    
    lblMessage = [[UILabel alloc] initWithFrame:bgView.frame];
    lblMessage.textAlignment = NSTextAlignmentCenter;
    lblMessage.backgroundColor = [UIColor clearColor];
    lblMessage.font = NO_DATE_FOUND_FONT;
    lblMessage.lineBreakMode = NSLineBreakByWordWrapping;
    lblMessage.numberOfLines = 0;
    [bgView addSubview:lblMessage];
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
    activityIndicatorView.backgroundColor = [UIColor clearColor];
    activityIndicatorView.frame = CGRectMake(CGRectGetMidX(bgView.frame)-50, CGRectGetMidY(bgView.frame)-25, 100, 50);
    [bgView addSubview:activityIndicatorView];
    
    return bgView;
}
-(void)apiCallInitiated
{
    [activityIndicatorView startAnimating];
    lblMessage.text = @"";
}
-(void)apiCallEndWithResultCount:(NSInteger)count  pageNo:(NSInteger)pageNo
{
    [activityIndicatorView stopAnimating];
    
    if (pageNo == 1 && count == 0) {
        lblMessage.text = NSLocalizedString(@"No data found", nil);
    }
    else
    {
        lblMessage.text = @"";
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
