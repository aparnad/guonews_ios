//
//  MoreFollowListController.h
//  GUO
//
//  Created by mac on 07/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectionCell.h"
#import "FollowModel.h"
#import "YiRefreshFooter.h"


@interface MoreFollowListController : UIViewController
{
    NSMutableArray *ArrFollow;
    YiRefreshFooter *refreshFooter;
    int current_page,storeCurrentPage;
    BOOL is_search_open,is_api_call;
    NSMutableArray *storePrevArr;
    NSString *oldStr;
}
@property(strong,nonatomic) IBOutlet UITableView *aTableView;
@property(strong,nonatomic) IBOutlet UIView *headerView,*viewFollowers;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property(strong,nonatomic) NSString *header_str,*screen_identifier;
-(IBAction)onclick_back:(id)sender;
@property NSString *get_user_id;

@property NSString *api_name;
@property NSDictionary *searchParam;
@end
//MoreFollowListController
