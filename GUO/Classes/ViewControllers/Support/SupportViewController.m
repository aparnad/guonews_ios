//
//  SupportViewController.m
//  GUO Media
//
//  Created by Pawan Ramteke on 30/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "SupportViewController.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"
@interface SupportViewController ()
{
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet UILabel *lblHeader;
    
    __weak IBOutlet JVFloatLabeledTextField *txtFieldTo;
    __weak IBOutlet JVFloatLabeledTextField *txtFieldFrom;
    __weak IBOutlet JVFloatLabeledTextField *txtFieldemail;
    __weak IBOutlet JVFloatLabeledTextField *txtFieldSubject;
    __weak IBOutlet JVFloatLabeledTextView *txtFieldMessage;
    __weak IBOutlet UIButton *btnSubmit;
}
@end

@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor controllerBGColor];
    lblHeader.text = NSLocalizedString(@"nav_contact_us", nil);
    [Utils SetViewHeader:headerView headerLbl:lblHeader];
    
    txtFieldTo.text = @"support@guo.media";
    txtFieldFrom.text = UTILS.currentUser.userName;
    txtFieldemail.text = UTILS.currentUser.userEmail;
    txtFieldTo.font = [UIFont fontWithName:Font_regular size:16];
    txtFieldTo.floatingLabelActiveTextColor = header_color;
    txtFieldTo.floatingLabelFont = [UIFont fontWithName:Font_regular size:14];
    txtFieldTo.placeholder = NSLocalizedString(@"To", nil);
    
    txtFieldFrom.font = txtFieldTo.font;
    txtFieldFrom.floatingLabelActiveTextColor = txtFieldTo.floatingLabelActiveTextColor;
    txtFieldFrom.floatingLabelFont = txtFieldTo.floatingLabelFont;
    txtFieldFrom.placeholder = NSLocalizedString(@"From", nil);
    
    txtFieldemail.font = txtFieldTo.font;
    txtFieldemail.floatingLabelActiveTextColor = txtFieldTo.floatingLabelActiveTextColor;
    txtFieldemail.floatingLabelFont = txtFieldTo.floatingLabelFont;
    txtFieldemail.placeholder = NSLocalizedString(@"Your Email", nil);

    txtFieldSubject.font = txtFieldTo.font;
    txtFieldSubject.floatingLabelActiveTextColor = txtFieldTo.floatingLabelActiveTextColor;
    txtFieldSubject.floatingLabelFont = txtFieldTo.floatingLabelFont;
    txtFieldSubject.placeholder = NSLocalizedString(@"Subject", nil);

    txtFieldMessage.font = txtFieldTo.font;
    txtFieldMessage.floatingLabelActiveTextColor = txtFieldTo.floatingLabelActiveTextColor;
    txtFieldMessage.floatingLabelFont = txtFieldTo.floatingLabelFont;
    
    txtFieldMessage.placeholder = NSLocalizedString(@"Message", nil);

    btnSubmit.titleLabel.font = [UIFont fontWithName:Font_regular size:18];
    btnSubmit.backgroundColor = header_color;
    [btnSubmit setTitle:NSLocalizedString(@"Submit", nil) forState:UIControlStateNormal];
    [Utils makeHalfRoundedButton:btnSubmit];
    [btnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)btnSubmitClicked:(id)sender {
    [self.view endEditing:YES];
    if ([self validate]) {
        
        
        if (![UTILS checkIntenetShowError]) {
            return;
        }
        
        
        [UTILS ShowProgress];
        NSDictionary *param = @{
                                @"get":API_SUPPORT,
                                @"user_id":UTILS.currentUser.userId,
                                @"user_email":txtFieldemail.text,
                                @"subject":txtFieldSubject.text,
                                @"message":txtFieldMessage.text
                                };
        [REMOTE_API CallPOSTWebServiceWithParam:API_SUPPORT params:param sBlock:^(id responseObject) {
            [CustomAlertView ShowAlert:@"" withMessage:responseObject tapHandler:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
            [Utils HideProgress];
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
            [[Toast sharedInstance] makeToast:customErrorMsg position:TOAST_POSITION_BOTTOM];
        }];
                                
    }
}

-(BOOL)validate
{
    if([Utils RemoveWhiteSpaceFromText:txtFieldemail.text].length==0)
    {
        [CustomAlertView showAlert:@"" withMessage:NSLocalizedString(@"Please Enter Email", nil)];
        return NO;
    }
    
    if(![Utils EmailVerification:[Utils RemoveWhiteSpaceFromText:txtFieldemail.text]])
    {
        [CustomAlertView showAlert:@"" withMessage:NSLocalizedString(@"Please Enter Valid Email", nil)];
        return NO;
    }
    if([Utils RemoveWhiteSpaceFromText:txtFieldSubject.text].length==0)
    {
        [CustomAlertView showAlert:@"" withMessage:NSLocalizedString(@"Please Enter Subject", nil)];
        return NO;
    }
    if([Utils RemoveWhiteSpaceFromText:txtFieldMessage.text].length==0)
    {
        [CustomAlertView showAlert:@"" withMessage:NSLocalizedString(@"Please Enter Message", nil)];
        return NO;
    }
    return YES;
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
