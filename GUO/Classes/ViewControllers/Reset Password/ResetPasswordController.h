//
//  ResetPasswordController.h
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface ResetPasswordController : UIViewController
{
    
}
@property(strong,nonatomic) IBOutlet  TPKeyboardAvoidingScrollView *aScrollView;
@property(strong,nonatomic) IBOutlet UITextField *txtOldPwd,*txtNewPwd,*txtConfirmPwd;
@property(strong,nonatomic) IBOutlet UIButton *btnSave;

-(IBAction)onclick_back:(id)sender;
-(IBAction)onclick_save:(id)sender;
    
@property(strong,nonatomic) IBOutlet UILabel *lblSlogan;

@end
