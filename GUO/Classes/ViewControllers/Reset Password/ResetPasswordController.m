//
//  ResetPasswordController.m
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ResetPasswordController.h"

@interface ResetPasswordController ()

@end

@implementation ResetPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialization];
    // Do any additional setup after loading the view from its nib.
}
#pragma mark - Initialization of Data
-(void)initialization
{
    
    
    self.view.backgroundColor = view_BGColor;
    
    [Utils SetTextFieldProperties:self.txtOldPwd placeholder_txt:NSLocalizedString(@"old_pass", nil)];
    [Utils SetTextFieldProperties:self.txtNewPwd placeholder_txt:NSLocalizedString(@"new_pass", nil)];
    [Utils SetTextFieldProperties:self.txtConfirmPwd placeholder_txt:NSLocalizedString(@"conform_pass", nil)];

    
    [Utils SetButtonProperties:self.btnSave txt:NSLocalizedString(@"btn_save", nil)];
    
    self.lblSlogan.text = NSLocalizedString(@"guo_msg", nil);

    
}
#pragma mark - Button Click
-(IBAction)onclick_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)onclick_save:(id)sender
{
    if(![self validation])
    {
        //        DashboardController *CommonDocumentControllerNav = [(AppObj).monika_storyboard  instantiateViewControllerWithIdentifier:@"DashboardController"];
        //        [self.navigationController pushViewController:CommonDocumentControllerNav animated:YES];
        
//        Input :{
//            "query":1234,
//            "get":"change_password",
//            "user_id":54555,
//            "new":"chetan2006",
//            "confirm":"chetan2006"
//        }

        NSDictionary *params = @{
                                 @"get":API_CHANGE_PASSWORD,
                                 @"user_id":UTILS.currentUser.userId,
                                 @"new":[Utils RemoveWhiteSpaceFromText:self.txtNewPwd.text],
                                 @"confirm":[Utils RemoveWhiteSpaceFromText:self.txtConfirmPwd.text],
                                 };
        
        [UTILS ShowProgress];
        [REMOTE_API CallPOSTWebServiceWithParam:API_CHANGE_PASSWORD params:params sBlock:^(id responseObject) {
            
            self.txtOldPwd.text = nil;
            self.txtNewPwd.text = nil;

            self.txtConfirmPwd.text = nil;

            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:NSLocalizedString(@"Alert", nil)
                                         message:NSLocalizedString(@"pwd reset successfully", nil)
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"Ok", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            
                                            [self onclick_back:self];
                                        }];
            
            
            
            //Add your buttons to alert controller
            
            [alert addAction:yesButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
            [Utils HideProgress];
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
            [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        }];
        
        
    }
}

#pragma mark- validation function
-(BOOL)validation
{
    NSString *str=@"";
    
    if([Utils RemoveWhiteSpaceFromText:self.txtOldPwd.text].length==0)
    {
        str=[NSString stringWithFormat:NSLocalizedString(@"Please Enter Old Password", nil)];
    }
    if([Utils RemoveWhiteSpaceFromText:self.txtNewPwd.text].length==0)
    {
        str=[NSString stringWithFormat:@"%@\n%@",str,NSLocalizedString(@"Please Enter New Password", nil)];
    }
    if([Utils RemoveWhiteSpaceFromText:self.txtConfirmPwd.text].length==0)
    {
        str=[NSString stringWithFormat:@"%@\n%@",str,NSLocalizedString(@"Please Enter Confirm New Password", nil)];
    }
    
    else if(![[Utils RemoveWhiteSpaceFromText:self.txtConfirmPwd.text] isEqualToString:[Utils RemoveWhiteSpaceFromText:self.txtNewPwd.text]])
    {
        str=[NSString stringWithFormat:@"%@\n%@",str,NSLocalizedString(@"Password doesn't match", nil)];
    }
    else if (!(([Utils RemoveWhiteSpaceFromText:self.txtNewPwd.text].length >=6 && [Utils RemoveWhiteSpaceFromText:self.txtNewPwd.text].length <= 12) || ([Utils RemoveWhiteSpaceFromText:self.txtConfirmPwd.text].length >=6 && [Utils RemoveWhiteSpaceFromText:self.txtConfirmPwd.text].length <= 12)))
    {
        str=[NSString stringWithFormat:@"%@\n%@",str,NSLocalizedString(@"Password length must be of 6-12", nil)];
    }
    if([str isEqualToString:@""])
    {
        return false;
    }
    else
    {
        
        if([str hasPrefix:@"\n"])
        {
            str=[str substringFromIndex:1];
        }
        [Utils ShowAlert:str];
        
        return true;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
