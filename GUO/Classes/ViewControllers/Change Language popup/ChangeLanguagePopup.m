//
//  ChangeLanguagePopup.m
//  GUO
//
//  Created by mac on 12/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ChangeLanguagePopup.h"

@interface ChangeLanguagePopup ()

@end

@implementation ChangeLanguagePopup

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.frame = CGRectMake(0, 0, 300 , 200);
    self.view.layer.cornerRadius = 8;
    self.view.layer.masksToBounds = YES;
    self.lblEnglish.text = NSLocalizedString(@"english_text", nil);
    self.lblChinese.text = NSLocalizedString(@"chinese_text", nil);
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];
    if([[NSString stringWithFormat:@"%@",[GUOSettings GetLanguage]] isEqualToString:[NSString stringWithFormat:@"%@",SetEnglish]])
    {
        
        [self.imgEnglish setImage:[Utils setTintColorToUIImage:[UIImage imageNamed:@"radio-on-button"] color:header_color]];
        [self.imgChinese setImage:[Utils setTintColorToUIImage:[UIImage imageNamed:@"radio-off-button"] color:header_color]];
        lang_str = @"english";

    }
    else
    {
        [self.imgEnglish setImage:[Utils setTintColorToUIImage:[UIImage imageNamed:@"radio-off-button"] color:header_color]];
        [self.imgChinese setImage:[Utils setTintColorToUIImage:[UIImage imageNamed:@"radio-on-button"] color:header_color]];
        lang_str = @"chienese";

    
    }
    [Utils makeHalfRoundedButton:self.btnSave];
    self.btnSave.backgroundColor = header_color;
    [self.btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnSave setTitle:NSLocalizedString(@"btn_save", nil) forState:UIControlStateNormal];
    self.lblChinese.font = self.lblEnglish.font = [UIFont fontWithName:Font_regular size:btn_font_size];
    
    self.lblHeader.text = NSLocalizedString(@"nav_change_language", nil);

    // Do any additional setup after loading the view from its nib.
}
#pragma mark - button click event
-(IBAction)onclick_english:(id)sender
{
    lang_str = @"english";
    [self.imgEnglish setImage:[Utils setTintColorToUIImage:[UIImage imageNamed:@"radio-on-button"] color:header_color]];
    [self.imgChinese setImage:[Utils setTintColorToUIImage:[UIImage imageNamed:@"radio-off-button"] color:header_color]];

}
-(IBAction)onclick_chinese:(id)sender
{
    lang_str = @"chienese";
    [self.imgEnglish setImage:[Utils setTintColorToUIImage:[UIImage imageNamed:@"radio-off-button"] color:header_color]];
    [self.imgChinese setImage:[Utils setTintColorToUIImage:[UIImage imageNamed:@"radio-on-button"] color:header_color]];
}


-(IBAction)onclick_save:(id)sender
{
    
    if([lang_str isEqualToString:@"english"])
    {
        [NSBundle setLanguage:SetEnglish];
        [GUOSettings SetLanguage:SetEnglish];
    }
    else if([lang_str isEqualToString:@"chienese"])
    {
        [NSBundle setLanguage:SetChinese];
        [GUOSettings SetLanguage:SetChinese];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Update_post_screen_control" object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Update_connection_screen_control" object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Update_message_screen_control" object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Update_notification_screen_control" object:self userInfo:nil];
    


    [[NSNotificationCenter defaultCenter] postNotificationName:@"Update_messages_screen_control" object:self userInfo:nil];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateMessagefield" object:self userInfo:nil];
    
    //Guo News Notifications
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Update_news_screen" object:self userInfo:nil];
   
    

    [self.view dismissPresentingPopup];
    
    
    [UTILS setUserLanguageOnServer];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
