//
//  ChangeLanguagePopup.h
//  GUO
//
//  Created by mac on 12/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeLanguagePopup : UIViewController
{
    NSString *lang_str;
}

@property(strong,nonatomic) IBOutlet UIView *headerView;
;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader,*lblEnglish,*lblChinese;
@property(strong,nonatomic) IBOutlet UIImageView *imgEnglish,*imgChinese;

@property(strong,nonatomic) IBOutlet UIButton *btnSave;

-(IBAction)onclick_save:(id)sender;
@end
