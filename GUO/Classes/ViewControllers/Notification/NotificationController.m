//
//  NotificationController.m
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "NotificationController.h"
#import "PostDetailsController.h"
#import "CreatePostController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "ALChatManager.h"

@interface NotificationController ()<UIScrollViewDelegate>
{
    UIRefreshControl *refreshControl;
    DGActivityIndicatorView *activityIndicatorView;
    UILabel *lblMessage;
    NSString *read_FromId;
    NSString *read_ToID;
    int lastReadIndex;
}
@end

@implementation NotificationController

- (void)viewDidLoad {
    [super viewDidLoad];

    _aTableView.estimatedRowHeight = 500;
    _aTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.view.backgroundColor = light_gray_bg_color;
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];
    _aTableView.backgroundView = [self tableBGView];

    //Cell Height Dic init
    [Utils RemoveTableViewHeaderFooterSpace:self.aTableView];
    notificationsArr = [[NSMutableArray alloc]init];
    
    
    [self.view addGestureRecognizer:[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureRecognized:)]];
    
    [Utils SetRoundedCorner:self.btn_header_userimg];
   // self.btn_header_userimg.layer.borderWidth = header_user_img_border;
   // self.btn_header_userimg.layer.borderColor = [UIColor whiteColor].CGColor;

    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Update_notification_screen_control" object:nil];//remove single noti
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocalization) name:@"Update_notification_screen_control" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateListOnLangChange) name:@"Update_notification_screen_list" object:nil];

    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(newNotification:) name:NEW_PUSH_NOTIFICATION object:nil];
    
    [self userNotificationListAPI];
    
    
    refreshControl=[[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshList:) forControlEvents:UIControlEventValueChanged];
    [_aTableView addSubview:refreshControl];
    
    refreshFooter=[[YiRefreshFooter alloc] init];
    
    __weak typeof(self) weakSelf = self;
    refreshFooter.beginRefreshingBlock=^(){
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            sleep(pullDownToRefreshSleepCount);
            [weakSelf userNotificationListAPI];
        });
    };
    
    
    [self.btnNews addTarget:self action:@selector(btnNewsClicked) forControlEvents:UIControlEventTouchUpInside];
    
   // [UTILS addFloating:self.view tab_height:0 screen_name:nil];

    [self addFlotingButton];

}


-(void)addFlotingButton
{
    UIButton *btnNewMessage = [[UIButton alloc]init];
    btnNewMessage.backgroundColor = floating_button_bg_color;
    btnNewMessage.layer.cornerRadius = 32.5;
    [btnNewMessage setImage:[UIImage imageNamed:@"tab_post"] forState:UIControlStateNormal];
    [self.view addSubview:btnNewMessage];
    
    [btnNewMessage enableAutolayout];
    [btnNewMessage trailingMargin:10];
    [btnNewMessage bottomMargin:10];
    [btnNewMessage fixWidth:65];
    [btnNewMessage fixHeight:65];
    [btnNewMessage addTarget:self action:@selector(btnNewPostClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(void)btnNewPostClicked
{
    CreatePostController *createPostVC = [(AppObj).pawan_storyboard  instantiateViewControllerWithIdentifier:@"CreatePostController"];
    [[UTILS topMostControllerNormal].navigationController pushViewController:createPostVC
                                                                    animated:YES];
    
    
    [createPostVC onPostSuccess:^(PostModel *model) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UPLOAD_SUCCESS_NOTIFICATION object:model];
    }];
}

-(void)btnNewsClicked
{
    [[Toast sharedInstance]makeToast:NSLocalizedString(@"Coming Soon", nil) position:TOAST_POSITION_BOTTOM];
    
}
-(void)refreshList:(UIRefreshControl *)refreshControl
{
    current_page = 1;
//    [UTILS ShowProgress];
    
    [self userNotificationListAPI];
}

-(void)setMaxNotificationIdAPI:(NSString *)notifcationId
{
    NSDictionary *param = @{
                            @"get":API_SET_MAX_NOTIFICATION_ID,
                            @"notification_id":notifcationId,
                            @"to_user_id":UTILS.currentUser.userId
                            };
    [REMOTE_API CallPOSTWebServiceWithParam:API_SET_MAX_NOTIFICATION_ID params:param sBlock:^(id responseObject) {
        //reset application badge count
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
    }];
}

-(void)userNotificationListAPI
{
    
    
    [self apiCallInitiated];
    if(notificationsArr.count == 0)
    {
       // [UTILS ShowProgress];
        current_page = 1;
    }
    /*NSDictionary *params = @{
     @"get":API_GET_POST,
     @"user_id":UTILS.currentUser.userId,
     @"limit":RecordLimit,
     @"page":[NSString stringWithFormat:@"%d",current_page]
     };*/

    NSDictionary *params = @{
                             @"get":API_GET_NOTIFICATION,
                             @"user_id":UTILS.currentUser.userId,
                             @"limit":RecordLimit,
                             @"page":[NSString stringWithFormat:@"%d",current_page]
                             };
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_GET_NOTIFICATION params:params sBlock:^(id responseObject) {
        
        if(current_page == 1)
        {
            notificationsArr = [[NSMutableArray alloc]init];
        }
        
        
        [notificationsArr addObjectsFromArray:responseObject];
        [refreshControl endRefreshing];
        
        if (notificationsArr.count) {
            NotificationModel *model = notificationsArr[0];
            read_FromId = model.notificationId;
           // if (self.navigationController.tabBarItem.badgeValue > 0) {
                [self setMaxNotificationIdAPI:read_FromId];
           // }
        }
        
       
        
        if([responseObject count] == 0)
        {
            [self RemovePullDownToRefresh];
        }
        else if([notificationsArr  count] >= [RecordLimit intValue])
        {
            [self AddPullDowntoRefresh];
        }
        
        [self.aTableView reloadData];
      //  [Utils HideProgress];
        
        [self apiCallEndWithResultCount:[notificationsArr count] pageNo:current_page];
        if([responseObject count] >0)
        {
            current_page += 1;
        }
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        //[Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        [self RemovePullDownToRefresh];
        [refreshControl endRefreshing];
        [self apiCallEndWithResultCount:0 pageNo:1];

    }];
    
}
#pragma mark- Remove pull down to refresh
-(void)RemovePullDownToRefresh
{
    //Remove Pull down to refresh when category_total_count match with array
    
    [refreshFooter remove_observer];
    
    refreshFooter.scrollView=nil;
    [refreshFooter.footerView removeFromSuperview];
    //self.aTableView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    
}
-(void)AddPullDowntoRefresh
{
    [self RemovePullDownToRefresh];
    refreshFooter.scrollView=self.aTableView;
    [refreshFooter footer];
}
-(void)updateLocalization
{
    self.lblHeader.text = NSLocalizedString(@"title_notification", nil);
    
   
  
}
-(void)updateListOnLangChange
{
    [notificationsArr removeAllObjects];
    [self.aTableView reloadData];
    [self userNotificationListAPI];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.tabBarItem.badgeValue = 0;

    [self updateLocalization];
    //[self newRotateAnimation];
}

-(void)newRotateAnimation
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionRepeat| UIViewAnimationCurveLinear
                     animations:^{
                         self.btnNews.transform = CGAffineTransformRotate(self.btnNews.transform, M_PI );
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    NSArray *visibleRows = [_aTableView visibleCells];
//    NotificationCell *lastVisibleCell = [visibleRows lastObject];
//    NSIndexPath *path = [_aTableView indexPathForCell:lastVisibleCell];
//    
//    lastReadIndex = (int)MAX(lastReadIndex, path.row);
//    NSLog(@"%d",lastReadIndex);
//}

#pragma mark- Menu swipe gesture
- (void)swipeGestureRecognized:(UISwipeGestureRecognizer *)sender
{
// Dismiss keyboard (optional)
[self.view endEditing:YES];
[self.frostedViewController.view endEditing:YES];

// Present the view controller
if(self.frostedViewController.swipeGestureEnabled)
{
    [self.frostedViewController presentMenuViewController];
}
}
#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return UITableViewAutomaticDimension;
//}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notificationsArr.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"NotificationCell"];
    if(cell==nil)
    {
        cell=[[NotificationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NotificationCell"];
        
        NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"NotificationCell" owner:self options:nil];
        cell=[menuarray objectAtIndex:0];
    }
    
    cell.selectionStyle = NO;

    NotificationModel *model = [notificationsArr objectAtIndex:indexPath.row];
    
    cell.indexPath = indexPath;
    cell.backgroundColor = [model.seen isEqualToString:@"0"] ? ColorRGB(232,239,249) : [UIColor whiteColor];
    
    [cell.imgUser sd_setImageWithURL:[Utils getThumbUrl:model.userPicture] placeholderImage:defaultUserImg];

    cell.lblDate.text = model.formattedTime;
    
    cell.lblDesc.attributedText = [self setFullMessage:model.userName message:model.message];
    cell.lblPostDescr.text = model.notificationDetailsModel.textPlain;
    
    if (model.notificationDetailsModel.photos.count) {
        cell.imgViewHtConstr.constant = 150;
        cell.btnPlayVideo.hidden = YES;
        Photos *photoModel1 = model.notificationDetailsModel.photos[0];
        [cell.postImgView sd_setImageWithURL:[Utils getThumbUrl:photoModel1.source] placeholderImage:defaultPostImg];
    }
    else{
        if (model.notificationDetailsModel.videos.count) {
            cell.imgViewHtConstr.constant = 150;
             cell.btnPlayVideo.hidden = NO;
            Photos *videoModel = model.notificationDetailsModel.videos[0];
            NSString *thumbURL = videoModel.source;
            thumbURL = [thumbURL stringByReplacingOccurrencesOfString:@"videos" withString:@"photos"];
            thumbURL = [[thumbURL stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
            [cell.postImgView sd_setImageWithURL:[Utils getThumbUrl:thumbURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (image !=nil) {
                    cell.btnPlayVideo.backgroundColor = [UIColor clearColor];
                }
            }];

        }
        else{
            cell.imgViewHtConstr.constant = 0;
        }
    }
    
    [cell onShowImgView:^(NSIndexPath *indexPath) {
        Photos *photoModel1 = model.notificationDetailsModel.photos[0];
        [UTILS showMultiImageViewer:@[photoModel1] currentPageIndex:0];
    }];
    
    [cell onPlayVideo:^(NSIndexPath *indexPath) {
        Photos *videoModel = model.notificationDetailsModel.videos[0];
        [self playVideoWithURL:[Utils getProperContentUrl:videoModel.source]];
        
    }];
    
    if ([model.userId isEqualToString:UTILS.currentUser.userId]) {
        cell.btnMessage.hidden = YES;
    }
    else
    {
         cell.btnMessage.hidden = NO;
    }
    
    [cell onDirectMessage:^(NSIndexPath *indexPath) {
        NotificationModel *model = [notificationsArr objectAtIndex:indexPath.row];

        if (model.userId) {
            
            ALChatManager* chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLICATION_ID];
            
            [chatManager launchChatForUserWithDefaultText:model.userId andFromViewController:[UTILS topMostControllerNormal]];
        }
      
    }];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cel forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *visibleRows = [tableView visibleCells];
    NotificationCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [tableView indexPathForCell:lastVisibleCell];

    lastReadIndex = (int)MAX(lastReadIndex, path.row);
    NSLog(@"%d",lastReadIndex);
}

-(void)playVideoWithURL:(NSURL *)videoUrl
{
    AVPlayer *player = [AVPlayer playerWithURL:videoUrl];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];
    [[UTILS topMostController] presentViewController:playerViewController animated:YES completion:nil];
}

-(NSMutableAttributedString *)setFullMessage:(NSString *)userName message:(NSString *)message
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",userName,message]];
    
//    [attributedString addAttribute:NSLinkAttributeName value:@"username://marcelofabri_" range:[[attributedString string] rangeOfString:@"@marcelofabri_"]];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:Font_Medium size:14] range:[[attributedString string] rangeOfString:userName]];
    return  attributedString;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationModel *notModel = [notificationsArr objectAtIndex:indexPath.row];
    if ([notModel.nodeType isEqualToString:@"post"]) {
        [self moveToPostDetails:notModel indexPath:indexPath];
    }
    else{
        if ([notModel.seen isEqualToString:@"0"]) {
            notModel.seen = @"1";
            [notificationsArr replaceObjectAtIndex:indexPath.row withObject:notModel];
            [_aTableView reloadData];
            [self setNotificationRead:notModel];
        }
        
    }
}

-(void)moveToPostDetails:(NotificationModel *)notModel indexPath:(NSIndexPath *)indexPath
{
    NSDictionary *param = @{
                            @"get":API_POST_DETAILS,
                            @"post_id":notModel.nodeUrl != nil ? notModel.nodeUrl : @"",
                            @"me_id":UTILS.currentUser.userId
                            };
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_POST_DETAILS_WITH_POST_MODEL params:param sBlock:^(id responseObject) {
        [Utils HideProgress];
        
        PostModel *model = responseObject;
        PostDetailsController *postDetailsVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"PostDetailsController"];
        postDetailsVC.selPostModel = model;
        [[UTILS topMostController].navigationController pushViewController:postDetailsVC animated:YES];
        
        if ([notModel.seen isEqualToString:@"0"]) {
            notModel.seen = @"1";
            [notificationsArr replaceObjectAtIndex:indexPath.row withObject:notModel];
            [_aTableView reloadData];
            [self setNotificationRead:notModel];
        }
        
        [Utils HideProgress];
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
}

-(void)setNotificationRead:(NotificationModel *)notModel
{
    NSDictionary *param = @{
                            @"get":API_SET_NOTIFICATION_READ,
                            @"from_notification_id":notModel.notificationId,
                            @"to_notification_id":notModel.notificationId,
                            @"to_user_id":UTILS.currentUser.userId
                            };
    [REMOTE_API CallPOSTWebServiceWithParam:API_SET_NOTIFICATION_READ params:param sBlock:^(id responseObject) {
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
    }];
}
#pragma mark - button click event

-(IBAction)onclick_view_profile_GUO:(id)sender
{
  [(AppObj).postView showUserProfile:GUO_USERID];
}
-(IBAction)onclick_more:(id)sender
{
    
}
-(IBAction)onclick_menu:(id)sender
{
    [self.frostedViewController.view endEditing:YES];
    [self.frostedViewController presentMenuViewController];
}


-(UIView *)tableBGView
{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64)];
    
    lblMessage = [[UILabel alloc] initWithFrame:bgView.frame];
    lblMessage.textAlignment = NSTextAlignmentCenter;
    lblMessage.backgroundColor = [UIColor clearColor];
    lblMessage.font = NO_DATE_FOUND_FONT;
    lblMessage.lineBreakMode = NSLineBreakByWordWrapping;
    lblMessage.numberOfLines = 0;
    [bgView addSubview:lblMessage];
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
    activityIndicatorView.backgroundColor = [UIColor clearColor];
    activityIndicatorView.frame = CGRectMake(CGRectGetMidX(bgView.frame)-50, CGRectGetMidY(bgView.frame)-25, 100, 50);
    [bgView addSubview:activityIndicatorView];
    
    return bgView;
}
-(void)apiCallInitiated
{
    [activityIndicatorView startAnimating];
    lblMessage.text = @"";
}
-(void)apiCallEndWithResultCount:(NSInteger)count  pageNo:(NSInteger)pageNo
{
    [activityIndicatorView stopAnimating];
    
    if (pageNo == 1 && count == 0) {
        lblMessage.text = NSLocalizedString(@"No data found", nil);
    }
    else
    {
        lblMessage.text = @"";
    }
    
   // [UTILS playSoundOnEndRefresh];

}

-(void)newNotification:(NSNotification *)notification
{
    current_page = 1;
    [self userNotificationListAPI];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
