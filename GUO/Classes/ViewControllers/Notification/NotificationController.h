//
//  NotificationController.h
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationCell.h"
#import "CommonDocumentController.h"
#import "YiRefreshFooter.h"
@interface NotificationController : UIViewController
{
    NSMutableArray *notificationsArr;
    int current_page;
    YiRefreshFooter *refreshFooter;

}

@property(strong,nonatomic) IBOutlet UITableView *aTableView;
@property(strong,nonatomic) IBOutlet UIView *headerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;

  @property(strong,nonatomic) IBOutlet UIButton *btnNews;

-(IBAction)onclick_menu:(id)sender;

-(IBAction)onclick_view_profile_GUO:(id)sender;
@property(strong,nonatomic) CommonDocumentController *commonDocNav;
@property(strong,nonatomic) IBOutlet UIButton *btn_header_userimg;

@end
