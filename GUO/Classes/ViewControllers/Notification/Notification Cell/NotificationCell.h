//
//  NotificationCell.h
//  GUO
//
//  Created by mac on 07/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UIImageView *imgUser;
@property(strong,nonatomic) IBOutlet UILabel *lblDesc,*lblDate,*lblSeparator;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblPostDescr;
@property (weak, nonatomic) IBOutlet UIImageView *postImgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewHtConstr;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayVideo;
@property (weak, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;

@property (nonatomic,copy)indexPathBlock imgBlock;
@property (nonatomic,copy)indexPathBlock videoBlock;
@property (nonatomic,copy)indexPathBlock messageBlock;

-(void)onShowImgView:(indexPathBlock)blk;
-(void)onPlayVideo:(indexPathBlock)blk;
-(void)onDirectMessage:(indexPathBlock)blk;

@end
