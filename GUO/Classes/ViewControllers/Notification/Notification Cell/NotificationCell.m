//
//  NotificationCell.m
//  GUO
//
//  Created by mac on 07/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblDate.textColor = header_color;
    self.lblSeparator.backgroundColor = tbl_separator_color;
  
    [Utils SetRoundedCorner:self.imgUser widthHeight:40];
    
    self.lblDesc.font = self.lblPostDescr.font = [UIFont fontWithName:Font_regular size:notification_text_font_size];
    self.lblDate.font = [UIFont fontWithName:Font_Medium size:username_font_size];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImageView)];
    tap1.numberOfTapsRequired = 1;
    [self.postImgView setUserInteractionEnabled:YES];
    [self.postImgView addGestureRecognizer:tap1];
    
    [self.btnPlayVideo addTarget:self action:@selector(btnPlayVideoClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnMessage addTarget:self action:@selector(btnMessageClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(void)tapOnImageView
{
    if (self.imgBlock) {
        self.imgBlock(self.indexPath);
    }
}

-(void)btnPlayVideoClicked
{
    if (self.videoBlock) {
        self.videoBlock(self.indexPath);
    }
}

-(void)btnMessageClicked
{
    if (self.messageBlock) {
        self.messageBlock(self.indexPath);
    }
}

-(void)onShowImgView:(indexPathBlock)blk
{
    self.imgBlock = blk;
}

-(void)onPlayVideo:(indexPathBlock)blk
{
    self.videoBlock = blk;
}

-(void)onDirectMessage:(indexPathBlock)blk
{
    self.messageBlock = blk;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
