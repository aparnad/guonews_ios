//
//  PostDetailsController.m
//  GUO Media
//
//  Created by Pawan Ramteke on 16/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "PostDetailsController.h"
#import "PostsTableCell.h"
#import "CommentView.h"
#import "SubCommentCell.h"
#import "PostDetailsView.h"
#import "UITextView+Placeholder.h"
#import "Photos.h"
#import "Video.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "PostDetailsModel.h"
#import "CommentReplyView.h"
#import "DGActivityIndicatorView.h"
#import "PostCommentView.h"
#import "TranslatePopUp.h"
#import "ALChatManager.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface PostDetailsController ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    
    __weak IBOutlet UIImageView *imgViewDp;
    __weak IBOutlet UILabel *lblHeaderTitle;
    __weak IBOutlet UILabel *lblHeaderUserName;
    
    __weak IBOutlet UIImageView *imgBadge;
    __weak IBOutlet UILabel *lblTime;
    __weak IBOutlet UIView *viewNavigation;
    __weak IBOutlet UITableView *_tableView;
    PostDetailsView *detailsView;
     UITextView *txtViewComment;
    UIView *baseView;
    
    PostDetailsModel *detailsModel;
    UIRefreshControl *refreshControl;
    NSMutableArray *arrComments;
    DGActivityIndicatorView *activityIndicatorView;

}
@property (nonatomic,strong)CommentPopup *comment_popup;
@property (nonatomic,strong)SharePopup *share_popup;

@end

@implementation PostDetailsController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor controllerBGColor];
    arrComments = [NSMutableArray new];
    
    imgViewDp.layer.cornerRadius = 20;
    imgViewDp.layer.masksToBounds = YES;
    imgViewDp.layer.borderWidth = 1.0;
    imgViewDp.layer.borderColor = [UIColor whiteColor].CGColor;
    [Utils SetViewHeader:viewNavigation headerLbl:lblHeaderTitle];
    
    _tableView.tableHeaderView = [self tableHeaderView];

    [_tableView registerNib:[UINib nibWithNibName:@"SubCommentCell" bundle:nil] forCellReuseIdentifier:@"SubCommentCell"];
    
    [_tableView registerNib:[UINib nibWithNibName:@"PostsTableCell" bundle:nil] forCellReuseIdentifier:@"PostsTableCell"];

    _tableView.backgroundColor = [UIColor controllerBGColor];
  
    _tableView.estimatedRowHeight = 100;
    _tableView.rowHeight = UITableViewAutomaticDimension;
   // _tableView.backgroundView = [self tableBGView];
    refreshControl=[[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshList:) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:refreshControl];

    
    
    
    lblHeaderUserName.font = [UIFont fontWithName:Font_Medium size:[UTILS getFlexibleHeight:userfull_name_font_size]];
  lblTime.font = [UIFont fontWithName:Font_regular size:[UTILS getFlexibleHeight:date_time_font_size]];
    
    
    

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [lblHeaderTitle addGestureRecognizer:tapRecognizer];
    
    _tableView.hidden = YES;
    [self getPostDetails];
    
    [self.view addSubview:[self tableBGView]];
}

-(UIView *)tableBGView
{
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
    activityIndicatorView.frame = CGRectMake(0,0,100,100);
    activityIndicatorView.center = self.view.center;
    [activityIndicatorView startAnimating];
    return activityIndicatorView;
}

-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender
{
    [self onclick_userprofile:self];
}
-(IBAction)onclick_userprofile:(id)sender
{

    [(AppObj).postView showUserProfile:detailsModel.userId];

}
-(void)refreshList:(UIRefreshControl *)refreshControl
{
    [self getPostDetails];
}

-(void)setupData
{
    [imgViewDp sd_setImageWithURL:[Utils getProperContentUrl:detailsModel.userPicture] placeholderImage:defaultUserImg];
    lblHeaderTitle.text = [Utils setFullname:detailsModel.userFirstname lname:detailsModel.userLastname username:detailsModel.userName];
    lblTime.text = detailsModel.formattedTime;
    imgBadge.hidden = NO;
//    lblHeaderUserName.text = [NSString stringWithFormat:@"@%@",self.selPostModel.userName];
}

-(UIView *)tableHeaderView
{
    UIView *headerView = [[UIView alloc]init];
    detailsView = [[[NSBundle mainBundle] loadNibNamed:@"PostDetailsView" owner:self options:nil] objectAtIndex:0];
    [headerView addSubview:detailsView];
    
    [detailsView.btnLike addTarget:self action:@selector(btnLikeClicked) forControlEvents:UIControlEventTouchUpInside];
    [detailsView.btnComment addTarget:self action:@selector(btnCommentClicked) forControlEvents:UIControlEventTouchUpInside];
    [detailsView.btnShare addTarget:self action:@selector(btnShareClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [detailsView.btnMore addTarget:self action:@selector(btnMoreClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [detailsView.btnTranslate addTarget:self action:@selector(btnTranslateClicked) forControlEvents:UIControlEventTouchUpInside];
    [detailsView.btnShareTranslate addTarget:self action:@selector(btnTranslateClicked) forControlEvents:UIControlEventTouchUpInside];
    [detailsView.btnMessage addTarget:self action:@selector(btnDirectMessageClicked) forControlEvents:UIControlEventTouchUpInside];
    
    //keep hidden default
    detailsView.btnMessage.hidden = YES;


    return headerView;
}

-(void)setupTableHeaderData
{
    detailsView.lblMainDescr.font = [UIFont fontWithName:Font_regular size:[UTILS getFlexibleHeight:post_desc]];
    detailsView.lblMainDescr.textColor = [UIColor blackColor];
    
    detailsView.viewUserDetailsHtConstr.constant = 0;
    
    detailsView.lblMainDescr.text = detailsModel.origin != nil ?detailsModel.textPlain : @"";
    
    
    if ([detailsModel.userId isEqualToString:UTILS.currentUser.userId]) {
        detailsView.btnMessage.hidden = YES;
    }
    else{
        detailsView.btnMessage.hidden = NO;
    }
    
    detailsView.btnShareTranslateHtConstr.constant = detailsView.lblMainDescr.text.length > 0 ? 20 : 0;

    if (detailsModel.origin != nil) {
        [self configureDataWithModel:detailsModel.origin];
        detailsView.btnTranslate.hidden = YES;
        detailsView.btnShareTranslate.hidden = detailsView.lblShareTranslate.text.length > 0 ? YES : NO;
        
        detailsView.sharDescTopConstr.constant = detailsView.btnShareTranslate.hidden ? 10 : 20;
    }
    else{
        [self configureDataWithModel:detailsModel];
       // detailsView.btnTranslate.hidden = detailsModel.textPlain.length > 0 ? NO : YES;
        detailsView.sharDescTopConstr.constant = 0;
        if (detailsModel.textPlain.length > 0) {
            detailsView.btnTranslate.hidden = detailsView.lblTranslate.text.length > 0 ? YES : NO;
        }
        else{
            detailsView.btnTranslate.hidden = YES;
        }
    }
    
    [detailsView.btnLike setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:detailsModel.likes]] forState:UIControlStateNormal];
    [detailsView.btnComment setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:detailsModel.comments]] forState:UIControlStateNormal];
    [detailsView.btnShare setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:detailsModel.shares]] forState:UIControlStateNormal];
    [detailsView.btnLike setImage:[[UIImage imageNamed:@"like_normal"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    detailsView.btnLike.tintColor = detailsModel.iLike ? header_color : [UIColor darkTextColor];
    
}

-(void)configureDataWithModel:(id)mod
{
    PostModel *model;
    if ([model isKindOfClass:[PostModel class]]) {
        model = mod;
    }
    else{
        model = (SharePostModel *)mod;
    }
    
    [detailsView.imgShareUserDp sd_setImageWithURL:[Utils getProperContentUrl:model.userPicture] placeholderImage:defaultPostImg];
    [detailsView.btnShareUserName setTitle:model.userName forState:UIControlStateNormal];
    detailsView.lblShareTime.text = model.formattedTime;
    //detailsView.lblShareDescription.text = model.textPlain;
    detailsView.txtViewShareDesciption.text = model.textPlain;
    detailsView.txtViewShareDesciption.textContainerInset = UIEdgeInsetsMake(model.textPlain.length > 0 ? 0 : -30, 0, 0, 0 );

    CGFloat padding = 30;
    if (model.photos.count) {
        detailsView.btnPlayVideo.hidden = YES;
        detailsView.videoView.hidden = YES;
        
        if (model.photos.count == 1) {
            
            detailsView.btnPoster2.hidden = YES;
            detailsView.btnPoster3.hidden = YES;
            detailsView.btnMorePoster.hidden = YES;
            
            Photos *photoModel = model.photos[0];
            [detailsView.imgPoster1 sd_setImageWithURL:[Utils getProperContentUrl:photoModel.source] placeholderImage:defaultPostImg];
            detailsView.poster1WidthConstr.constant = (Screen_Width - padding);
            
            [self setCornerForImage:detailsView.imgPoster1 corner:(UIRectCornerTopLeft|UIRectCornerBottomLeft|UIRectCornerTopRight|UIRectCornerBottomRight)];
            
        }
        else if (model.photos.count == 2){
            
            detailsView.btnPoster3.hidden = YES;
            detailsView.btnMorePoster.hidden = YES;
            
            Photos *photoModel1 = model.photos[0];
            
            [detailsView.imgPoster1 sd_setImageWithURL:[Utils getProperContentUrl:photoModel1.source] placeholderImage:defaultPostImg];

            detailsView.poster1WidthConstr.constant = (Screen_Width - padding)*0.5;
            detailsView.poster1HtConstr.constant = detailsView.poster1WidthConstr.constant;

            Photos *photoModel2 = model.photos[1];
            [detailsView.imgPoster2 sd_setImageWithURL:[Utils getProperContentUrl:photoModel2.source] placeholderImage:defaultPostImg];
            
            detailsView.poster2WidthConstr.constant = detailsView.poster1WidthConstr.constant;
            detailsView.poster2HtConstr.constant = detailsView.poster1HtConstr.constant;
            
            [self setCornerForImage:detailsView.imgPoster1 corner:(UIRectCornerTopLeft|UIRectCornerBottomLeft)];
            
            [self setCornerForImage:detailsView.imgPoster2 corner:(UIRectCornerTopRight|UIRectCornerBottomRight)];

        }
        else if (model.photos.count >= 3){
            
            if (model.photos.count > 3) {
                detailsView.btnMorePoster.hidden = NO;
                [detailsView.btnMorePoster setTitle:[NSString stringWithFormat:@"+%lu",model.photos.count - 3] forState:UIControlStateNormal];
            }
            else{
                detailsView.btnMorePoster.backgroundColor = [UIColor clearColor];
            }
            
            Photos *photoModel1 = model.photos[0];
            [detailsView.imgPoster1 sd_setImageWithURL:[Utils getProperContentUrl:photoModel1.source] placeholderImage:defaultPostImg];
            detailsView.poster1WidthConstr.constant = (Screen_Width - padding)*0.6;
            detailsView.poster2HtConstr.constant = 99;
            
            Photos *photoModel2 = model.photos[1];
            [detailsView.imgPoster2 sd_setImageWithURL:[Utils getProperContentUrl:photoModel2.source] placeholderImage:defaultPostImg];
            detailsView.poster2WidthConstr.constant = (Screen_Width - padding)*0.4;
            detailsView.poster2HtConstr.constant = 99;
            
            Photos *photoModel3 = model.photos[2];
            [detailsView.imgPoster3 sd_setImageWithURL:[Utils getProperContentUrl:photoModel3.source] placeholderImage:defaultPostImg];
            
            [self setCornerForImage:detailsView.imgPoster1 corner:(UIRectCornerTopLeft|UIRectCornerBottomLeft)];
            [self setCornerForImage:detailsView.imgPoster2 corner:UIRectCornerTopRight];
            [self setCornerForImage:detailsView.imgPoster3 corner:UIRectCornerBottomRight];

        }
    }
    else if (model.photos.count == 0 && model.videos.count == 0){
        detailsView.poster1HtConstr.constant = 0;
        detailsView.btnPoster2.hidden = YES;
        detailsView.btnPoster3.hidden = YES;
        detailsView.btnMorePoster.hidden = YES;
        
    }
    else if (model.videos.count){
        
        detailsView.btnPoster2.hidden = YES;
        detailsView.btnPoster3.hidden = YES;
        detailsView.btnMorePoster.hidden = YES;
        
        detailsView.poster1WidthConstr.constant = (Screen_Width - padding);
        detailsView.poster1HtConstr.constant = 200;
        
        [detailsView.btnPlayVideo addTarget:self action:@selector(btnPlayVideoClicked) forControlEvents:UIControlEventTouchUpInside];

        Photos *videoModel = model.videos[0];
        
        detailsView.btnPlayVideo.backgroundColor = [UIColor blackColor];
        detailsView.videoView.hidden = YES;
        
        NSString *thumbURL = videoModel.source;
        
        thumbURL = [thumbURL stringByReplacingOccurrencesOfString:@"videos" withString:@"photos"];
        thumbURL = [[thumbURL stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
        
        [detailsView.imgPoster1 sd_setImageWithURL:[Utils getThumbUrl:thumbURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (image != nil) {
                detailsView.btnPlayVideo.backgroundColor = [UIColor clearColor];
            }
        }];
        
        [self setCornerForImage:detailsView.imgPoster1 corner:(UIRectCornerTopLeft|UIRectCornerBottomLeft|UIRectCornerTopRight|UIRectCornerBottomRight)];

      //  AVPlayer *player = [AVPlayer playerWithURL:[Utils getProperContentUrl:videoModel.source]];
       // detailsView.playerViewController.player = player;
    }
    
    [detailsView onPoster1Clicked:^{
        [UTILS showMultiImageViewer:model.photos currentPageIndex:0];
    }];
    
    [detailsView onPoster2Clicked:^{
        [UTILS showMultiImageViewer:model.photos currentPageIndex:1];

    }];
    
    [detailsView onMorePosterClicked:^{
        [UTILS showMultiImageViewer:model.photos currentPageIndex:2];
    }];
}


-(void)btnPlayVideoClicked
{
    Photos *videoModel = detailsModel.videos[0];
    AVPlayer *player = [AVPlayer playerWithURL:[Utils getProperContentUrl:videoModel.source]];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];
    [self presentViewController:playerViewController animated:YES completion:nil];
}

-(void)btnLikeClicked
{
    PostModel *model = self.selPostModel;
    int likeCount = [model.likes intValue];
    model.likes = !model.iLike ? @(likeCount+1).stringValue : @(likeCount-1).stringValue;
    model.iLike = !model.iLike;
    self.selPostModel = model;
    
    [detailsView.btnLike setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:self.selPostModel.likes]] forState:UIControlStateNormal];
    detailsView.btnLike.tintColor = self.selPostModel.iLike ? header_color : [UIColor darkTextColor];
    
    [self likePostService:detailsModel.postId isLike:detailsModel.iLike completion:^{
        detailsModel.iLike = !detailsModel.iLike;
        if (self.block) {
            self.block(self.selPostModel,self.indexPath);
        }
        
    }];
}

-(void)btnCommentClicked
{
    PostCommentView *viewShare = [[PostCommentView alloc]initWithFrame:self.view.bounds model:self.selPostModel];
   // viewShare.isComment = YES;
    [self.navigationController.view addSubview:viewShare];
    
    [viewShare enableAutolayout];
    [viewShare leadingMargin:0];
    [viewShare trailingMargin:0];
    [viewShare topMargin:0];
    [viewShare bottomMargin:0];
    
    [viewShare updatePostModel:^(PostModel *model) {
        self.selPostModel = model;
        [detailsView.btnComment setTitle:[NSString stringWithFormat:@"(%@)",self.selPostModel.comments] forState:UIControlStateNormal];
        [self getPostDetails];
        
        if (self.block) {
            self.block(self.selPostModel,self.indexPath);
        }
    }];
    
//    _comment_popup = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"CommentPopup"];
//    _comment_popup.model  = self.selPostModel;
//    [UTILS showCustomPopup:_comment_popup.view];
//    [_comment_popup updatePostModel:^(PostModel *model) {
//        self.selPostModel = model;
//        [detailsView.btnComment setTitle:[NSString stringWithFormat:@"(%@)",self.selPostModel.comments] forState:UIControlStateNormal];
//        [self getPostDetails];
//        self.block(self.selPostModel,self.indexPath);
//
//    }];
}

-(void)btnShareClicked
{
    _share_popup = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"SharePopup"];
    _share_popup.model  = self.selPostModel;
    [UTILS showCustomPopup:_share_popup.view];
    [_share_popup updatePostModel:^(PostModel *model) {
        NSInteger count = [detailsModel.shares integerValue];
        count++;
        [detailsView.btnShare setTitle:@(count).stringValue forState:UIControlStateNormal];
        if (self.shareBlk) {
            self.shareBlk(model,self.indexPath);
        }
    }];
}

-(void)btnMoreClicked
{
    if(detailsModel.userId == UTILS.currentUser.userId)
    {
        [self showLoginUserOption];
    }
    else
    {
        [self showOtherUserOption:self.selPostModel];
    }
}

-(void)btnTranslateClicked
{
    [self translateAPI:detailsModel];
}

-(void)btnDirectMessageClicked
{
    ALChatManager* chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLICATION_ID];
    
    [chatManager launchChatForUserWithDefaultText:detailsModel.userId andFromViewController:[UTILS topMostControllerNormal]];
}

-(void)translateAPI:(PostDetailsModel *)model
{
    NSDictionary *param = @{
                            @"get":API_TRANSLATE_TEXT,
                            @"translation_text":model.textPlain
                            };
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_TRANSLATE_TEXT params:param sBlock:^(id responseObject) {
        [Utils HideProgress];
        if (detailsModel.origin == nil) {
            detailsView.lblTranslate.text = responseObject;
            detailsView.viewTranslateHtConstr.constant = 20;
            detailsView.viewShareTranslatedByHtConstr.constant = 0;
            detailsView.btnTranslate.hidden = YES;
        }
        else{
            detailsView.lblShareTranslate.text = responseObject;
            detailsView.btnShareTranslate.hidden = YES;
            detailsView.sharDescTopConstr.constant = 10;
            detailsView.viewTranslateHtConstr.constant = 0;
            detailsView.viewShareTranslatedByHtConstr.constant = 20;

        }
        [self sizeHeaderToFit];
      //  [self showTranslatePopup:model text:responseObject];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
    }];
}

-(void)showTranslatePopup:(PostModel *)model text:(NSString *)text
{
    TranslatePopUp *popup = [[TranslatePopUp alloc]initWithFrame:[UIScreen mainScreen].bounds model:model translatedText:text];
    [[UTILS topMostControllerNormal].navigationController.view addSubview:popup];
    
    [popup enableAutolayout];
    [popup leadingMargin:0];
    [popup trailingMargin:0];
    [popup topMargin:0];
    [popup bottomMargin:0];
}

-(void)showLoginUserOption
{
    
    NSArray *arr = @[
                     //                     NSLocalizedString(@"Share via Direct Message", nil),
                     NSLocalizedString(@"Copy Link to Post", nil)/*,
                                                                  NSLocalizedString(@"Pin Post", nil)*/,
                     NSLocalizedString(@"Delete Post", nil)
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        if(index == 0)//copylink
        {
            [UTILS copyLink:self.selPostModel.postId];
            
        }
        else if (index == 1) {//delete post
            [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"Are you sure, you want to delete post?", nil) okHandler:^{
                [self deletePostAPI:self.selPostModel];
            }];
        }
        
    }];;
}


-(void)showOtherUserOption:(PostModel*)model
{
    NSArray *arr = @[
                     NSLocalizedString(@"Copy Link to Post", nil),
                     NSLocalizedString(@"Mute Post", nil),
                     [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"block_connection", nil),[Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName]]/*,
                     NSLocalizedString(@"Report Post", nil)*/
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        
        if(index == 0)//copy link
        {
            [UTILS copyLink:model.postId];
            
        }
        else if (index == 1) {//mute post
            [self mutePost:model];
            
        }
        else if(index == 2)// block user
        {
            [self blockUser:model];
            
        }
        else if(index == 3)// report post
        {
           // [self showReportUserPopup:model];
            
        }
        
        
    }];
    
    
}

#pragma mark- Block User
-(void)blockUser:(PostModel*)model
{
    [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"block_user_alert_msg", nil)  okHandler:^{
        
       
        [self callBlockAPI:detailsModel.userId completion:^{
            if (self.deleteBlk) {
                self.deleteBlk();
            }
        }];
        
    }];
    
}

-(void)callBlockAPI:(NSString *)blockUserId completion:(void(^)())completion
{
    NSDictionary *params = @{
                             @"get":API_BLOCK_USER,
                             @"user_id":UTILS.currentUser.userId,
                             @"block_id":blockUserId
                             };
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_UNBLOCK_USER params:params sBlock:^(id responseObject) {
        [Utils HideProgress];
        if (completion) {
            completion();
        }
        [CustomAlertView showAlert:@"" withMessage:responseObject];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
}

#pragma mark - MutePost
-(void)mutePost:(PostModel*)model
{
    //    [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"block_user_alert_msg", nil)  okHandler:^{
    
    
    NSDictionary *params = @{
                             @"get":API_MUTE_POST,
                             @"user_id":UTILS.currentUser.userId,
                             @"post_id":model.postId
                             };
    
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_MUTE_POST params:params sBlock:^(id responseObject) {
        
        
        [Utils HideProgress];
        if (self.deleteBlk) {
            self.deleteBlk();
        }
        
        [CustomAlertView showAlert:@"" withMessage:responseObject];
        
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
 
}

-(void)getPostDetails
{
    NSDictionary *param = @{
        @"get":API_POST_DETAILS,
        @"post_id":self.selPostModel.postId,
        @"me_id":UTILS.currentUser.userId
        };
   // [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_POST_DETAILS params:param sBlock:^(id responseObject) {
        _tableView.hidden = NO;
        [Utils HideProgress];
        [activityIndicatorView removeFromSuperview];
        detailsModel = responseObject;
        
        if (detailsModel) {
            PostModel *mod = [PostModel modelObjectWithDictionary:[detailsModel dictionaryRepresentation]];
            self.selPostModel = mod;
        }
        [self setupData];
        [self setupTableHeaderData];
        arrComments = [NSMutableArray arrayWithArray:detailsModel.postComments];
        [_tableView reloadData];
        [refreshControl endRefreshing];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [refreshControl endRefreshing];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];

    }];
}



-(void)likePostService:(NSString *)postId isLike:(BOOL)isLike completion:(void(^)())completion
{
    NSString *api = isLike ? API_UNLIKE_POST : API_LIKE_POST;
    NSDictionary *param = @{
                            @"get":api,
                            @"post_id":postId,
                            @"user_id":UTILS.currentUser.userId
                            };
    //[UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:api params:param sBlock:^(id responseObject) {
        if (completion) {
            completion();
        }
        CountModel *contM = (AppObj).app_count_model;
        contM.likes = isLike ? contM.likes - 1 : contM.likes + 1;
        (AppObj).app_count_model = contM;
        
        [Utils HideProgress];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
       // [CustomAlertView showAlert:@"" withMessage:customErrorMsg];

    }];
}

-(void)likeCommentService:(NSIndexPath *)indexPath completion:(void(^)())completion
{
    PostComments *commentModel = arrComments[indexPath.row];

    NSString *apiName = commentModel.iLike ? API_UNLIKE_COMMENT : API_LIKE_COMMENT;
    NSDictionary *param = @{
                            @"get":apiName,
                            @"comment_id":commentModel.commentId,
                            @"user_id":UTILS.currentUser.userId
                            };
    
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:apiName params:param sBlock:^(id responseObject) {
        [Utils HideProgress];
        [[Toast sharedInstance]makeToast:responseObject position:TOAST_POSITION_BOTTOM];
        commentModel.iLike = !commentModel.iLike;
        int likeCount = [commentModel.likes intValue];
        likeCount = commentModel.iLike ? likeCount + 1 : likeCount - 1;
        commentModel.likes = @(likeCount).stringValue;
        [arrComments replaceObjectAtIndex:indexPath.row withObject:commentModel];
        [_tableView reloadData];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];

    }];

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if (arrComments.count == 0) {
//        [activityIndicatorView startAnimating];
//    }
//    else{
//        [activityIndicatorView stopAnimating];
//    }
    return arrComments.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostComments *commentModel = arrComments[indexPath.row];
    
    SubCommentCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SubCommentCell"];
    cell.indexpath = indexPath;
    cell.btnVideo.hidden = YES;
    if ([commentModel.image isEqualToString:@""] || commentModel.image == nil) {
        if ([commentModel.video isEqualToString:@""] || commentModel.video == nil) {
            cell.imgViewCommHtConstr.constant = 0;
            cell.imgViewCommTopConstr.constant = 0;
        }
        else{
            cell.btnVideo.hidden = NO;
            cell.imgViewCommHtConstr.constant = 60;
            cell.imgViewCommTopConstr.constant = 10;
            NSString *thumbURL = commentModel.video;
            thumbURL = [thumbURL stringByReplacingOccurrencesOfString:@"videos" withString:@"photos"];
            thumbURL = [[thumbURL stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
            [cell.imgViewComment sd_setImageWithURL:[Utils getThumbUrl:thumbURL] placeholderImage:defaultPostImg];
        }
    }
    else{
        cell.btnVideo.hidden = YES;
        cell.imgViewCommHtConstr.constant = 60;
        cell.imgViewCommTopConstr.constant = 10;
        [cell.imgViewComment sd_setImageWithURL:[Utils getThumbUrl:commentModel.image] placeholderImage:defaultPostImg];
    }

    if (commentModel.commentReplies.count) {
        cell.btnMoreRepHtConstr.constant = 30;
        [cell.btnMoreReplies setTitle:[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"View", nil),[Utils suffixNumber:[NSString stringWithFormat:@"%d",(int)commentModel.commentReplies.count]],commentModel.commentReplies.count > 1 ?NSLocalizedString(@"replies", nil):NSLocalizedString(@"btn_reply", nil)] forState:UIControlStateNormal];
        [cell.btnMoreReplies setTitleColor:header_color forState:UIControlStateNormal];
    }
    else{
        cell.btnMoreRepHtConstr.constant = 0;
        [cell.btnMoreReplies setTitle:@"" forState:UIControlStateNormal];
        [cell.btnMoreReplies setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    [cell.btnLike setImage:[[UIImage imageNamed:@"like_normal"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
    cell.btnLike.tintColor = commentModel.iLike ? header_color : [UIColor darkTextColor];
    
    [cell.imgViewDp sd_setImageWithURL:[Utils getProperContentUrl:commentModel.userPicture] placeholderImage:defaultUserImg];
    cell.lblName.text = [Utils setFullname:commentModel.userFirstname lname:commentModel.userLastname username:commentModel.userName];
    cell.lblComment.text = commentModel.textPlain;
    cell.lblTime.text = commentModel.formattedTime;
    [cell.btnLike setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:commentModel.likes]] forState:UIControlStateNormal];
    
    cell.imgVerified.hidden = [commentModel.authorVerified isEqualToString:@"1"] ? NO : YES;
    
    [cell.btnComment setTitle:[NSString stringWithFormat:@"(%lu)",(unsigned long)commentModel.commentReplies.count] forState:UIControlStateNormal];

    [cell onButtonMoreRepliesClicked:^(NSIndexPath *indexPath) {
        
        [self showReplyListView:indexPath];
    }];
    
    [cell onShowProfile:^(NSIndexPath *indexPath) {
        PostComments *commentModel = arrComments[indexPath.row];
      
        
        /*UserProfileController *userProfileVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"UserProfileController"];
        userProfileVC.other_user_id = commentModel.userId;
        [[UTILS topMostControllerNormal].navigationController pushViewController:userProfileVC animated:YES];*/
        
        [UTILS showUserProfileSingleController:commentModel.userId];


    }];
    
    [cell onButtonLikeClicked:^(NSIndexPath *indexPath) {
        [self likeCommentService:indexPath completion:^{
            
        }];
    }];
    
    [cell onButtonCommentClicked:^(NSIndexPath *indexPath) {
        PostComments *commentModel = arrComments[indexPath.row];
        
        PostModel *model = [[PostModel alloc]init];
        model.userId = commentModel.authorId;
        model.postId = commentModel.commentId;
        model.userFirstname = commentModel.userFirstname;
        model.userLastname = @"";
        model.time = commentModel.time;
        model.textPlain = commentModel.textPlain;
        model.userPicture = commentModel.authorPicture;
        
        PostCommentView *viewShare = [[PostCommentView alloc]initWithFrame:self.view.bounds model:model];
        viewShare.isComment = YES;
        [self.navigationController.view addSubview:viewShare];
        
        [viewShare enableAutolayout];
        [viewShare leadingMargin:0];
        [viewShare trailingMargin:0];
        [viewShare topMargin:0];
        [viewShare bottomMargin:0];
        
        [viewShare updatePostModel:^(PostModel *model) {
                [self getPostDetails];
        }];
        
//        _comment_popup = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"CommentPopup"];
//        _comment_popup.isComment = YES;
//        _comment_popup.model  = model;
//        [UTILS showCustomPopup:_comment_popup.view];
//
//        [_comment_popup updatePostModel:^(PostModel *model) {
//            [self getPostDetails];
//        }];
    }];
    
    [cell onViewImage:^(NSIndexPath *indexPath) {
        PostComments *commentModel = arrComments[indexPath.row];
        
        if ([commentModel.image isEqualToString:@""] || commentModel.image == nil) {
            [self playVideoWithURL:[Utils getProperContentUrl:commentModel.video]];
        }
        else{
            Photos *model = [[Photos alloc]init];
            model.source = commentModel.image;
            [UTILS showImageViewer:@[model]];

        }
    }];
    
    [cell onMoreButtonClicked:^(NSIndexPath *indexPath) {
        if ([commentModel.userId isEqualToString:UTILS.currentUser.userId]) {
            [self showSelfCommentsMoreOptions:indexPath];
        }
        else{
            if ([detailsModel.userId isEqualToString:UTILS.currentUser.userId]) {
                [self showOthersCommentsInSelfPostMoreOptions:indexPath];
            }
            else{
                [self showOthersCommentsInOthersPostMoreOptions:indexPath];
            }
        }
    }];
    
    return cell;
}

-(void)playVideoWithURL:(NSURL *)videoUrl
{
    AVPlayer *player = [AVPlayer playerWithURL:videoUrl];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];
    [[UTILS topMostController] presentViewController:playerViewController animated:YES completion:nil];
}

-(void)showReplyListView:(NSIndexPath *)indexPath
{
    PostComments *model = arrComments[indexPath.row];

    CommentReplyView *replyView = [[CommentReplyView alloc]initWithFrame:self.view.bounds withCommentModel:model];
    replyView.postOwnerId = detailsModel.authorId;
    [self.navigationController.view addSubview:replyView];
    
    [replyView enableAutolayout];
    [replyView leadingMargin:0];
    [replyView trailingMargin:0];
    [replyView topMargin:0];
    [replyView bottomMargin:0];
    
    
    [replyView onBtnLikeCliked:^BOOL{
        [self likeCommentService:indexPath completion:^{
            
        }];
        return YES;
    }];
    
    [replyView onBtnCommentClicked:^{
        
        PostModel *postModel = [[PostModel alloc]init];
        model.userId = model.authorId;
        postModel.postId = model.commentId;
        postModel.userFirstname = model.userFirstname;
        postModel.userLastname = @"";
        postModel.time = model.time;
        postModel.textPlain = model.textPlain;
        postModel.userPicture = model.authorPicture;
        
        PostCommentView *viewShare = [[PostCommentView alloc]initWithFrame:self.view.bounds model:postModel];
        viewShare.isComment = YES;
        [[UTILS topMostController].navigationController.view addSubview:viewShare];
        
        [viewShare enableAutolayout];
        [viewShare leadingMargin:0];
        [viewShare trailingMargin:0];
        [viewShare topMargin:0];
        [viewShare bottomMargin:0];
        
        [viewShare updatePostModel:^(PostModel *model) {
            [self getPostDetails];
        }];
        
        return YES;
    }];
    
    [replyView onDeleteCommentClicked:^BOOL{
        [self getPostDetails];
        return YES;

    }];
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    CommentView *viewComment = [[[NSBundle mainBundle] loadNibNamed:@"CommentView" owner:self options:nil] objectAtIndex:0];
//    viewComment.backgroundColor = [UIColor controllerBGColor];
//    return viewComment;
//}


//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView *footer = [[UIView alloc]initWithFrame:CGRectMake(10, 0, Screen_Width - 20, 1)];
//    footer.backgroundColor = [UIColor grayColor];
//    return footer;
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 1;
//}


- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self sizeHeaderToFit];
}

- (void)sizeHeaderToFit
{
    UIView *header = _tableView.tableHeaderView;
    
    [header setNeedsLayout];
    [header layoutIfNeeded];
    
    CGFloat height = [header systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = header.frame;
    
    frame.size.height = height;
    header.frame = frame;
    
    detailsView.frame = frame;
    _tableView.tableHeaderView = header;
}

-(void)onLikeCommentSuccess:(ButtonClickedBlock)blk
{
    self.block = blk;
}
-(void)onPostDelete:(noArgBlock)blk
{
    self.deleteBlk = blk;
}

-(void)onShareSuccess:(ButtonClickedBlock)blk
{
    self.shareBlk = blk;
}
-(void)setCornerForImage:(UIImageView *)imgView corner:(UIRectCorner)corner
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [imgView setNeedsLayout];
        [imgView layoutIfNeeded];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:imgView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(10.0, 10.0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = imgView.bounds;
        maskLayer.path  = maskPath.CGPath;
        imgView.layer.mask = maskLayer;
    });
}


-(void)deletePostAPI:(PostModel*)post_model
{
    [UTILS ShowProgress];
    
    NSDictionary *params = @{
                             @"get":API_DELETE_POST,
                             @"post_id":post_model.postId
                             };
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_DELETE_POST params:params sBlock:^(id responseObject) {
        
        [[Toast sharedInstance] makeToast:responseObject position:TOAST_POSITION_BOTTOM];
        //        [CustomAlertView showAlert:@"" withMessage:responseObject];
      //  [PostArr removeObject:post_model];
        [Utils HideProgress];
       // [self reloadData];
        
        CountModel *contM = (AppObj).app_count_model;
        contM.posts = contM.posts - 1;
        (AppObj).app_count_model = contM;
        if (self.deleteBlk) {
            self.deleteBlk();
        }
        [self.navigationController popViewControllerAnimated:YES];
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
}

-(void)deleteCommentAPI:(NSString *)commentId
{
   NSDictionary *param =  @{
                            @"get":API_DELETE_COMMENT,
                            @"comment_id":commentId,
                           };
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_DELETE_COMMENT params:param sBlock:^(id responseObject) {
        [Utils HideProgress];
        [[Toast sharedInstance]makeToast:responseObject position:TOAST_POSITION_BOTTOM];
        [self getPostDetails];
        
        PostModel *model = self.selPostModel;
        model.comments = @([model.comments intValue] - 1).stringValue;
        self.selPostModel = model;
        
        if (self.block) {
            self.block(self.selPostModel,self.indexPath);
        }
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
    }];
}


-(void)showOthersCommentsInSelfPostMoreOptions:(NSIndexPath *)indexPath
{
    PostComments *model = arrComments[indexPath.row];
    
    NSArray *arr = @[
                     [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"block_connection", nil),[Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName]],
                     NSLocalizedString(@"Delete", nil)
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        if(index == 0)//copylink
        {
            [self callBlockAPI:model.userId completion:^{
                
            }];
        }
        else if (index == 1) {//delete post
            [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"msg_delete_account", nil) okHandler:^{
                [self deleteCommentAPI:model.commentId];
            }];
        }
        
    }];
}

-(void)showSelfCommentsMoreOptions:(NSIndexPath *)indexPath
{
    PostComments *model = arrComments[indexPath.row];
    
    NSArray *arr = @[
                     NSLocalizedString(@"Delete", nil)
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        if(index == 0)//copylink
        {
            [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"msg_delete_account", nil) okHandler:^{
                [self deleteCommentAPI:model.commentId];
            }];
        }
    }];
}

-(void)showOthersCommentsInOthersPostMoreOptions:(NSIndexPath *)indexPath
{
    PostComments *model = arrComments[indexPath.row];
    NSArray *arr = @[
                     [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"block_connection", nil),[Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName]],
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        if(index == 0)//copylink
        {
            [self callBlockAPI:model.userId completion:^{
                
            }];
        }
    }];
}


@end
