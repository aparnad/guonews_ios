//
//  PostDetailsController.h
//  GUO Media
//
//  Created by Pawan Ramteke on 16/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ButtonClickedBlock)(PostModel *model,NSIndexPath *indexPath);
@interface PostDetailsController : UIViewController
@property (nonatomic,strong)PostModel *selPostModel;
@property (nonatomic,strong)NSIndexPath *indexPath;
@property (nonatomic,copy) ButtonClickedBlock block;
@property (nonatomic,copy) ButtonClickedBlock shareBlk;
@property (nonatomic,copy) ButtonClickedBlock moreBlk;
@property (nonatomic,copy) noArgBlock deleteBlk;



-(void)onLikeCommentSuccess:(ButtonClickedBlock)blk;
-(void)onShareSuccess:(ButtonClickedBlock)blk;
-(void)onMoreButton:(ButtonClickedBlock)blk;
-(void)onPostDelete:(noArgBlock)blk;

@end
