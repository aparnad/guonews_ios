//
//  CommentReplyView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 01/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "CommentReplyView.h"
#import "SubCommentCell.h"
#import "CommentView.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface CommentReplyView()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tblView;
    NSMutableArray *arrReplies;
    PostComments *commentModel;
}
@end

@implementation CommentReplyView

-(instancetype)initWithFrame:(CGRect)frame withCommentModel:(PostComments *)model
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        arrReplies = [NSMutableArray arrayWithArray:model.commentReplies];
        commentModel = model;
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        self.alpha = 0;
        UIButton *btn = [[UIButton alloc]initWithFrame:self.bounds];
        [self addSubview:btn];
        [btn addTarget:self action:@selector(btnBGClicked) forControlEvents:UIControlEventTouchUpInside];
        
        tblView = [[UITableView alloc]initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, Screen_Height*0.8)];
        tblView.backgroundColor = [UIColor whiteColor];
        tblView.dataSource = self;
        tblView.delegate = self;
        tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:tblView];
        
        tblView.estimatedRowHeight = 100;
        tblView.rowHeight = UITableViewAutomaticDimension;
        
        tblView.estimatedSectionHeaderHeight = 100;
        tblView.sectionHeaderHeight = UITableViewAutomaticDimension;
        
        [tblView registerNib:[UINib nibWithNibName:@"SubCommentCell" bundle:nil] forCellReuseIdentifier:@"SubCommentCell"];

        [UIView animateWithDuration:0.3 animations:^{
            self.alpha = 1.0;
            CGRect frame = tblView.frame;
            frame.origin.y = self.frame.size.height - frame.size.height;
            tblView.frame = frame;
        }];
    }
    return self;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrReplies.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubCommentCell"];
    
    CommentReplies *model = arrReplies[indexPath.row];
    cell.indexpath = indexPath;
    
    if ([model.image isEqualToString:@""] || model.image == nil) {
        if ([model.video isEqualToString:@""] || model.video == nil) {
            cell.imgViewCommHtConstr.constant = 0;
            cell.imgViewCommTopConstr.constant = 0;
        }
        else{
            cell.btnVideo.hidden = NO;
            cell.imgViewCommHtConstr.constant = 60;
            cell.imgViewCommTopConstr.constant = 10;
            NSString *thumbURL = model.video;
            thumbURL = [thumbURL stringByReplacingOccurrencesOfString:@"videos" withString:@"photos"];
            thumbURL = [[thumbURL stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
            [cell.imgViewComment sd_setImageWithURL:[Utils getThumbUrl:thumbURL] placeholderImage:defaultPostImg];
        }
    }
    else{
        cell.btnVideo.hidden = YES;
        cell.imgViewCommHtConstr.constant = 60;
        cell.imgViewCommTopConstr.constant = 10;
        [cell.imgViewComment sd_setImageWithURL:[Utils getThumbUrl:model.image] placeholderImage:defaultPostImg];
    }

    [cell.imgViewDp sd_setImageWithURL:[Utils getProperContentUrl:model.userPicture] placeholderImage:defaultUserImg];
    cell.lblName.text = cell.lblName.text = [Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName];
    cell.lblComment.text = model.textPlain;
    cell.btnMoreRepHtConstr.constant = 0;
    [cell.btnLike setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:model.likes]] forState:UIControlStateNormal];
    [cell.btnLike setImage:[[UIImage imageNamed:@"like_normal"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
    cell.btnLike.tintColor = model.iLike ? header_color : [UIColor darkTextColor];

    cell.btnComment.hidden = YES;
    
    [cell onButtonLikeClicked:^(NSIndexPath *indexPath) {
        [self likeCommentService:indexPath completion:^{
            
        }];
    }];
    
    [cell onMoreButtonClicked:^(NSIndexPath *indexPath) {
        CommentReplies *model = arrReplies[indexPath.row];
        if ([model.userId isEqualToString:UTILS.currentUser.userId]) {
            [self showSelfCommentsMoreOptions:indexPath];
        }
        else{
            if ([self.postOwnerId isEqualToString:UTILS.currentUser.userId]) {
                [self showOthersCommentsInSelfPostMoreOptions:indexPath];
            }
            else{
                [self showOthersCommentsInOthersPostMoreOptions:indexPath];
            }
        }
    }];
    
    [cell onViewImage:^(NSIndexPath *indexPath) {
        
        CommentReplies *model = arrReplies[indexPath.row];

        if ([model.image isEqualToString:@""] || model.image == nil) {
            [self playVideoWithURL:[Utils getProperContentUrl:model.video]];
        }
        else{
            Photos *Phmodel = [[Photos alloc]init];
            Phmodel.source = model.image;
            [UTILS showImageViewer:@[Phmodel]];

        }
        
    }];
    
    [cell onShowProfile:^(NSIndexPath *indexPath) {
        CommentReplies *model = arrReplies[indexPath.row];
      
        
        /*UserProfileController *userProfileVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"UserProfileController"];
        userProfileVC.other_user_id = model.userId;
        [[UTILS topMostControllerNormal].navigationController pushViewController:userProfileVC animated:YES];*/
        
        [UTILS showUserProfileSingleController:model.userId];


    }];
    
    return cell;
}

-(void)playVideoWithURL:(NSURL *)videoUrl
{
    AVPlayer *player = [AVPlayer playerWithURL:videoUrl];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];
    [[UTILS topMostController] presentViewController:playerViewController animated:YES completion:nil];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CommentView *viewComment = [[[NSBundle mainBundle] loadNibNamed:@"CommentView" owner:self options:nil] objectAtIndex:0];
    viewComment.backgroundColor = [UIColor controllerBGColor];
    
    [viewComment.imgViewDp sd_setImageWithURL:[Utils getProperContentUrl:commentModel.userPicture] placeholderImage:defaultUserImg];
    viewComment.lblName.text = [NSString stringWithFormat:@"@%@",commentModel.userName];//[NSString stringWithFormat:@"%@ %@",commentModel.userFirstname,commentModel.userLastname];
    viewComment.lblTime.text = commentModel.time;
    viewComment.lblComment.text = commentModel.textPlain;
    
    [viewComment.btnLike setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:commentModel.likes]] forState:UIControlStateNormal];
    [viewComment.btnComment setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:[NSString stringWithFormat:@"%lu",(unsigned long)commentModel.commentReplies.count]]] forState:UIControlStateNormal];
    [viewComment.btnLike setImage:[[UIImage imageNamed:@"like_normal"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    viewComment.btnLike.tintColor = commentModel.iLike ? header_color : [UIColor darkTextColor];
    
    [viewComment.btnLike addTarget:self action:@selector(btnLikeClicked) forControlEvents:UIControlEventTouchUpInside];
     [viewComment.btnComment addTarget:self action:@selector(btnCommentClicked) forControlEvents:UIControlEventTouchUpInside];
    return viewComment;
}

-(void)likeCommentService:(NSIndexPath *)indexPath completion:(void(^)())completion
{
    CommentReplies *model = arrReplies[indexPath.row];
    
    NSString *apiName = model.iLike ? API_UNLIKE_COMMENT : API_LIKE_COMMENT;
    NSDictionary *param = @{
                            @"get":apiName,
                            @"comment_id":model.commentId,
                            @"user_id":UTILS.currentUser.userId
                            };
    
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:apiName params:param sBlock:^(id responseObject) {
        [Utils HideProgress];
        [[Toast sharedInstance]makeToast:responseObject position:TOAST_POSITION_BOTTOM];
        model.iLike = !model.iLike;
        int likeCount = [model.likes intValue];
        likeCount = model.iLike ? likeCount + 1 : likeCount - 1;
        model.likes = @(likeCount).stringValue;
        [arrReplies replaceObjectAtIndex:indexPath.row withObject:model];
        [tblView reloadData];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
    
}

-(void)deleteCommentAPI:(NSString *)commentId
{
    NSDictionary *param =  @{
                             @"get":API_DELETE_COMMENT,
                             @"comment_id":commentId,
                             };
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_DELETE_COMMENT params:param sBlock:^(id responseObject) {
        [Utils HideProgress];
        [[Toast sharedInstance]makeToast:responseObject position:TOAST_POSITION_BOTTOM];
//        [self getPostDetails];
//
//        PostModel *model = self.selPostModel;
//        model.comments = @([model.comments intValue] - 1).stringValue;
//        self.selPostModel = model;
//
//        if (self.block) {
//            self.block(self.selPostModel,self.indexPath);
//        }
        if (self.deleteBlock) {
            self.deleteBlock();
        }
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
    }];
}

-(void)callBlockAPI:(NSString *)blockUserId completion:(void(^)())completion
{
    NSDictionary *params = @{
                             @"get":API_BLOCK_USER,
                             @"user_id":UTILS.currentUser.userId,
                             @"block_id":blockUserId
                             };
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_UNBLOCK_USER params:params sBlock:^(id responseObject) {
        [Utils HideProgress];
//        if (completion) {
//            completion();
//        }
        [CustomAlertView showAlert:@"" withMessage:responseObject];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
}


-(void)showOthersCommentsInSelfPostMoreOptions:(NSIndexPath *)indexPath
{
    CommentReplies *model = arrReplies[indexPath.row];
    
    NSArray *arr = @[
                     [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"block_connection", nil),[Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName]],
                     NSLocalizedString(@"Delete", nil)
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        if(index == 0)//copylink
        {
            [self callBlockAPI:model.userId completion:^{
                
            }];
        }
        else if (index == 1) {//delete post
            [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"msg_delete_account", nil) okHandler:^{
                [self deleteCommentAPI:model.commentId];
            }];
        }
        
    }];
}

-(void)showSelfCommentsMoreOptions:(NSIndexPath *)indexPath
{
    CommentReplies *model = arrReplies[indexPath.row];
    
    NSArray *arr = @[
                     NSLocalizedString(@"Delete", nil)
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        if(index == 0)//copylink
        {
            [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"msg_delete_account", nil) okHandler:^{
                [self deleteCommentAPI:model.commentId];
            }];
        }
    }];
}

-(void)showOthersCommentsInOthersPostMoreOptions:(NSIndexPath *)indexPath
{
    CommentReplies *model = arrReplies[indexPath.row];
    NSArray *arr = @[
                     [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"block_connection", nil),[Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName]],
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        if(index == 0)//copylink
        {
            [self callBlockAPI:model.userId completion:^{
                
            }];
        }
    }];
}


-(void)btnLikeClicked{
    if (self.likeBlk) {
        self.likeBlk();
    }
}

-(void)btnCommentClicked{
    if (self.commentBlock) {
        self.commentBlock();
    }
}

-(void)onBtnLikeCliked:(buttonBlock)blk
{
    self.likeBlk = blk;
}
-(void)onBtnCommentClicked:(buttonBlock)blk
{
    self.commentBlock = blk;
}
-(void)onDeleteCommentClicked:(buttonBlock)blk
{
    self.deleteBlock = blk;
}

-(void)btnBGClicked
{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.0;
        CGRect frame = tblView.frame;
        frame.origin.y = self.frame.size.height;
        tblView.frame = frame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
@end
