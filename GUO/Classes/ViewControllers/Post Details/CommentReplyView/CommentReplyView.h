//
//  CommentReplyView.h
//  GUO Media
//
//  Created by Pawan Ramteke on 01/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef BOOL(^buttonBlock)();
@interface CommentReplyView : UIView
-(instancetype)initWithFrame:(CGRect)frame withCommentModel:(PostComments *)model;

@property (nonatomic,copy)buttonBlock likeBlk;
@property (nonatomic,copy)buttonBlock commentBlock;
@property (nonatomic,copy)buttonBlock deleteBlock;

@property (nonatomic,strong)NSString *postOwnerId;

-(void)onBtnLikeCliked:(buttonBlock)blk;
-(void)onBtnCommentClicked:(buttonBlock)blk;
-(void)onDeleteCommentClicked:(buttonBlock)blk;

@end
