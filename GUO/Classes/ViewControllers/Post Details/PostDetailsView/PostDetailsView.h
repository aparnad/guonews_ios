//
//  PostDetailsView.h
//  GUO Media
//
//  Created by Pawan Ramteke on 16/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
typedef void(^clickBlock)();
@interface PostDetailsView : UIView

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewUserDetailsHtConstr;
@property (weak, nonatomic) IBOutlet UILabel *lblMainDescr;
@property (weak, nonatomic) IBOutlet UILabel *lblTranslate;

@property (weak, nonatomic) IBOutlet UIButton *btnShareUserDp;
@property (weak, nonatomic) IBOutlet UIImageView *imgShareUserDp;

@property (weak, nonatomic) IBOutlet UIButton *btnShareUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblShareTime;
@property (weak, nonatomic) IBOutlet UILabel *lblShareDescription;
@property (weak, nonatomic) IBOutlet UITextView *txtViewShareDesciption;

@property (weak, nonatomic) IBOutlet UILabel *lblShareTranslate;

@property (weak, nonatomic) IBOutlet UIButton *btnPoster1;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;

@property (weak, nonatomic) IBOutlet UIImageView *imgPoster1;
@property (weak, nonatomic) IBOutlet UIButton *btnPoster2;
@property (weak, nonatomic) IBOutlet UIImageView *imgPoster2;
@property (weak, nonatomic) IBOutlet UIButton *btnPoster3;
@property (weak, nonatomic) IBOutlet UIImageView *imgPoster3;
@property (weak, nonatomic) IBOutlet UIButton *btnMorePoster;

@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic)AVPlayerViewController *playerViewController;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayVideo;

@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnTranslate;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnTranslateHtConstr;

@property (weak, nonatomic) IBOutlet UIButton *btnShareTranslate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnShareTranslateHtConstr;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *poster1WidthConstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *poster1HtConstr;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *poster2WidthConstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *poster2HtConstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sharDescTopConstr;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTranslateHtConstr;
@property (weak, nonatomic) IBOutlet UILabel *lblTranslatedBy;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewShareTranslatedByHtConstr;
@property (weak, nonatomic) IBOutlet UILabel *lblShareTranslatedBy;

@property (nonatomic,strong)AVPlayerLayer *videoLayer;


@property (nonatomic,copy)clickBlock poster1Block;
@property (nonatomic,copy)clickBlock poster2Block;
@property (nonatomic,copy)clickBlock morePosterBlock;
@property (nonatomic,copy)clickBlock moreButtonBlock;

-(void)onPoster1Clicked:(clickBlock)blk;
-(void)onPoster2Clicked:(clickBlock)blk;
-(void)onMorePosterClicked:(clickBlock)blk;
-(void)onMoreButtonClicked:(clickBlock)blk;

@end
