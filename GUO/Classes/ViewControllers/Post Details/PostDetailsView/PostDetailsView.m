//
//  PostDetailsView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 16/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "PostDetailsView.h"

@implementation PostDetailsView
@synthesize playerViewController;
-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.imgShareUserDp.layer.cornerRadius = 20;
    self.imgShareUserDp.layer.masksToBounds = YES;
    self.imgShareUserDp.layer.borderWidth = 1.0;
    self.imgShareUserDp.layer.borderColor = header_color.CGColor;

    [Utils makeHalfRoundedButton:self.btnTranslate];
    [self.btnTranslate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.btnTranslate.layer.masksToBounds = YES;
    [self.btnTranslate setTitle:NSLocalizedString(@"Translate", nil) forState:UIControlStateNormal];
    
    [Utils makeHalfRoundedButton:self.btnShareTranslate];
    [self.btnShareTranslate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.btnShareTranslate.layer.masksToBounds = YES;
    [self.btnShareTranslate setTitle:NSLocalizedString(@"Translate", nil) forState:UIControlStateNormal];
    
    self.btnTranslate.titleLabel.font = [UIFont fontWithName:Font_Medium size:12];
    self.btnShareTranslate.titleLabel.font = self.btnTranslate.titleLabel.font;

//    playerViewController = [AVPlayerViewController new];
//    [self.videoView addSubview:playerViewController.view];
//    playerViewController.showsPlaybackControls = YES;
//    playerViewController.view.userInteractionEnabled = YES;
//
//    [playerViewController.view enableAutolayout];
//    [playerViewController.view leadingMargin:0];
//    [playerViewController.view trailingMargin:0];
//    [playerViewController.view topMargin:0];
//    [playerViewController.view bottomMargin:0];
    
    [self.btnPoster1 addTarget:self action:@selector(btnPoster1Clicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnPoster2 addTarget:self action:@selector(btnPoster2Clicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnMorePoster addTarget:self action:@selector(btnMorePosterClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnMore addTarget:self action:@selector(btnMoreClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.lblMainDescr.font = [UIFont fontWithName:Font_regular size:post_desc];
    self.lblShareDescription.font = self.txtViewShareDesciption.font = [UIFont fontWithName:Font_regular size:post_desc];

    self.lblShareTranslate.font = self.lblMainDescr.font;
    self.lblTranslate.font = self.lblMainDescr.font;
    
    self.lblTranslatedBy.font = [UIFont fontWithName:Font_regular size:14];
    self.lblTranslatedBy.textColor = header_color;

    self.lblShareTranslatedBy.font = self.lblTranslatedBy.font;
    self.lblShareTranslatedBy.textColor = self.lblShareTranslatedBy.textColor;

    self.txtViewShareDesciption.textContainerInset = UIEdgeInsetsMake(-30, 0, 0, 0 );
    
//    self.videoLayer = [[AVPlayerLayer alloc]init];
//    self.videoLayer.backgroundColor = [UIColor clearColor].CGColor;
//    self.videoLayer.videoGravity = AVLayerVideoGravityResize;
//    [self.viewSingleImagePost.imgOnePost1.layer addSublayer:self.videoLayer];

}

-(void)btnPoster1Clicked
{
    if (self.poster1Block) {
        self.poster1Block();
    }
}

-(void)btnPoster2Clicked
{
    if (self.poster2Block) {
        self.poster2Block();
    }
}

-(void)btnMorePosterClicked
{
    if (self.morePosterBlock) {
        self.morePosterBlock();
    }
}
-(void)btnMoreClicked
{
    if (self.moreButtonBlock) {
        self.moreButtonBlock();
    }
}

-(void)onPoster1Clicked:(clickBlock)blk
{
    self.poster1Block = blk;
}

-(void)onPoster2Clicked:(clickBlock)blk
{
    self.poster2Block = blk;
}

-(void)onMorePosterClicked:(clickBlock)blk
{
    self.morePosterBlock = blk;
}
-(void)onMoreButtonClicked:(clickBlock)blk{
    
    self.moreButtonBlock = blk;
}


@end
