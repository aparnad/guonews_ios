//
//  CommentView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 16/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "CommentView.h"


@implementation CommentView

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.imgViewDp.layer.cornerRadius = 25;
    self.imgViewDp.layer.masksToBounds = YES;
}

@end
