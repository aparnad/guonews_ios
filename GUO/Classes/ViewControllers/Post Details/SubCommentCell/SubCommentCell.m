//
//  SubCommentCell.m
//  GUO Media
//
//  Created by Pawan Ramteke on 16/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "SubCommentCell.h"

@implementation SubCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imgViewDp.layer.cornerRadius = 20;
    self.imgViewDp.layer.masksToBounds = YES;
    
    [self.btnImgDp addTarget:self action:@selector(btnImgDpClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnName addTarget:self action:@selector(btnImgDpClicked) forControlEvents:UIControlEventTouchUpInside];

    [self.btnMoreReplies addTarget:self action:@selector(btnMoreRepliesClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnLike addTarget:self action:@selector(btnLikeClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnComment addTarget:self action:@selector(btnCommentClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnViewImage addTarget:self action:@selector(btnViewImageClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnMore addTarget:self action:@selector(btnMoreClicked) forControlEvents:UIControlEventTouchUpInside];

    self.btnViewImage.layer.cornerRadius = 10;
   // self.btnViewImage.layer.masksToBounds = YES;
    self.lblName.font = [UIFont fontWithName:Font_Medium size:self.lblName.font.pointSize];
    self.lblTime.font = [UIFont fontWithName:Font_regular size:self.lblTime.font.pointSize];
    self.lblComment.font = [UIFont fontWithName:Font_regular size:self.lblComment.font.pointSize];
}

-(void)btnImgDpClicked
{
    if (self.btnShowProfileBlock) {
        self.btnShowProfileBlock(self.indexpath);
    }
}

-(void)btnMoreRepliesClicked
{
    if (self.btnMoreReplyBlock) {
        self.btnMoreReplyBlock(self.indexpath);
    }
}

-(void)btnLikeClicked
{
    if (self.btnLikeBlock) {
        self.btnLikeBlock(self.indexpath);
    }
}

-(void)btnCommentClicked
{
    if (self.btnCommentBlock) {
        self.btnCommentBlock(self.indexpath);
    }
}

-(void)btnViewImageClicked
{
    if (self.btnViewImageBlock) {
        self.btnViewImageBlock(self.indexpath);
    }
}

-(void)btnMoreClicked
{
    if (self.btnMoreBlock) {
        self.btnMoreBlock(self.indexpath);
    }
}

-(void)onButtonMoreRepliesClicked:(ButtonClickBlock)blk
{
    self.btnMoreReplyBlock = blk;
}

-(void)onButtonLikeClicked:(ButtonClickBlock)blk
{
    self.btnLikeBlock = blk;
}

-(void)onButtonCommentClicked:(ButtonClickBlock)blk
{
    self.btnCommentBlock = blk;
}

-(void)onViewImage:(ButtonClickBlock)blk
{
    self.btnViewImageBlock = blk;
}

-(void)onMoreButtonClicked:(ButtonClickBlock)blk
{
    self.btnMoreBlock = blk;
}

-(void)onShowProfile:(ButtonClickBlock)blk
{
    self.btnShowProfileBlock = blk;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
