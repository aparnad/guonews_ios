//
//  SubCommentCell.h
//  GUO Media
//
//  Created by Pawan Ramteke on 16/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ButtonClickBlock)(NSIndexPath *indexPath);
@interface SubCommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewDp;
@property (weak, nonatomic) IBOutlet UIButton *btnImgDp;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnName;

@property (weak, nonatomic) IBOutlet UILabel *lblComment;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewComment;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewCommHtConstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewCommTopConstr;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreReplies;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnMoreRepHtConstr;
@property (weak, nonatomic) IBOutlet UIButton *btnViewImage;
@property (weak, nonatomic) IBOutlet UIButton *btnVideo;
@property (weak, nonatomic) IBOutlet UIImageView *imgVerified;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;

@property (weak, nonatomic) NSIndexPath *indexpath;
@property (nonatomic,copy)ButtonClickBlock btnMoreReplyBlock;
@property (nonatomic,copy)ButtonClickBlock btnLikeBlock;
@property (nonatomic,copy)ButtonClickBlock btnCommentBlock;
@property (nonatomic,copy)ButtonClickBlock btnViewImageBlock;
@property (nonatomic,copy)ButtonClickBlock btnMoreBlock;
@property (nonatomic,copy)ButtonClickBlock btnShowProfileBlock;


-(void)onShowProfile:(ButtonClickBlock)blk;
-(void)onButtonMoreRepliesClicked:(ButtonClickBlock)blk;
-(void)onButtonLikeClicked:(ButtonClickBlock)blk;
-(void)onButtonCommentClicked:(ButtonClickBlock)blk;
-(void)onViewImage:(ButtonClickBlock)blk;
-(void)onMoreButtonClicked:(ButtonClickBlock)blk;

@end
