//
//  ReportUserPopup.h
//  GUO
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "PostModel.h"
#import "CustomPostTableView.h"


typedef void(^commentBlock)(PostModel *model);



@interface ReportUserPopup : UIViewController
{
    
}
@property(strong,nonatomic) PostModel *model;
@property(strong,nonatomic) IBOutlet UIView *headerView,*footerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property(strong,nonatomic) IBOutlet LPlaceholderTextView *txtDetail;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;

@property (nonatomic,assign)BOOL isComment;
@property (strong, nonatomic) IBOutlet UIButton *btnReply;
-(IBAction)onclick_Reply:(id)sender;

@property (nonatomic,copy)commentBlock updatedPost;
-(void)updatePostModel:(commentBlock)blk;


@end
