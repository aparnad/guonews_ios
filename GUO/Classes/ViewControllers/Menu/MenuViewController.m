//
//  MenuViewController.m
//  GUO
//
//  Created by Parag on 6/6/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "MenuViewController.h"
#import "LowerMenuCell.h"
#import "DashboardController.h"
#import "PostController.h"
#import "AppDelegate.h"


@interface MenuViewController ()<UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>
  @property (weak, nonatomic) IBOutlet UITableView *upperMenuTableView;
  @property (weak, nonatomic) IBOutlet UITableView *lowerMenuTableView;
  
  @property (weak, nonatomic) IBOutlet UILabel *nameSubLabel;
  @property (weak, nonatomic) IBOutlet UILabel *nameLabel;
  @property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
  
  
  @property (strong, nonatomic) NSMutableArray *upperMenuData;
  @property (strong, nonatomic) NSMutableArray *lowerMenuData;
  @end

@implementation MenuViewController

  
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
 // [self.lowerMenuTableView registerClass:[LowerMenuCell class] forCellReuseIdentifier:LowerMenuCellIdentifier];
  [self tempDataForMenu];
  [self setupIconView];
  self.lowerMenuTableView.backgroundColor = [UIColor lightGrayColor];
}
  
  
  -(void)setupIconView{
    self.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profileImageView.layer.borderWidth = 3.0;
    self.profileImageView.clipsToBounds = YES;
    self.profileImageView.layer.cornerRadius = 50.0;
    self.nameLabel.text = @"Jonathan Gordan";
    self.nameSubLabel.text = @"@jonathangordan";
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(profileTouched:)];
    tap.delegate  = self;
    [self.profileImageView addGestureRecognizer:tap];
    self.profileImageView.userInteractionEnabled = YES;
    
  }
  
  //TODO :PARAG : need to replace with actual data if required dynamic menu
  -(void)tempDataForMenu{
    
    self.upperMenuData = [[NSMutableArray alloc] initWithObjects:@"Post(1k)",@"Following(5k)",@"Followers(10k)",@"Likes(4k)",@"Broadcasts(3k)", nil];
    
    
    self.lowerMenuData = [[NSMutableArray alloc] initWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:@"ic_tv_normal",@"icon",@"TV",@"title",@"(05)",@"val", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"ic_news_normal",@"icon",@"Latest News",@"title",@"(05)",@"val", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"ic_account_normal",@"icon",@"Account Settings",@"title",@"",@"val", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"ic_privacy_normal",@"icon",@"Privacy",@"title",@"",@"val", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"ic_terms_conditions",@"icon",@"Terms & Conditions",@"title",@"",@"val", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"ic_about_normal",@"icon",@"About us",@"title",@"",@"val", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"ic_contact_normal",@"icon",@"Contact us",@"title",@"",@"val", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"ic_logout",@"icon",@"Logout",@"title",@"",@"val", nil],nil];
  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
  -(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.view.backgroundColor = [UIColor colorWithRed:160.0/255.0 green:160.0/255.0 blue:160.0/255.0 alpha:0.8];
  }
  
#pragma mark tableview delegate and datasource
  
  
  
  -(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
  }
  
  -(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
   return self.lowerMenuData.count + 1;
    
  }
  
  -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0)
    return self.upperMenuData.count * 46;
   return 60.0;
    
  }
  
  -(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell ;
    if (indexPath.row == 0){
      cell = [tableView dequeueReusableCellWithIdentifier:UpperMenuCellIdentifier];
      if (cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:UpperMenuCellIdentifier];
        
      }
      
      //cell.textLabel.text = [self.upperMenuData objectAtIndex:indexPath.row];
      [cell.contentView addSubview:[self addUpperMenuDataToFirstCell]];
      return cell;
    }
    else
    {
      
      NSDictionary *dictData = [self.lowerMenuData objectAtIndex:indexPath.row - 1];
      LowerMenuCell *lowerCell = (LowerMenuCell *)[tableView dequeueReusableCellWithIdentifier:LowerMenuCellIdentifier];
      if (lowerCell == nil){
       lowerCell = [[LowerMenuCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:UpperMenuCellIdentifier];
        
      }
      lowerCell.nameMenuLabel.text = [dictData objectForKey:@"title"];
      lowerCell.menuIconView.image = [UIImage imageNamed:[dictData objectForKey:@"icon"]];
      lowerCell.infoLabel.text = [dictData objectForKey:@"val"];
      return lowerCell;
    }
    return cell;
  }
  
  
  -(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self cloaseDrawer];
    NSInteger index = indexPath.row - 1;
    NSInteger menuVal = self.upperMenuData.count + index;
    switch (menuVal) {
      case TV :
        break;
        
      case News:
        
        break;
        
      case Account:
        [self displayAccountSettings];
        break;
        
      case Privacy :
        break;
        
      case Terms:
        
        break;
        
      case About:
        
        break;
        
      case Contact:
        
        break;
        
      case Logout :{
        
        AppDelegate *deleagte = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [deleagte displayLandingVC];
        break;
      }
        
      default:
        break;
    }
    
  }
  
#pragma mark action
  
  - (IBAction)closeDrawerTouched:(id)sender {
    
    [self  cloaseDrawer];
  }
  
  -(void)cloaseDrawer{
//    if ([self.target isKindOfClass:[DashboardController class]]){
//      DashboardController *dashboardController = (DashboardController*)self.target;
//      [dashboardController closeDrawer];
//    }
//    else if ([self.target isKindOfClass:[PostController class]]){
//        PostController *dashboardController = (PostController*)self.target;
//        [dashboardController closeDrawer];
//    }
  }
  
  -(void)upperMenuSelected:(UIButton *)btn{
    [self cloaseDrawer];
    switch (btn.tag) {
      case Posts:
        
        break;
        
      case Following:
        
        break;
        
      case Followers:
        
        break;
      case Likes:
        
        break;
      case Broadcast:
        break;
      
      
        
      default:
        break;
    }
    
  }


-(void)displayAccountSettings{
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PD_Storyboard" bundle:[NSBundle mainBundle]];
  UIViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"AccountSettingViewController"];
  [(AppObj).navController pushViewController:vc animated:YES];
}

  -(UIView *)addUpperMenuDataToFirstCell{
    UIView *containerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 70, self.upperMenuData.count * 46)];
    CGFloat xPos = 10;
    CGFloat yPos = 0;
    for(int index = 0; index < self.upperMenuData.count; index ++){
      
      
      UILabel *btnMenu = [[UILabel alloc]init];
      [btnMenu setFrame:CGRectMake(xPos, yPos, containerView.frame.size.width, 45)];
      btnMenu.tag = index;
      [btnMenu setText:[self.upperMenuData objectAtIndex:index]];
      UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
      [btn setFrame:CGRectMake(xPos, yPos, containerView.frame.size.width, 45)];
      btn.tag =  index;
      [btn addTarget:self action:@selector(upperMenuSelected:) forControlEvents:UIControlEventTouchUpInside];
      yPos = yPos + 45;
    
      
      UIView *separatorLine = [[UIView alloc]initWithFrame:CGRectMake(0, yPos, containerView.frame.size.width, 1)];
      separatorLine.backgroundColor = [UIColor grayColor];
      [containerView addSubview:separatorLine];
      [containerView addSubview:btn];
      [containerView addSubview:btnMenu];
      [containerView bringSubviewToFront:btnMenu];
      yPos = yPos + 1;
      
      
      
    }
    return containerView;
  }

-(void)profileTouched:(UIGestureRecognizer *)gesture{
  [self cloaseDrawer];
}
  
@end
