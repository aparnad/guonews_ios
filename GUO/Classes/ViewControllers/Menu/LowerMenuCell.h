//
//  LowerMenuCell.h
//  GUO
//
//  Created by Parag on 6/6/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LowerMenuCell : UITableViewCell
  @property (weak, nonatomic) IBOutlet UILabel *nameMenuLabel;
  @property (weak, nonatomic) IBOutlet UIImageView *menuIconView;
  @property (weak, nonatomic) IBOutlet UILabel *infoLabel;
  
@end
