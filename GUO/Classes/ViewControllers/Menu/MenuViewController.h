//
//  MenuViewController.h
//  GUO
//
//  Created by Parag on 6/6/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#define tagBtnUpperMenu 2000

typedef enum : NSUInteger {
  Posts = 0,
  Following,
  Followers,
  Likes,
  Broadcast,
  TV,
  News,
  Account,
  Privacy,
  Terms,
  About,
  Contact,
  Logout,
} MenuItem;



@protocol MenuControllerDelegate
@optional
  -(void)menuTouched:(MenuItem)index;
  
@end

@interface MenuViewController : UIViewController
  @property(weak, nonatomic) id  target;
  @property(weak, nonatomic) id <MenuControllerDelegate> delegate;

@end
