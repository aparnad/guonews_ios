//
//  MessageController.h
//  GUO
//
//  Created by mac on 05/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageCell.h"
#import "NewMessageController.h"

@interface MessageController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *ArrMsg;
}
@property(strong,nonatomic) IBOutlet UITableView *aTableView;
@property(strong,nonatomic) IBOutlet UIView *headerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;

-(IBAction)onclick_menu:(id)sender;
-(IBAction)onclick_new_chat:(id)sender;
@property(strong,nonatomic) IBOutlet UITextField *txtSearch;
-(IBAction)onclick_view_profile_GUO:(id)sender;
@property(strong,nonatomic) CommentPopup *comment_popup;
@property(strong,nonatomic) IBOutlet UIButton *btn_header_userimg;
@property(strong,nonatomic) CommonDocumentController *commonDocNav;

@end
