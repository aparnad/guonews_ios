//
//  MessageCell.m
//  GUO
//
//  Created by mac on 05/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    
    self.LblSeparator.backgroundColor = tbl_separator_color;

    self.imgIndicator.image = [Utils setTintColorToUIImage:self.imgIndicator.image color:[UIColor grayColor]];
    [Utils SetRoundedCorner:self.imgUser];
    
        self.lblName.font = [UIFont fontWithName:Font_Medium size:userfull_name_font_size];
        self.lblMsg.font = [UIFont fontWithName:Font_regular size:username_font_size];
    self.lblDate.font = [UIFont fontWithName:Font_regular size:date_time_font_size];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
