//
//  MessageCell.h
//  GUO
//
//  Created by mac on 05/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCell : UITableViewCell
{
    
}
@property(strong,nonatomic) IBOutlet UIImageView *imgUser,*imgIndicator;
@property(strong,nonatomic) IBOutlet UILabel *lblName,*lblMsg,*lblDate,*LblSeparator;
@end
