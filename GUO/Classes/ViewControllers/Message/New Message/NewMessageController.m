//
//  NewMessageController.m
//  GUO
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "NewMessageController.h"

@interface NewMessageController ()

@end

@implementation NewMessageController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];

    [Utils SearchContainerDesign:self.txtSearch ContainerView:self.view];
    self.view.backgroundColor = light_gray_bg_color;
    // Do any additional setup after loading the view.
    self.lblHeader.text = NSLocalizedString(@"txt_title_new_message", nil);
    self.txtSearch.placeholder = NSLocalizedString(@"txt_search", nil);

}
#pragma mark- button click event
-(IBAction)onclick_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
