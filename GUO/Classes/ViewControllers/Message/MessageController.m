//
//  MessageController.m
//  GUO
//
//  Created by mac on 05/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "MessageController.h"

@interface MessageController ()

@end

@implementation MessageController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils SearchContainerDesign:self.txtSearch ContainerView:self.view];
    self.view.backgroundColor = light_gray_bg_color;

    ArrMsg = [[NSMutableArray alloc]init];
   /* [ArrMsg addObject:@{@"Name":@"Ronan",
                       @"LastSenderName":@"Jackson",
                       @"Message":@"Hello",
                       @"Time":@"5 minutes ago",
                       @"ImgName":@"1"
                        }];
    [ArrMsg addObject:@{@"Name":@"Brayden",
                        @"LastSenderName":@"Ethan",
                        @"Message":@"I'll call you later",
                        @"Time":@"1 hour ago",
                        @"ImgName":@"2"
                        }];
    [ArrMsg addObject:@{@"Name":@"Hugo",
                        @"LastSenderName":@"Noah",
                        @"Message":@"What about you?",
                        @"Time":@"10 hours ago",
                        @"ImgName":@"3"
                        }];
    [ArrMsg addObject:@{@"Name":@"Diego",
                        @"LastSenderName":@"Logan",
                        @"Message":@"Sorry, Today I will not come",
                        @"Time":@"23 hours ago",
                        @"ImgName":@"4"
                        }];
    [ArrMsg addObject:@{@"Name":@"Antonio",
                        @"LastSenderName":@"Lucas",
                        @"Message":@"It's very beautiful",
                        @"Time":@"3 days ago",
                        @"ImgName":@"5"
                        }];
    [ArrMsg addObject:@{@"Name":@"Marco",
                        @"LastSenderName":@"James",
                        @"Message":@"Don't worry!! I will be always there for u.",
                        @"Time":@"10 days ago",
                        @"ImgName":@"6"
                        }];
    [ArrMsg addObject:@{@"Name":@"Steffan",
                        @"LastSenderName":@"Brick",
                        @"Message":@"yes yes sure, I will do anything for u.",
                        @"Time":@"20 days ago",
                        @"ImgName":@"7"
                        }];
    [ArrMsg addObject:@{@"Name":@"Devin",
                        @"LastSenderName":@"Bay",
                        @"Message":@"What are you doing?",
                        @"Time":@"24 days ago",
                        @"ImgName":@"8"
                        }];
    [ArrMsg addObject:@{@"Name":@"Ethan",
                        @"LastSenderName":@"Amias",
                        @"Message":@"I'm in meeting.",
                        @"Time":@"1 months ago",
                        @"ImgName":@"9"
                        }];
    [ArrMsg addObject:@{@"Name":@"Aaron",
                        @"LastSenderName":@"Ahmet",
                        @"Message":@"You should try all the things that you want.",
                        @"Time":@"2 months ago",
                        @"ImgName":@"10"
                        }];*/
    UILabel *noDataLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(self.headerView.frame), Screen_Width - (20*2), self.view.frame.size.height - CGRectGetMaxY(self.headerView.frame))];
    noDataLbl.text = @"No message available";
    noDataLbl.numberOfLines = 0;
    noDataLbl.textAlignment = NSTextAlignmentCenter;
    [noDataLbl setFont:[UIFont fontWithName:Font_Medium size:header_font_size]];
    noDataLbl.textColor = header_color;
    [self.view addSubview:noDataLbl];
    
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];
    // Do any additional setup after loading the view.
    self.aTableView.delegate = self;
    self.aTableView.dataSource = self;

    self.aTableView.rowHeight = UITableViewAutomaticDimension;
    [self.view addGestureRecognizer:[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureRecognized:)]];
    
    [Utils SetRoundedCorner:self.btn_header_userimg];
   // self.btn_header_userimg.layer.borderWidth = header_user_img_border;
   // self.btn_header_userimg.layer.borderColor = [UIColor whiteColor].CGColor;


    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Update_message_screen_control" object:nil];//remove single noti
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocalization) name:@"Update_message_screen_control" object:nil];
    
    
    [UTILS addFloating:self.view tab_height:0 screen_name:@"message"];

    
}
-(void)updateLocalization
{
    self.lblHeader.text = NSLocalizedString(@"title_message", nil);
    self.txtSearch.placeholder = NSLocalizedString(@"txt_search_for_message", nil);
    
    [self.aTableView reloadData];

    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateLocalization];
}
#pragma mark- Menu swipe gesture
- (void)swipeGestureRecognized:(UISwipeGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    if(self.frostedViewController.swipeGestureEnabled)
    {
        [self.frostedViewController presentMenuViewController];
    }
}

#pragma mark - Tableview Delegate and Datasource
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrMsg.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    MessageCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    if(cell==nil)
    {
        cell=[[MessageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageCell"];
        
        NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"MessageCell" owner:self options:nil];
        cell=[menuarray objectAtIndex:0];
    }
    NSDictionary *dic= [ArrMsg objectAtIndex:indexPath.row];
    cell.imgUser.image = [UIImage imageNamed:[dic objectForKey:@"ImgName"]];
    cell.lblName.text = [dic objectForKey:@"Name"];
    cell.lblMsg.text = [NSString stringWithFormat:@"%@: %@",[dic objectForKey:@"LastSenderName"],[dic objectForKey:@"Message"]];
    cell.lblDate.text = [dic objectForKey:@"Time"];

    
    cell.selectionStyle = NO;
//    if(ArrMsg.count-1 == indexPath.row)
//    {
//        cell.LblSeparator.hidden = YES;
//    }
    return cell;
}

#pragma mark- Button click
-(IBAction)onclick_menu:(id)sender
{
    [self.frostedViewController.view endEditing:YES];
    [self.frostedViewController presentMenuViewController];
}
-(IBAction)onclick_new_chat:(id)sender
{
    
    NewMessageController *newmessageNav = [(AppObj).monika_storyboard  instantiateViewControllerWithIdentifier:@"NewMessageController"];
    newmessageNav.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:newmessageNav animated:YES];

}
-(IBAction)onclick_view_profile_GUO:(id)sender
{
    [(AppObj).postView showUserProfile:GUO_USERID];

    
}
#pragma mark- Textfield delegate method
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
