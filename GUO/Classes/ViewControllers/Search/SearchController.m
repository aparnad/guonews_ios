//
//  SearchController.m
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "SearchController.h"

@interface SearchController ()

@end

@implementation SearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];

    [Utils SearchContainerDesign:self.txtSearch ContainerView:self.view];
    self.view.backgroundColor = light_gray_bg_color;
    // Do any additional setup after loading the view.
    [self.view addGestureRecognizer:[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureRecognized:)]];
   
}
-(void)updateLocalization
{
    self.lblHeader.text = NSLocalizedString(@"title_search", nil);
    self.txtSearch.placeholder = NSLocalizedString(@"search_for_global", nil);}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateLocalization];

}
#pragma mark- Menu swipe gesture
- (void)swipeGestureRecognized:(UISwipeGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    if(self.frostedViewController.swipeGestureEnabled)
    {
        [self.frostedViewController presentMenuViewController];
    }
}

#pragma mark- button click event
-(IBAction)onclick_menu:(id)sender
{
    [self.frostedViewController.view endEditing:YES];
    [self.frostedViewController presentMenuViewController];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
