//
//  SlideMenuVC.h
//  BeThereBeautyC
//
//  Created by YOGENDRA  on 30/09/16.
//  Copyright © 2016 Leocan1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import "UIViewController+REFrostedViewController.h"
#import "SideMenuCell.h"
#import "DEMONavigationController.h"
#import "SliderHeaderCell.h"
#import "CommonDocumentController.h"
#import "AccountSettingViewController.h"
#import "ProfileViewController.h"
#import "MoreFollowListController.h"
#import "LandingViewController.h"
#import "BroadcastViewController.h"
#import "ChangeLanguagePopup.h"
#import "EditProfileController.h"
#import "PostController.h"
#import "UserProfileController.h"
#import "UIImage+AFNetworking.h"


@interface SlideMenuVC : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UINavigationController *navControl;
    UINavigationController *demo_navController;
    NSArray *menuArr,*headerTitleArr;

}
@property(strong,nonatomic)IBOutlet UITableView *menu_table,*header_table;
@property(strong,nonatomic)IBOutlet UIView *tbl_header_view;
@property(strong,nonatomic)IBOutlet UILabel *lblName,*lblUname;
@property(strong,nonatomic)IBOutlet UIImageView *imgUser;

@property (nonatomic, assign) CGFloat size;
@property (nonatomic, assign) CGFloat HeaderviewHeight;
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, strong) UIColor *backgroundColor NS_DEPRECATED_IOS(7_0, 8_0);
@property (nonatomic, strong) UIColor *selectionColor;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIFont *sectionTitleFont;
@property (nonatomic, strong) UIColor *sectionTitleColor;
@property (nonatomic, strong) UIColor *iconsColor;

@property (weak, nonatomic) UIView *bgView;
    @property(strong,nonatomic)IBOutlet UIButton *btnChangeLanguage;

@property(strong,nonatomic) CommonDocumentController *commonDocNav;
@property(strong,nonatomic) AccountSettingViewController *AccountSettingNav;
@property(strong,nonatomic) ProfileViewController *profileNav;
@property(strong,nonatomic) MoreFollowListController *followingVC;
@property(strong,nonatomic) BroadcastViewController *broadcastVC;
@property(strong,nonatomic) ChangeLanguagePopup *changeLanguagePopup;
@property(strong,nonatomic) PostController *postvc;
@property(strong,nonatomic) UserProfileController *userProfileVC;

-(IBAction)onclick_localization:(id)sender;
-(IBAction)onclick_ViewProfile:(id)sender;
-(IBAction)onclick_following:(id)sender;
-(IBAction)onclick_followers:(id)sender;

@property(strong,nonatomic) IBOutlet UILabel *lblFollowings,*lblFollowers,*lblTopBorder;

@end
