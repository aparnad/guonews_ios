//
//  SideMenuCell.h
//  VKSideMenuExample
//
//  Created by Dell on 27/11/17.
//  Copyright © 2017 Vladislav Kovalyov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCell : UITableViewCell
{
    
}
@property(strong,nonatomic) IBOutlet UILabel *Lbltitle,*lblCount;
@property(strong,nonatomic) IBOutlet UIImageView *Img;
@end
