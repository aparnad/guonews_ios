//
//  SideMenuCell.m
//  VKSideMenuExample
//
//  Created by Dell on 27/11/17.
//  Copyright © 2017 Vladislav Kovalyov. All rights reserved.
//

#import "SideMenuCell.h"

@implementation SideMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.Lbltitle.textColor = self.lblCount.textColor = [UIColor blackColor];
    self.Lbltitle.font = [UIFont fontWithName:Font_regular size:drawer_Cell_title_font];
    self.lblCount.font = [UIFont fontWithName:Font_regular size:drawer_Cell_title_font];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
