//
//  SlideMenuVC.m
//  BeThereBeautyC
//
//  Created by YOGENDRA  on 30/09/16.
//  Copyright © 2016 Leocan1. All rights reserved.
//

#import "SlideMenuVC.h"
#import "ALChatManager.h"
#import "SupportViewController.h"


#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define ROOTVC [[[[UIApplication sharedApplication] delegate] window] rootViewController]
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define  tbl_header_cell_height 38.0

@class CustomTabVC;

@interface SlideMenuVC ()


@end

@implementation SlideMenuVC
@synthesize bgView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.size                       = 220;
    self.rowHeight                  = 48;
    self.sectionTitleFont   = [UIFont systemFontOfSize:15.];
    self.selectionColor     = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.3];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    self.backgroundColor    = [UIColor whiteColor];
#pragma clang diagnostic pop
    self.textColor          = UIColorFromRGB(0x252525);
    self.iconsColor         = UIColorFromRGB(0x8f8f8f);

    
    // Setup content table view
    self.menu_table.separatorColor   = [UIColor clearColor];
    self.menu_table.backgroundColor  = [UIColor clearColor];
    self.view.backgroundColor = light_gray_bg_color;
    
    self.lblFollowings.font =  [UIFont fontWithName:Font_regular size:[UTILS getFlexibleHeight:drawer_following_follower]];
    self.lblFollowers.font =  [UIFont fontWithName:Font_regular size:[UTILS getFlexibleHeight:drawer_following_follower]];
    self.lblName.font = [UIFont fontWithName:Font_Medium size:drawer_User_title_font_size];
    self.lblUname.font = [UIFont fontWithName:Font_regular size:username_font_size];

    self.lblFollowings.adjustsFontSizeToFitWidth = YES;
    self.lblFollowers.adjustsFontSizeToFitWidth = YES;
    
    self.lblFollowers.minimumScaleFactor = self.lblFollowings.minimumScaleFactor = 0.5;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelFollowrTapped)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [ self.lblFollowers addGestureRecognizer:tapGestureRecognizer];
     self.lblFollowers.userInteractionEnabled = YES;
    
    
    UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelFollowingTapped)];
    tapGestureRecognizer1.numberOfTapsRequired = 1;
    [ self.lblFollowings addGestureRecognizer:tapGestureRecognizer1];
    self.lblFollowings.userInteractionEnabled = YES;

}
-(void)labelFollowrTapped
{
    [self onclick_followers:nil];
}

-(void)labelFollowingTapped
{
    [self onclick_following:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  
    self.lblName.text = [NSString stringWithFormat:@"%@",[Utils setFullname:UTILS.currentUser.userFirstname lname:UTILS.currentUser.userLastname username:UTILS.currentUser.userName]];

    
    self.lblUname.text = nil;//UTILS.currentUser.userName;
    

    
    
//    [self.imgUser sd_setImageWithURL:[Utils getProperContentUrl:UTILS.currentUser.userPicture] placeholderImage:defaultUserImg];

//setProfilePic
    if([Utils getProfilePic:p_name])
    {
        self.imgUser.image =[Utils getProfilePic:p_name];
      
    }
    else
    {

        [self.imgUser sd_setImageWithURL:[Utils getThumbUrl:UTILS.currentUser.userPicture] placeholderImage:defaultUserImg completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            [Utils setProfilePic:image pic_name:p_name];
          //  self.imgUser.image =image;

        }];
    }
    
    menuArr = @[@{@"title":NSLocalizedString(@"post_header_title", nil),
                  @"icon":@"ic_menu_post",
                  @"is_menu":@"1",
                  @"count":[NSString stringWithFormat:@"%d",(int)(AppObj).app_count_model.posts]},
                @{@"title":NSLocalizedString(@"likes", nil),
                  @"icon":@"ic_menu_like",
                  @"is_menu":@"1",
                  @"count":[NSString stringWithFormat:@"%d",(int)(AppObj).app_count_model.likes]},
                @{@"title":NSLocalizedString(@"News", nil),
                  @"icon":@"ic_news",
                  @"is_menu":@"1",
                  @"count":@""},
                @{@"title":NSLocalizedString(@"nav_account_setting", nil),
                  @"icon":@"ic_account_normal",
                  @"is_menu":@"1",
                  @"count":@""},
                @{@"title":NSLocalizedString(@"nav_change_language", nil),
                  @"icon":@"ic_change_language",
                  @"is_menu":@"1",
                  @"count":@""},
                @{@"title":NSLocalizedString(@"nav_privacy", nil),
                  @"icon":@"ic_privacy_normal",
                  @"is_menu":@"1",
                  @"count":@""},
                @{@"title":NSLocalizedString(@"nav_terms", nil),
                  @"icon":@"ic_terms_conditions",
                  @"is_menu":@"1",
                  @"count":@""},
                @{@"title":NSLocalizedString(@"nav_contact_us", nil),
                  @"icon":@"ic_contact_normal",
                  @"is_menu":@"1",
                  @"count":@""},
               
                @{@"title":NSLocalizedString(@"nav_logout", nil),
                  @"icon":@"ic_logout",
                  @"is_menu":@"1",
                  @"count":@""}];
    
    
    
    
    
//    (AppObj).currIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    self.tbl_header_view.backgroundColor = header_color;
    self.lblTopBorder.backgroundColor =header_color;

    [Utils SetRoundedCorner:self.imgUser];
    //self.imgUser.layer.borderWidth = 3;
    //self.imgUser.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lblUname.textColor = light_gray_bg_color;
    

    
    [self.btnChangeLanguage.titleLabel setFont:[UIFont fontWithName:Font_regular size:drawer_User_title_font_size]];
    if([[NSString stringWithFormat:@"%@",[GUOSettings GetLanguage]] isEqualToString:[NSString stringWithFormat:@"%@",SetEnglish]])
    {
        [self.btnChangeLanguage setTitle:[NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(@"nav_change_language", nil),English_str] forState:UIControlStateNormal];
    }
    else
    {
        [self.btnChangeLanguage setTitle:[NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(@"nav_change_language", nil),Chinese_str] forState:UIControlStateNormal];
        
    }
    
    [self.menu_table reloadData];
    
  
    self.lblFollowings.text = [NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(@"txt_following", nil),[Utils suffixNumber:[NSString stringWithFormat:@"%d",(int)(AppObj).app_count_model.followings]]];
    self.lblFollowers.text = [NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(@"txt_followers", nil),[Utils suffixNumber:[NSString stringWithFormat:@"%d",(int)(AppObj).app_count_model.followers]]];
    
   
}



#pragma mark UITableView Delegate


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.tbl_header_view.frame.size.height;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    return self.tbl_header_view;
}


#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [menuArr objectAtIndex:indexPath.row];
    if([[dict objectForKey:@"is_menu"] boolValue])
    {
        return 44;
    }
    else
    {
        return tbl_header_cell_height;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    
    return menuArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    
    
    NSDictionary *dict = [menuArr objectAtIndex:indexPath.row];
    if([[dict objectForKey:@"is_menu"] boolValue])
    {
        SideMenuCell *cell=(SideMenuCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"SideMenuCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        cell.clipsToBounds=YES;
        
        
        cell.Lbltitle.text = [dict valueForKey:@"title"];
        
        if([[dict valueForKey:@"count"] length]>0)
        {
            cell.lblCount.text = [NSString stringWithFormat:@"(%@)",[dict valueForKey:@"count"]];
        }
        else
        {
            cell.lblCount.text = nil;
        }
        
        if((AppObj).currIndexPath !=nil && [(AppObj).currIndexPath compare:indexPath]==NSOrderedSame)
        {
            cell.Lbltitle.textColor    = header_color;
            cell.Img.image=[Utils setTintColorToUIImage:[UIImage imageNamed:[dict valueForKey:@"icon"]] color:header_color];
        }
        else
        {
            cell.Lbltitle.textColor    = [UIColor blackColor];
            cell.Img.image=[UIImage imageNamed:[dict valueForKey:@"icon"]];
        }
        //    cell.SeperatorLBl.backgroundColor    = Sliding_HeaderBgColor;
        
        cell.backgroundColor = light_gray_bg_color;
        cell.selectionStyle = NO;
        
        
        return cell;
        
    }
    else
    {
        SliderHeaderCell *cell=(SliderHeaderCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"SliderHeaderCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        cell.clipsToBounds=YES;
        cell.lblTitle.text = [NSString stringWithFormat:@"%@ (%@)",[dict valueForKey:@"title"],[dict valueForKey:@"count"]];
        cell.selectionStyle = NO;
        return cell;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [self.frostedViewController hideMenuViewController];

    (AppObj).currIndexPath = indexPath;
    [tableView reloadData];
    
    self.commonDocNav = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"CommonDocumentController"];

    if (indexPath.row == 0 ) //post //Like
    {
        [AppObj LoadSlidingMenu:YES];

       // UTILS.apiToCall = API_GET_POST;
        self.postvc = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"PostController"];
        self.postvc.is_came_from_sliding_menu = TRUE;
        self.postvc.apiName = API_GET_POST;
        [[UTILS topMostController].navigationController pushViewController:self.postvc animated:YES];

    }
     else if (indexPath.row == 1)
     {
         self.postvc = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"PostController"];
         self.postvc.is_came_from_sliding_menu = TRUE;
        // UTILS.apiToCall = API_LIKED_POST;
         self.postvc.apiName = API_LIKED_POST;
         [[UTILS topMostController].navigationController pushViewController:self.postvc animated:YES];
     }
    else if (indexPath.row == 3) //Account setting
    {
        self.AccountSettingNav = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"AccountSettingViewController"];
        [[UTILS topMostController].navigationController pushViewController:self.AccountSettingNav animated:YES];
    }
    else  if (indexPath.row == 4) // Chnage language
    {
        [self onclick_localization:self];
    }
   
    else  if (indexPath.row == 5) //Privacy
    {
        self.commonDocNav.headerStr = NSLocalizedString(@"nav_privacy", nil);
        self.commonDocNav.webUrlStr = privacy_url;
        self.commonDocNav.screen_name = privacy_screen;

        [[UTILS topMostController].navigationController pushViewController:self.commonDocNav animated:YES];
    }
    else  if (indexPath.row == 6) //t and c
    {
        self.commonDocNav.headerStr = NSLocalizedString(@"nav_terms", nil);
        self.commonDocNav.webUrlStr = tnc_url;
        self.commonDocNav.screen_name = tnc_screen;
        [[UTILS topMostController].navigationController pushViewController:self.commonDocNav animated:YES];
    }
    else if(indexPath.row  == 7)
    {
        SupportViewController *supportVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"SupportViewController"];
        [[UTILS topMostController].navigationController pushViewController:supportVC animated:YES];
    }
    else if (indexPath.row == 2)//News
    {
        [AppObj loadNewsSlidingMenu:YES];
        CustomTabVC *customTabVC = (CustomTabVC*)[[UIStoryboard storyboardWithName:@"News" bundle:nil] instantiateViewControllerWithIdentifier:@"CUSTOM_TAB"];
        [self.navigationController pushViewController:customTabVC animated:true];
    }
    else if(indexPath.row  == 8) //Logout
    {
        [self logoutButtonPressed];
    }
    return;
//    {
//
//        self.broadcastVC = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"BroadcastViewController"];
//
//        [self.navigationController pushViewController:self.broadcastVC animated:YES];
//
//    }
//    else if (indexPath.row == 6) //Account setting
//    {
//        self.AccountSettingNav = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"AccountSettingViewController"];
//
//        [self.navigationController pushViewController:self.AccountSettingNav animated:YES];
//    }
//    
//    else if(indexPath.row == 7) //Privacy
//    {
//        self.commonDocNav.headerStr = NSLocalizedString(@"nav_privacy", nil);
//        self.commonDocNav.webUrlStr = privacy_url;
//        [self.navigationController pushViewController:self.commonDocNav animated:YES];
//        
//
//    }
//    else if(indexPath.row == 8) //t&c
//    {
//        self.commonDocNav.headerStr = NSLocalizedString(@"nav_terms", nil);
//        self.commonDocNav.webUrlStr = tnc_url;
//
//        [self.navigationController pushViewController:self.commonDocNav animated:YES];
//    }
//    else if(indexPath.row == 9) //about us
//    {
//        self.commonDocNav.headerStr = NSLocalizedString(@"nav_about", nil);
//        self.commonDocNav.webUrlStr = aboutus_url;
//
//        [self.navigationController pushViewController:self.commonDocNav animated:YES];
//    }
//    else if(indexPath.row == 10) //contact us
//    {
//        self.commonDocNav.headerStr = NSLocalizedString(@"nav_contact_us", nil);
//        self.commonDocNav.webUrlStr = contactus_url;
//
//        [self.navigationController pushViewController:self.commonDocNav animated:YES];
//    }
//    else if(indexPath.row == 11) //logout
//    {
//        [self logoutButtonPressed];
//        //        [self.navigationController popToRootViewControllerAnimated:YES];
//    }
//    else
//    {
//        [self.frostedViewController hideMenuViewController];
//    }
//    return;
//
//    demo_navController.navigationBarHidden=YES;
//    self.frostedViewController.contentViewController = demo_navController;
//    
//    [self.frostedViewController hideMenuViewController];
    
}
- (void)logoutButtonPressed
{
    [self.frostedViewController hideMenuViewController];

    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:NSLocalizedString(@"nav_logout", nil)
                                 message:NSLocalizedString(@"message_logout", nil)
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"message_dialog_yes", nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    
                                     [self logoutAPI];
                                    
                                    
                                    UTILS.currentUser = nil;
                                    [GUOSettings setIsLogin:nil];
                                    [GUOSettings SetLanguage:nil];
                                    
                                   

                                    [self logoutApplozic];
                                    
                                    [Utils removeProfilePicPath];
                                    LandingViewController *navVC = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
                                    navVC.is_came_from_sliding_menu = false;
                                    
                                   
                                    (AppObj).app_count_model = [CountModel new];
                                     (AppObj).navController=[[UINavigationController alloc] initWithRootViewController:navVC];
                                    (AppObj).navController.navigationBarHidden=YES;
                                    (AppObj).window.rootViewController = (AppObj).navController;
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"message_dia_cancel", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [NSUserDefaults.standardUserDefaults removeObjectForKey:@"IsNewsFIrstTime"];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)displayAccountSettings{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PD_Storyboard" bundle:[NSBundle mainBundle]];
    UIViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"AccountSettingViewController"];
    
    [(AppObj).navController pushViewController:vc animated:YES];
}


#pragma mark- Button click
-(IBAction)onclick_post:(id)sender
{
    [self.frostedViewController hideMenuViewController];

    self.postvc = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"PostController"];
    self.postvc.is_came_from_sliding_menu = TRUE;
    [[UTILS topMostController].navigationController pushViewController:self.postvc animated:YES];
}
-(IBAction)onclick_following:(id)sender
{
    [self.frostedViewController hideMenuViewController];

    self.followingVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"MoreFollowListController"];
    self.followingVC.header_str = NSLocalizedString(@"txt_following", nil);
    self.followingVC.screen_identifier = followingscreen;
    self.followingVC.get_user_id= UTILS.currentUser.userId;
    [[UTILS topMostController].navigationController pushViewController:self.followingVC animated:YES];
}
-(IBAction)onclick_followers:(id)sender
{
    [self.frostedViewController hideMenuViewController];

    self.followingVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"MoreFollowListController"];
    self.followingVC.get_user_id= UTILS.currentUser.userId;
    self.followingVC.header_str = NSLocalizedString(@"txt_followers", nil);
    self.followingVC.screen_identifier = followersscreen;

    [[UTILS topMostController].navigationController pushViewController:self.followingVC animated:YES];
}

-(IBAction)onclick_localization:(id)sender
{
    [self.frostedViewController hideMenuViewController];
    
    self.changeLanguagePopup = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"ChangeLanguagePopup"];
    [UTILS showCustomPopup:self.changeLanguagePopup.view];
}

-(IBAction)onclick_ViewProfile:(id)sender;
{
    [self.frostedViewController hideMenuViewController];
   
    
    if (![UTILS checkIntenetShowError]) {
        return;
    }
    
    
    self.userProfileVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"UserProfileController"];
    self.userProfileVC.is_came_From_slide_menu = TRUE;
    self.userProfileVC.other_user_id = UTILS.currentUser.userId;
    [[UTILS topMostController].navigationController pushViewController:self.userProfileVC animated:YES];
    
    
    
    
//        [self.frostedViewController hideMenuViewController];
//        self.editProfileNav = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"EditProfileController"];
//    
//        [self.navigationController pushViewController:self.editProfileNav animated:YES];


}

-(void)logoutApplozic
{
    ALRegisterUserClientService *registerUserClientService = [[ALRegisterUserClientService alloc] init];
    [registerUserClientService logoutWithCompletionHandler:^(ALAPIResponse *response, NSError *error) {
        if(!error && [response.status isEqualToString:@"success"])
        {
            NSLog(@"Logout success");
        }
        else
        {
            NSLog(@"Logout failed with response : %@",response.response);
        }
    }];
    
}

-(void)logoutAPI
{

    NSDictionary *params = @{
                             @"get":API_LOGOUT,
                             @"user_id":UTILS.currentUser.userId,
                             @"device_token":[GUOSettings getDeviceToken],
                             @"device_type":DEVICE_TYPE
                             };
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_LOGOUT params:params sBlock:^(id responseObject) {
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
       // [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
