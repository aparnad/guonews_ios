//
//  SliderHeaderCell.h
//  GUO
//
//  Created by mac on 09/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderHeaderCell : UITableViewCell
{
    
}
@property(strong,nonatomic) IBOutlet UILabel *lblTitle,*lblSeparator;
@end
