//
//  SliderHeaderCell.m
//  GUO
//
//  Created by mac on 09/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "SliderHeaderCell.h"

@implementation SliderHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblSeparator.backgroundColor = tbl_separator_color;
    self.lblTitle.font = [UIFont fontWithName:Font_regular size:drawer_Cell_title_font];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
