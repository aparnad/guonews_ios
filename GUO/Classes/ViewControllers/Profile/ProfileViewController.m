//
//  ProfileViewController.m
//  GUO
//
//  Created by Parag on 6/7/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ProfileViewController.h"
#import "ProfileCellWithTextView.h"
#import "ProfileCellWIthTextField.h"
#import "UIView+DropShadow.h"

@interface ProfileViewController () <UITableViewDelegate, UITableViewDataSource, ProfileCellWithTextViewDelegate, ProfileCellWIthTextFieldDelegate>
  @property (weak, nonatomic) IBOutlet UIView *containerView;
  @property (weak, nonatomic) IBOutlet UIButton *btnCamera;
  @property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfProfileFullName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfProfileEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfProfileUsername;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *tvProfileAboutMe;

  @property (weak, nonatomic) IBOutlet UIButton *btnSave;
  
  @property (strong, nonatomic) NSMutableArray * profileData;

@property (weak, nonatomic) IBOutlet UIScrollView *profileScrollView;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self profileFormData];
  [self setupProfile];
    // Do any additional setup after loading the view.
  
}
-(void)setupProfile{
  self.btnCamera.layer.cornerRadius = 21;
  self.btnCamera.clipsToBounds = YES;
  
  //self.imageViewProfile.layer.borderWidth = 4.0;
 // self.imageViewProfile.layer.borderColor = [UIColor whiteColor].CGColor;
  self.imageViewProfile.layer.cornerRadius = 110/2;
  self.imageViewProfile.clipsToBounds = YES;
  
  self.btnSave.layer.cornerRadius = 45/2 ;
  self.btnSave.clipsToBounds = YES;
  
  
  [self.tfProfileEmail addDropShadow:UIColor.grayColor
                       withOffset:CGSizeMake(0, 1)
                           radius:0.5f
                          opacity:.4];
  
  [self.tfProfileFullName addDropShadow:UIColor.grayColor
                          withOffset:CGSizeMake(0, 1)
                              radius:0.5f
                             opacity:.4];
  
  [self.tfProfileUsername addDropShadow:UIColor.grayColor
                             withOffset:CGSizeMake(0, 1)
                                 radius:0.5f
                                opacity:.4];
  
  [self.tvProfileAboutMe addDropShadow:UIColor.grayColor
                             withOffset:CGSizeMake(0, 1)
                                 radius:0.5f
                                opacity:.4];
  
  
  self.tfProfileEmail.floatingLabelFont = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
  self.tfProfileEmail.floatingLabelTextColor = [UIColor lightGrayColor];
  self.tfProfileEmail.floatingLabelActiveTextColor = [UIColor lightGrayColor];
  self.tfProfileEmail.keepBaseline = YES;
  
  
  self.tfProfileUsername.floatingLabelFont = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
  self.tfProfileUsername.floatingLabelTextColor = [UIColor lightGrayColor];
  self.tfProfileUsername.floatingLabelActiveTextColor = [UIColor lightGrayColor];
  self.tfProfileUsername.keepBaseline = YES;
  
  
    self.tfProfileFullName.floatingLabelFont = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
  self.tfProfileFullName.floatingLabelTextColor = [UIColor lightGrayColor];
  self.tfProfileFullName.floatingLabelActiveTextColor = [UIColor lightGrayColor];
  self.tfProfileFullName.keepBaseline = YES;
  
  self.tvProfileAboutMe.floatingLabelFont = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
  self.tvProfileAboutMe.font = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
  self.tvProfileAboutMe.placeholder = @"About me";
  self.tvProfileAboutMe.placeholderLabel.font = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
  self.tvProfileAboutMe.floatingLabelTextColor = [UIColor lightGrayColor];
  self.tvProfileAboutMe.floatingLabelActiveTextColor = [UIColor lightGrayColor];
  
}
  -(void)profileFormData{
    
    self.profileData = [[NSMutableArray alloc]init];
    [self.profileData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Full Name",@"title",@"Full name",@"placeholder",@"1",@"isTF", nil]];
     [self.profileData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Email",@"title",@"Email",@"placeholder",@"1",@"isTF", nil]];
     [self.profileData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"User Name",@"title",@"User name",@"placeholder",@"1",@"isTF", nil]];
     [self.profileData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"About me",@"title",@"About me",@"placeholder",@"0",@"isTF", nil]];
  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtnTouched:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}
  
- (IBAction)btnSaveTouched:(id)sender {
  
}
- (IBAction)btnCameraTouched:(id)sender {
}

  
#pragma mark tableview delegate and datasource
  
  -(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
  }
  
  -(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.profileData.count;
    
  }
  
  -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *profileDict = [self.profileData objectAtIndex:indexPath.row];
    if ([[profileDict objectForKey:@"isTF"] boolValue])
    return 85;
    else
    return 180;
    
  }
  
  -(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell ;
    NSDictionary *profileDict = [self.profileData objectAtIndex:indexPath.row];
    if ([[profileDict objectForKey:@"isTF"] boolValue]){
      ProfileCellWIthTextField *cellTF = (ProfileCellWIthTextField *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPRofileWithTF];
      if (cellTF == nil){
        cellTF = [[ProfileCellWIthTextField alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierPRofileWithTF];
        
      }
      cellTF.delegate = self;
      cellTF.lblTitle.text = [profileDict objectForKey:@"title"];
      cellTF.textFieldProfile.placeholder = [profileDict objectForKey:@"placeholder"];
      return cellTF;
    }
    else{
      ProfileCellWithTextView *cellTV = (ProfileCellWithTextView *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPRofileWithTV];
      if (cellTV == nil){
        cellTV = [[ProfileCellWithTextView alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierPRofileWithTV];
        
      }
      cellTV.delegate = self;
      cellTV.lblTitle.text = [profileDict objectForKey:@"title"];
     // cellTV.textViewProfile.placeholder = [profileDict objectForKey:@""];
      return cellTV;
    }
    return cell;
  }
  
  
  -(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
  }

#pragma mark Profile text delagte

-(void)textFieldBeginEditing{
  
  
}

-(void)textViewBeginEditing{
  
  
}

-(void)textFieldEndEditing{
  
}

-(void)textViewEndEditing{
  
  
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
