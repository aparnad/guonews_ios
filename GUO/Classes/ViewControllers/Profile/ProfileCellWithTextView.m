//
//  ProfileCellWithTextView.m
//  GUO
//
//  Created by Parag on 6/8/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ProfileCellWithTextView.h"

@interface ProfileCellWithTextView() <UITextViewDelegate>
@end

@implementation ProfileCellWithTextView

- (void)awakeFromNib {
    [super awakeFromNib];
  self.textViewProfile.floatingLabelFont = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
  self.textViewProfile.placeholder = @"About me";
  self.textViewProfile.placeholderLabel.font = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
  self.textViewProfile.floatingLabelTextColor = [UIColor lightGrayColor];
  self.textViewProfile.floatingLabelActiveTextColor = [UIColor lightGrayColor];
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
  [self.delegate textViewBeginEditing];
}

-(void)textViewDidEndEditing:(UITextView *)textView{
  [self.delegate textViewEndEditing];
}

@end
