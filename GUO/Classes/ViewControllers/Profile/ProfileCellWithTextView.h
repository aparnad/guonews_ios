//
//  ProfileCellWithTextView.h
//  GUO
//
//  Created by Parag on 6/8/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextView.h"

#define CellIdentifierPRofileWithTV @"CellTVIdentifier"

@protocol ProfileCellWithTextViewDelegate
@optional

-(void)textViewBeginEditing;
-(void)textViewEndEditing;
@end

@interface ProfileCellWithTextView : UITableViewCell
  @property (weak, nonatomic) IBOutlet UILabel *lblTitle;
  @property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *textViewProfile;
  @property(weak, nonatomic) id <ProfileCellWithTextViewDelegate>delegate;
  
@end
