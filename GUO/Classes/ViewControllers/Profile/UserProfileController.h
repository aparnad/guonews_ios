//
//  UserProfileController.h
//  GUO
//
//  Created by Pawan Ramteke on 14/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "BaseViewController.h"
#import "EditProfileController.h"
#import "UserModel.h"

@interface UserProfileController : BaseViewController
{
}

@property(strong,nonatomic) EditProfileController *editProfileNav;
@property(strong,nonatomic) MoreFollowListController *followingVC;
@property BOOL is_came_From_slide_menu;

@property NSString *other_user_id;
@end
