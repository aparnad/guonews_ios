//
//  UserProfileController.m
//  GUO
//
//  Created by Pawan Ramteke on 14/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "UserProfileController.h"
#import "ProfileHeaderView.h"
#import "HMSegmentedControl.h"
#import "ProfilePhotosCollectionCell.h"
#import "ProfileCollectionCell.h"
#import "GlobalPostCollectionCell.h"
#import "AboutCollectionCell.h"
#import "VerificationPopup.h"
#import "DGActivityIndicatorView.h"
#import "ProfileTableCell.h"
#import <Applozic/Applozic.h>
#import "CreatePostController.h"
#import "ALChatManager.h"

#define TAB_PHOTOS  @"Photos"
#define TAB_POSTS   @"Posts"
#define TAB_LIKES   @"Likes"
@interface UserProfileController ()<UITableViewDataSource,UITableViewDelegate>
{
    
    __weak IBOutlet UITableView *_tableView;
    
    __weak IBOutlet UIView *headerView;
    
    __weak IBOutlet UIImageView *imgHeaderDp;
    
    __weak IBOutlet UILabel *lblHeaderTitle;
    
    UIImageView *imgViewCoverPic;
    UIImageView *imgViewDp;
    UILabel *lblUserName;
    UILabel *lblBio;
    UIImageView *vipIcon;
    UIButton *btnFollow;
    UIButton *btnSetupProfile,*btnFollowing,*btnFollowers;
    UIButton *btnVerifyAccount;
    UILabel *lblJoinDate;

    NSArray *arrTabTitles;
    HMSegmentedControl *segmentedControl;
    
    int current_page;
    int postCurrentPage;
    int likePostCurrentPage;
    UserModel *userModel;
    
    NSMutableDictionary *mainDict;
    NSArray *arrTitles;
    
    NSLayoutConstraint *coverHtConstr;
    NSLayoutConstraint *dpHtConstr;
    NSLayoutConstraint *dpWidthConstr;

    NSLayoutConstraint *bioHtConstr;

    NSInteger selSegIndex;
}


@property(strong,nonatomic) VerificationPopup *verifyPopup;

@end

@implementation UserProfileController

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = header_color;

    mainDict = [NSMutableDictionary new];
    
    UTILS.userIdOfVisitedProfile = self.other_user_id;

    [self UserProfileAPI];
    [self followCountAPI];
    
    imgHeaderDp.alpha = 0;
    imgHeaderDp.layer.cornerRadius = 20;
    imgHeaderDp.layer.masksToBounds = YES;
    
    lblHeaderTitle.alpha = 0;
    lblHeaderTitle.font = [UIFont fontWithName:Font_Medium size:18];
    
    _tableView.estimatedRowHeight = 400;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    _tableView.tableHeaderView = [self tableHeaderView];
    _tableView.backgroundColor = header_color;
    _tableView.delaysContentTouches = YES;
    _tableView.canCancelContentTouches = YES;
   // _tableView.bounces = NO;
    _tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    if([self.other_user_id isEqualToString:UTILS.currentUser.userId])
    {
        [self setupProfileData];
    }
}

-(UIView *)tableHeaderView
{
    UIView *header = [[UIView alloc]init];
    header.backgroundColor = [UIColor controllerBGColor];
    
    imgViewCoverPic = [[UIImageView alloc]init];
    imgViewCoverPic.backgroundColor = header_color;
    imgViewCoverPic.contentMode = UIViewContentModeScaleAspectFill;
    imgViewCoverPic.clipsToBounds = YES;
    [header addSubview:imgViewCoverPic];
    [imgViewCoverPic enableAutolayout];
    [imgViewCoverPic leadingMargin:0];
    [imgViewCoverPic topMargin:0];
    [imgViewCoverPic trailingMargin:0];
    
    coverHtConstr = [NSLayoutConstraint constraintWithItem:imgViewCoverPic attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:150];
    [imgViewCoverPic addConstraint:coverHtConstr];
    
    imgViewDp = [[UIImageView alloc]init];
    [header addSubview:imgViewDp];
    
    imgViewDp.layer.cornerRadius = 35;
   // imgViewDp.layer.borderWidth = 3;
   // imgViewDp.layer.borderColor = header_color.CGColor;
    imgViewDp.contentMode = UIViewContentModeScaleAspectFill;
    imgViewDp.layer.masksToBounds = YES;
    imgViewDp.image = defaultUserImg;
    
    [imgViewDp enableAutolayout];
    [imgViewDp leadingMargin:20];
    [imgViewDp belowView:-50 toView:imgViewCoverPic];
    
    dpWidthConstr = [NSLayoutConstraint constraintWithItem:imgViewDp attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:70];

    dpHtConstr = [NSLayoutConstraint constraintWithItem:imgViewDp attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:70];
    
    [imgViewDp addConstraint:dpHtConstr];
    [imgViewDp addConstraint:dpWidthConstr];
    
    lblUserName = [[UILabel alloc]init];
    [header addSubview:lblUserName];
    
    [lblUserName enableAutolayout];
    [lblUserName leadingMargin:30];
    [lblUserName belowView:0 toView:imgViewDp];
    [lblUserName fixHeight:30];
    
    vipIcon = [[UIImageView alloc]init];
    [header addSubview:vipIcon];
    vipIcon.image = [UIImage imageNamed:@"ic_badge_small"];
    vipIcon.hidden = YES;
    [header addSubview:vipIcon];
    
    UIImageView *imgCalendar = [[UIImageView alloc]init];
    imgCalendar.image = [UIImage imageNamed:@"ic_calendar"];
    [header addSubview:imgCalendar];
    
    [imgCalendar enableAutolayout];
    [imgCalendar leadingMargin:30];
    [imgCalendar belowView:5 toView:lblUserName];
    [imgCalendar fixWidth:20];
    [imgCalendar fixHeight:20];
    
    lblJoinDate = [[UILabel alloc]init];
    lblJoinDate.font = [UIFont fontWithName:Font_regular size:14];
    [header addSubview:lblJoinDate];
    
    [lblJoinDate enableAutolayout];
    [lblJoinDate enableAutolayout];
    [lblJoinDate addToRight:5 ofView:imgCalendar];
    [lblJoinDate trailingMargin:20];
    [lblJoinDate belowView:5 toView:lblUserName];
    

    
    lblBio = [[UILabel alloc]init];
    lblBio.font = [UIFont fontWithName:Font_regular size:16];
    lblBio.numberOfLines = 0;
    [header addSubview:lblBio];
    
    [lblBio enableAutolayout];
    [lblBio leadingMargin:30];
    [lblBio trailingMargin:20];
    [lblBio belowView:10 toView:lblJoinDate];
    
    bioHtConstr = [NSLayoutConstraint constraintWithItem:lblBio attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
    
    [lblBio addConstraint:bioHtConstr];
    
    btnFollow = [[UIButton alloc]init];
    [btnFollow setBackgroundColor:header_color];
    [btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnFollow.layer.borderColor = header_color.CGColor;
    btnFollow.layer.borderWidth = 2;
    btnFollow.clipsToBounds = YES;
    btnFollow.layer.cornerRadius = 15;
    [btnFollow.titleLabel setFont:[UIFont fontWithName:Font_Medium size:15]];
    btnFollow.hidden = YES;
    [btnFollow addTarget:self action:@selector(onclick_Follow:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:btnFollow];
    
    
    [btnFollow enableAutolayout];
    [btnFollow trailingMargin:20];
    [btnFollow fixWidth:130];
    [btnFollow belowView:10 toView:imgViewCoverPic];
    [btnFollow fixHeight:30];
    
    
   

    
    

    [vipIcon enableAutolayout];
    [vipIcon addToRight:5 ofView:lblUserName];
    [vipIcon centerYToView:lblUserName];
    [vipIcon fixWidth:20];
    [vipIcon fixHeight:20];
    
    
    UIImageView *imgFollowing = [[UIImageView alloc]init];
    imgFollowing.image = [UIImage imageNamed:@"ic_followings"];
    [header addSubview:imgFollowing];
    
    [imgFollowing enableAutolayout];
    [imgFollowing leadingMargin:20];
    [imgFollowing fixWidth:20];
    [imgFollowing fixHeight:20];
    
    btnFollowing = [[UIButton alloc]init];
    [header addSubview:btnFollowing];
    [btnFollowing.titleLabel setFont:[UIFont fontWithName:Font_regular size:username_font_size]];
    [btnFollowing setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnFollowing addTarget:self action:@selector(Onclick_following:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnFollowing enableAutolayout];
    [btnFollowing addToRight:5 ofView:imgFollowing];
    [btnFollowing belowView:0 toView:lblBio];
    [btnFollowing fixHeight:40];
    [btnFollowing bottomMargin:10];
    [imgFollowing centerYToView:btnFollowing];

    UIImageView *imgFollowers = [[UIImageView alloc]init];
    imgFollowers.image = [UIImage imageNamed:@"ic_followers"];
    [header addSubview:imgFollowers];
    
    [imgFollowers enableAutolayout];
    [imgFollowers addToRight:20 ofView:btnFollowing];
    [imgFollowers fixWidth:20];
    [imgFollowers fixHeight:20];
    
    btnFollowers = [[UIButton alloc]init];
    [header addSubview:btnFollowers];
    [btnFollowers.titleLabel setFont:[UIFont fontWithName:Font_regular size:username_font_size]];
    [btnFollowers setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnFollowers addTarget:self action:@selector(onclick_followers:) forControlEvents:UIControlEventTouchUpInside];

    [btnFollowers enableAutolayout];
    [btnFollowers addToRight:5 ofView:imgFollowers];
    [btnFollowers equalTopToView:btnFollowing];
    [btnFollowers fixHeight:40];
    
    [imgFollowers centerYToView:btnFollowers];

    UIButton *btnDirectMessage = [[UIButton alloc]init];
    [btnDirectMessage setImage:[UIImage imageNamed:@"ic_direct_message"] forState:UIControlStateNormal];
    [btnDirectMessage addTarget:self action:@selector(btnDirectMessageClicked) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:btnDirectMessage];
    
    [btnDirectMessage enableAutolayout];
    [btnDirectMessage trailingMargin:0];
    [btnDirectMessage fixWidth:40];
    [btnDirectMessage fixHeight:40];
    [btnDirectMessage centerYToView:lblJoinDate];
    
    
    if ([_other_user_id isEqualToString:UTILS.currentUser.userId]) {
        btnDirectMessage.hidden = YES;
    }
    else{
        btnDirectMessage.hidden = NO;
    }
    
    
    btnSetupProfile = [[UIButton alloc]init];
    [header addSubview:btnSetupProfile];
    btnSetupProfile.layer.cornerRadius = 15;
    btnSetupProfile.layer.masksToBounds = YES;
    [btnSetupProfile setTitleColor:header_color forState:UIControlStateNormal];
    btnSetupProfile.backgroundColor = [UIColor whiteColor];
    [btnSetupProfile.titleLabel setFont:[UIFont fontWithName:Font_Medium size:drawer_Cell_title_font]];
    [btnSetupProfile setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Setup Profile", nil)] forState:UIControlStateNormal];
    [btnSetupProfile addTarget:self action:@selector(btnSetupProfileClicked:) forControlEvents:UIControlEventTouchUpInside];
    btnSetupProfile.hidden = YES;
    
    [btnSetupProfile enableAutolayout];
    [btnSetupProfile trailingMargin:20];
    [btnSetupProfile belowView:-60 toView:imgViewCoverPic];
    [btnSetupProfile equalWidthToView:btnFollow];
    [btnSetupProfile equalHeightToView:btnFollow];
    
    btnVerifyAccount = [[UIButton alloc]init];
    [header addSubview:btnVerifyAccount];
    btnVerifyAccount.layer.cornerRadius = btnSetupProfile.layer.cornerRadius;
    btnVerifyAccount.layer.shadowColor = [[UIColor blackColor] CGColor];
    btnVerifyAccount.layer.shadowOffset = CGSizeMake(0, 2.0f);
    btnVerifyAccount.layer.shadowOpacity = 1.0f;
    btnVerifyAccount.layer.shadowRadius = 0.0f;
    btnVerifyAccount.layer.masksToBounds = NO;
    btnVerifyAccount.backgroundColor = RGB(228, 193, 54);
    [btnVerifyAccount.titleLabel setFont:[UIFont fontWithName:Font_Medium size:drawer_Cell_title_font]];
    btnVerifyAccount.hidden = YES;
    [btnVerifyAccount addTarget:self action:@selector(btnVerifyAccountClicked:) forControlEvents:UIControlEventTouchUpInside];
    if([self.other_user_id isEqualToString:UTILS.currentUser.userId]) // Current user
    {
        [btnVerifyAccount setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Verify Account", nil)] forState:UIControlStateNormal];
    }
    
    [btnVerifyAccount enableAutolayout];
    [btnVerifyAccount trailingMargin:20];
    [btnVerifyAccount equalTopToView:btnFollow];
    [btnVerifyAccount equalWidthToView:btnFollow];
    [btnVerifyAccount equalHeightToView:btnFollow];
    
    
    //Setting iCon
    
    UIButton* btnSetting = [[UIButton alloc]init];
    [btnSetting setBackgroundColor:[UIColor clearColor]];
    [btnSetting addTarget:self action:@selector(settingClicked:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:btnSetting];
    [btnSetting setImage:[UIImage imageNamed:@"ic_account_normal"] forState:UIControlStateNormal];
    [btnSetting enableAutolayout];
    [btnSetting centerYToView:btnVerifyAccount];
    [btnSetting fixWidth:30];
    
    if ([UTILS.currentUser.userVerified isEqualToString:@"0"]) {
        [btnSetting addToLeft:10 ofView:btnVerifyAccount];
    }
    else{
        [btnSetting trailingMargin:10];
    }
    //
    [btnSetting fixHeight:30];
    
    
    if (![self.other_user_id isEqualToString:UTILS.currentUser.userId]) {
        btnSetting.hidden = YES;
    }
    
    return header;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self sizeHeaderToFit];
}

- (void)sizeHeaderToFit
{
    UIView *header = _tableView.tableHeaderView;
    
  //  [header setNeedsLayout];
  //  [header layoutIfNeeded];
    
    CGFloat height = [header systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = header.frame;
    
    frame.size.height = height;
    header.frame = frame;
    
    _tableView.tableHeaderView = header;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellId"];
    if (!cell) {
        cell = [[ProfileTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellId"];
    }
    //cell.userInteractionEnabled = NO;
   
    cell.dataDict = mainDict;
    if ([UTILS.currentUser.userId isEqualToString:GUO_USERID] || [self.other_user_id isEqualToString:GUO_USERID]) {
        cell.userBiography = userModel.userBiography;
    }
    [cell onCollectionScroll:^(int pageNo) {
        [segmentedControl setSelectedSegmentIndex:pageNo animated:YES];
    }];
    
    [cell onLoadMore:^(NSInteger index) {
        if (index == 1) {
            [self getUserPhotosAPI];
        }
        else if (index == 0){
            [self userPostListAPI];
        }
        else if (index == 2){
            [self getUserLikedPosts];
        }
    }];
    
    [cell onRefresh:^(NSInteger index) {
        
    }];
    
    [cell onScrollToTop:^{
       // cell.userInteractionEnabled = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"disable_child_scroll" object:nil];

    }];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return Screen_Height - 50;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   
    arrTabTitles = @[NSLocalizedString(@"post_header_title", nil),NSLocalizedString(@"Media", nil),NSLocalizedString(@"likes", nil)];
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:arrTabTitles];
    segmentedControl.frame = CGRectMake(0, 0, Screen_Width, 50);
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.selectionIndicatorColor = [UIColor blackColor];
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont fontWithName:Font_Medium size:segment_tab_font_size]};
    segmentedControl.backgroundColor = [UIColor whiteColor];
    segmentedControl.selectionIndicatorHeight = 3;
    [segmentedControl setSelectedSegmentIndex:selSegIndex animated:YES];
    return segmentedControl;
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)updateApplozicProfilePic
{
    NSLog(@"UTILS.currentUser.userPicture = %@",UTILS.currentUser.userPicture);
    ALUserService *userService = [ALUserService new];
    [userService updateUserDisplayName:UTILS.currentUser.userName
                          andUserImage:[[Utils getThumbUrl:UTILS.currentUser.userPicture] absoluteString]
                            userStatus:@""
                        withCompletion:^(id theJson, NSError *error) {
                            
                            NSLog(@"Applozic pic updated successfully = %@",theJson);
                            
                        }];
    
}

-(void)UserProfileAPI
{
    [UTILS ShowProgress];
    
    NSDictionary *params = @{
                             @"get":API_GET_USER_PROFILE,
                             @"user_id":self.other_user_id,
                             @"me_id":UTILS.currentUser.userId,
                             };
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_GET_USER_PROFILE params:params sBlock:^(id responseObject) {
        
        
        userModel = responseObject;
        [self setupProfileData];
        [Utils HideProgress];
        [self getUserPhotosAPI];
        [self userPostListAPI];
        [self getUserLikedPosts];
        [self VerifiedButton:userModel];
        
        [self updateApplozicProfilePic];
        
        
        if (![self.other_user_id isEqualToString:UTILS.currentUser.userId])//Other User
        {
            btnSetupProfile.hidden = YES;
            
            /*NSString *status_title = userModel.isFollowing ? NSLocalizedString(@"txt_following", nil) :  NSLocalizedString(@"btn_follow", nil);

            [btnFollow setTitle:status_title forState:UIControlStateNormal];*/
            [self updateFollowButtonTitle:userModel];

            
            
            btnFollow.hidden = NO;
        }
        else//Current User
        {
            btnSetupProfile.hidden = NO;

        }
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
}

-(void)setupProfileData
{
    lblUserName.text = [NSString stringWithFormat:@"%@",[Utils setFullname:userModel.userFirstname lname:userModel.userLastname username:userModel.userName]];
    
    lblHeaderTitle.text = lblUserName.text;
    
    lblBio.text = userModel.userBiography;
    
    lblJoinDate.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Joined", nil),[self getFormattedJoiningDate:userModel.userRegistered]];
    
    [lblBio sizeToFit];
    
    //show bio for only GUO Miles
    if (lblBio.text.length > 0 && [self.other_user_id isEqualToString:GUO_USERID]) {
        bioHtConstr.constant = lblBio.frame.size.height;
    }
    
    
    
  //  lblBio.numberOfLines = 0;
   // [lblBio sizeToFit];
    
    if([userModel.userId isEqualToString:UTILS.currentUser.userId])
    {
        if([Utils getProfilePic:p_name])
        {
            imgViewDp.image = [Utils getProfilePic:p_name];
        }
        else
        {
            [imgViewDp sd_setImageWithURL:[Utils getProperContentUrl:UTILS.currentUser.userPicture] placeholderImage:defaultUserImg completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
               
                
                if (image) {
                    [Utils setProfilePic:image pic_name:p_name];
                    imgViewDp.image =image;
                }
            }];
        }
        
        if([Utils getProfilePic:w_name])
        {
            imgViewCoverPic.image =[Utils getProfilePic:w_name];
        }
        else
        {
            [imgViewCoverPic sd_setImageWithURL:[Utils getProperContentUrl:UTILS.currentUser.userCover] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                [Utils setProfilePic:image pic_name:w_name];
                imgViewCoverPic.image =image;
            }];
        }
    }
    else
    {
        [imgViewDp sd_setImageWithURL:[Utils getProperContentUrl:userModel.userPicture] placeholderImage:defaultUserImg];
        [imgViewCoverPic sd_setImageWithURL:[Utils getProperContentUrl:userModel.userCover] placeholderImage:nil];
    }
   
    
//    vipIcon.hidden= userModel.userVerified ? NO : YES;
 //   [self sizeHeaderToFit];
}


-(void)getUserPhotosAPI
{
    if ([mainDict objectForKey:TAB_PHOTOS] == nil) {
        current_page = 1;
    }
    NSDictionary *params = @{
                             @"get":API_SEARCH,
                             @"keyword":API_SEARCH_KEYWORD,
                             @"type":API_SEARCH_TYPE,
                             @"user_id":userModel.userId,
                             @"limit":RecordLimit,
                             @"page":[NSString stringWithFormat:@"%d",current_page]
                             };

    [REMOTE_API CallPOSTWebServiceWithParam:API_SEARCH params:params sBlock:^(id responseObject) {
        
        
        if([responseObject count] > 0)
        {
            current_page += 1;
        }
        
        
        NSMutableArray *arrPhotosVideos = [NSMutableArray new];
        for (PostModel *model in responseObject) {
            for (Photos *photosModel in model.photos) {
                [arrPhotosVideos addObject:photosModel];
            }
            
            for (Photos *videosModel in model.videos) {
                [arrPhotosVideos addObject:videosModel];
            }
        }
        
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[mainDict objectForKey:TAB_PHOTOS]];
        [arr addObjectsFromArray:arrPhotosVideos];
        
        [mainDict setObject:arr forKey:TAB_PHOTOS];
        [_tableView reloadData];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
   }];
}

-(void)userPostListAPI
{
//    GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
//
//    [cell apiCallInitiated];
    
    NSArray *arr = [mainDict objectForKey:TAB_POSTS];
    if(arr.count == 0)
    {
        postCurrentPage = 1;
    }
    
    NSDictionary *params= @{
                            @"get":API_GET_POST,
                            @"user_id":userModel.userId,
                            @"me_id":[userModel.userId isEqualToString:UTILS.currentUser.userId] ? @"-1" : UTILS.currentUser.userId,
                            @"limit":RecordLimit,
                            @"page":[NSString stringWithFormat:@"%d",postCurrentPage],
                            @"self":@"1"
                        
                            };
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_GET_POST params:params sBlock:^(id responseObject) {
    
        NSArray *response = responseObject;
        if (response.count) {
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[mainDict objectForKey:TAB_POSTS]];
            
            [arr addObjectsFromArray:response];
            
            [mainDict setObject:arr forKey:TAB_POSTS];

            postCurrentPage += 1;
        }
        else{
            
        }
        [_tableView reloadData];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
    }];
    
}


-(void)getUserLikedPosts
{
    
//    GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
//
//    [cell apiCallInitiated];
    NSArray *arr = [mainDict objectForKey:TAB_LIKES];
    if(arr.count == 0)
    {
        likePostCurrentPage = 1;
    }

    NSDictionary *params= @{
                            @"get":API_LIKED_POST,
                            @"user_id":userModel.userId,
                            @"me_id":[userModel.userId isEqualToString:UTILS.currentUser.userId] ? @"-1" : UTILS.currentUser.userId,
                            @"limit":RecordLimit,
                            @"page":[NSString stringWithFormat:@"%d",likePostCurrentPage]
                            };
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_LIKED_POST params:params sBlock:^(id responseObject) {
        
       
        
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[mainDict objectForKey:TAB_LIKES]];
        [arr addObjectsFromArray:responseObject];
        [mainDict setObject:arr forKey:TAB_LIKES];
        
//        GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
//        [cell apiCallEndWithResultCount:[arr count] pageNo:likePostCurrentPage];
        
        if([responseObject count] > 0)
        {
            likePostCurrentPage += 1;
        }
        
        [_tableView reloadData];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
//        GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
//        [cell apiCallEndWithResultCount:0 pageNo:1];
    }];
}

-(void)followCountAPI
{
    __block NSString *total_following,*total_followers;
    NSDictionary *params = @{
                             @"get":API_GET_COUNT,
                             @"user_id":self.other_user_id
                             };

    [REMOTE_API CallPOSTWebServiceWithParam:API_GET_COUNT params:params sBlock:^(id responseObject) {
        
        CountModel *c_model = [[CountModel alloc]init];
        c_model = responseObject;
        total_following = [Utils suffixNumber:[NSString stringWithFormat:@"%d",(int)c_model.followings]];
        total_followers = [Utils suffixNumber:[NSString stringWithFormat:@"%d",(int)c_model.followers]];
        [btnFollowing setTitle:[NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(@"txt_following", nil),total_following] forState:UIControlStateNormal];
        [btnFollowers setTitle:[NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(@"txt_followers", nil),total_followers] forState:UIControlStateNormal];
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (![scrollView isKindOfClass:[UITableView class]]) {
        int page = scrollView.contentOffset.x / scrollView.frame.size.width;
        [segmentedControl setSelectedSegmentIndex:page animated:YES];
        selSegIndex = page;
    }
}


- (void)btnSetupProfileClicked:(id)sender {

    self.editProfileNav = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"EditProfileController"];

    [self.navigationController pushViewController:self.editProfileNav animated:YES];
    
    [self.editProfileNav onUpdateProfileSuccess:^(NSString *value){
        imgViewCoverPic.image =[Utils getProfilePic:w_name];
        imgViewDp.image = [Utils getProfilePic:p_name];
        lblUserName.text = value;
    }];

}
-(void)VerifiedButton:(UserModel*)model
{
    if([model.userVerified isEqualToString:@"1"])
    {
       // btnVerifyAccount.hidden = YES;
        vipIcon.hidden = NO;
    }
    else
    {
       // btnVerifyAccount.hidden = NO;
        vipIcon.hidden = YES;
        
    }
    
    if ([UTILS.currentUser.userId isEqualToString:self.other_user_id]) {
        if([model.userVerified isEqualToString:@"1"])
        {
            btnVerifyAccount.hidden = YES;
        }
        else
        {
            btnVerifyAccount.hidden = NO;
        }
    }
}

-(void)btnDirectMessageClicked
{
    ALChatManager* chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLICATION_ID];
    
    [chatManager launchChatForUserWithDefaultText:_other_user_id andFromViewController:[UTILS topMostControllerNormal]];
}

-(void)verifyAccountAPI
{

    
    NSDictionary *params = @{
                             @"get":API_VERIFIED_PROFILE,
                             @"user_id":UTILS.currentUser.userId,
                             @"email":UTILS.currentUser.userEmail
                             };
    
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_VERIFIED_PROFILE params:params sBlock:^(id responseObject) {
        
        self.verifyPopup = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"VerificationPopup"];
        self.verifyPopup.strDisplayMessage= responseObject;
        [UTILS showCustomPopup:self.verifyPopup.view];
        [Utils HideProgress];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        
        
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
    
}
- (void)btnVerifyAccountClicked:(id)sender {
    if([UTILS.currentUser.userId isEqualToString:self.other_user_id])
    {
//        self.verifyPopup = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"VerificationPopup"];
//        [UTILS showCustomPopup:self.verifyPopup.view];
        
        if (UTILS.currentUser.userEmail.length == 0) {
            
            [CustomAlertView showAlert:@"" withMessage:NSLocalizedString(@"Please update your email to verify account.", nil)];
        }
        else{
            [self verifyAccountAPI];

        }
        
    }
}
-(void)follow_unfollow_API:(NSString*)API_NAME
{
    NSDictionary *params = @{
                             @"get":API_NAME,
                             @"user_id":UTILS.currentUser.userId,
                             @"following_id":userModel.userId
                             };
    
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_NAME params:params sBlock:^(id responseObject) {
        userModel.isFollowing=!userModel.isFollowing;
       
        CountModel *model = (AppObj).app_count_model;
        if ([API_NAME isEqualToString:API_FOLLOW]) {
            model.followings = model.followings + 1;
        }
        else{
            model.followings = model.followings - 1;
        }
        (AppObj).app_count_model = model;
        /*NSString *status_title = NSLocalizedString(@"btn_follow", nil);
        if(userModel.isFollowing)
        {
            status_title = NSLocalizedString(@"txt_following", nil);
        }
        [btnFollow setTitle:status_title forState:UIControlStateNormal];
        */
        [self updateFollowButtonTitle:userModel];

        [Utils HideProgress];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        
        
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
    
}

-(void)updateFollowButtonTitle:(UserModel*)model
{
    NSString *status_title = NSLocalizedString(@"btn_follow", nil);
    
    //default color
    btnFollow.backgroundColor = [UIColor clearColor];
    [btnFollow setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    if(model.isFollowing)
    {
        status_title = NSLocalizedString(@"txt_following", nil);
        btnFollow.backgroundColor = header_color;
        [btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    [btnFollow setTitle:status_title forState:UIControlStateNormal];
}

-(void)segmentedControlChangedValue:(HMSegmentedControl *)segmentControl
{
  //  [_collectionView setContentOffset: CGPointMake(segmentControl.selectedSegmentIndex * Screen_Width, 0) animated:YES];
    
    ProfileTableCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell setCollectionContentOffSet:segmentControl.selectedSegmentIndex];
    selSegIndex = segmentControl.selectedSegmentIndex;

}


- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCreatePostClicked:(id)sender {
    CreatePostController *createPostVC = [(AppObj).pawan_storyboard  instantiateViewControllerWithIdentifier:@"CreatePostController"];
    [self.navigationController pushViewController:createPostVC animated:YES];

    [createPostVC onPostSuccess:^(PostModel *model) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UPLOAD_SUCCESS_NOTIFICATION object:model];
    }];

}


- (void)Onclick_following:(id)sender {
    self.followingVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"MoreFollowListController"];
    self.followingVC.screen_identifier = followingscreen;
    self.followingVC.header_str = NSLocalizedString(@"txt_following", nil);
    self.followingVC.get_user_id= userModel.userId;

    [self.navigationController pushViewController:self.followingVC animated:YES];

}

- (void)onclick_followers:(id)sender {
    self.followingVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"MoreFollowListController"];
    self.followingVC.screen_identifier = followersscreen;
    self.followingVC.header_str = NSLocalizedString(@"txt_followers", nil);
    self.followingVC.get_user_id= userModel.userId;

    [self.navigationController pushViewController:self.followingVC animated:YES];
}
-(void)onclick_Follow:(id)sender
{
    if(userModel.isFollowing)
    {
        [self follow_unfollow_API:API_UNFOLLOW];
    }
    else
    {
        [self follow_unfollow_API:API_FOLLOW];
        
    }
}

-(void)settingClicked:(id)sender
{
    AccountSettingViewController *account = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"AccountSettingViewController"];
    [[UTILS topMostController].navigationController pushViewController:account animated:YES];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    CGRect frame = imgViewCoverPic.frame;

//    CGFloat y = 150 - (scrollView.contentOffset.y+20);
//    CGFloat height = MIN(MAX(y, 150), 400);
//    coverHtConstr.constant = height;

    ProfileTableCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (scrollView.contentOffset.y > 0) {
        cell.userInteractionEnabled = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"enable_child_scroll" object:nil];
        [UIView animateWithDuration:0.2 animations:^{
            headerView.layer.backgroundColor = header_color.CGColor;
            imgHeaderDp.alpha = 1;
            imgHeaderDp.image = imgViewDp.image;
            lblHeaderTitle.alpha = 1;
        }];
    }
    else{
//        cell.userInteractionEnabled = NO;
        cell.userInteractionEnabled = YES;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"disable_child_scroll" object:nil];


        [UIView animateWithDuration:0.2 animations:^{
            headerView.layer.backgroundColor = [UIColor clearColor].CGColor;
            imgHeaderDp.alpha = 0;
            lblHeaderTitle.alpha = 0;
        }];
    }
    
    CGFloat inset = 60;

    if (scrollView.contentOffset.y < inset && scrollView.contentOffset.y > 0) {
    }
    else{
        if (scrollView.contentOffset.y > 0) {
            scrollView.contentInset = UIEdgeInsetsMake(Screen_Height <= 568 ? 60 : 40,0,0,0);
            headerView.layer.zPosition = 0;
            UIView *header = _tableView.tableHeaderView;

            NSLog(@"Header Height = %f",header.frame.size.height + 50);
            NSLog(@"Content offset = %f",scrollView.contentOffset.y);

//            if (scrollView.contentOffset.y  >= header.frame.size.height + 50 - 10) {
//                //enable inner table scrolling
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"enable_child_scroll" object:nil];
//
//
//
//            }
//            else
//            {
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"disable_child_scroll" object:nil];
//
//
//            }
        }
        else{
            scrollView.contentInset = UIEdgeInsetsMake(0,0,0,0);
            headerView.layer.zPosition = 0;
        }
        
       //scrollView.contentSize = CGSizeZero;
    }
 
}

-(NSString *)getFormattedJoiningDate:(NSString *)dateStr
{
    NSDateFormatter *dateformater=[[NSDateFormatter alloc]init];
    dateformater.dateFormat=@"yyyy-MM-dd HH:mm:ss";
    
    NSDate *date = [dateformater dateFromString:dateStr];
//    dateformater.dateFormat=@"MMMM yyyy";
    dateformater.dateFormat=@"MM/dd/yyyy";

    if([dateformater stringFromDate:date]!=nil)
    {
        return [dateformater stringFromDate:date];
    }
    else
    {
        return @"";
    }
}
@end
