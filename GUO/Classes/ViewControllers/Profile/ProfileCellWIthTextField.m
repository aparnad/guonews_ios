//
//  ProfileCellWIthTextField.m
//  GUO
//
//  Created by Parag on 6/8/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ProfileCellWIthTextField.h"
@interface ProfileCellWIthTextField() <UITextFieldDelegate>
@end

@implementation ProfileCellWIthTextField

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
  self.textFieldProfile.floatingLabelFont = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
  self.textFieldProfile.floatingLabelTextColor = [UIColor lightGrayColor];
  self.textFieldProfile.floatingLabelActiveTextColor = [UIColor lightGrayColor];
  self.textFieldProfile.keepBaseline = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
  [self.delegate textFieldBeginEditing];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
  [self.delegate textFieldEndEditing];
  
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
  [self.textFieldProfile endEditing:YES];
  return YES;
}

@end
