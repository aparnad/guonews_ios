//
//  VerificationPopup.m
//  GUO
//
//  Created by Pawan Ramteke on 14/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "VerificationPopup.h"

@interface VerificationPopup ()
{
    
    __weak IBOutlet UIImageView *imgViewTop;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIButton *btnOk;
}
@end

@implementation VerificationPopup

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    btnOk.layer.cornerRadius = 20;
    btnOk.backgroundColor = header_color;
    lblTitle.text = _strDisplayMessage;
}
- (IBAction)btnOkClicked:(id)sender {
    [self.view dismissPresentingPopup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
