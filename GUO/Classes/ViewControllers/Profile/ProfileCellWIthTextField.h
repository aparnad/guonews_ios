//
//  ProfileCellWIthTextField.h
//  GUO
//
//  Created by Parag on 6/8/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"


#define CellIdentifierPRofileWithTF @"CellTFIdentifier"

@protocol ProfileCellWIthTextFieldDelegate
@optional

-(void)textFieldBeginEditing;
-(void)textFieldEndEditing;
@end

@interface ProfileCellWIthTextField : UITableViewCell
  @property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *textFieldProfile;
  @property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property(weak, nonatomic) id <ProfileCellWIthTextFieldDelegate>delegate;
  
@end
