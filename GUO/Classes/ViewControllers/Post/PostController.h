//
//  PostController.h
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostCell.h"
#import "MenuViewController.h"
#import "SharePopup.h"
#import "CommentPopup.h"
#import "CommonDocumentController.h"


@interface PostController : UIViewController
{
    NSMutableDictionary *height_dic;
    MenuViewController *menuVC;
    int side_space;

}

@property(strong,nonatomic) IBOutlet UIView *headerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
    @property(strong,nonatomic) IBOutlet UIButton *btn_header_userimg,*btnMenu,*btnCreate,*btnNews;

@property(strong,nonatomic) SharePopup *share_popup;
@property(strong,nonatomic) CommentPopup *comment_popup;
@property(strong,nonatomic) CommonDocumentController *commonDocNav;
@property BOOL is_came_from_sliding_menu;

@property (nonatomic,strong)NSString *apiName;

-(IBAction)onclick_menu:(id)sender;
-(IBAction)onclick_more:(id)sender;
-(IBAction)onclick_view_profile_GUO:(id)sender;
-(void)showUserProfile:(NSString*)user_id;


@end
