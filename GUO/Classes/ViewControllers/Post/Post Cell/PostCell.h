//
//  PostCell.h
//  GUO
//
//  Created by mac on 07/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UIView *outerContainerView;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIButton *btnMoreOption;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;
@property (strong, nonatomic) IBOutlet UIView *imgContainerView,*bottomContainerView;
@property (strong, nonatomic) IBOutlet UIImageView *img1,*img2,*img3,*imgVIPTag;
@property (strong, nonatomic) IBOutlet UIButton *btnViewMoreImg;

@property (strong, nonatomic) IBOutlet UILabel *lblTotalLikeCount,*lblTotalCommentCount,*lblTotalShareCount;
@property (strong, nonatomic) IBOutlet UIButton *btnLike,*btnComment,*btnShare;

@property CGFloat img_container_height,space_between_images;

@end
