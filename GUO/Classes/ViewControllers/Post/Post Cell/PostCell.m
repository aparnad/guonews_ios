//
//  PostCell.m
//  GUO
//
//  Created by mac on 07/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "PostCell.h"

@implementation PostCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.outerContainerView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = tbl_separator_color;
    [self.btnMoreOption setImage:[Utils setTintColorToUIImage:self.btnMoreOption.imageView.image color:gray_icon_color] forState:UIControlStateNormal];
    
    self.lblTotalLikeCount.textColor= self.lblTotalCommentCount.textColor=self.lblTotalShareCount.textColor = gray_icon_color;
    
    [Utils SetRoundedCorner:self.imgUser];
    self.img_container_height = 200;
    self.space_between_images = 3;


    [self.btnViewMoreImg setTitle:@"More..." forState:UIControlStateNormal];
    self.lblName.font = [UIFont fontWithName:Font_Medium size:userfull_name_font_size];
    self.lblTime.font = [UIFont fontWithName:Font_regular size:date_time_font_size];
    self.lblDesc.font = [UIFont fontWithName:Font_regular size:post_desc];

    self.lblTotalLikeCount.font = [UIFont fontWithName:Font_regular size:username_font_size];

    self.lblTotalShareCount.font = [UIFont fontWithName:Font_regular size:username_font_size];

    self.lblTotalCommentCount.font = [UIFont fontWithName:Font_regular size:username_font_size];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
