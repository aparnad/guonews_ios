//
//  CustomPostTableView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 27/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "CustomPostTableView.h"
#import "PostsTableCell.h"
#import "PostModel.h"
#import "Photos.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "PostDetailsController.h"
#import "ReportUserPopup.h"
#import "DGActivityIndicatorView.h"
#import "ShareView.h"
#import "PostCommentView.h"
#import "TranslatePopUp.h"
#import "ALChatManager.h"
#import "VideoPlayerController.h"
#import "OneImageView.h"
@interface CustomPostTableView()
{
    NSMutableArray *PostArr;
    NSArray *otherUserOptionsArr,*loginUserOptionArr;
    NSString *searchType;
    BOOL fromGlobalSearch;
    DGActivityIndicatorView *activityIndicatorView;
    UILabel *lblMessage;

}

@property (nonatomic,strong)CommentPopup *comment_popup;
@property(strong,nonatomic) CommentPopup *share_popup;
@property(strong,nonatomic) ReportUserPopup *reportuser_popup;



@end

@implementation CustomPostTableView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      
        [self setup];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame fromProfile:(BOOL)fromProfile
{
    self = [super initWithFrame:frame];
    if (self) {
        self.fromProfile = fromProfile;
        [self setup];
    }
    return self;
}
-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

-(void)setup
{
    
    self.backgroundColor = [UIColor controllerBGColor];
    searchType = UTILS.searchType;

    [Utils RemoveTableViewHeaderFooterSpace:self];
  //  PostArr = [[NSMutableArray alloc]init];
    
    self.delegate = self;
    self.dataSource = self;
    
    self.estimatedRowHeight = 400;
    self.rowHeight = UITableViewAutomaticDimension;

    UINib *nib = [UINib nibWithNibName:@"PostsTableCell" bundle:nil];
    [self registerNib:nib forCellReuseIdentifier:@"PostCellId"];
    
    self.refreshControl=[[UIRefreshControl alloc]init];
    [self.refreshControl addTarget:self action:@selector(refreshList:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.refreshControl];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;

 
    self.backgroundView = [self tableBGView];
    
//    if (UTILS.isFromGlobalSearch) {
//        [activityIndicatorView stopAnimating];
//    }

    
    (AppObj).is_first_time_add_observer = true;
//    if (UTILS.isFromGlobalSearch) {
//
//        self.apiName = API_SEARCH;
//        current_page = 1;
//        [self callGlobalSearchAPI:UTILS.searchText];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(globalSearchTextChanged:) name:NOTIFICATION_GLOBAL_SEARCH object:nil];
//    }
//
}

-(void)refreshList:(UIRefreshControl *)refreshControl
{
//    if (fromGlobalSearch) {
        if (self.block) {
            self.block();
        }
        [refreshControl endRefreshing];
//    }
//    else{
  //      current_page = 1;
    //    //    [Utils ShowProgremss];
        
  //      [self userPostListAPI];
  //  }
    
}

-(void)likePostAPI:(PostModel*)model API_NAME:(NSString*)API_NAME idxpath:(NSIndexPath*)idxpath
{
  //  [UTILS ShowProgress];
    
    NSDictionary *params = @{
                             @"get":API_NAME,
                             @"user_id":UTILS.currentUser.userId,
                             @"post_id":model.postId
                             };
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_NAME params:params sBlock:^(id responseObject) {
        
        [Utils HideProgress];
        
        CountModel *contM = (AppObj).app_count_model;
        contM.likes = model.iLike ? contM.likes + 1 : contM.likes - 1;
        (AppObj).app_count_model = contM;
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
        [Utils HideProgress];
    }];
    
    [self reloadData];
}

-(void)deletePostAPI:(PostModel*)post_model
{
    [UTILS ShowProgress];
    
    NSDictionary *params = @{
                             @"get":API_DELETE_POST,
                             @"post_id":post_model.postId
                             };
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_DELETE_POST params:params sBlock:^(id responseObject) {
        
        [[Toast sharedInstance]makeToast:responseObject position:TOAST_POSITION_BOTTOM];
//        [CustomAlertView showAlert:@"" withMessage:responseObject];
        [PostArr removeObject:post_model];
        [Utils HideProgress];
        [self reloadData];
        
        CountModel *contM = (AppObj).app_count_model;
        contM.posts = contM.posts - 1;
        (AppObj).app_count_model = contM;
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if (PostArr == nil) {
//        [activityIndicatorView startAnimating];
//    }
//    else{
//        [activityIndicatorView stopAnimating];
//    }
    return PostArr.count;
}




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostsTableCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PostCellId"];
    cell.indexPath = indexPath;
    [self configureCell:cell indexPath:indexPath];
    return cell;
}

-(void)configureCell:(PostsTableCell *)cell indexPath:(NSIndexPath *)indexPath
{
    PostModel *model = PostArr[indexPath.row];
    
    [cell.btnTranslate setTitle:NSLocalizedString(@"Translate", nil) forState:UIControlStateNormal];

    if (model.origin == nil) {
        cell.shareViewHtConstr.constant = 0;
        [self configureDataUsingModel:model cell:cell indexPath:indexPath];
        cell.btnMore.hidden = NO;
        cell.baseViewLeadingConstr.constant = 5;
        cell.baseView.layer.borderWidth = 0;
       // cell.btnTranslate.hidden = NO;
        
        cell.lblTranslate.text = model.translatedString ? model.translatedString : @"";
        cell.viewTranslatedByHtConstr.constant = model.translatedString ? 40 : 0;
        if (model.textPlain.length > 0) {
            cell.btnTranslate.hidden = cell.lblTranslate.text.length > 0 ? YES : NO;
        }
        else{
            cell.btnTranslate.hidden = YES;
        }
        cell.lblShareTranslate.text = @"";
        cell.viewShareTranslatedByHtConstr.constant = 0;
    }
    else {
        cell.lblShareDescTopConstr.constant = 10;
        cell.btnTranslate.hidden = YES;
        cell.baseView.layer.borderWidth = 1;
        cell.baseView.layer.borderColor = RGB(240, 240, 240).CGColor;
        cell.btnMore.hidden = YES;
        [cell.imgViewShareDp sd_setImageWithURL:[Utils getProperContentUrl:model.userPicture] placeholderImage:defaultUserImg];
        cell.lblShareName.text = [Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName];
        cell.lblShareTIme.text = model.formattedTime;
        cell.lblShareDescription.text = model.textPlain;
        cell.btnShareTranslate.hidden = cell.lblShareDescription.text.length > 0 ? NO : YES;
        cell.btnShareTranslateHtConst.constant = cell.lblShareDescription.text.length > 0 ? 20 : 0;
        
        [self configureDataUsingModel:model.origin cell:cell indexPath:indexPath];
        cell.baseViewLeadingConstr.constant = 10;
        
        cell.lblShareTranslate.text = model.translatedString ? model.translatedString : @"";
        cell.viewShareTranslatedByHtConstr.constant = model.translatedString ? 20 : 0;
        cell.lblTranslate.text = @"";
        cell.viewTranslatedByHtConstr.constant = 0;
        cell.btnShareTranslate.hidden = cell.lblShareTranslate.text.length >0 ? YES : NO;

        cell.shareViewHtConstr.constant = cell.btnShareTranslate.hidden ? 140 : 100;
        
    }
    
    [cell.btnLike setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:model.likes]] forState:UIControlStateNormal];
    [cell.btnComment setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:model.comments]] forState:UIControlStateNormal];
    [cell.btnShare setTitle:[NSString stringWithFormat:@"(%@)",[Utils suffixNumber:model.shares]] forState:UIControlStateNormal];
    
    [cell.btnLike setImage:[[UIImage imageNamed:@"like_normal"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
    cell.btnLike.tintColor = model.iLike ? header_color : [UIColor darkTextColor];
    [cell onMoreButtonClicked:^(NSIndexPath *indexPath) {
       // [[Utils getSharedInstance]showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:otherUserOptionsArr];
        
        if(model.userId == UTILS.currentUser.userId)
        {
            [self showLoginUserOption:model];
        }
        else
        {
            [self showOtherUserOption:model];
        }
    }];
    [cell viewProfile:^(NSIndexPath *indexPath) {
        // [[Utils getSharedInstance]showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:otherUserOptionsArr];
        
        if (self.showUserProfileGlobalSearchBlock) {
            
            self.showUserProfileGlobalSearchBlock(model.userId);
        }
        else{
            [(AppObj).postView showUserProfile:model.userId];
        }
    }];
    
    [cell onLikeButtonClicked:^(NSIndexPath *indexPath) {
        
        PostModel *model = PostArr[indexPath.row];
        model.iLike = !model.iLike;
        int likeCount = [model.likes intValue];
        likeCount = model.iLike ? likeCount + 1 : likeCount - 1;
        model.likes = @(likeCount).stringValue;
        [PostArr replaceObjectAtIndex:indexPath.row withObject:model];
        [self reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

        NSString *API_NAME;
        
        API_NAME = model.iLike ? API_LIKE_POST : API_UNLIKE_POST;
      
        [self likePostAPI:model API_NAME:API_NAME idxpath:indexPath];
        
        }];
    
    [cell onCommenButtonClicked:^(NSIndexPath *indexPath) {
        [self showCommentPopup:model indxpath:indexPath];
    }];
    
    [cell onShareButtonClicked:^(NSIndexPath *indexPath) {
        PostModel *model = PostArr[indexPath.row];
        [self showSharePopup:model indxpath:indexPath];
    }];
    
    [cell onSocialShareButtonClicked:^(NSIndexPath *indexPath) {
        PostModel *model = PostArr[indexPath.row];
       // https://stage.guo.media/posts/101292
     //   NSURL *shareURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@posts/%@",[BASE_URL stringByReplacingOccurrencesOfString:@"api.php" withString:@""],model.postId]];
        
        NSURL *shareURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@posts/%@",WEB_DOMAIN,model.postId]];

        [UTILS sharePostToSocial:shareURL];
    }];
    
    
    if ([model.userId isEqualToString:UTILS.currentUser.userId]) {
        cell.btnChat.hidden = YES;
    }
    else{
        cell.btnChat.hidden = NO;
    }
    
    [cell onChatButtonClicked:^(NSIndexPath *indexPath) {
        PostModel *model = PostArr[indexPath.row];

        ALChatManager* chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLICATION_ID];
        
        [chatManager launchChatForUserWithDefaultText:model.userId andFromViewController:[UTILS topMostControllerNormal]];
    }];
    
}

-(NSString *)abbreviateNumber:(int)num withDecimal:(int)dec {
    
    NSString *abbrevNum;
    float number = (float)num;
    
    NSArray *abbrev = @[@"K", @"M", @"B"];
    
    for (int i = abbrev.count - 1; i >= 0; i--) {
        
        // Convert array index to "1000", "1000000", etc
        int size = pow(10,(i+1)*3);
        
        if(size <= number) {
            // Here, we multiply by decPlaces, round, and then divide by decPlaces.
            // This gives us nice rounding to a particular decimal place.
            number = round(number*dec/size)/dec;
            
            NSString *numberString = [self floatToString:number];
            
            // Add the letter for the abbreviation
            abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
            
            NSLog(@"%@", abbrevNum);
            
        }
        else
        {
            abbrevNum = [NSString stringWithFormat:@"%f",number];
        }
        
    }
    return abbrevNum;
}
- (NSString *) floatToString:(float) val {
    
    NSString *ret = [NSString stringWithFormat:@"%.2f", val];
    unichar c = [ret characterAtIndex:[ret length] - 1];
    
    while (c == 48) { // 0
        ret = [ret substringToIndex:[ret length] - 1];
        c = [ret characterAtIndex:[ret length] - 1];
        
        //After finding the "." we know that everything left is the decimal number, so get a substring excluding the "."
        if(c == 46) { // .
            ret = [ret substringToIndex:[ret length] - 1];
        }
    }
    
    return ret;
}
-(void)configureDataUsingModel:(id)mod cell:(PostsTableCell *)cell indexPath:(NSIndexPath *)indexPath
{
    cell.imgViewPoster2.hidden = YES;
    cell.imgViewPoster3.hidden = YES;
    cell.viewSingleImagePost.btnPlayVideo.hidden = YES;
    cell.videoView.hidden = YES;
    cell.btnMorePhotos.hidden = YES;
    
    cell.viewSingleImagePost.hidden = YES;
    cell.viewTwoImagePost.hidden = YES;
    cell.viewThreeImagePost.hidden = YES;
    
    cell.viewPostContainerHtConstr.constant = 200;
    PostModel *model;
    if ([mod isKindOfClass:[PostModel class]]) {
        model = mod;
    }
    else{
        model = (SharePostModel *)mod;
    }
    
    [cell.imgViewDp sd_setImageWithURL:[Utils getThumbUrl:model.userPicture] placeholderImage:defaultUserImg];
    cell.lblName.text = [Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName];
    cell.lblTime.text = model.formattedTime;//[Utils getFormattedDate:model.time];
    
    cell.txtViewDescription.text = model.textPlain;
    cell.txtViewDescription.textContainerInset = UIEdgeInsetsMake(model.textPlain.length > 0 ? 0 : -30, 0, 0, 0 );

    //cell.lblDescription.text = model.textPlain;
    cell.lblDescription.lineBreakMode = NSLineBreakByTruncatingTail;
    cell.videoLayer.hidden = YES;
    
    [cell configureCell:nil videoURL:nil];
    if (model.photos.count >= 3) {
         if ([model isKindOfClass:[SharePostModel class]]) {
             cell.imgPostWdthConstr.constant = (Screen_Width - 31)* 0.6;
             cell.imgPostHtConstr.constant = (Screen_Width - 31)* 0.6;

             cell.imgPoster2WdthConstr.constant = (Screen_Width - 31)* 0.4;
             cell.imgPoster2HtConstr.constant = ((Screen_Width - 31)* 0.6)/2 - 1;

         }
         else{
             cell.imgPostWdthConstr.constant = (Screen_Width - 27)* 0.6;
             cell.imgPostHtConstr.constant = (Screen_Width - 27)* 0.6;

             cell.imgPoster2WdthConstr.constant = (Screen_Width - 27)* 0.4;
             cell.imgPoster2HtConstr.constant = ((Screen_Width - 27)* 0.6)/2 - 1;

         }

        cell.viewThreeImagePost.hidden = NO;
        cell.imgViewPoster2.hidden = NO;
        cell.imgViewPoster3.hidden = NO;
    
        
        Photos *photoModel1 = model.photos[0];
        Photos *photoModel2 = model.photos[1];
        Photos *photoModel3 = model.photos[2];
        
        [cell.viewThreeImagePost.imgThreePost1 sd_setImageWithURL:[Utils getThumbUrl:photoModel1.source] placeholderImage:defaultPostImg];
        [cell.viewThreeImagePost.imgThreePost2 sd_setImageWithURL:[Utils getThumbUrl:photoModel2.source] placeholderImage:defaultPostImg];
        [cell.viewThreeImagePost.imgThreePost3 sd_setImageWithURL:[Utils getThumbUrl:photoModel3.source] placeholderImage:defaultPostImg];
        
//        [cell.imgViewPoster sd_setImageWithURL:[Utils getThumbUrl:photoModel1.source] placeholderImage:defaultPostImg];
//        [cell.imgViewPoster2 sd_setImageWithURL:[Utils getThumbUrl:photoModel2.source] placeholderImage:defaultPostImg];
//        [cell.imgViewPoster3 sd_setImageWithURL:[Utils getThumbUrl:photoModel3.source] placeholderImage:defaultPostImg];
        
        if (model.photos.count == 3) {
            cell.viewThreeImagePost.btnMorePhotos.hidden = YES;
            [cell.viewThreeImagePost.btnMorePhotos setTitle:@"" forState:UIControlStateNormal];
        }
        else{
            cell.viewThreeImagePost.btnMorePhotos.hidden = NO;
            [cell.viewThreeImagePost.btnMorePhotos setTitle:[NSString stringWithFormat:@"+%lu",model.photos.count - 3] forState:UIControlStateNormal];
        }
    }
    else if (model.photos.count == 2){
        
        if ([model isKindOfClass:[SharePostModel class]]) {
            cell.imgPostWdthConstr.constant = (Screen_Width - 30)* 0.5;
            cell.imgPostHtConstr.constant = (Screen_Width - 30)* 0.5;
            cell.imgPoster2HtConstr.constant = (Screen_Width - 30)* 0.5;
            cell.imgPoster2WdthConstr.constant = (Screen_Width - 30)* 0.5;
        }
        else{
            cell.imgPostWdthConstr.constant = (Screen_Width - 28)* 0.5;
            cell.imgPostHtConstr.constant = (Screen_Width - 28)* 0.5;
            cell.imgPoster2HtConstr.constant = (Screen_Width - 28)* 0.5;
            cell.imgPoster2WdthConstr.constant = (Screen_Width - 28)* 0.5;

        }
        
        cell.viewTwoImagePost.hidden = NO;
        
        cell.imgViewPoster2.hidden = NO;

        Photos *photoModel1 = model.photos[0];
        Photos *photoModel2 = model.photos[1];
       
        [cell.viewTwoImagePost.imgTwoPost1 sd_setImageWithURL:[Utils getThumbUrl:photoModel1.source] placeholderImage:defaultPostImg];
        [cell.viewTwoImagePost.imgTwoPost2 sd_setImageWithURL:[Utils getThumbUrl:photoModel2.source] placeholderImage:defaultPostImg];
        
//        [cell.imgViewPoster sd_setImageWithURL:[Utils getThumbUrl:photoModel1.source] placeholderImage:defaultPostImg];
//        [cell.imgViewPoster2 sd_setImageWithURL:[Utils getThumbUrl:photoModel2.source] placeholderImage:defaultPostImg];
    }
    else if (model.photos.count == 0){
        if (model.videos.count) {
            
            cell.viewSingleImagePost.hidden = NO;
            cell.imgPostHtConstr.constant = 200;
            
            if ([model isKindOfClass:[SharePostModel class]]) {
                cell.imgPostWdthConstr.constant = (Screen_Width - 30);
            }
            else{
                cell.imgPostWdthConstr.constant = (Screen_Width - 26);
            }
            
            cell.viewSingleImagePost.btnPlayVideo.hidden = NO;
            
      //      [self setCornerForImage:cell.imgViewPoster corner:(UIRectCornerTopLeft | UIRectCornerBottomLeft | UIRectCornerTopRight | UIRectCornerBottomRight)];

            Photos *videoModel = model.videos[0];
            
            if (model.videoThumbnail) {
                cell.imgViewPoster.image = model.videoThumbnail;
                cell.viewSingleImagePost.btnPlayVideo.backgroundColor = [UIColor clearColor];
            }
            else{
              //  cell.imgViewPoster.image = defaultPostImg;
                cell.viewSingleImagePost.btnPlayVideo.backgroundColor = [UIColor blackColor];
                cell.videoView.hidden = NO;
                
                NSString *thumbURL = videoModel.source;
//
                thumbURL = [thumbURL stringByReplacingOccurrencesOfString:@"videos" withString:@"photos"];
                thumbURL = [[thumbURL stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
//                [cell.imgViewPoster sd_setImageWithURL:[Utils getThumbUrl:thumbURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//                    if (image !=nil) {
//                        cell.viewSingleImagePost.btnPlayVideo.backgroundColor = [UIColor clearColor];
//                    }
//                }];
              
                [cell.viewSingleImagePost.imgOnePost1 sd_setImageWithURL:[Utils getThumbUrl:thumbURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if (image !=nil) {
                        cell.viewSingleImagePost.btnPlayVideo.backgroundColor = [UIColor clearColor];
                    }
                }];
                
                Photos *videoModel = model.videos[0];
                [cell configureCell:thumbURL videoURL:[[Utils getProperContentUrl:videoModel.source] absoluteString]];
            }
            
        }
        else{
            cell.viewSingleImagePost.hidden = YES;
            cell.imgPostHtConstr.constant = 0;
             cell.viewPostContainerHtConstr.constant = 0;
        }
    }
    else{
        if ([model isKindOfClass:[SharePostModel class]]) {
            cell.imgPostWdthConstr.constant = (Screen_Width - 30);
        }
        else{
            cell.imgPostWdthConstr.constant = (Screen_Width - 26);
        }
        
        cell.viewSingleImagePost.hidden = NO;
        
        cell.imgPostHtConstr.constant = 200;
       
        
        Photos *photoModel1 = model.photos[0];
        
        NSURL *url = [Utils getThumbUrl:photoModel1.source];
        
         [cell.viewSingleImagePost.imgOnePost1 sd_setImageWithURL:url placeholderImage:defaultPostImg];
//        [cell.imgViewPoster sd_setImageWithURL:url placeholderImage:defaultPostImg];
    }
    
    [cell onPlayButtonClicked:^(NSIndexPath *indexPath) {
        
        [cell pauseVideo];
        PostModel *mod = PostArr[indexPath.row];
        Photos *videoModel;
        if (mod.origin != nil) {
            SharePostModel *model = mod.origin;
            videoModel = model.videos[0];
        }
        else{
            videoModel = mod.videos[0];
        }
        [self playVideoWithURL:[Utils getProperContentUrl:videoModel.source] time:[cell getPlayerCurrentTime]];
        
    }];
    
    [cell onMorePhotosButtonClicked:^(NSIndexPath *indexPath) {
        PostModel *mod = PostArr[indexPath.row];
        
        if (mod.origin != nil) {
            SharePostModel *model = mod.origin;
            [UTILS showMultiImageViewer:model.photos currentPageIndex:2];
        }
        else{
            [UTILS showMultiImageViewer:model.photos currentPageIndex:2];
        }
    }];
    
    [cell onSinglePosterClick:^(NSIndexPath *indexPath) {
        PostModel *mod = PostArr[indexPath.row];
        
        if (mod.origin != nil) {
            SharePostModel *model = mod.origin;
            [UTILS showMultiImageViewer:model.photos currentPageIndex:0];
        }
        else{
            [UTILS showMultiImageViewer:model.photos currentPageIndex:0];
        }
        
    }];
    
    [cell onMultiPosterClick:^(NSIndexPath *indexPath) {
        PostModel *mod = PostArr[indexPath.row];
        if (mod.origin != nil) {
            SharePostModel *model = mod.origin;
            [UTILS showMultiImageViewer:model.photos currentPageIndex:1];
        }
        else{
            [UTILS showMultiImageViewer:model.photos currentPageIndex:1];
        }
        
    }];
    
    
    [cell onTranslateButtonClicked:^(NSIndexPath *indexPath) {
        
        [self translateAPI:indexPath];
    }];
    
    [cell onTextViewTap:^(NSIndexPath *indexPath) {
        [self tableView:self didSelectRowAtIndexPath:indexPath];
    }];

}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cel forRowAtIndexPath:(NSIndexPath *)indexPath
{
    return;
    PostsTableCell *cell= (PostsTableCell *)cel;
    
    PostModel *model = PostArr[indexPath.row];
    if (model.origin != nil) {
        model = model.origin;
    }
    if (model.photos.count >= 3) {
        [self setCornerForImage:cell.imgViewPoster corner:(UIRectCornerTopLeft | UIRectCornerBottomLeft)];
        [self setCornerForImage:cell.imgViewPoster2 corner:UIRectCornerTopRight];
        [self setCornerForImage:cell.imgViewPoster3 corner:UIRectCornerBottomRight];
        [self setCornerForImage:cell.btnMore corner:UIRectCornerBottomRight];
    }
     else if (model.photos.count == 2){
         [self setCornerForImage:cell.imgViewPoster corner:(UIRectCornerTopLeft | UIRectCornerBottomLeft)];
         [self setCornerForImage:cell.imgViewPoster2 corner:(UIRectCornerTopRight | UIRectCornerBottomRight)];
     }
     else{
         [self setCornerForImage:cell.imgViewPoster corner:(UIRectCornerTopLeft | UIRectCornerBottomLeft | UIRectCornerTopRight | UIRectCornerBottomRight)];
         [self setCornerForImage:cell.btnPlayVideo corner:(UIRectCornerTopLeft | UIRectCornerBottomLeft | UIRectCornerTopRight | UIRectCornerBottomRight)];

     }
}

-(void)setCornerForImage:(UIView *)imgView corner:(UIRectCorner)corner
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [imgView setNeedsLayout];
        [imgView layoutIfNeeded];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:imgView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(10.0, 10.0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = imgView.bounds;
        maskLayer.path  = maskPath.CGPath;
        imgView.layer.mask = maskLayer;
    });
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostsTableCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell pauseVideo];
    PostModel *model = PostArr[indexPath.row];
    if (self.rowBlock) {
        self.rowBlock(model,indexPath);
        return;
    }
    
    PostDetailsController *postDetailsVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"PostDetailsController"];
    postDetailsVC.selPostModel = model;
    [UTILS topMostController].hidesBottomBarWhenPushed = NO;
    [[UTILS topMostController].navigationController pushViewController:postDetailsVC animated:YES];
    
    [postDetailsVC onLikeCommentSuccess:^(PostModel *model, NSIndexPath *indexPath) {
        [PostArr replaceObjectAtIndex:indexPath.row withObject:model];
        [tableView reloadData];
    }];
    
    [postDetailsVC onShareSuccess:^(PostModel *model, NSIndexPath *indexPath) {
        [PostArr insertObject:model atIndex:0];
        [tableView reloadData];
    }];
//    [postDetailsVC onPostDelete:^(PostModel *model, NSIndexPath *indexPath) {
//        [PostArr removeObjectAtIndex:indexPath.row];
//        [tableView reloadData];
//    }];
    
    [postDetailsVC onPostDelete:^{
        [self deletePost:indexPath];
    }];
    
    
    
}

-(void)like_comments_onDetails:(PostModel *)model indexPath:(NSIndexPath *)indexPath
{
    [PostArr replaceObjectAtIndex:indexPath.row withObject:model];
    [self reloadData];

}

-(void)addSharedPost:(PostModel *)model
{
    [PostArr insertObject:model atIndex:0];
    [self reloadData];
}

-(void)deletePost:(NSIndexPath *)indexPath
{
    [PostArr removeObjectAtIndex:indexPath.row];
    [self reloadData];
}

-(void)playVideoWithURL:(NSURL *)videoUrl time:(CMTime)currentTime
{
    AVPlayer *player = [AVPlayer playerWithURL:videoUrl];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [playerViewController.player.currentItem seekToTime:currentTime.value > 0 ? currentTime : kCMTimeZero];
    [player play];
    [[UTILS topMostController] presentViewController:playerViewController animated:YES completion:nil];
}

-(void)getVideoThumbnailFromURL:(NSURL *)videoURL indexPath:(NSIndexPath *)idxPath
{
    UIApplication *application = [UIApplication sharedApplication];
    
    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithName:@"MyTask" expirationHandler:^
                                                 {
                                                     [application endBackgroundTask:bgTask];
                                                     bgTask = UIBackgroundTaskInvalid;
                                                 }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                   {
                       AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
                       
                       AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                       
                       generator.appliesPreferredTrackTransform = YES;
                       
                       CMTime time = CMTimeMakeWithSeconds(0,15);
                       
                       AVAssetImageGeneratorCompletionHandler handler = ^(CMTime timeRequested, CGImageRef image, CMTime timeActual, AVAssetImageGeneratorResult result, NSError *error)
                       {
                           if (result == AVAssetImageGeneratorSucceeded)
                           {
                               UIImage  *thumbnail = [UIImage imageWithCGImage: image];
//                               imgView.image = thumbnail;
                               PostModel *model = PostArr[idxPath.row];
                               model.videoThumbnail = thumbnail;
                               //[self reloadData];
                           }
                           else
                           {
                              
                           }
                       };
                       generator.maximumSize = CGSizeMake(Screen_Width - 20, 200);
                       
                       [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:time]] completionHandler:handler];
                       
                       [application endBackgroundTask:bgTask];
                       bgTask = UIBackgroundTaskInvalid;
                   });
    
}

-(void)showOtherUserOption:(PostModel*)model
{
    NSArray *arr = @[
                     NSLocalizedString(@"Copy Link to Post", nil),
                     NSLocalizedString(@"Mute Post", nil),
                     [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"block_connection", nil),[Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName]],
                     NSLocalizedString(@"Report Post", nil)
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        
        if(index == 0)//copy link
        {
            [UTILS copyLink:model.postId];
            
        }
        else if (index == 1) {//mute post
            [self mutePost:model];

        }
        else if(index == 2)// block user
        {
            [self blockUser:model];
            
        }
        else if(index == 3)// report post
        {
            [self showReportUserPopup:model];
            
        }
        
        
    }];
    
    
}

-(void)showLoginUserOption:(PostModel*)model
{
    
    NSArray *arr = @[
                     //                     NSLocalizedString(@"Share via Direct Message", nil),
                     NSLocalizedString(@"Copy Link to Post", nil)/*,
                                                                  NSLocalizedString(@"Pin Post", nil)*/,
                     NSLocalizedString(@"Delete Post", nil)
                     ];
    
    [UTILS showActionSheet:NSLocalizedString(@"action_sheet_select_one", nil) optionsArray:arr completion:^(NSInteger index) {
        if(index == 0)//copylink
        {
            [UTILS copyLink:model.postId];
            
            
        }
        else if (index == 1) {//delete post
            [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"Are you sure, you want to delete post?", nil) okHandler:^{
                [self deletePostAPI:model];
            }];
        }
       
    }];
    
    
    
}

#pragma mark - MutePost
-(void)mutePost:(PostModel*)model
{
//    [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"block_user_alert_msg", nil)  okHandler:^{

        
        NSDictionary *params = @{
                                 @"get":API_MUTE_POST,
                                 @"user_id":UTILS.currentUser.userId,
                                 @"post_id":model.postId
                                 };
        
        [UTILS ShowProgress];
        [REMOTE_API CallPOSTWebServiceWithParam:API_MUTE_POST params:params sBlock:^(id responseObject) {
            
            
            [Utils HideProgress];
            [PostArr removeObject:model];
            [self reloadData];
            //  current_page = 1;
            //  [self userPostListAPI];
            
            [CustomAlertView showAlert:@"" withMessage:responseObject];
            
            
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
            [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        }];
        
        
//    }];
    
}

-(void)translateAPI:(NSIndexPath *)indexPath
{
    PostModel *model = PostArr[indexPath.row];

    NSDictionary *param = @{
                           @"get":API_TRANSLATE_TEXT,
                           @"translation_text":model.textPlain
                           };
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_TRANSLATE_TEXT params:param sBlock:^(id responseObject) {
        [Utils HideProgress];
        model.translatedString = responseObject;
        [PostArr replaceObjectAtIndex:indexPath.row withObject:model];
        [self reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
       // [self reloadData];
       // [self showTranslatePopup:model text:responseObject];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
       [Utils HideProgress];
    }];
}

-(void)showTranslatePopup:(PostModel *)model text:(NSString *)text
{
    TranslatePopUp *popup = [[TranslatePopUp alloc]initWithFrame:[UIScreen mainScreen].bounds model:model translatedText:text];
    [[UTILS topMostControllerNormal].navigationController.view addSubview:popup];
    
    [popup enableAutolayout];
    [popup leadingMargin:0];
    [popup trailingMargin:0];
    [popup topMargin:0];
    [popup bottomMargin:0];
}
#pragma mark- Block User
-(void)blockUser:(PostModel*)model
{
    [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"block_user_alert_msg", nil)  okHandler:^{
        
        NSDictionary *params = @{
                                 @"get":API_BLOCK_USER,
                                 @"user_id":UTILS.currentUser.userId,
                                 @"block_id":model.userId
                                 };
        
        [UTILS ShowProgress];
        [REMOTE_API CallPOSTWebServiceWithParam:API_UNBLOCK_USER params:params sBlock:^(id responseObject) {
            
            
            [Utils HideProgress];
            [PostArr removeObject:model];
            [self reloadData];
          //  current_page = 1;
          //  [self userPostListAPI];

            [CustomAlertView showAlert:@"" withMessage:responseObject];
            
            
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
            [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        }];
        
        
    }];
    
}



-(void)showCommentPopup:(PostModel *)model indxpath:(NSIndexPath*)idxpath
{
    PostCommentView *viewShare = [[PostCommentView alloc]initWithFrame:self.bounds model:model];
    [[UTILS topMostControllerNormal].navigationController.view addSubview:viewShare];

    [viewShare enableAutolayout];
    [viewShare leadingMargin:0];
    [viewShare trailingMargin:0];
    [viewShare topMargin:0];
    [viewShare bottomMargin:0];

    [viewShare updatePostModel:^(PostModel *model) {
        [PostArr replaceObjectAtIndex:idxpath.row withObject:model];
        [self reloadData];
    }];

//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateCustomPopup" object:nil];//remove single noti
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCustomPopup:) name:@"updateCustomPopup" object:nil];
//
//
//    _comment_popup = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"CommentPopup"];
//    _comment_popup.model  = model;
//    [UTILS showCustomPopup:_comment_popup.view];
//    [_comment_popup updatePostModel:^(PostModel *model) {
//        NSLog(@"count:%@",model.comments);
//        [PostArr replaceObjectAtIndex:idxpath.row withObject:model];
//        [self reloadRowsAtIndexPaths:@[idxpath] withRowAnimation:UITableViewRowAnimationNone];
//    }];
    
    
}
-(void)updateCustomPopup:(NSNotification*)aNoti
{
    if(aNoti.object!=nil)
    {
        PostModel *model=[aNoti.object objectForKey:@"model"];
        NSInteger idxpath=[PostArr indexOfObject:model];
        
        _comment_popup = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"CommentPopup"];
        _comment_popup.model  = model;
        _comment_popup.dicDetail = aNoti.object;
        [UTILS showCustomPopup:_comment_popup.view];
     
        
        
        [_comment_popup updatePostModel:^(PostModel *model) {
            NSLog(@"count:%@",model.comments);
            [PostArr replaceObjectAtIndex:idxpath withObject:model];
            NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:idxpath inSection:0];
            [self reloadRowsAtIndexPaths:@[tempIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
    }
}

-(void)showSharePopup:(PostModel *)mod indxpath:(NSIndexPath*)idxpath
{
    
    _share_popup = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"SharePopup"];
    _share_popup.model  = mod;
    [UTILS showCustomPopup:_share_popup.view];

    [_share_popup updatePostModel:^(PostModel *model) {
        NSLog(@"count:%@",model.shares);

        [PostArr insertObject:model atIndex:0];

        int shareCount = [mod.shares intValue];
        shareCount = shareCount+1;
        mod.shares = @(shareCount).stringValue;

        [PostArr replaceObjectAtIndex:idxpath.row withObject:mod];
        [self reloadData];

        [[Toast sharedInstance]makeToast:NSLocalizedString(@"Post shared successfully", nil) position:TOAST_POSITION_BOTTOM];
      //  [PostArr replaceObjectAtIndex:idxpath.row withObject:model];
      //  [self reloadRowsAtIndexPaths:@[idxpath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}
-(void)showReportUserPopup:(PostModel *)model
{
    _reportuser_popup = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"ReportUserPopup"];
    _reportuser_popup.model  = model;
    [UTILS showCustomPopup:_reportuser_popup.view];
    
    [_reportuser_popup updatePostModel:^(PostModel *model) {
        NSLog(@"count:%@",model.shares);
        //        [PostArr replaceObjectAtIndex:idxpath.row withObject:model];
        //        [self reloadRowsAtIndexPaths:@[idxpath] withRowAnimation:UITableViewRowAnimationNone];
    }];
    
}

-(void)insertNewPost:(PostModel *)model
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",@"temp_video_thumb"]];
    
    NSData *pngData = [NSData dataWithContentsOfFile:imagePath];
    if(pngData != nil)
    {
         UIImage *image = [UIImage imageWithData:pngData];
        model.videoThumbnail = image;
    }
   
    dispatch_async(dispatch_get_main_queue(), ^{
        [PostArr insertObject:model atIndex:0];
        [self reloadData];
        [[Toast sharedInstance]makeToast:@"Post uploaded successfully" position:TOAST_POSITION_BOTTOM];
    });
}

-(void)dealloc
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    UTILS.apiToCall = nil;
}

-(void)setArrSearchData:(NSArray *)arrSearchData
{
//    fromGlobalSearch = YES;
    PostArr = [NSMutableArray arrayWithArray:arrSearchData];
    
    [self reloadData];
}

-(void)onRefreshOnGlobalSearch:(globalSearchBlock)blk
{
    self.block = blk;
}

-(void)onscrollToTop:(noArgBlock)block
{
    self.blk = block;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.contentOffset.y <= 0) {
        
        if (self.blk) {
            self.blk();
        }
    }
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostsTableCell *videoCell = (PostsTableCell *)cell;
    if (videoCell && videoCell.videoURL != nil) {
        [[VideoPlayerController getSharedInstance] removeFromSuperLayer:videoCell.videoLayer url:videoCell.videoURL];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self playPauseVideos];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        [self playPauseVideos];
    }
}

-(void)appEnteredFromBackground
{
    [[VideoPlayerController getSharedInstance] pausePlayeVideosFor:self appEnteredFromBackground:YES];
}



-(void)playPauseVideos
{
    [[VideoPlayerController getSharedInstance]pausePlayeVideosFor:self appEnteredFromBackground:NO];
}

-(void)didSelectRow:(SelectRowBlock)block
{
    self.rowBlock = block;
}


-(UIView *)tableBGView
{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64)];
    lblMessage = [[UILabel alloc] initWithFrame:bgView.frame];
    lblMessage.textAlignment = NSTextAlignmentCenter;
    lblMessage.backgroundColor = [UIColor clearColor];
    lblMessage.font = NO_DATE_FOUND_FONT;
    lblMessage.lineBreakMode = NSLineBreakByWordWrapping;
    lblMessage.numberOfLines = 0;
    [bgView addSubview:lblMessage];
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
    activityIndicatorView.backgroundColor = [UIColor clearColor];
    activityIndicatorView.frame = CGRectMake(CGRectGetMidX(bgView.frame)-50, CGRectGetMidY(bgView.frame)-25, 100, 50);
    [bgView addSubview:activityIndicatorView];
    
    return bgView;
}
-(void)apiCallInitiated
{
    [activityIndicatorView startAnimating];
    lblMessage.text = @"";
}
-(void)apiCallEndWithResultCount:(NSInteger)count  pageNo:(NSInteger)pageNo
{
    [activityIndicatorView stopAnimating];
    [self.refreshControl endRefreshing];
    
    if (pageNo == 1 && count == 0) {
        lblMessage.text = NSLocalizedString(@"No data found", nil);
    }
    else
    {
        lblMessage.text = @"";
    }
    
   // [UTILS playSoundOnEndRefresh];

    
}
-(void)onUserProfileClicked:(stringBlock)block
{
    self.showUserProfileGlobalSearchBlock = block;
}

-(void)didMoveToWindow
{
    for (PostsTableCell *cell in self.visibleCells) {
        [cell pauseVideo];
    }
}
@end
