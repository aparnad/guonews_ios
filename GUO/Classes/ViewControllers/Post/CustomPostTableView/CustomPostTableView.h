//
//  CustomPostTableView.h
//  GUO Media
//
//  Created by Pawan Ramteke on 27/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentPopup.h"
#import "SharePopup.h"
#import "CallbackTypes.h"
typedef void(^globalSearchBlock)();
typedef void(^SelectRowBlock)(PostModel *selModel,NSIndexPath *indexPath);
@class CommentPopup;
@interface CustomPostTableView : UITableView<UITableViewDataSource,UITableViewDelegate>
{
    int current_page;
    KLCPopup *popup;
    NSString *screen_name;
}
@property(nonatomic,strong) NSString* userIDForPost;
@property(nonatomic,strong) NSString* apiName;
@property(nonatomic,strong) NSDictionary* searchParams;
@property (nonatomic,strong)NSArray *arrSearchData;
@property (nonatomic,assign)BOOL fromProfile;
@property (nonatomic,copy)globalSearchBlock block;
@property (nonatomic,copy)noArgBlock blk;
@property (nonatomic,copy)SelectRowBlock rowBlock;

@property (nonatomic,copy) stringBlock showUserProfileGlobalSearchBlock;


-(instancetype)initWithFrame:(CGRect)frame fromProfile:(BOOL)fromProfile;

-(void)insertNewPost:(PostModel *)model;

-(void)onRefreshOnGlobalSearch:(globalSearchBlock)blk;

-(void)onscrollToTop:(noArgBlock)block;

-(void)didSelectRow:(SelectRowBlock)block;

-(void)onUserProfileClicked:(stringBlock)block;


-(void)like_comments_onDetails:(PostModel *)model indexPath:(NSIndexPath *)indexPath;

-(void)addSharedPost:(PostModel *)model;

-(void)deletePost:(NSIndexPath *)indexPath;

-(void)apiCallInitiated;
-(void)apiCallEndWithResultCount:(NSInteger)count  pageNo:(NSInteger)pageNo;


@end
