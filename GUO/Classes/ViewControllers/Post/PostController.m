//
//  PostController.m
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "PostController.h"
#import "MenuViewController.h"
#import "CreatePostController.h"
#import "CustomPostTableView.h"
#import "YiRefreshFooter.h"
#import "PostDetailsController.h"
#import <Applozic/Applozic.h>
#import "ALChatManager.h"

@interface PostController ()<MenuControllerDelegate>
{
    
    __weak IBOutlet CustomPostTableView *postTblView;
    int currentPage;
    NSMutableArray *arrData;
    YiRefreshFooter *refreshFooter;
    UIButton *newPostButton;
    BOOL isRefreshControlAPICall;
}

@end

@implementation PostController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.view.backgroundColor = [UIColor controllerBGColor];
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];

    if(!self.is_came_from_sliding_menu)
    {
        [self.view addGestureRecognizer:[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureRecognized:)]];
        self.btnNews.hidden = NO;
    }
    else
    {
        [self.btnMenu setImage:[UIImage imageNamed:@"back_Img"] forState:UIControlStateNormal];
        self.btn_header_userimg.hidden = YES;
        self.btnCreate.hidden = YES;
        self.btnNews.hidden = YES;
    }
    self.tabBarController.tabBar.barTintColor =  header_color;
    [Utils SetRoundedCorner:self.btn_header_userimg];
  //  self.btn_header_userimg.layer.borderWidth = header_user_img_border;
  //  self.btn_header_userimg.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Update_post_screen_control" object:nil];//remove single noti
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocalization) name:@"Update_post_screen_control" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ActionSeetForGlobalSearchPeopleTab" object:nil];//remove single noti
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ActionSeetForGlobalSearchPeopleTab:) name:@"ActionSeetForGlobalSearchPeopleTab" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onclick_more:) name:@"onclick_post_options" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadSuccessNotification:) name:UPLOAD_SUCCESS_NOTIFICATION object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNewPushForApp:)
                                                 name:NEW_PUSH_NOTIFICATION
                                               object:nil];
    
    refreshFooter=[[YiRefreshFooter alloc] init];
    
    __weak typeof(self) weakSelf = self;
    refreshFooter.beginRefreshingBlock=^(){
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            sleep(pullDownToRefreshSleepCount);
            [weakSelf callAPI];
        });
    };
    
    
    arrData = [NSMutableArray new];
    self.apiName = self.apiName ? self.apiName : API_GET_POST;
    [postTblView apiCallInitiated];
    [self callAPI];
    
    [postTblView onRefreshOnGlobalSearch:^{
        [arrData removeAllObjects];
        newPostButton.hidden = NO;
        
        isRefreshControlAPICall = YES;

        [self callAPI];
    }];
    
    [postTblView didSelectRow:^(PostModel *selModel, NSIndexPath *indexPath) {
        PostDetailsController *postDetailsVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"PostDetailsController"];
        postDetailsVC.selPostModel = selModel;
        postDetailsVC.indexPath = indexPath;
        self.hidesBottomBarWhenPushed = NO;
        [self.navigationController pushViewController:postDetailsVC animated:YES];
        
        
//        ALChatManager* chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLICATION_ID];
//
//        [chatManager launchChatForUserWithDefaultText:selModel.userId andFromViewController:self];
        
        [postDetailsVC onLikeCommentSuccess:^(PostModel *model, NSIndexPath *indexPath) {
            [postTblView like_comments_onDetails:model indexPath:indexPath];
        }];
        
        [postDetailsVC onShareSuccess:^(PostModel *model, NSIndexPath *indexPath) {
            [postTblView addSharedPost:model];
           
        }];
        
        [postDetailsVC onPostDelete:^{
            [postTblView deletePost:indexPath];
        }];
    }];
    
    
    [self.btnNews addTarget:self action:@selector(btnNewsClicked) forControlEvents:UIControlEventTouchUpInside];

   
    //check from apns
    [self checkAPNSPush];
    
    [self handleShareExtention];

    [self addFlotingButton];

}

-(void)addFlotingButton
{
    UIButton *btnNewMessage = [[UIButton alloc]init];
    btnNewMessage.backgroundColor = floating_button_bg_color;
    btnNewMessage.layer.cornerRadius = 32.5;
    [btnNewMessage setImage:[UIImage imageNamed:@"tab_post"] forState:UIControlStateNormal];
    [self.view addSubview:btnNewMessage];
    
    [btnNewMessage enableAutolayout];
    [btnNewMessage trailingMargin:10];
    [btnNewMessage bottomMargin:10];
    [btnNewMessage fixWidth:65];
    [btnNewMessage fixHeight:65];
    [btnNewMessage addTarget:self action:@selector(btnNewPostClicked) forControlEvents:UIControlEventTouchUpInside];
}


-(void)handleShareExtention
{
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.message.extension1"];
    NSDictionary *imgDic = [defaults objectForKey:@"guo_ext"];
    
    //[CustomAlertView showAlert:@"" withMessage:@"Test extention controller"];
    
    if (imgDic) {
        
        
        ALMessage * theMessage = [ALMessage new];
        
        NSData *imgData = imgDic[@"imgData"];
        NSString *message = imgDic[@"message"];
        NSString *imgPath = imgDic[@"imagePath"];
        
        
        
        if (imgData) {
            NSString *tempPath = [NSTemporaryDirectory() stringByAppendingString:@"tempImage.JPG"];
            [imgData writeToFile:tempPath atomically:YES];
            
            theMessage.imageFilePath = tempPath;
            theMessage.contentType = 1;
            
        }
        
        theMessage.type = @"5";
        theMessage.createdAtTime = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970] * 1000];
        theMessage.deviceKey = [ALUserDefaultsHandler getDeviceKeyString];
        theMessage.message = message?message:@"";
        theMessage.sendToDevice = NO;
        theMessage.shared = NO;
        theMessage.fileMeta = nil;
        theMessage.storeOnDevice = NO;
        theMessage.key = [[NSUUID UUID] UUIDString];
        theMessage.delivered = NO;
        theMessage.fileMetaKey = nil;
        theMessage.source = SOURCE_IOS;
        
        //    You have to pass the image file path, content type and message text above. For example: content type for images is 1. Then call the below method:
        
        ALChatManager* chatmanager = [[ALChatManager alloc] init];
        [chatmanager launchContactScreenWithMessage:theMessage andFromViewController:[UTILS topMostControllerNormal]];
        
        //reset the defualts
        [defaults setObject:nil forKey:@"guo_ext"];
        [defaults synchronize];
        
    }
    
    
}

-(void)btnNewPostClicked
{
    CreatePostController *createPostVC = [(AppObj).pawan_storyboard  instantiateViewControllerWithIdentifier:@"CreatePostController"];
    [[UTILS topMostControllerNormal].navigationController pushViewController:createPostVC
                                                                    animated:YES];
    
    
    [createPostVC onPostSuccess:^(PostModel *model) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UPLOAD_SUCCESS_NOTIFICATION object:model];
    }];
}


-(void)btnNewsClicked
{
    [[Toast sharedInstance]makeToast:NSLocalizedString(@"Coming Soon", nil) position:TOAST_POSITION_BOTTOM];

}




-(void)updateLocalization
{
    self.lblHeader.text=NSLocalizedString(@"post_header_title", nil);
    [postTblView reloadData];


}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self showNewPostToast];

  //  [UTILS addFloating:self.view tab_height:0 screen_name:nil];

    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:postTblView name:NOTIFICATION_GLOBAL_SEARCH object:nil];

    } @catch (NSException *exception) {

    } @finally {

    }

    (AppObj).golbalSearchScreenOpen =@"";
    [self updateLocalization];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMQTTNotification:)
                                                 name:@"MQTT_APPLOZIC_01"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAPNS:)
                                                 name:@"pushNotification"
                                               object:nil];
    
  
    
    
    
     //[self newRotateAnimation];
}

-(void)newRotateAnimation
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionRepeat| UIViewAnimationCurveLinear
                     animations:^{
                         self.btnNews.transform = CGAffineTransformRotate(self.btnNews.transform, M_PI );
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

#pragma mark - APNS

-(void)checkAPNSPush
{
    if (UTILS.apnsDictionary) {
        
        if([UTILS.apnsDictionary[@"aps"] objectForKey:@"postId"]){
            
            NSString *msg = UTILS.apnsDictionary[@"aps"][@"alert"];
            
            PostModel *model = [PostModel new];
            model.postId = [UTILS.apnsDictionary[@"aps"] objectForKey:@"postId"];
            PostDetailsController *postDetailsVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"PostDetailsController"];
            postDetailsVC.selPostModel = model;
            [UTILS topMostController].hidesBottomBarWhenPushed = NO;
            [[UTILS topMostController].navigationController pushViewController:postDetailsVC animated:YES];
            
            UTILS.apnsDictionary = nil;
            
        }
    }
}
-(void)showMQTTNotification:(NSNotification *)notifyObject
{
    ALMessage * alMessage = (ALMessage *)notifyObject.object;
    
    BOOL flag = (alMessage.groupId && [ALChannelService isChannelMuted:alMessage.groupId]);
    
    if (![alMessage.type isEqualToString:@"5"] && !flag && ![alMessage isMsgHidden])
    {
        ALNotificationView * alNotification = [[ALNotificationView alloc] initWithAlMessage:alMessage
                                                                           withAlertMessage:alMessage.message];
        
        [alNotification nativeNotification:self];
    }
}

-(void)handleNewPushForApp:(NSNotification *)notification
{
    NSDictionary *apnsDict = notification.object;
    
    if (apnsDict) {
        
        NSString *msg = apnsDict[@"aps"][@"alert"];
        
        //[CustomAlertView showAlert:@"" withMessage:msg];

        
        //if msg is for new post then show toast animation
        if ([msg  containsString:@"new post"] || [msg  containsString:@"新帖子"]) {
            [self showNewPostToast];
            
           // [CustomAlertView showAlert:@"" withMessage:@"got it"];
        }
        
       
    }
    
}

-(void)handleAPNS:(NSNotification *)notification
{
    /*NSString * contactId = notification.object;
    NSLog(@"USER_PROFILE_VC_NOTIFICATION_OBJECT : %@",contactId);
    NSDictionary *dict = notification.userInfo;
    NSNumber * updateUI = [dict valueForKey:@"updateUI"];
    NSString * alertValue = [dict valueForKey:@"alertValue"];
    
    ALPushAssist *pushAssist = [ALPushAssist new];
    
    NSArray * myArray = [contactId componentsSeparatedByString:@":"];
    NSNumber * channelKey = nil;
    if(myArray.count > 2)
    {
        channelKey = @([myArray[1] intValue]);
    }
    
    if([updateUI isEqualToNumber:[NSNumber numberWithInt:APP_STATE_ACTIVE]] && pushAssist.isUserProfileVCOnTop)
    {
        NSLog(@"######## USER PROFILE VC : APP_STATE_ACTIVE #########");
        
        ALMessage *alMessage = [[ALMessage alloc] init];
        alMessage.message = alertValue;
        NSArray *myArray = [alMessage.message componentsSeparatedByString:@":"];
        
        if(myArray.count > 1)
        {
            alertValue = [NSString stringWithFormat:@"%@", myArray[1]];
        }
        else
        {
            alertValue = myArray[0];
        }
        
        alMessage.message = alertValue;
        alMessage.contactIds = contactId;
        alMessage.groupId = channelKey;
        
        if ((channelKey && [ALChannelService isChannelMuted:alMessage.groupId]) || [alMessage isMsgHidden])
        {
            return;
        }
        
        ALNotificationView * alNotification = [[ALNotificationView alloc] initWithAlMessage:alMessage
                                                                           withAlertMessage:alMessage.message];
        [alNotification nativeNotification:self];
    }
    else if([updateUI isEqualToNumber:[NSNumber numberWithInt:APP_STATE_INACTIVE]])
    {
        NSLog(@"######## USER PROFILE VC : APP_STATE_INACTIVE #########");
        
        [self.tabBarController setSelectedIndex:0];
        UINavigationController *navVC = (UINavigationController *)self.tabBarController.selectedViewController;
        ALMessagesViewController *msgVC = (ALMessagesViewController *)[[navVC viewControllers] objectAtIndex:0];
        if(channelKey)
        {
            msgVC.channelKey = channelKey;
        }
        else
        {
            msgVC.channelKey = nil;
        }
        [msgVC createDetailChatViewController:contactId];
    }*/
    
    self.tabBarController.selectedIndex = 1;
}

-(void)callAPI
{
    currentPage = arrData.count == 0 ? 1 : currentPage+1;
    
    NSDictionary *params= @{
                            @"get":self.apiName,
                            @"user_id":UTILS.currentUser.userId,
                            @"me_id":self.is_came_from_sliding_menu ? @"-1" : UTILS.currentUser.userId,
                            @"limit":RecordLimit,
                            @"page":[NSString stringWithFormat:@"%d",currentPage],
                            @"self": self.is_came_from_sliding_menu?@"1":@"0"
                            };
    
    [REMOTE_API CallPOSTWebServiceWithParam:self.apiName params:params sBlock:^(id responseObject) {
        
        [arrData addObjectsFromArray:responseObject];
        NSLog(@"ArrData:%d",arrData.count);
        postTblView.arrSearchData = arrData;
        
        [postTblView apiCallEndWithResultCount:[arrData count] pageNo:currentPage];
        
        if([responseObject count] == 0)
        {
            [self RemovePullDownToRefresh];
        }
        else if(arrData.count >= [RecordLimit intValue])
        {
            [self AddPullDowntoRefresh];
        }
        
        [postTblView.refreshControl endRefreshing];
        
        if (isRefreshControlAPICall) {
            [UTILS playSoundOnEndRefresh];
            isRefreshControlAPICall = NO;
        }
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
        //on error 0 record and page 1

        [postTblView apiCallEndWithResultCount:0 pageNo:1];
        [postTblView.refreshControl endRefreshing];
        
  }];
}


#pragma mark- Remove pull down to refresh
-(void)RemovePullDownToRefresh
{
    //Remove Pull down to refresh when category_total_count match with array
    
    [refreshFooter remove_observer];
    
    refreshFooter.scrollView=nil;
    [refreshFooter.footerView removeFromSuperview];
   // postTblView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    
}
-(void)AddPullDowntoRefresh
{
    [self RemovePullDownToRefresh];
    refreshFooter.scrollView=postTblView;
    [refreshFooter footer];
}


#pragma mark- Menu swipe gesture
- (void)swipeGestureRecognized:(UISwipeGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    if(self.frostedViewController.swipeGestureEnabled)
    {
        [self.frostedViewController presentMenuViewController];
    }
}

-(IBAction)onclick_view_profile_GUO:(id)sender
{
    [(AppObj).postView showUserProfile:GUO_USERID];

}

-(IBAction)onclick_like:(UIButton*)sender
{
//    NSMutableDictionary *aDic = [[NSMutableDictionary alloc]initWithDictionary:[PostArr objectAtIndex:sender.tag]];
//    int get_like_count = [[aDic objectForKey:@"like"] intValue];
//    if(![[aDic objectForKey:@"is_selected"] boolValue])
//    {
//        get_like_count +=1;
//    }
//    else
//    {
//        get_like_count -=1;
//    }
//    [aDic setObject:[NSString stringWithFormat:@"%d",get_like_count] forKey:@"like"];
//
//    [aDic setObject:[NSString stringWithFormat:@"%d",![[aDic objectForKey:@"is_selected"] boolValue]] forKey:@"is_selected"];
//    [PostArr replaceObjectAtIndex:sender.tag withObject:aDic];
//    [self.aTableView reloadData];
}
-(IBAction)onclick_comment:(UIButton*)sender
{
//    self.comment_popup = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"CommentPopup"];
//    self.comment_popup.dataDic  = [PostArr objectAtIndex:sender.tag];
//    [AppObj showCustomPopup:self.comment_popup.view];

}


-(IBAction)onclick_menu:(id)sender
{
    if(!self.is_came_from_sliding_menu)
    {
        [self.frostedViewController.view endEditing:YES];
        [self.frostedViewController presentMenuViewController];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)closeDrawer{
    
    [UIView animateWithDuration:0.3 animations:^{
        [menuVC.view setFrame:CGRectMake(-640, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}

-(IBAction)onclick_logout:(id)sender
{
    //    [self.navigationController popToRootView/ControllerAnimated:YES];
    // Delete User credential from NSUserDefaults and other data related to user
    
    
    UIViewController* rootController = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    
    (AppObj).navController=[[UINavigationController alloc] initWithRootViewController:rootController];
    (AppObj).navController.navigationBarHidden=YES;
    (AppObj).window.rootViewController = (AppObj).navController;
    
}

- (IBAction)createPostClicked:(id)sender {
    [self pushToCreatePostController];
}

-(void)pushToCreatePostController
{
    CreatePostController *createPostVC = [(AppObj).pawan_storyboard  instantiateViewControllerWithIdentifier:@"CreatePostController"];
    [self.navigationController pushViewController:createPostVC animated:YES];
    
    [createPostVC onPostSuccess:^(PostModel *model) {
        [[Toast sharedInstance]makeToast:@"Post uploaded successfully" position:TOAST_POSITION_BOTTOM];
        [postTblView insertNewPost:model];
    }];
}


-(void)uploadSuccessNotification:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[PostModel class]]) {
        PostModel *model = notification.object;
        [postTblView insertNewPost:model];
    }
}


#pragma mark : MenuControllerDelegate
  -(void)menuTouched:(MenuItem)item{
    switch (item) {
      case Posts:
      
      break;
      case Followers:
      
      break;
      case Following:
      
      break;
      case Likes:
      
      break;
      case Broadcast:
      
      break;
      case TV:
      
      break;
      case News:
      
      break;
      case Account:
      
      break;
      case Privacy:
      
      break;
      case Terms:
      
      break;
      case About:
      
      break;
      case Contact:
      
      break;
      case Logout:
      
      break;
      
      
      default:
      break;
    }
  }

-(IBAction)ActionSeetForGlobalSearchPeopleTab:(UIButton*)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"message_dialog_cancel", nil).capitalizedString style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"block_connection", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void)showUserProfile:(NSString*)user_id
{
   /* UserProfileController *userProfileVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"UserProfileController"];
    userProfileVC.other_user_id = user_id;
    [[UTILS topMostController].navigationController pushViewController:userProfileVC animated:YES];*/
    
    [UTILS showUserProfile:user_id];

}

-(void)showNewPostToast
{
    
    float yPosition = 90;
    if (!newPostButton) {
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        //UIWindow *window = delegate.window;
        newPostButton = [UIButton buttonWithType:UIButtonTypeCustom];
        newPostButton.frame = CGRectMake((Screen_Width-150)/2, -yPosition, 150, 44);
        [newPostButton setTitle:NSLocalizedString(@"See New Posts", nil) forState:UIControlStateNormal];
        newPostButton.backgroundColor = floating_button_bg_color;
        newPostButton.layer.cornerRadius = 44/2;
        [self.view addSubview:newPostButton];
        
        [self.view bringSubviewToFront:newPostButton];
        
        [newPostButton addTarget:self action:@selector(getNewPostOnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    
    newPostButton.hidden = NO;
    newPostButton.frame = CGRectMake((Screen_Width-150)/2, -yPosition, 150, 44);

  
    
    [UIView animateWithDuration:0.3 animations:^{
        newPostButton.frame = CGRectMake((Screen_Width-150)/2, yPosition, 150, 44);

    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.3 delay:4 options:UIViewAnimationOptionCurveLinear animations:^{
//             newPostButton.frame = CGRectMake((Screen_Width-150)/2, -yPosition, 150, 44);
//        } completion:^(BOOL finished) {
//
//        }];
    }];
    
    
}
-(void)getNewPostOnClick
{
    float yPosition = 90;

   // [postTblView apiCallInitiated];
    [postTblView.refreshControl beginRefreshing];
    [postTblView setContentOffset:CGPointMake(0, -postTblView.refreshControl.frame.size.height) animated:YES];
    
    
    [UIView animateWithDuration:0.3 animations:^{
        newPostButton.frame = CGRectMake((Screen_Width-150)/2, -yPosition, 150, 44);
        
    } completion:^(BOOL finished) {
       
    }];

    [arrData removeAllObjects];
    [self callAPI];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
