//
//  CommentPopup.m
//  GUO
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "CommentPopup.h"

@interface CommentPopup ()

@end

@implementation CommentPopup

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils SetRoundedCorner:self.imgUser];
    self.lblHeader.textColor = header_color;
    
    self.btnTapImageChoose.hidden = YES;
    
    self.view.backgroundColor = popup_bg_color;
    
    self.view.frame = CGRectMake(0, 0, Screen_Width - 50 , 200);
    self.lblDesc.frame = CGRectMake(self.imgUser.frame.origin.x,self.lblDesc.frame.origin.y, self.view.frame.size.width - (self.imgUser.frame.origin.x*2), 10);
    [self.lblDesc sizeToFit];
    
    if(self.lblDesc.frame.size.height>50)
    {
        self.lblDesc.frame = CGRectMake(self.imgUser.frame.origin.x,self.lblDesc.frame.origin.y, self.view.frame.size.width - (self.imgUser.frame.origin.x*2), 50);
    }
    
    self.txtDetail.frame = CGRectMake(self.txtDetail.frame.origin.x,CGRectGetMaxY(self.lblDesc.frame) + 5, self.view.frame.size.width - (self.txtDetail.frame.origin.x*2), self.txtDetail.frame.size.height);

    self.footerView.frame = CGRectMake(0, CGRectGetMaxY(self.txtDetail.frame) + 5, self.view.frame.size.width, self.footerView.frame.size.height);
    
    [self.btnClose setImage:[Utils setTintColorToUIImage:self.btnClose.imageView.image color:[UIColor whiteColor]] forState:UIControlStateNormal];
    self.btnClose.backgroundColor = ColorRGBA(0, 0, 0, 0.8);
    [Utils SetRoundedCorner:self.btnClose];
    self.btnClose.layer.borderWidth = 0;
    if(self.dicDetail!=nil)
    {
        self.model = [self.dicDetail objectForKey:@"model"];
        self.txtDetail.text = [self.dicDetail objectForKey:@"comment_detail"];
        selected_img = (AppObj).selectedPopupImg;
        self.imgSelected.image = selected_img;
        if(selected_img)
        {
            self.btnTapImageChoose.hidden = YES;
        }
        else
        {
            self.btnTapImageChoose.hidden = NO;
        }
    }
    self.lblName.text = [Utils setFullname:self.model.userFirstname lname:self.model.userFirstname username:self.model.userName];
    self.lblTime.text =  self.model.time;
    self.lblDesc.text =  self.model.textPlain;
    
    if(selected_img)
    {
        self.imgSelected.frame = CGRectMake(self.imgSelected.frame.origin.x, CGRectGetMaxY(self.footerView.frame) + 5, 80, 80);
        self.btnClose.frame = CGRectMake(CGRectGetMaxX(self.imgSelected.frame) - self.btnClose.frame.size.width, self.imgSelected.frame.origin.y, self.btnClose.frame.size.width, self.btnClose.frame.size.height);
        self.view.frame = CGRectMake(0, 0, Screen_Width - 50 , CGRectGetMaxY(self.imgSelected.frame) + 10);
    }
    else
    {
        self.view.frame = CGRectMake(0, 0, Screen_Width - 50 , CGRectGetMaxY(self.footerView.frame));
    }

    
    self.footerView.backgroundColor = self.headerView.backgroundColor = medium_gray_bg_color;
    [Utils makeHalfRoundedButton:self.btnReply];
    
    [Utils SetTextViewProperties:self.txtDetail placeholder_txt:nil];
    self.view.layer.cornerRadius = textfield_corner_radius;
    self.view.layer.masksToBounds = YES;


    [self.imgUser setImageWithURL:[Utils getProperContentUrl:self.model.userPicture] placeholderImage:defaultUserImg];

    // Do any additional setup after loading the view from its nib.
    
    self.lblName.font = [UIFont fontWithName:Font_Medium size:userfull_name_font_size];
    self.lblTime.font = [UIFont fontWithName:Font_regular size:date_time_font_size];
    self.lblDesc.font = [UIFont fontWithName:Font_regular size:username_font_size];
    self.lblHeader.font = [UIFont fontWithName:Font_Medium size:header_font_size];
    
    self.lblHeader.text = NSLocalizedString(@"write_comment", nil);
    [self.btnReply setTitle:NSLocalizedString(@"btn_reply", nil) forState:UIControlStateNormal];

}
-(void)commentAPI
{
    
    [UTILS ShowProgress];
    [self.txtDetail resignFirstResponder];

    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setValue:API_COMMENT_IDENTIFICATION forKey:@"get"];
    [param setValue:UTILS.currentUser.userId forKey:@"user_id"];
    [param setValue: _isComment ? API_REPLY_TO_COMMENT : API_REPLY_TO_POST forKey: @"handle"];
    [param setValue:self.model.postId forKey:@"node_id"];
    [param setValue:[Utils RemoveWhiteSpaceFromText:self.txtDetail.text]  forKey:@"message"];
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_COMMENT_IDENTIFICATION params:param sBlock:^(id responseObject) {
        
        int commentCount = [self.model.comments intValue];
        commentCount +=1;
        self.model.comments = @(commentCount).stringValue;
        

        //        post_tbl setUpdatedPostModel:(mode)
        
        if (self.updatedPost) {
            self.updatedPost(self.model);
        }
        [self.view dismissPresentingPopup];

        [Utils HideProgress];
        
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [self.view dismissPresentingPopup];

        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];

        
    }];
    
}


#pragma mark- Onclick Share
-(IBAction)onclick_close:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        selected_img = nil;
        (AppObj).selectedPopupImg = nil;
        self.btnTapImageChoose.hidden = NO;
        self.imgSelected = nil;
        self.view.frame = CGRectMake(0, 0, Screen_Width - 50 , CGRectGetMaxY(self.footerView.frame));
        
    });
  
}
-(IBAction)onclick_Reply:(id)sender
{
    [self commentAPI];
}
-(void)updatePostModel:(commentBlock)blk
{
    self.updatedPost = blk;
}

- (IBAction)choosePhoto:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Take Photo", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            //[CustomAlertView showAlert:@"Alert" withMessage:@"Camera not available"];
            return;
        }
        
        [self presentImagePickerController:UIImagePickerControllerSourceTypeCamera mediaTypes:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Browse Photos", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self presentImagePickerController:UIImagePickerControllerSourceTypePhotoLibrary mediaTypes:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"message_dia_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    // Present action sheet.
    [self  presentViewController:actionSheet animated:YES completion:nil];
}
-(void)presentImagePicker
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Take Photo", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            //[CustomAlertView showAlert:@"Alert" withMessage:@"Camera not available"];
            return;
        }
        
        [self presentImagePickerController:UIImagePickerControllerSourceTypeCamera mediaTypes:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Record Video", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self presentImagePickerController:UIImagePickerControllerSourceTypeCamera mediaTypes:(NSString *) kUTTypeMovie];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"message_dia_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    // Present action sheet.
    [self  presentViewController:actionSheet animated:YES completion:nil];
}

-(void)presentImagePickerController:(UIImagePickerControllerSourceType)sourceType mediaTypes:(NSString*)mediaTypes
{
    UIImagePickerController *pickerController = [UIImagePickerController new];
    pickerController.sourceType = sourceType;
    if (mediaTypes) {
        pickerController.mediaTypes = @[mediaTypes];
    }
    
    pickerController.delegate = self;
    [self presentViewController:pickerController animated:YES completion:^{
        pickerController.navigationBar.tintColor = [UIColor blackColor];
    }];
}


- (void)imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [KLCPopup dismissAllPopups];
    self.btnTapImageChoose.hidden = YES;
    selected_img =[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    (AppObj).selectedPopupImg = selected_img;
    self.imgSelected.image =(AppObj).selectedPopupImg;
   
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [self reopenPopup];

}
-(void)reopenPopup
{
    [KLCPopup dismissAllPopups];

    (AppObj).selectedPopupImg = selected_img;
    NSDictionary *dic=@{@"model":self.model,
                        @"index_path_row":@"0",
                        @"comment_detail":self.txtDetail.text
                        };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateCustomPopup" object:dic];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [KLCPopup dismissAllPopups];


    [picker dismissViewControllerAnimated:YES completion:nil];
    [self reopenPopup];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
