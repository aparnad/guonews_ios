//
//  ShareView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 16/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ShareView.h"

@interface ShareView()
{
    UIView *baseView;
    UITextView *txtViewComment;
    UIImageView *imgViewDP;
    
    UIView *viewInfo;
    UILabel *lblUserName;
    UILabel *lblTime;
    
    UIView *baseTextView;
    UIImageView *imgAttachment;
    
    UIView *bgView;
}
@end

@implementation ShareView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        self.alpha = 0;
        
        
        TPKeyboardAvoidingScrollView *baseScroll = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:self.bounds];
        [self addSubview:baseScroll];
        
        UIButton *btnClose = [[UIButton alloc]initWithFrame:self.bounds];
        [baseScroll addSubview:btnClose];
        [btnClose addTarget:self action:@selector(btnCloseClicked) forControlEvents:UIControlEventTouchUpInside];
        
        baseView = [[UIView alloc]initWithFrame:CGRectMake(20, CGRectGetMidY(baseScroll.frame) - 160, baseScroll.frame.size.width - 40, 320)];
        baseView.backgroundColor = [UIColor whiteColor];
        baseView.layer.cornerRadius = 5;
        baseView.clipsToBounds = YES;
        [baseScroll addSubview:baseView];
        
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, baseView.frame.size.width, 50)];
        lblTitle.font = [UIFont fontWithName:Font_Medium size:20];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.text = NSLocalizedString(@"share_post", nil);
        lblTitle.backgroundColor = [UIColor controllerBGColor];
        lblTitle.textColor = [UIColor blackColor];//header_color;
        [baseView addSubview:lblTitle];
        
        baseTextView = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(lblTitle.frame)+10, baseView.frame.size.width - 20, 120)];
        baseTextView.layer.borderWidth = 0.8;
        baseTextView.layer.borderColor = [UIColor controllerBGColor].CGColor;
        baseTextView.layer.cornerRadius = 5;
        [baseView addSubview:baseTextView];

        txtViewComment = [[UITextView alloc]initWithFrame:CGRectMake(0,0,baseTextView.frame.size.width,80)];
        txtViewComment.font = [UIFont fontWithName:Font_regular size:18];
        txtViewComment.textColor = [UIColor darkTextColor];
        [baseTextView addSubview:txtViewComment];
        
        UIButton *btnGallery = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(baseTextView.frame) - 50, CGRectGetMaxY(txtViewComment.frame), 40, 40)];
        [btnGallery setImage:[UIImage imageNamed:@"ic_gallery"] forState:UIControlStateNormal];
        [btnGallery addTarget:self action:@selector(btnGalleryClicked) forControlEvents:UIControlEventTouchUpInside];
        [baseTextView addSubview:btnGallery];
        
        imgAttachment = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(baseTextView.bounds)-85, CGRectGetMaxY(txtViewComment.frame), 80, 0)];
        imgAttachment.userInteractionEnabled = YES;
        imgAttachment.clipsToBounds = YES;
        [baseTextView addSubview:imgAttachment];
        
        UIButton *btnDeleteAttachment = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgAttachment.bounds)-20, 0, 20, 20)];
        [btnDeleteAttachment setImage:[UIImage imageNamed:@"ic_close_white"] forState:UIControlStateNormal];
        btnDeleteAttachment.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        btnDeleteAttachment.layer.cornerRadius = 10;
        btnDeleteAttachment.layer.masksToBounds = YES;
        [btnDeleteAttachment addTarget:self action:@selector(btnDeleteAttachement) forControlEvents:UIControlEventTouchUpInside];
        [imgAttachment addSubview:btnDeleteAttachment];
        
        viewInfo = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(baseTextView.frame)+10, baseTextView.frame.size.width, 50)];
        [baseView addSubview:viewInfo];
        
        imgViewDP = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        imgViewDP.layer.cornerRadius = 25;
        imgViewDP.layer.masksToBounds = YES;
        imgViewDP.image = defaultUserImg;
        imgViewDP.layer.borderWidth = 1.0;
        imgViewDP.layer.borderColor = header_color.CGColor;
        [viewInfo addSubview:imgViewDP];
        
        lblUserName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgViewDP.frame)+10, CGRectGetMinY(imgViewDP.frame), baseView.frame.size.width - CGRectGetMaxX(imgViewDP.frame) - 10 , 30)];
        lblUserName.text = @"Guo";
        lblUserName.font = [UIFont fontWithName:Font_Medium size:20];
        [viewInfo addSubview:lblUserName];
       
        lblTime = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(lblUserName.frame), CGRectGetMaxY(lblUserName.frame), lblUserName.frame.size.width ,20)];
        lblTime.text = @"2018-07-16 08:11:32";
        lblTime.font = [UIFont fontWithName:Font_regular size:13];
        [viewInfo addSubview:lblTime];
        
        
        bgView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(viewInfo.frame)+20, baseView.frame.size.width, 50)];
        bgView.backgroundColor = [UIColor controllerBGColor];
        [baseView addSubview:bgView];
        
        UIButton *btnShare = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(bgView.bounds) - 110, CGRectGetMidY(bgView.bounds)-20, 100, 40)];
        btnShare.layer.borderWidth = 1.0;
        btnShare.layer.borderColor = header_color.CGColor;
        btnShare.layer.cornerRadius = 20;
        btnShare.titleLabel.font = [UIFont fontWithName:Font_Medium size:18];
        [btnShare setTitle:NSLocalizedString(@"btn_share", nil) forState:UIControlStateNormal];
        [btnShare setTitleColor:header_color forState:UIControlStateNormal];
        [bgView addSubview:btnShare];
        
        [UIView animateWithDuration:0.3 animations:^{
            self.alpha = 1;
        }];
    }
    return self;
}

-(void)btnGalleryClicked
{
    [UTILS presentGalleryPicker:^(UIImage *image) {
        imgAttachment.image = image;
        [self showAttachment];
    } sorceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

-(void)btnCloseClicked
{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)btnDeleteAttachement
{
    imgAttachment.image = nil;
    [self hideAttachment];
}

-(void)showAttachment
{
    imgAttachment.frame = CGRectMake(imgAttachment.frame.origin.x, imgAttachment.frame.origin.y, imgAttachment.frame.size.width, imgAttachment.frame.size.width);
    
    baseTextView.frame = CGRectMake(baseTextView.frame.origin.x, baseTextView.frame.origin.y, baseTextView.frame.size.width, CGRectGetMaxY(imgAttachment.frame)+10);

    [self commonConfig];

}

-(void)hideAttachment
{
    imgAttachment.frame = CGRectMake(imgAttachment.frame.origin.x, imgAttachment.frame.origin.y, imgAttachment.frame.size.width, 0);
    
    baseTextView.frame = CGRectMake(baseTextView.frame.origin.x, baseTextView.frame.origin.y, baseTextView.frame.size.width, CGRectGetMaxY(imgAttachment.frame)+40);

    
   [self commonConfig];
}

-(void)commonConfig
{
    
    viewInfo.frame = CGRectMake(viewInfo.frame.origin.x, CGRectGetMaxY(baseTextView.frame)+10, viewInfo.frame.size.width,viewInfo.frame.size.height);
    
    bgView.frame = CGRectMake(bgView.frame.origin.x, CGRectGetMaxY(viewInfo.frame)+10, bgView.frame.size.width, bgView.frame.size.height);
    
    [UIView animateWithDuration:0.1 animations:^{
        CGRect baseRect = baseView.frame;
        baseRect.size.height = CGRectGetMaxY(bgView.frame)+20;
        baseRect.origin.y = baseRect.size.height/2;
        baseView.frame = baseRect;
    }];
}
@end
