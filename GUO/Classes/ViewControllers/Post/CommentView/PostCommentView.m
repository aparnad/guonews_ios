//
//  CommentView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 16/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "PostCommentView.h"
#import "AWSS3.h"
#import "UIImage+fixOrientation.h"
#import "CreatePostModel.h"
@interface PostCommentView()<UITextViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    UIView *baseView;
    UITextView *txtViewComment;
    UIImageView *imgViewDP;
    
    UIView *viewInfo;
    UILabel *lblUserName;
    UILabel *lblTime;
    
    UIButton *btnReply;
    
    UIImageView *imgAttachment;
    UIView *bgView;
    UILabel *lblComment;
    PostModel *selModel;
    UIImage *selImage;
    
    AWSS3TransferManagerUploadRequest *imgUploadRequest;
    AWSS3TransferManagerUploadRequest *thumbUploadRequest;
    
    UIProgressView *progressView;
    DGActivityIndicatorView *activityIndicatorView;
    NSMutableArray *arrReq;
    BOOL uploadStarted;
    int count;
    
    UIView *hiddenView;
    
    NSMutableArray *arrUploadTask;

}
@end

@implementation PostCommentView

-(instancetype)initWithFrame:(CGRect)frame model:(PostModel *)postModel
{
    self = [super initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
    if (self) {
        
        arrReq = [NSMutableArray new];
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        self.alpha = 0;
        
        arrUploadTask = [NSMutableArray new];
        
        selModel = postModel;
        TPKeyboardAvoidingScrollView *baseScroll = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:self.bounds];
        [self addSubview:baseScroll];
        
        
        baseScroll.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        
        baseView = [[UIView alloc]initWithFrame:CGRectMake(20, CGRectGetMidY(baseScroll.frame) - 160, baseScroll.frame.size.width - 40, 320)];
        baseView.backgroundColor = [UIColor whiteColor];
        baseView.layer.cornerRadius = 5;
        baseView.clipsToBounds = YES;
        [baseScroll addSubview:baseView];
        
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, baseView.frame.size.width, 50)];
        lblTitle.font = [UIFont fontWithName:Font_Medium size:20];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.text = NSLocalizedString(@"write_comment", nil);
        lblTitle.backgroundColor = [UIColor controllerBGColor];
        lblTitle.textColor = [UIColor blackColor];//header_color;
        [baseView addSubview:lblTitle];
        
        UIButton *btnClose = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(baseView.bounds) - 50, 0, 50, 50)];
        [baseView addSubview:btnClose];
        [btnClose addTarget:self action:@selector(btnCloseClicked) forControlEvents:UIControlEventTouchUpInside];
        [btnClose setImage:[UIImage imageNamed:@"ic_close"] forState:UIControlStateNormal];
        
        progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        progressView.frame = CGRectMake(0, CGRectGetMaxY(lblTitle.frame), baseView.frame.size.width, 5);
        progressView.progressTintColor = header_color;
        progressView.trackTintColor = [UIColor whiteColor];
        [[progressView layer]setMasksToBounds:TRUE];
        progressView.clipsToBounds = YES;
        [baseView addSubview:progressView];
        
        viewInfo = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(lblTitle.frame)+10, baseView.frame.size.width - 10, 50)];
        [baseView addSubview:viewInfo];
        
        imgViewDP = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        imgViewDP.layer.cornerRadius = 25;
        imgViewDP.layer.masksToBounds = YES;
        imgViewDP.image = defaultUserImg;
        imgViewDP.layer.borderWidth = 1.0;
        imgViewDP.layer.borderColor = header_color.CGColor;
        [viewInfo addSubview:imgViewDP];
        
        lblUserName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgViewDP.frame)+10, CGRectGetMinY(imgViewDP.frame), baseView.frame.size.width - CGRectGetMaxX(imgViewDP.frame) - 10 , 30)];
        lblUserName.text = @"Guo";
        lblUserName.font = [UIFont fontWithName:Font_Medium size:20];
        [viewInfo addSubview:lblUserName];
        
        lblTime = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(lblUserName.frame), CGRectGetMaxY(lblUserName.frame), lblUserName.frame.size.width ,20)];
        lblTime.text = @"2018-07-16 08:11:32";
        lblTime.font = [UIFont fontWithName:Font_regular size:13];
        [viewInfo addSubview:lblTime];
        
        lblComment = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(viewInfo.frame)+10, baseView.frame.size.width - 20, 50)];
        lblComment.font = [UIFont fontWithName:Font_regular size:16];
        lblComment.numberOfLines = 2;
        [baseView addSubview:lblComment];
        
        txtViewComment = [[UITextView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(lblComment.frame)+10, baseView.frame.size.width - 20, 100)];
        txtViewComment.font = [UIFont fontWithName:Font_regular size:18];
        txtViewComment.textColor = [UIColor darkTextColor];
        txtViewComment.layer.borderWidth = 0.8;
        txtViewComment.layer.borderColor = [UIColor controllerBGColor].CGColor;
        txtViewComment.layer.cornerRadius = 5;
        txtViewComment.delegate = self;
        [baseView addSubview:txtViewComment];
        
        imgAttachment = [[UIImageView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(txtViewComment.frame)+10, 80, 0)];
        imgAttachment.userInteractionEnabled = YES;
        imgAttachment.clipsToBounds = YES;
        imgAttachment.layer.cornerRadius = 5;
        [baseView addSubview:imgAttachment];
        
        UIButton *btnDeleteAttachment = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgAttachment.bounds)-20, 0, 20, 20)];
        [btnDeleteAttachment setImage:[UIImage imageNamed:@"ic_close_white"] forState:UIControlStateNormal];
        btnDeleteAttachment.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        btnDeleteAttachment.layer.cornerRadius = 10;
        btnDeleteAttachment.layer.masksToBounds = YES;
        [btnDeleteAttachment addTarget:self action:@selector(btnDeleteAttachement) forControlEvents:UIControlEventTouchUpInside];
        [imgAttachment addSubview:btnDeleteAttachment];
                
        bgView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(imgAttachment.frame)+20, baseView.frame.size.width, 50)];
        bgView.backgroundColor = [UIColor controllerBGColor];
        [baseView addSubview:bgView];
        
        btnReply = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(bgView.bounds) - 110, CGRectGetMidY(bgView.bounds)-15, 100, 30)];
        btnReply.layer.borderWidth = 2.0;
        btnReply.layer.cornerRadius = 15;
        btnReply.titleLabel.font = [UIFont fontWithName:Font_Medium size:18];
        [btnReply setTitle:NSLocalizedString(@"btn_reply", nil) forState:UIControlStateNormal];
        [btnReply addTarget:self action:@selector(btnReplyClicked)
           forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:btnReply];
        [self disableReplyButton];
        
        UIButton *btnGallery = [[UIButton alloc]initWithFrame:CGRectMake(10, CGRectGetMidY(btnReply.frame) - 20, 40, 40)];
        [btnGallery setImage:[UIImage imageNamed:@"ic_gallery"] forState:UIControlStateNormal];
        [btnGallery addTarget:self action:@selector(btnGalleryClicked) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:btnGallery];
        
        UIButton *btnVideo = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btnGallery.frame), CGRectGetMidY(btnReply.frame) - 20, 40, 40)];
        [btnVideo setImage:[UIImage imageNamed:@"ic_video"] forState:UIControlStateNormal];
        [btnVideo addTarget:self action:@selector(btnVideoClicked) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:btnVideo];
        
        
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
        activityIndicatorView.backgroundColor = [UIColor clearColor];
        activityIndicatorView.frame = CGRectMake(CGRectGetMidX(bgView.bounds)-50, btnVideo.frame.origin.y, 100, 50);
        [bgView addSubview:activityIndicatorView];
        
        CGRect baseRect = baseView.frame;
        baseRect.size.height = CGRectGetMaxY(bgView.frame);
        baseRect.origin.y = 100;
        baseView.frame = baseRect;
        
        lblUserName.text = postModel.userName;
        lblTime.text =  postModel.time;
        lblComment.text =  postModel.textPlain;
        
        
        hiddenView = [[UIView alloc]initWithFrame:CGRectMake(0, 50, baseView.frame.size.width, baseView.frame.size.height - 50)];
        [baseView addSubview:hiddenView];
        hiddenView.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.alpha = 1;
        }];
    }
    return self;
}

-(void)disableReplyButton
{
    btnReply.userInteractionEnabled = NO;
    [btnReply setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    btnReply.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)enableReplyButton
{
    btnReply.userInteractionEnabled = YES;
    [btnReply setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnReply.layer.borderColor = [UIColor blackColor].CGColor;
}

-(void)btnReplyClicked
{
    if (arrReq.count) {
       // [UTILS ShowProgress];
        for (CreatePostModel *model in arrReq) {
            [self upload:model.uploadRequest];
        }
        uploadStarted = YES;
        hiddenView.alpha = 1;
        [activityIndicatorView startAnimating];
    }
    else{
        [self commentAPIwithPhotoUrl:nil videoURL:nil];
    }
//    if (imgUploadRequest != nil) {
//        [UTILS ShowProgress];
//        [self upload:thumbUploadRequest];
//        [self upload:imgUploadRequest];
//    }
//    else{
//        [self commentAPI:nil];
//    }
}

-(BOOL)validate
{
    if ([txtViewComment.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length == 0) {
        return NO;
    }
    
    if (selImage == nil) {
        return NO;
    }

    return YES;
}

-(void)commentAPIwithPhotoUrl:(NSString *)photo videoURL:(NSString *)video
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [txtViewComment resignFirstResponder];
    });
    
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setValue:API_COMMENT_IDENTIFICATION forKey:@"get"];
    [param setValue:UTILS.currentUser.userId forKey:@"user_id"];
    [param setValue: _isComment ? API_REPLY_TO_COMMENT : API_REPLY_TO_POST forKey: @"handle"];
    [param setValue:selModel.postId forKey:@"node_id"];
   // dispatch_async(dispatch_get_main_queue(), ^{
        [param setValue:[Utils RemoveWhiteSpaceFromText:txtViewComment.text]  forKey:@"message"];
  //  });
    
    if (photo) {
        [param setValue:[Utils RemoveWhiteSpaceFromText:photo]  forKey:@"photo"];
    }
    
    if (video) {
        [param setValue:[Utils RemoveWhiteSpaceFromText:video]  forKey:@"video"];
    }
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_COMMENT_IDENTIFICATION params:param sBlock:^(id responseObject) {
        
        int commentCount = [selModel.comments intValue];
        commentCount +=1;
        selModel.comments = @(commentCount).stringValue;
        
        if (self.updatedPost) {
            self.updatedPost(selModel);
        }
        [self hideView];
        [Utils HideProgress];
        
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [self btnCloseClicked];
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
    
}

-(void)btnGalleryClicked
{
    [UTILS showActionSheet:@"" optionsArray:@[NSLocalizedString(@"Take Photo", nil),NSLocalizedString(@"Browse Photos", nil)] completion:^(NSInteger index) {
            [self showImagePicker:index == 0 ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary];
    }];
}

-(void)btnVideoClicked
{
    [UTILS showActionSheet:@"" optionsArray:@[NSLocalizedString(@"Record Video", nil),NSLocalizedString(@"Browse Video", nil)] completion:^(NSInteger index) {
        if (index == 0) {
            [self presentImagePickerController:UIImagePickerControllerSourceTypeCamera mediaTypes:(NSString *) kUTTypeMovie];
        }
        else{
            UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
            mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
            //mediaUI.allowsEditing = YES;
            mediaUI.delegate = self;
            [[UTILS topMostController] presentViewController:mediaUI animated:YES completion:nil];
        }
    }];
}

-(void)presentImagePickerController:(UIImagePickerControllerSourceType)sourceType mediaTypes:(NSString*)mediaTypes
{
    UIImagePickerController *pickerController = [UIImagePickerController new];
    pickerController.sourceType = sourceType;
    if (mediaTypes) {
        pickerController.mediaTypes = @[mediaTypes];
    }
    
    pickerController.delegate = self;
    [[UTILS topMostController] presentViewController:pickerController animated:YES completion:^{
        pickerController.navigationBar.tintColor = [UIColor blackColor];
    }];
}


- (void)imagePickerController:(UIImagePickerController *) Picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self endEditing:YES];
    
    NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
    [self createVideoUploadRequest:videoURL];
    [Picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}


-(void)showImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    [UTILS presentGalleryPicker:^(UIImage *image) {
        imgAttachment.image = image;
        selImage = image;
        [self showAttachment];
        [self enableReplyButton];
        [self createUploadRequest:image];
    } sorceType:sourceType];
}

-(void)btnCloseClicked
{
    if (uploadStarted) {
        [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"Do you want to stop uploading process?", nil) okHandler:^{
            [self cancelAllUploads];
            [self hideView];
        }];
    }
    else{
        [self hideView];
    }
}

-(void)hideView{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
-(void)cancelAllUploads
{
    for (CreatePostModel *model in arrReq) {
        AWSS3TransferManagerUploadRequest *uploadRequest = model.uploadRequest;
        [[uploadRequest cancel] continueWithBlock:^id(AWSTask *task) {
            if (task.error) {
                NSLog(@"The cancel request failed: [%@]", task.error);
            }
            
            return nil;
        }];
    }
}

-(void)btnDeleteAttachement
{
    imgAttachment.image = nil;
    [self hideAttachment];
    imgUploadRequest = nil;
    if (txtViewComment.text.length != 0) {
        [self enableReplyButton];
    }
    else{
        [self disableReplyButton];
    }
}

-(void)showAttachment
{
    imgAttachment.frame = CGRectMake(imgAttachment.frame.origin.x, imgAttachment.frame.origin.y, imgAttachment.frame.size.width, imgAttachment.frame.size.width);
    
    [self commonConfig];
    
}

-(void)hideAttachment
{
    imgAttachment.frame = CGRectMake(imgAttachment.frame.origin.x, imgAttachment.frame.origin.y, imgAttachment.frame.size.width, 0);
    
    [self commonConfig];
}

-(void)commonConfig
{
    
    bgView.frame = CGRectMake(bgView.frame.origin.x, CGRectGetMaxY(imgAttachment.frame)+10, bgView.frame.size.width, bgView.frame.size.height);
    
    [UIView animateWithDuration:0.1 animations:^{
        CGRect baseRect = baseView.frame;
        baseRect.size.height = CGRectGetMaxY(bgView.frame);
        baseRect.origin.y = 100;
        baseView.frame = baseRect;
        
        CGRect rect = hiddenView.frame;
        rect.size.height = baseView.frame.size.height;
        hiddenView.frame = rect;
    }];
}

-(void)updatePostModel:(commentBlock)blk
{
    self.updatedPost = blk;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *value = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if (value.length > 0) {
        [self enableReplyButton];
    }
    else if (selImage ==  nil){
        [self disableReplyButton];
    }
    return YES;
}

-(void)createVideoUploadRequest:(NSURL *)videoURL
{
    arrReq = [NSMutableArray new];

    NSArray *yearMonth = [Utils getCurrentYearandMonth];
    NSString *extension = [videoURL pathExtension];
    NSString *videoName = [Utils getVideoName:extension];

    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.body = videoURL;//[NSURL fileURLWithPath:filePath];
    uploadRequest.key = videoName;
    NSString *bucketPath = [NSString stringWithFormat:@"%@%@/%@/%@",S3_bUCKET_NAME,@"uploads/videos",yearMonth[0],yearMonth[1]];
    uploadRequest.bucket = bucketPath;
    uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    
    CreatePostModel *model = [[CreatePostModel alloc]init];
    model.uploadRequest = uploadRequest;

    //Generate thumbnail request from VIDEO URL
    
    UIImage *thumbImg = [self generateVideoThumbImage:videoURL];
    
    thumbImg = [thumbImg fixOrientation];
    imgAttachment.image = thumbImg;
    selImage = thumbImg;
    [self showAttachment];
    [self enableReplyButton];

    AWSS3TransferManagerUploadRequest *thumbUploadRequest = [AWSS3TransferManagerUploadRequest new];
    NSString *thumbName =  [[videoName stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
    
    NSString *filePath = [self saveThumbnailImage:thumbImg imageName:thumbName];
    
    thumbUploadRequest.body = [NSURL fileURLWithPath:filePath];
    thumbUploadRequest.key = thumbName;
    NSString *bucketPath1 = [NSString stringWithFormat:@"%@%@/%@",S3_bUCKET_NAME,@"uploads/photos",@"thumb"];
    thumbUploadRequest.bucket = bucketPath1;
    thumbUploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    
    CreatePostModel *thumbModel = [[CreatePostModel alloc]init];
    thumbModel.uploadRequest = thumbUploadRequest;

    [arrReq addObject:thumbModel];
    [arrReq addObject:model];
}

-(UIImage *)generateVideoThumbImage : (NSURL *)videoURL
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generateImg.appliesPreferredTrackTransform = YES;
    NSError *error = NULL;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    NSLog(@"error==%@, Refimage==%@", error, refImg);
    
    UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
    return FrameImage;
}

-(void)createUploadRequest:(UIImage *)image
{
    arrReq = [NSMutableArray new];
    NSArray *yearMonth = [Utils getCurrentYearandMonth];

    NSString *imgName = [Utils getImageName:@"jpg"];
    NSString *filePath = [self saveImage:image imageName:imgName];
    
    imgUploadRequest = [AWSS3TransferManagerUploadRequest new];
    imgUploadRequest.body = [NSURL fileURLWithPath:filePath];
    imgUploadRequest.key = imgName;
    NSString *bucketPath = [NSString stringWithFormat:@"%@%@/%@/%@",S3_bUCKET_NAME,@"uploads/photos",yearMonth[0],yearMonth[1]];
    imgUploadRequest.bucket = bucketPath;
    imgUploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    
    CreatePostModel *model = [[CreatePostModel alloc]init];
    model.uploadRequest = imgUploadRequest;
    
    //Generate Request for Thumbnails
    UIImage *thumbImg= [Utils generatePhotoThumbnail:image];
    
   // thumbImg =  [UIImage imageWithCGImage:[thumbImg CGImage] scale:[thumbImg scale] orientation:UIImageOrientationRight];
    
    NSString *thumbFilePath = [self saveThumbnailImage:thumbImg imageName:imgName];
    
    thumbUploadRequest = [AWSS3TransferManagerUploadRequest new];
    thumbUploadRequest.body = [NSURL fileURLWithPath:thumbFilePath];
    thumbUploadRequest.key = imgName;
    NSString *thumbBucketPath = [NSString stringWithFormat:@"%@%@/%@",S3_bUCKET_NAME,@"uploads/photos",@"thumb"];
    thumbUploadRequest.bucket = thumbBucketPath;
    thumbUploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    
    CreatePostModel *thumbModel = [[CreatePostModel alloc]init];
    thumbModel.uploadRequest = thumbUploadRequest;

    
    [arrReq addObject:thumbModel];
    [arrReq addObject:model];
}

-(void)upload:(AWSS3TransferManagerUploadRequest *)uploadRequest{
    
    /*
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                switch (task.error.code) {
                    case AWSS3TransferManagerErrorCancelled:
                    case AWSS3TransferManagerErrorPaused:
                    {
                        
                    }
                    break;
                        
                    default:
                        NSLog(@"Upload failed: [%@]", task.error);
                        [CustomAlertView showAlert:@"" withMessage:task.error.localizedDescription];
                        [Utils HideProgress];
                        break;
                }
            } else {
                NSLog(@"Upload failed: [%@]", task.error);
                [CustomAlertView showAlert:@"" withMessage:task.error.localizedDescription];
                [Utils HideProgress];
            }
        }
        
        if (task.result) {
            
          //  [arrReq removeObject:uploadRequest];
            count++;
            if (arrReq.count == count) {
                
                NSString *url = [uploadRequest.bucket stringByReplacingOccurrencesOfString:S3_bUCKET_NAME withString:@""];
                url = [[url stringByAppendingString:@"/"] stringByAppendingString:uploadRequest.key];
                if ([url containsString:@"photos"]) {
                    [self commentAPIwithPhotoUrl:url videoURL:nil];
                }
                else{
                    [self commentAPIwithPhotoUrl:nil videoURL:url];
                }
            }
        }
        return nil;
    }];
    
    __weak typeof(AWSS3TransferManagerUploadRequest *) weakSelf = uploadRequest;
    weakSelf.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (uploadRequest.state) {
                case AWSS3TransferManagerRequestStateRunning:
                {
                    if (arrReq.count) {
                        if (totalBytesExpectedToSend > 0) {
                            CGFloat progress = (float)((double) totalBytesSent / totalBytesExpectedToSend) * 100;
                            
                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uploadRequest = %@", weakSelf];
                            NSArray *filterArray = [arrReq filteredArrayUsingPredicate:predicate];
                            
                            
                            NSInteger index = [arrReq indexOfObject:filterArray[0]];
                            CreatePostModel *model = arrReq[index];
                            model.progress = progress;
                            [arrReq replaceObjectAtIndex:index withObject:model];
                            
                            CGFloat totalProgress = 0;
                            
                            for (CreatePostModel *model in arrReq) {
                                totalProgress = totalProgress + model.progress;
                            }
                            
                            totalProgress = totalProgress/arrReq.count;
                            NSLog(@"%f",totalProgress/100);
                            
                            [progressView setProgress:totalProgress/100];
                        }
                    }
                }
                    break;
                    
                default:
                    break;
            }
        });
    };
     
     */
    
    
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility S3TransferUtilityForKey:APP_AWS_ACCELERATION_KEY];
    
    
    AWSS3TransferUtilityUploadExpression *expression = [AWSS3TransferUtilityUploadExpression new];
    AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
        
        NSLog(@"ERROOROROOOOROR 2222222 => %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            count++;
            if (arrReq.count == count) {
                
                NSString *url = [uploadRequest.bucket stringByReplacingOccurrencesOfString:S3_bUCKET_NAME withString:@""];
                url = [url stringByReplacingOccurrencesOfString:@"uploads/" withString:@""];

                
                url = [[url stringByAppendingString:@"/"] stringByAppendingString:uploadRequest.key];
                
                if ([url containsString:@"photos"]) {
                    [self commentAPIwithPhotoUrl:url videoURL:nil];
                }
                else{
                    [self commentAPIwithPhotoUrl:nil videoURL:url];
                }
            }
        });
    };
    
    NSString *keyName = [[[uploadRequest.bucket stringByReplacingOccurrencesOfString:S3_bUCKET_NAME withString:@""] stringByAppendingString:@"/"] stringByAppendingString:uploadRequest.key];
    
    [[transferUtility uploadFile:uploadRequest.body bucket:S3_bUCKET_NAME key:keyName contentType:@"image/jpeg" expression:expression completionHandler:completionHandler] continueWithSuccessBlock:^id _Nullable(AWSTask<AWSS3TransferUtilityUploadTask *> * _Nonnull task) {
        
        NSLog(@"ERROOROROOOOROR => %@",task.error);
        
        return nil;
    }];
    
    
    expression.progressBlock = ^(AWSS3TransferUtilityTask *task, NSProgress *progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something e.g. Update a progress bar.
            
            if (![arrUploadTask containsObject:task]) {
                [arrUploadTask addObject:task];
            }
            
            //  NSLog(@"PROGRESS => %lf",progress.fractionCompleted);
            //  NSLog(@"TOTAL PROGRESS => %lf",totalProgress);
           CGFloat totalProgress = 0;
            for (AWSS3TransferUtilityTask *taskObj in arrUploadTask) {
                totalProgress += taskObj.progress.fractionCompleted;
            }
            
            totalProgress = totalProgress/arrReq.count;
            
            [progressView setProgress:totalProgress];
            
        });
    };
    
    
}

-(NSString *)saveImage:(UIImage *)image imageName:(NSString *)imgName
{
    NSArray *yearMonth = [Utils getCurrentYearandMonth];
    NSString *filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:yearMonth[0]] stringByAppendingPathComponent:yearMonth[1]];
    
    NSError *error = nil;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"reating 'upload' directory failed: [%@]", error);
    }
    filePath = [filePath stringByAppendingPathComponent:imgName];
    
    NSData *pngData = UIImageJPEGRepresentation(image, IMAGE_COMPRESSION);
    
    [pngData writeToFile:filePath atomically:YES];
    
    return filePath;
}

-(NSString *)saveThumbnailImage:(UIImage *)image imageName:(NSString *)imgName
{
    NSArray *yearMonth = [Utils getCurrentYearandMonth];
    NSString *filePath = [[[NSTemporaryDirectory() stringByAppendingPathComponent:yearMonth[0]] stringByAppendingPathComponent:yearMonth[1]] stringByAppendingPathComponent:@"Thumb"];
    
    NSError *error = nil;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"reating 'upload' directory failed: [%@]", error);
    }
    filePath = [filePath stringByAppendingPathComponent:imgName];
    
    NSData *pngData = UIImageJPEGRepresentation(image, 1);
    
    [pngData writeToFile:filePath atomically:YES];
    
    return filePath;
}

@end

