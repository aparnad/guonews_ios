//
//  SharePopup.h
//  GUO
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LPlaceholderTextView.h"
#import "PostModel.h"



typedef void(^shareBlock)(PostModel *model);


@interface SharePopup : UIViewController
{
    
}

@property (nonatomic,strong) PostModel *model;

@property(strong,nonatomic) IBOutlet UIView *headerView,*footerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property(strong,nonatomic) IBOutlet LPlaceholderTextView *txtDetail;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;

@property (strong, nonatomic) IBOutlet UIButton *btnShare;
-(IBAction)onclick_share:(id)sender;


@property (nonatomic,copy)shareBlock updatedPost;
-(void)updatePostModel:(shareBlock)blk;

@end
