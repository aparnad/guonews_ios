//
//  SharePopup.m
//  GUO
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "SharePopup.h"

@interface SharePopup ()
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@end

@implementation SharePopup

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils SetRoundedCorner:self.imgUser];
    self.lblHeader.textColor = [UIColor blackColor];
    
    self.view.backgroundColor = popup_bg_color;
    self.view.frame = CGRectMake(0, 0, Screen_Width - 50 , 200);

    self.lblDesc.frame = CGRectMake(self.imgUser.frame.origin.x,self.lblDesc.frame.origin.y, self.view.frame.size.width - (self.imgUser.frame.origin.x*2), 10);
    [self.lblDesc sizeToFit];
    
    if(self.lblDesc.frame.size.height>50)
    {
        self.lblDesc.frame = CGRectMake(self.imgUser.frame.origin.x,self.lblDesc.frame.origin.y, self.view.frame.size.width - (self.imgUser.frame.origin.x*2), 50);
    }
    self.footerView.frame = CGRectMake(0, CGRectGetMaxY(self.lblDesc.frame) + 5, self.view.frame.size.width, self.footerView.frame.size.height);
    
    self.view.frame = CGRectMake(0, 0, Screen_Width - 50 , CGRectGetMaxY(self.footerView.frame));

    
    self.footerView.backgroundColor = self.headerView.backgroundColor = medium_gray_bg_color;
    [Utils makeHalfRoundedButton:self.btnShare];

    [Utils SetTextViewProperties:self.txtDetail placeholder_txt:nil];
    self.view.layer.cornerRadius = textfield_corner_radius;
    self.view.layer.masksToBounds = YES;
    

    self.lblName.text = [Utils setFullname:self.model.userFirstname lname:self.model.userFirstname username:_model.userName];

    
    self.lblTime.text =  self.model.time;
    self.lblDesc.text =  self.model.textPlain;
    
    
    [self.imgUser setImageWithURL:[Utils getProperContentUrl:self.model.userPicture] placeholderImage:defaultUserImg];
    

    // Do any additional setup after loading the view from its nib.
    self.lblName.font = [UIFont fontWithName:Font_Medium size:userfull_name_font_size];
    self.lblTime.font = [UIFont fontWithName:Font_regular size:date_time_font_size];
    self.lblDesc.font = [UIFont fontWithName:Font_regular size:username_font_size];
    self.lblHeader.font = [UIFont fontWithName:Font_Medium size:header_font_size];

    self.lblHeader.text = NSLocalizedString(@"share_post", nil);
    [self.btnShare setTitle:NSLocalizedString(@"btn_share", nil) forState:UIControlStateNormal];
    
    self.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;

}


#pragma mark- Onclick Share
-(IBAction)onclick_share:(id)sender
{
    [self shareAPI];
}
-(void)shareAPI
{
    [SVProgressHUD show];
    //[UTILS ShowProgress];
    [self.txtDetail resignFirstResponder];
    
    NSDictionary *params = @{
                             @"get":API_SHARE_POST,
                             @"user_id":UTILS.currentUser.userId,
                             @"post_id":self.model.postId,
                             @"comment":[Utils RemoveWhiteSpaceFromText:self.txtDetail.text]                             };
    
    //Input: {
    //    "query":1234,
    //    "get":"share",
    //    "user_id":54555,
    //    "post_id":97484,
    //    “comment”:”test comments”

    
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_SHARE_POST params:params sBlock:^(id responseObject) {
        
//        int shareCount = [self.model.shares intValue];
//        shareCount +=1;
//        self.model.shares = @(shareCount).stringValue;
//
//
//        //        post_tbl setUpdatedPostModel:(mode)
        
        if (self.updatedPost) {
            self.updatedPost(responseObject);
        }
        [self.view dismissPresentingPopup];
        
        [Utils HideProgress];
        
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [self.view dismissPresentingPopup];
        
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        
        
    }];
    
}
-(void)updatePostModel:(shareBlock)blk
{
    self.updatedPost = blk;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
