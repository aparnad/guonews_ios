//
//  CommonDocumentController.h
//  GUO
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonDocumentController : UIViewController<UIWebViewDelegate>
{
    
}
@property(strong,nonatomic) IBOutlet UIView *headerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property(strong,nonatomic) IBOutlet UIWebView *aWebView;
@property(strong,nonatomic) NSString *headerStr,*webUrlStr;
@property(strong,nonatomic) IBOutlet UITextView *aPrivacyPolicy,*aTermsNConditions;
@property(strong,nonatomic) NSString *screen_name;
@property(strong,nonatomic) NSString *fileName;

-(IBAction)onclick_Menu:(id)sender;
@end
