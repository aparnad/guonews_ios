//
//  CommonDocumentController.m
//  GUO
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "CommonDocumentController.h"

@interface CommonDocumentController ()

@end

@implementation CommonDocumentController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.aWebView.delegate=self;
    self.aWebView.backgroundColor = view_BGColor;
    self.lblHeader.text = self.headerStr;
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];
//    [Utils LoadWebViewURL:self.aWebView URL_Str:self.webUrlStr];
    
    
    
    [self.view addGestureRecognizer:[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureRecognized:)]];
    
    
    // Do any additional setup after loading the view.
//    if([Utils NetworkRechability])
//    {
//        //        [SVProgressHUD showWithStatus:Please_Wait maskType:SVProgressHUDMaskTypeNone];
//        [UTILS ShowProgress];
//        [Utils LoadWebViewURL:self.aWebView URL_Str:self.webUrlStr];
//        
//    }
//    else
//    {
//        [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Alert", nil) message:CheckInternetConnection delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil] show];
//
//        
//    }
    
    
    NSString* htmlString;

    if([self.screen_name isEqualToString:privacy_screen])
    {
        self.aTermsNConditions.hidden = YES;
        self.aPrivacyPolicy.hidden = NO;
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:NSLocalizedString(@"privacy-policy-file_name", nil) ofType:@"html"];
        htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];

    }
    else
    {
        self.aTermsNConditions.hidden = NO;
        self.aPrivacyPolicy.hidden = YES;
        
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:NSLocalizedString(@"tnc-file_name", nil) ofType:@"html"];
        htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];

    }
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64)];
    
    [webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    
    [self.view addSubview:webView];
    
    self.aWebView.delegate=self;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [Utils HideProgress];
}
#pragma mark- Webview delegate method
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    webView.hidden=NO;
    [Utils HideProgress];
    
}
#pragma mark- Menu swipe gesture
- (void)swipeGestureRecognized:(UISwipeGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    if(self.frostedViewController.swipeGestureEnabled)
    {
        [self.frostedViewController presentMenuViewController];
    }
}


#pragma mark- Button click event
-(IBAction)onclick_back:(id)sender;
{
  //  [self.frostedViewController.view endEditing:YES];
    //[self.frostedViewController presentMenuViewController];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
