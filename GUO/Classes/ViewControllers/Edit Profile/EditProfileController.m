//
//  EditProfileController.m
//  GUO
//
//  Created by mac on 14/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "EditProfileController.h"
#import "UIImage+fixOrientation.h"
#import "UploadProgressView.h"
#import "CLImageEditor.h"
#import <RSKImageCropper/RSKImageCropper.h>


#define  cover_img @"cover"
#define  profile_img @"profile"

@interface EditProfileController ()<CLImageEditorDelegate,RSKImageCropViewControllerDelegate>
{
    UploadProgressView *progressView;
    CGFloat totalProgress;
}
@end

@implementation EditProfileController

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    arrUploadReq = [NSMutableArray new];
    arrAttachments = [NSMutableArray new];
    arrImgUploadUrls = [NSMutableArray new];
    arrVideoUploadUrls = [NSMutableArray new];
 
    int side_spc = 8;

    self.txtEmail = [[JVFloatLabeledTextField alloc]initWithFrame:CGRectMake(side_spc, side_spc, self.outerView.frame.size.width - (side_spc*2), kJVFieldHeight)];

    self.txtEmail.floatingLabelFont = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
    self.txtEmail.floatingLabelTextColor = [UIColor lightGrayColor];
    self.txtEmail.floatingLabelActiveTextColor = [UIColor lightGrayColor];
    self.txtEmail.keepBaseline = YES;
    [self.outerView addSubview:self.txtEmail];

    self.txtUsername = [[JVFloatLabeledTextField alloc]initWithFrame:CGRectMake(side_spc, CGRectGetMaxY(self.txtEmail.frame) + 10, self.outerView.frame.size.width - (side_spc*2), kJVFieldHeight)];

    self.txtUsername.floatingLabelFont = [UIFont systemFontOfSize:kJVFieldFloatingLabelFontSize];
    self.txtUsername.floatingLabelTextColor = [UIColor lightGrayColor];
    self.txtUsername.floatingLabelActiveTextColor = [UIColor lightGrayColor];
    self.txtUsername.keepBaseline = YES;
    [self.outerView addSubview:self.txtUsername];
    
    int size_c = 80;
    _circleProgressBar = [[CircleProgressBar alloc]initWithFrame:CGRectMake(Screen_Width/2 - size_c/2, Screen_Height/2 -size_c/2, size_c, size_c)];
    [Utils SetRoundedCorner:_circleProgressBar];
    _circleProgressBar.layer.borderColor =[UIColor grayColor].CGColor;
    _circleProgressBar.layer.borderWidth = 2;
    _circleProgressBar.layer.zPosition = -1;
    _circleProgressBar.layer.masksToBounds = YES;
    _circleProgressBar.backgroundColor = [UIColor clearColor];
    _circleProgressBar.tintColor = header_color;
    _circleProgressBar.hidden = YES;
    [self.view addSubview:_circleProgressBar];

    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    [Utils DrawLine:CGRectMake(self.txtfullName.frame.origin.x, CGRectGetMaxY(self.txtfullName.frame) - separator_height, self.txtfullName.frame.size.width,separator_height) view:self.outerView color:header_color];

    [Utils DrawLine:CGRectMake(self.txtEmail.frame.origin.x, CGRectGetMaxY(self.txtEmail.frame) - separator_height, self.txtEmail.frame.size.width, separator_height)  view:self.outerView color:header_color];
    [Utils DrawLine:CGRectMake(self.txtUsername.frame.origin.x, CGRectGetMaxY(self.txtUsername.frame) - separator_height, self.txtUsername.frame.size.width, separator_height)  view:self.outerView color:header_color];

    
    self.txtfullName.tintColor = self.txtfullName.textColor = [UIColor blackColor];

    self.txtEmail.tintColor = self.txtEmail.textColor = [UIColor blackColor];
    self.txtUsername.tintColor = self.txtUsername.textColor = [UIColor blackColor];
    
    [self.txtfullName setFont:[UIFont fontWithName:Font_regular size:txtfield_font_size]];

    [self.txtEmail setFont:[UIFont fontWithName:Font_regular size:txtfield_font_size]];
    [self.txtUsername setFont:[UIFont fontWithName:Font_regular size:txtfield_font_size]];

    self.txtfullName.placeholder = NSLocalizedString(@"txt_name_title", nil);

    self.txtEmail.placeholder = NSLocalizedString(@"txt_email_title", nil);
    self.txtUsername.placeholder = NSLocalizedString(@"txt_username", nil);
    
    self.txtfullName.text = [Utils setFullname:UTILS.currentUser.userFirstname lname:UTILS.currentUser.userLastname username:UTILS.currentUser.userName];

    
    self.txtUsername.text = UTILS.currentUser.userName;
    self.txtEmail.text = UTILS.currentUser.userEmail;

    if([Utils getProfilePic:p_name])
    {
        self.imgProfile.image =[Utils getProfilePic:p_name];
    }
    else
    {
        [self.imgProfile sd_setImageWithURL:[Utils getProperContentUrl:UTILS.currentUser.userPicture] placeholderImage:defaultUserImg completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [Utils setProfilePic:image pic_name:p_name];
            self.imgProfile.image =image;
        }];
    }
    
    if([Utils getProfilePic:w_name])
    {
        self.imgCover.image =[Utils getProfilePic:w_name];
        
    }
    else
    {
        [self.imgCover sd_setImageWithURL:[Utils getProperContentUrl:UTILS.currentUser.userCover] placeholderImage:defaultPostImg completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [Utils setProfilePic:image pic_name:w_name];
            self.imgCover.image =image;
            
        }];
    }
    


    
    [self.btnSave setTitle:NSLocalizedString(@"btn_save", nil) forState:UIControlStateNormal];

    self.view.backgroundColor = light_gray_bg_color;

    [Utils makeHalfRoundedButton:self.btnSave];
    
    [self.btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnSave setBackgroundColor:header_color];

    [Utils SetRoundedCorner:self.imgProfile];
    self.imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgProfile.layer.borderWidth = 2;


    self.txtUsername.delegate = self;
    self.aScrollView.contentSize = CGSizeMake(Screen_Width, CGRectGetMaxY(self.btnSave.frame) + 15);
    // Do any additional setup after loading the view.
}
#define NONACCEPTABLE_PASSWORD_CHARACTERS @" "

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {

    if (textField==_txtUsername) {
        NSCharacterSet *cs = [NSCharacterSet characterSetWithCharactersInString:NONACCEPTABLE_PASSWORD_CHARACTERS];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{

    [self.view endEditing:YES];
    //Do stuff here...
}
-(IBAction)onclick_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)onclick_coverImg:(id)sender
{
    img_identification = cover_img;
    [self ShowChnageImagePopup];
}
-(IBAction)onclick_profileImg:(id)sender
{
    img_identification = profile_img;
    [self ShowChnageImagePopup];
}
-(void)ShowChnageImagePopup
{
    UIActionSheet *action=[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"message_dialog_cancel", nil).capitalizedString destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Choose From Gallery", nil),NSLocalizedString(@"Take Photo", nil), nil];
    
    [action showInView:self.view];

}
-(IBAction)onclick_save:(id)sender
{
    if(![self validation])
    {
        
        if (![UTILS checkIntenetShowError]) {
            return;
        }

       
            NSDictionary *params = @{
                                     @"get":API_EDIT_PROFILE_FIELD,
                                     @"user_id":UTILS.currentUser.userId,
//                                     @"firstname":[Utils RemoveWhiteSpaceFromText:self.txtfullName.text],
                                     @"about_me":@"moreinfo"
                                     };

            NSMutableDictionary *updatedprams = [[NSMutableDictionary alloc]initWithDictionary:params];
            
            if(![UTILS.currentUser.userEmail isEqualToString:[Utils RemoveWhiteSpaceFromText:self.txtEmail.text]])
            {
                [updatedprams setObject:[Utils RemoveWhiteSpaceFromText:self.txtEmail.text] forKey:@"email"];
            }
            
            if(![UTILS.currentUser.userName isEqualToString:self.txtUsername.text])
            {
                [updatedprams setObject:[Utils RemoveWhiteSpaceFromText:self.txtUsername.text] forKey:@"username"];
            }
            
            
            [UTILS ShowProgress];
            [REMOTE_API CallPOSTWebServiceWithParam:API_EDIT_PROFILE_FIELD params:updatedprams sBlock:^(id responseObject) {
                UserModel *loginModel = responseObject;
                Utils.getSharedInstance.currentUser = loginModel;
                [GUOSettings setIsLogin:loginModel];

                
                [Utils HideProgress];
                if (self.updateBlk) {
                    self.updateBlk([Utils RemoveWhiteSpaceFromText:self.txtUsername.text]);
                }
                [CustomAlertView showAlert:@"" withMessage:NSLocalizedString(@"Profile successfully changed", nil)];

                
            } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
                [Utils HideProgress];
                [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
            }];
        
        

    }
}

#pragma mark- validation function
-(BOOL)validation
{
    NSString *str=@"";
    
    if([Utils RemoveWhiteSpaceFromText:self.txtEmail.text].length==0)
    {
        str=[NSString stringWithFormat:@"%@",NSLocalizedString(@"Please Enter Email", nil)];
    }
    else  if(![Utils EmailVerification:[Utils RemoveWhiteSpaceFromText:self.txtEmail.text]])
    {
        str=[NSString stringWithFormat:@"%@\n%@",str,NSLocalizedString(@"Please Enter Valid Email", nil)];
    }
    if([Utils RemoveWhiteSpaceFromText:self.txtUsername.text].length==0)
    {
        str=[NSString stringWithFormat:@"%@\n%@",str,NSLocalizedString(@"Please Enter Username", nil)];
    }
   
    
    if([str isEqualToString:@""])
    {
        return false;
    }
    else
    {
        
        if([str hasPrefix:@"\n"])
        {
            str=[str substringFromIndex:1];
        }
        [Utils ShowAlert:str];
        
        return true;
    }
}
// Profile pic change
#pragma mark - Image Picker
//or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
   
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
    
    if([img_identification isEqualToString:cover_img])
    {
        CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:image];
        editor.delegate = self;
        editor.onlyCrop = YES;
        
        
        [self presentViewController:editor animated:YES completion:nil];
        
    }
    else{
        
             //circle cropping
        RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image];
        imageCropVC.delegate = self;
       // [[UTILS topMostControllerNormal].navigationController pushViewController:imageCropVC animated:YES];
        
        [self presentViewController:imageCropVC animated:YES completion:nil];

    }

    
    // [self processImageAfterSelection:image];
    
}

-(void)upload:(AWSS3TransferManagerUploadRequest *)uploadRequest{

    [UTILS ShowProgress];
    
    
    /*
    totalProgress = 0;
   uploadRequest.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (arrUploadReq.count) {
                if (totalBytesExpectedToSend > 0) {
                    CGFloat progress = (float)((double) totalBytesSent / totalBytesExpectedToSend) * 100;
                    NSLog(@"%f",progress);
                    
                    progress = progress/100.0;
                    
                    if (progress>=1) {
                        progress = 1;
                    }
                    
                    [progressView setProgress:progress];
                }
            }
        });
    };
    
   // [self showProgressView];

    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
//    [self targetMethod];
    [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            [Utils HideProgress];
            [progressView removeFromSuperview];

            if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                switch (task.error.code) {
                    case AWSS3TransferManagerErrorCancelled:
                    case AWSS3TransferManagerErrorPaused:
                    {
                        
                    }
                    break;
                        
                    default:
                        NSLog(@"Upload failed: [%@]", task.error);
                        [CustomAlertView showAlert:@"" withMessage:task.error.localizedDescription];
                        break;
                }
            } else {
                [Utils HideProgress];
                    [progressView removeFromSuperview];
                NSLog(@"Upload failed: [%@]", task.error);
            }
        }
        
        if (task.result) {
            dispatch_async(dispatch_get_main_queue(), ^{

                
                NSString *url = [uploadRequest.bucket stringByReplacingOccurrencesOfString:S3_bUCKET_NAME withString:@""];
                url = [[url stringByAppendingString:@"/"] stringByAppendingString:uploadRequest.key];
               
                arrImgUploadUrls = [[NSMutableArray alloc]init];
                [arrImgUploadUrls addObject:url];
                
                [progressView removeFromSuperview];
            
                if([img_identification isEqualToString:cover_img])
                {
                    [self updateProfilePicAPI:API_EDIT_COVER_PIC_TYPE];
                }
                else
                {
                    [self updateProfilePicAPI:API_EDIT_PROFILE_PIC_TYPE];
                }
            });
        }
        
        return nil;
    }];
     
     */
    
    
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility S3TransferUtilityForKey:APP_AWS_ACCELERATION_KEY];
    
    
    AWSS3TransferUtilityUploadExpression *expression = [AWSS3TransferUtilityUploadExpression new];
    AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
        
        NSLog(@"ERROOROROOOOROR 2222222 => %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *url = [uploadRequest.bucket stringByReplacingOccurrencesOfString:S3_bUCKET_NAME withString:@""];
              url = [url stringByReplacingOccurrencesOfString:@"uploads/" withString:@""];
            url = [[url stringByAppendingString:@"/"] stringByAppendingString:uploadRequest.key];
            
            arrImgUploadUrls = [[NSMutableArray alloc]init];
            [arrImgUploadUrls addObject:url];
            
            [progressView removeFromSuperview];
            
            if([img_identification isEqualToString:cover_img])
            {
                [self updateProfilePicAPI:API_EDIT_COVER_PIC_TYPE];
            }
            else
            {
                [self updateProfilePicAPI:API_EDIT_PROFILE_PIC_TYPE];
            }
        });
    };
    
    NSString *keyName = [[[uploadRequest.bucket stringByReplacingOccurrencesOfString:S3_bUCKET_NAME withString:@""] stringByAppendingString:@"/"] stringByAppendingString:uploadRequest.key];
    
    [[transferUtility uploadFile:uploadRequest.body bucket:S3_bUCKET_NAME key:keyName contentType:@"image/jpeg" expression:expression completionHandler:completionHandler] continueWithSuccessBlock:^id _Nullable(AWSTask<AWSS3TransferUtilityUploadTask *> * _Nonnull task) {
        
        NSLog(@"ERROOROROOOOROR => %@",task.error);
        
        return nil;
    }];
    
    
    expression.progressBlock = ^(AWSS3TransferUtilityTask *task, NSProgress *progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something e.g. Update a progress bar.
            
//            if (![arrUploadTask containsObject:task]) {
//                [arrUploadTask addObject:task];
//            }
//
//            //  NSLog(@"PROGRESS => %lf",progress.fractionCompleted);
//            //  NSLog(@"TOTAL PROGRESS => %lf",totalProgress);
//            totalProgress = 0;
//            for (AWSS3TransferUtilityTask *taskObj in arrUploadTask) {
//                totalProgress += taskObj.progress.fractionCompleted;
//            }
//
//            totalProgress = totalProgress/arrAllUploadReq.count;
//
//            [progressView setProgress:totalProgress];
//
        });
    };
}

-(void)processImageAfterSelection:(UIImage*)image
{
    NSArray *yearMonth = [Utils getCurrentYearandMonth];
   // UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    image = [image fixOrientation];
    NSString *imgName = [Utils getImageName:@"jpg"];
    NSString *filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:yearMonth[0]] stringByAppendingPathComponent:yearMonth[1]];
    NSError *error = nil;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"reating 'upload' directory failed: [%@]", error);
    }
    
    filePath = [filePath stringByAppendingPathComponent:imgName];
    NSData *pngData = UIImageJPEGRepresentation(image, IMAGE_COMPRESSION);
    [pngData writeToFile:filePath atomically:YES];
    
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.body = [NSURL fileURLWithPath:filePath];
    uploadRequest.key = imgName;
    NSString *bucketPath = [NSString stringWithFormat:@"%@%@/%@/%@",S3_bUCKET_NAME,@"uploads/photos",yearMonth[0],yearMonth[1]];
    uploadRequest.bucket = bucketPath;
    uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    
    if([img_identification isEqualToString:cover_img])
    {
        self.imgCover.image = image;
    }
    else
    {
        self.imgProfile.image = image;
    }
    NSString *fileName = [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingString:@".png"];
    NSData * imageData = UIImagePNGRepresentation(image);
    
    [imageData writeToFile:filePath atomically:YES];
    
    NSString *S3BucketName = [NSString stringWithFormat:@"%@%@/%@/%@",S3_bUCKET_NAME,@"uploads/photos",yearMonth[0],yearMonth[1]];
    uploadRequest.bucket = S3BucketName;
    
    [arrUploadReq addObject:uploadRequest];
    
    //Generate Request for Thumbnails
    UIImage *thumbImg= [Utils generatePhotoThumbnail:image];
    NSString *thumbFilePath = [self saveThumbnailImage:thumbImg imageName:imgName];
    
    AWSS3TransferManagerUploadRequest *thumbUploadRequest = [AWSS3TransferManagerUploadRequest new];
    thumbUploadRequest.body = [NSURL fileURLWithPath:thumbFilePath];
    thumbUploadRequest.key = imgName;
    NSString *thumbBucketPath = [NSString stringWithFormat:@"%@%@/%@",S3_bUCKET_NAME,@"uploads/photos",@"thumb"];
    thumbUploadRequest.bucket = thumbBucketPath;
    thumbUploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    
    [self upload:uploadRequest];
    [self uploadThumbnailReq:thumbUploadRequest];
}

-(NSString *)saveThumbnailImage:(UIImage *)image imageName:(NSString *)imgName
{
    NSArray *yearMonth = [Utils getCurrentYearandMonth];
    NSString *filePath = [[[NSTemporaryDirectory() stringByAppendingPathComponent:yearMonth[0]] stringByAppendingPathComponent:yearMonth[1]] stringByAppendingPathComponent:@"Thumb"];
    
    NSError *error = nil;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"reating 'upload' directory failed: [%@]", error);
    }
    filePath = [filePath stringByAppendingPathComponent:imgName];
    
    NSData *pngData = UIImageJPEGRepresentation(image, 1);
    
    [pngData writeToFile:filePath atomically:YES];
    
    return filePath;
}

-(void)uploadThumbnailReq:(AWSS3TransferManagerUploadRequest *)thumbReq
{
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    [[transferManager upload:thumbReq] continueWithBlock:^id(AWSTask *task){
     
        if (task.result) {
            
        }
        return nil;
    }];
}

-(void)targetMethod
{
    [timer invalidate];


//    _circleProgressBar.hidden=NO;
    AWSS3TransferManagerUploadRequest *uploadRequest = [arrUploadReq objectAtIndex:0];
    uploadRequest.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (totalBytesExpectedToSend > 0) {
                CGFloat progress = (float)((double) totalBytesSent / totalBytesExpectedToSend) * 100;
                NSLog(@"//// Origional Progress :%@",[NSString stringWithFormat:@"%.2f%%",progress]);

//                NSLog(@"****totalBytesSent combine:%f",_circleProgressBar.progress);


                
                if (progress >= 100) {
                    [timer invalidate];
                    [Utils HideProgress];
                    _circleProgressBar.hidden=YES;


                    
                }
            }
        });
    };

}




-(void)updateProfilePicAPI:(NSString*)type_name
{

    if(arrImgUploadUrls.count>0)
    {
        NSMutableDictionary *param = [NSMutableDictionary new];
        [param setValue:API_EDIT_PROFILE_PIC forKey:@"get"];
        [param setValue:UTILS.currentUser.userId forKey:@"user_id"];
        [param setValue:arrImgUploadUrls[0] forKey:@"picture"];
        [param setValue:type_name forKey:@"type"];

        [UTILS ShowProgress];
        [REMOTE_API CallPOSTWebServiceWithParam:API_EDIT_PROFILE_PIC params:param sBlock:^(id responseObject) {
            [Utils HideProgress];
            UserModel *loginModel = responseObject;
            NSLog(@"%@",UTILS.currentUser.userId);
            Utils.getSharedInstance.currentUser = loginModel;
            [GUOSettings setIsLogin:loginModel];

            if([img_identification isEqualToString:profile_img])
            {
                [Utils setProfilePic:self.imgProfile.image pic_name:p_name];
                
                [CustomAlertView showAlert:@"" withMessage:NSLocalizedString(@"Profile pic updated successfully", nil)];


            }
            else
            {
                [Utils setProfilePic:self.imgCover.image pic_name:w_name];
                
                [CustomAlertView showAlert:@"" withMessage:NSLocalizedString(@"Cover pic updated successfully", nil)];

            }

            if (self.updateBlk) {
                self.updateBlk(_txtUsername.text);
            }
            
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
        }];
    }
    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex ==0)
    {
        //open gallery
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.delegate = self;
       // imagePickerController.allowsEditing = YES;
        [imagePickerController setEditing:YES animated:YES];
        //        [self presentViewController:imagePickerController animated:YES completion:nil];
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self presentViewController:imagePickerController animated:NO completion:nil];
            }];
            
        }
        else{
            
            [self presentViewController:imagePickerController animated:NO completion:nil];
        }
        
    }
    else if(buttonIndex ==1)
    {
        //open camera
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [Utils ShowAlert:@"Device has no camera."];
        }
        else
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    [self presentViewController:picker animated:NO completion:nil];
                }];
            }
            else{
                [self presentViewController:picker animated:NO completion:nil];
            }
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


-(void)showProgressView
{
    progressView = [[UploadProgressView alloc]initWithFrame:self.view.frame];
    [self.navigationController.view addSubview:progressView];
    
    [progressView enableAutolayout];
    [progressView leadingMargin:0];
    [progressView trailingMargin:0];
    [progressView topMargin:0];
    [progressView bottomMargin:0];
    
    [progressView onCloseClicked:^{
        [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"Do you want to stop uploading process?", nil) okHandler:^{
            [progressView removeFromSuperview];
            [self cancelAllUploads];
        }];
    }];
    
}

-(void)onUpdateProfileSuccess:(stringBlock)blk
{
    self.updateBlk = blk;
}

-(void)cancelAllUploads
{
    for (AWSS3TransferManagerUploadRequest *uploadRequest in arrUploadReq) {
        [[uploadRequest cancel] continueWithBlock:^id(AWSTask *task) {
            if (task.error) {
                NSLog(@"The cancel request failed: [%@]", task.error);
            }

            return nil;
        }];
    }

    [arrUploadReq removeAllObjects];

    [[Toast sharedInstance]makeToast:NSLocalizedString(@"Uploading cancelled.", nil) position:TOAST_POSITION_BOTTOM];
}

#pragma mark - Image Editor Delegates
-(void)imageEditor:(CLImageEditor *)editor didFinishEditingWithImage:(UIImage *)image
{
    [self dismissViewControllerAnimated:editor completion:nil];
    
    [self processImageAfterSelection:image];
    //imgView.image = image;
}

#pragma mark - Circular crop delegate
// Crop image has been canceled.
- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller
{
   // [self.navigationController popViewControllerAnimated:YES];
     [self dismissViewControllerAnimated:controller completion:nil];
}

// The original image has been cropped. Additionally provides a rotation angle used to produce image.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect
                  rotationAngle:(CGFloat)rotationAngle
{
   // self.imageView.image = croppedImage;
    
     [self processImageAfterSelection:croppedImage];
  //  [self.navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:controller completion:nil];

}

// The original image will be cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                  willCropImage:(UIImage *)originalImage
{
    // Use when `applyMaskToCroppedImage` set to YES.
    //[SVProgressHUD show];
}


@end
