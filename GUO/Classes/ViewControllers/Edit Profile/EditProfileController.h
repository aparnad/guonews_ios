//
//  EditProfileController.h
//  GUO
//
//  Created by mac on 14/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LPlaceholderTextView.h"
#import "JVFloatLabeledTextField.h"
#import <Applozic/QBImagePickerController.h>
#import <AWSS3/AWSS3.h>
//#import "AWSS3.h"
#import "CircleProgressBar.h"
#import "MBProgressHUD.h"

@interface EditProfileController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSString *img_identification;
    NSMutableArray *arrAttachments;
    NSMutableArray *arrUploadReq;
    NSMutableArray *arrImgUploadUrls;
    NSMutableArray *arrVideoUploadUrls;
    NSTimer *timer;
    
}
@property(strong,nonatomic) IBOutlet UIImageView *imgCover,*imgProfile;
@property(strong,nonatomic) IBOutlet TPKeyboardAvoidingScrollView *aScrollView;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *txtfullName,*txtEmail,*txtUsername;
@property(strong,nonatomic) IBOutlet UIButton *btnSave;
@property(strong,nonatomic) IBOutlet UIButton *btnCoverImg;
@property(strong,nonatomic) IBOutlet UIView *outerView;
@property (strong,nonatomic) stringBlock updateBlk;
-(IBAction)onclick_coverImg:(id)sender;
-(IBAction)onclick_profileImg:(id)sender;
-(IBAction)onclick_save:(id)sender;
-(IBAction)onclick_back:(id)sender;
@property (strong, nonatomic) IBOutlet CircleProgressBar *circleProgressBar;

-(void)onUpdateProfileSuccess:(stringBlock)blk;
@end
