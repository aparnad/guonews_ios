//
//  GlobalSearchController.m
//  GUO
//
//  Created by Pawan Ramteke on 09/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "GlobalSearchController.h"
#import "HMSegmentedControl.h"
#import "GlobalSearchCollectionCell.h"
#import "GlobalPostCollectionCell.h"
#import <Applozic/Applozic.h>
#import "ALChatManager.h"


@interface GlobalSearchController ()<UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    __weak IBOutlet UITextField *txtFieldSearch;
    __weak IBOutlet UIView *viewBasePager;
    NSArray *arrSegments;
    __weak IBOutlet UICollectionView *_collectionView;
    HMSegmentedControl *segmentedControl;
    BOOL keyBoardDismissed;
    NSMutableDictionary *mainDict;
    int postCurrentPage;
    int peopleCurrentPage;
    int mediaCurrentPage;
    __weak IBOutlet UILabel *lblSearchHint;
    
    NSTimer *searchTimer;
}
@end

@implementation GlobalSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UTILS addFloating:self.view tab_height:50 screen_name:nil];

   // txtFieldSearch.enablesReturnKeyAutomatically = YES;
    (AppObj).golbalSearchScreenOpen =NOTIFICATION_GLOBAL_SEARCH;
    
    
    txtFieldSearch.font = [UIFont fontWithName:Font_regular size:15];
    selected_type = global_search_type_post;
    UTILS.searchType = selected_type;

    mainDict = [NSMutableDictionary new];
    
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];
    self.view.backgroundColor = header_color;
    [self setupSearchTextField];
    [self setupSegmentedControl];
    
    _collectionView.backgroundColor = [UIColor controllerBGColor];
    [_collectionView registerClass:[GlobalSearchCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    [_collectionView registerClass:[GlobalPostCollectionCell class] forCellWithReuseIdentifier:@"PostCell"];
    
    lblSearchHint.font = [UIFont fontWithName:Font_regular size:16];
    lblSearchHint.textColor = [UIColor blackColor];
    lblSearchHint.text = NSLocalizedString(@"Try searching for posts, people, or keywords", nil);

}
-(void)viewWillAppear:(BOOL)animated
{
    UTILS.isFromGlobalSearch = YES;
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMQTTNotification:)
                                                 name:@"MQTT_APPLOZIC_01"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAPNS:)
                                                 name:@"pushNotification"
                                               object:nil];

}

-(void)showMQTTNotification:(NSNotification *)notifyObject
{
    ALMessage * alMessage = (ALMessage *)notifyObject.object;
    
    BOOL flag = (alMessage.groupId && [ALChannelService isChannelMuted:alMessage.groupId]);
    
    if (![alMessage.type isEqualToString:@"5"] && !flag && ![alMessage isMsgHidden])
    {
        ALNotificationView * alNotification = [[ALNotificationView alloc] initWithAlMessage:alMessage
                                                                           withAlertMessage:alMessage.message];
        
        [alNotification nativeNotification:self];
    }
}

-(void)handleAPNS:(NSNotification *)notification
{
    NSString * contactId = notification.object;
     NSLog(@"USER_PROFILE_VC_NOTIFICATION_OBJECT : %@",contactId);
     NSDictionary *dict = notification.userInfo;
     NSNumber * updateUI = [dict valueForKey:@"updateUI"];
     NSString * alertValue = [dict valueForKey:@"alertValue"];
     
     ALPushAssist *pushAssist = [ALPushAssist new];
     
     NSArray * myArray = [contactId componentsSeparatedByString:@":"];
     NSNumber * channelKey = nil;
     if(myArray.count > 2)
     {
     channelKey = @([myArray[1] intValue]);
     }
     
     if([updateUI isEqualToNumber:[NSNumber numberWithInt:APP_STATE_ACTIVE]] && pushAssist.isUserProfileVCOnTop)
     {
     NSLog(@"######## USER PROFILE VC : APP_STATE_ACTIVE #########");
     
     ALMessage *alMessage = [[ALMessage alloc] init];
     alMessage.message = alertValue;
     NSArray *myArray = [alMessage.message componentsSeparatedByString:@":"];
     
     if(myArray.count > 1)
     {
     alertValue = [NSString stringWithFormat:@"%@", myArray[1]];
     }
     else
     {
     alertValue = myArray[0];
     }
     
     alMessage.message = alertValue;
     alMessage.contactIds = contactId;
     alMessage.groupId = channelKey;
     
     if ((channelKey && [ALChannelService isChannelMuted:alMessage.groupId]) || [alMessage isMsgHidden])
     {
     return;
     }
     
     ALNotificationView * alNotification = [[ALNotificationView alloc] initWithAlMessage:alMessage
     withAlertMessage:alMessage.message];
     [alNotification nativeNotification:self];
     }
     else if([updateUI isEqualToNumber:[NSNumber numberWithInt:APP_STATE_INACTIVE]])
     {
     NSLog(@"######## USER PROFILE VC : APP_STATE_INACTIVE #########");
     
     [self.tabBarController setSelectedIndex:0];
     UINavigationController *navVC = (UINavigationController *)self.tabBarController.selectedViewController;
    // ALMessagesViewController *msgVC = (ALMessagesViewController *)[[navVC viewControllers] objectAtIndex:0];
//     if(channelKey)
//     {
//     msgVC.channelKey = channelKey;
//     }
//     else
//     {
//     msgVC.channelKey = nil;
//     }
//     [msgVC createDetailChatViewController:contactId];
     }

    ALChatManager* chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLICATION_ID];

    [chatManager launchChatForUserWithDisplayName:contactId withGroupId:channelKey andwithDisplayName:nil andFromViewController:self];

}
-(void)viewWillDisappear:(BOOL)animated{
     UTILS.isFromGlobalSearch = NO;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrSegments.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1 || indexPath.row == 2) {
        GlobalPostCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PostCell" forIndexPath:indexPath];
        cell.indexPath = indexPath;
        
        cell.arrData = indexPath.row == 0 ? [mainDict objectForKey:global_search_type_post] : [mainDict objectForKey:global_search_type_media];
        
        [cell onRefreshList:^(NSIndexPath *idxPath) {
            if (txtFieldSearch.text.length > 0) {
                if (idxPath.row == 0) {
                    [mainDict setObject:@[] forKey:global_search_type_post];
                    [self getGlobalSearchPosts];
                }
                else if (idxPath.row == 2) {
                    [mainDict setObject:@[] forKey:global_search_type_media];
                    [self getGlobalSearchMedias];
                }
            }
        }];
        
        [cell onLoadMore:^{
            if (indexPath.row == 0) {
                [self getGlobalSearchPosts];
            }
            else{
                [self getGlobalSearchMedias];
            }
        }];
        
        [cell didSelectRow:^(PostModel *selModel, NSIndexPath *indexPath) {
            
        }];
        
        return cell;
    }
    
    GlobalSearchCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.indexPath = indexPath;
    cell.arrData = [mainDict objectForKey:global_search_type_people];
    
    [cell onFollowClicked:^(NSIndexPath *indexPath) {
        [self follow_unfollow_API:indexPath];
    }];
    
    [cell onRefreshList:^(NSIndexPath *indexPath) {
        if (txtFieldSearch.text.length > 0) {
            [mainDict setObject:@[] forKey:global_search_type_people];
            [self getGlobalSearchPeoples];
        }else{
            [cell endRefreshing];
        }
    }];
    
    [cell onLoadMore:^{
        [self getGlobalSearchPeoples];
    }];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    [segmentedControl setSelectedSegmentIndex:page animated:YES];
}

-(void)setupSearchTextField
{
    UIImageView *envelopeView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 0, 25, 25)];
    envelopeView.image = [UIImage imageNamed:@"search_selected"] ;
    //    envelopeView.image = [Utils setTintColorToUIImage:envelopeView.image color:tbl_separator_color];
    envelopeView.alpha = 1;
    envelopeView.clipsToBounds = YES;
    envelopeView.contentMode = UIViewContentModeScaleAspectFit;
    UIView *test=  [[UIView alloc]initWithFrame:CGRectMake(8, 0, 40, 25)];
    [test addSubview:envelopeView];
    [ txtFieldSearch.leftView setFrame:envelopeView.frame];
     txtFieldSearch.leftView =test;
     txtFieldSearch.leftViewMode = UITextFieldViewModeAlways;
     txtFieldSearch.tintColor = [UIColor whiteColor];
     txtFieldSearch.textColor = [UIColor whiteColor];


    [ txtFieldSearch setValue:[UIColor whiteColor]
             forKeyPath:@"_placeholderLabel.textColor"];
     txtFieldSearch.layer.cornerRadius =  txtFieldSearch.frame.size.height/2;
     txtFieldSearch.layer.masksToBounds = YES;
     txtFieldSearch.backgroundColor = search_for_everything_color;
    [ txtFieldSearch setFont:[UIFont fontWithName:Font_regular size:search_textfield_font_size]];
    
    [txtFieldSearch setPlaceholder:NSLocalizedString(@"search_for_global", nil)];
    
}


-(void)setupSegmentedControl
{
    arrSegments = @[NSLocalizedString(@"bottom_tab_people", nil),NSLocalizedString(@"bottom_tab_posts", nil),NSLocalizedString(@"bottom_tab_media", nil)];


    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:arrSegments];
    segmentedControl.frame = CGRectMake(0, 0, Screen_Width, 50);
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.selectionIndicatorColor = [UIColor whiteColor];
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:18]};
    segmentedControl.backgroundColor = header_color;
    segmentedControl.selectionIndicatorHeight = 3;
    [viewBasePager addSubview:segmentedControl];
}

-(void)segmentedControlChangedValue:(HMSegmentedControl *)segmentControl
{
    if(segmentControl.selectedSegmentIndex==1)
    {
        selected_type = global_search_type_post;
        UTILS.searchType = selected_type;
    }
    else if(segmentControl.selectedSegmentIndex==0)
    {
        selected_type = global_search_type_people;
    }
    else
    {
        selected_type = global_search_type_media;
    }
    
    UTILS.searchType = selected_type;
    [_collectionView setContentOffset: CGPointMake(segmentControl.selectedSegmentIndex * Screen_Width, 0) animated:YES];
}

#pragma mark- Button click event
-(IBAction)onclick_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    keyBoardDismissed = NO;
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if (searchTimer != nil) {
        [searchTimer invalidate];
        searchTimer = nil;
    }

    searchTimer = [NSTimer scheduledTimerWithTimeInterval: 0.5 target: self selector: @selector(searchForKeyword:) userInfo: searchStr repeats: NO];
    
    return YES;
}

- (void) searchForKeyword:(NSTimer *)timer
{
    // retrieve the keyword from user info
    NSString *keyword = (NSString*)timer.userInfo;
    
    // perform your search (stubbed here using NSLog)
    NSLog(@"Searching for keyword %@", keyword);
    
    UTILS.searchText = keyword;
    lblSearchHint.hidden = YES;
    
    [mainDict setObject:@[] forKey:global_search_type_post];
    [mainDict setObject:@[] forKey:global_search_type_people];
    [mainDict setObject:@[] forKey:global_search_type_media];
    
    [self getGlobalSearchPosts];
    [self getGlobalSearchPeoples];
    [self getGlobalSearchMedias];
}


-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    //lblSearchHint.hidden = NO;
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    if ([textField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length > 0) {
//
//        UTILS.searchText = textField.text;
//        if (!keyBoardDismissed) {
//
//            lblSearchHint.hidden = YES;
//
//            [mainDict setObject:@[] forKey:global_search_type_post];
//            [mainDict setObject:@[] forKey:global_search_type_people];
//            [mainDict setObject:@[] forKey:global_search_type_media];
//
//            [self getGlobalSearchPosts];
//            [self getGlobalSearchPeoples];
//            [self getGlobalSearchMedias];
//
//
//        }
//
//    }
    keyBoardDismissed = YES;
    [textField endEditing:YES];
    return YES;
}

-(void)getGlobalSearchPosts
{
    GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [cell apiCallInitiated];

    NSArray *arr = [mainDict objectForKey:global_search_type_post];
    if (arr.count == 0) {
        postCurrentPage = 1;
    }

    NSDictionary *dic = @{@"get":API_SEARCH,
                          @"type":global_search_type_post,
                          @"keyword":txtFieldSearch.text,
                          @"limit":RecordLimit,
                          @"page":@(postCurrentPage).stringValue,
                          @"user_id":UTILS.currentUser.userId};
    [REMOTE_API CallPOSTWebServiceWithParam:API_SEARCH params:dic sBlock:^(id responseObject) {
        [Utils HideProgress];
       
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[mainDict objectForKey:global_search_type_post]];
        [arr addObjectsFromArray:responseObject];
        [mainDict setObject:arr forKey:global_search_type_post];
        
        GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*) [_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell apiCallEndWithResultCount:[arr count] pageNo:mediaCurrentPage];

        if([responseObject count] > 0)
        {
            postCurrentPage += 1;
        }
        
        [_collectionView reloadData];
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        
        GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*) [_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell apiCallEndWithResultCount:0 pageNo:1];
    }];
}

-(void)getGlobalSearchPeoples
{
    
    GlobalSearchCollectionCell *cell = (GlobalSearchCollectionCell*)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    [cell apiCallInitiated];
   
    
    NSArray *arr = [mainDict objectForKey:global_search_type_people];
    if (arr.count == 0) {
        peopleCurrentPage = 1;
    }
    
    NSDictionary *dic = @{@"get":API_SEARCH,
                          @"type":global_search_type_people,
                          @"keyword":txtFieldSearch.text,
                          @"limit":RecordLimit,
                          @"page":@(peopleCurrentPage).stringValue,
                          @"user_id":UTILS.currentUser.userId};
    [REMOTE_API CallPOSTWebServiceWithParam:API_GLOBAL_SEARCH_PEOPLE params:dic sBlock:^(id responseObject) {
        [Utils HideProgress];
        
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[mainDict objectForKey:global_search_type_people]];
        [arr addObjectsFromArray:responseObject];
        [mainDict setObject:arr forKey:global_search_type_people];
        
      //  GlobalSearchCollectionCell *cell = (GlobalSearchCollectionCell *)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
       // [cell endRefreshing];
 
        [cell apiCallEndWithResultCount:[arr count] pageNo:peopleCurrentPage];
        
        if([responseObject count] > 0)
        {
            peopleCurrentPage += 1;
        }
        
        [_collectionView reloadData];

    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        GlobalSearchCollectionCell *cell = (GlobalSearchCollectionCell*)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        
        [cell apiCallEndWithResultCount:0 pageNo:1];
    }];
}

-(void)getGlobalSearchMedias
{
    
    GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*) [_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    [cell apiCallInitiated];
    
    
    NSArray *arr = [mainDict objectForKey:global_search_type_media];
    if (arr.count == 0) {
        mediaCurrentPage = 1;
    }
    
    NSDictionary *dic = @{@"get":API_SEARCH,
                          @"type":global_search_type_media,
                          @"keyword":txtFieldSearch.text,
                          @"limit":RecordLimit,
                          @"page":@(mediaCurrentPage).stringValue,
                          @"user_id":UTILS.currentUser.userId};
    [REMOTE_API CallPOSTWebServiceWithParam:API_SEARCH params:dic sBlock:^(id responseObject) {
        [Utils HideProgress];
       
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[mainDict objectForKey:global_search_type_media]];
        [arr addObjectsFromArray:responseObject];
        [mainDict setObject:arr forKey:global_search_type_media];
        
        GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*) [_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        [cell apiCallEndWithResultCount:[arr count] pageNo:mediaCurrentPage];
        
        
        if([responseObject count] > 0)
        {
            mediaCurrentPage += 1;
        }
        
        [_collectionView reloadData];
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        
        GlobalPostCollectionCell *cell = (GlobalPostCollectionCell*) [_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        [cell apiCallEndWithResultCount:0 pageNo:1];
    }];
}

-(void)follow_unfollow_API:(NSIndexPath *)indexpath
{
    
    NSMutableArray *arr = [NSMutableArray arrayWithArray:[mainDict objectForKey:global_search_type_people]];
    
    FollowModel *model = arr[indexpath.row];

    NSString *API_name = model.isFollowing ? API_UNFOLLOW : API_FOLLOW;
    NSDictionary *params = @{
                             @"get": API_name,
                             @"user_id":UTILS.currentUser.userId,
                             @"following_id":model.userId
                             };
    
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_name params:params sBlock:^(id responseObject) {
        
        model.isFollowing=!model.isFollowing;
        [arr replaceObjectAtIndex:indexpath.row withObject:model];
        [mainDict setObject:arr forKey:global_search_type_people];
        
        [Utils HideProgress];
        [_collectionView reloadData];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        
        
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
    
}

@end
