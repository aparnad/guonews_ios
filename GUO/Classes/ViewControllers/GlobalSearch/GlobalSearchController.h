//
//  GlobalSearchController.h
//  GUO
//
//  Created by Pawan Ramteke on 09/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlobalSearchController : UIViewController
{
    NSString *selected_type;
    NSDictionary *param;
}
@property(strong,nonatomic) IBOutlet UIView *headerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;

@end
