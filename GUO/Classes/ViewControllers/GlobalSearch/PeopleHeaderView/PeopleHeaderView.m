//
//  PeopleHeaderView.m
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "PeopleHeaderView.h"
#import "PeopleCollectionCell.h"
@interface PeopleHeaderView()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *_collectionView;
}
@end

@implementation PeopleHeaderView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor controllerBGColor];
        UILabel *lblPeople = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, self.frame.size.width - 20, 20)];
        lblPeople.text = NSLocalizedString(@"bottom_tab_people", nil);
        [self addSubview:lblPeople];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(lblPeople.frame), self.frame.size.width, 150) collectionViewLayout:layout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:_collectionView];
        
        [_collectionView registerClass:[PeopleCollectionCell class] forCellWithReuseIdentifier:@"PeopleCollectionCell"];
    }
    return self;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PeopleCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PeopleCollectionCell" forIndexPath:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width - 20)/4.2 - 7, 130);
}

@end
