//
//  PeopleCollectionCell.m
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "PeopleCollectionCell.h"

@implementation PeopleCollectionCell
@synthesize imgViewDP;
@synthesize lblUserName;
@synthesize lblFirstName;
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        imgViewDP = [[UIImageView alloc]init];
        imgViewDP.layer.cornerRadius = 35;
        imgViewDP.layer.masksToBounds = YES;
        imgViewDP.layer.borderWidth = 2;
        imgViewDP.layer.borderColor = [UIColor whiteColor].CGColor;
        imgViewDP.image = [UIImage imageNamed:@"1"];
        [self addSubview:imgViewDP];
        
        [imgViewDP enableAutolayout];
        [imgViewDP centerX];
        [imgViewDP fixWidth:70];
        [imgViewDP topMargin:10];
        [imgViewDP fixHeight:70];
        
        lblFirstName = [[UILabel alloc]init];
        lblFirstName.font = [UIFont systemFontOfSize:12];
        lblFirstName.text = @"Guo Wengui";
        lblFirstName.textAlignment = NSTextAlignmentCenter;
        [self addSubview:lblFirstName];
        
        [lblFirstName enableAutolayout];
        [lblFirstName leadingMargin:2];
        [lblFirstName trailingMargin:2];
        [lblFirstName belowView:5 toView:imgViewDP];
        
        lblUserName = [[UILabel alloc]init];
        lblUserName.font = [UIFont systemFontOfSize:12];
        lblUserName.text = @"@Pawan";
        lblUserName.textAlignment = NSTextAlignmentCenter;
        lblUserName.textColor = [UIColor grayColor];
        [self addSubview:lblUserName];
        
        [lblUserName enableAutolayout];
        [lblUserName leadingMargin:2];
        [lblUserName trailingMargin:2];
        [lblUserName belowView:3 toView:lblFirstName];
    }
    return self;
}
@end
