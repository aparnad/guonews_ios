//
//  PeopleCollectionCell.h
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PeopleCollectionCell : UICollectionViewCell

@property (nonatomic,strong)UIImageView *imgViewDP;
@property (nonatomic,strong)UILabel *lblFirstName;
@property (nonatomic,strong)UILabel *lblUserName;

@end
