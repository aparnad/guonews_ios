//
//  LandingViewController.m
//  GUO
//
//  Created by Parag on 6/7/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "LandingViewController.h"

@interface LandingViewController ()
  @property (weak, nonatomic) IBOutlet UIView *containerView;
  @property (weak, nonatomic) IBOutlet UIButton *btnEnglish;
  @property (weak, nonatomic) IBOutlet UIButton *btnChinese;
  @property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
  @property (weak, nonatomic) IBOutlet UIImageView *imageViewBg;
  @property (weak, nonatomic) IBOutlet UIButton *btnGUO;
  @property (weak, nonatomic) IBOutlet UIButton *btnLogin;
  
@end

@implementation LandingViewController

  
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UserModel *u_model=[GUOSettings getIsLogin];
    if(u_model.userId!=nil)
    {
        Utils.getSharedInstance.currentUser = u_model;
        [AppObj LoadSlidingMenu:NO];
        return;
    }
    
    
    
  [self setupUIComponents];
    // Do any additional setup after loading the view.
}
  
  -(void)setupUIComponents{
    
    //self.imageViewProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    //self.imageViewProfile.layer.borderWidth = 4.0;
    self.imageViewProfile.clipsToBounds = YES;
    self.imageViewProfile.layer.cornerRadius = 60;
    [self.view bringSubviewToFront:self.imageViewProfile];
    self.containerView.clipsToBounds = YES;
    self.containerView.layer.cornerRadius = 15;
    
    self.btnGUO.clipsToBounds = YES;
    self.btnGUO.layer.cornerRadius = 8;
    
    self.btnLogin.clipsToBounds = YES;
    self.btnLogin.layer.cornerRadius = 8;
    
    self.btnEnglish.clipsToBounds = YES;
    self.btnEnglish.layer.cornerRadius = 8;
    
    self.btnChinese.clipsToBounds = YES;
    self.btnChinese.layer.cornerRadius = 8;
    
       
    
    
    self.containerView.clipsToBounds = YES;
      [self ChangeControlLanguage];
      [self.btnGUO.titleLabel setFont:[UIFont fontWithName:Font_Medium size:landing_page_button]];
      [self.btnLogin.titleLabel setFont:[UIFont fontWithName:Font_Medium size:landing_page_button]];
      [self.btnEnglish.titleLabel setFont:[UIFont fontWithName:Font_Medium size:btn_font_size]];
      [self.btnChinese.titleLabel setFont:[UIFont fontWithName:Font_Medium size:btn_font_size]];
      if(iPhone5 || iPhone4)
      {
          [self.lblChangeLangTitle setFont:[UIFont fontWithName:Font_Medium size:15]];
      }
      else
      {
          [self.lblChangeLangTitle setFont:[UIFont fontWithName:Font_Medium size:20]];
      }
  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)ChangeControlLanguage
{
    if([[NSString stringWithFormat:@"%@",[GUOSettings GetLanguage]] isEqualToString:[NSString stringWithFormat:@"%@",SetEnglish]])
    {
        
        [self.btnChinese setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnEnglish setTitleColor:header_color forState:UIControlStateNormal];

    }
    else
    {
        [self.btnChinese setTitleColor:header_color forState:UIControlStateNormal];
        [self.btnEnglish setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    if(self.is_came_from_sliding_menu)
    {
        [self.btnLogin setTitle:NSLocalizedString(@"post_title", nil) forState:UIControlStateNormal];

    }
    else
    {
        [self.btnLogin setTitle:NSLocalizedString(@"btn_login", nil) forState:UIControlStateNormal];
    }
    [self.btnLogin setBackgroundColor:header_color];
    [self chnageTitleLbl];
}
  -(void)chnageTitleLbl
{
    self.lblChangeLangTitle.text = NSLocalizedString(@"select_your_language", nil);

}
#pragma MArk Actions
  
- (IBAction)btnChineseTouched:(id)sender {

    [NSBundle setLanguage:SetChinese];

    [GUOSettings SetLanguage:SetChinese];
   [self.btnChinese setTitleColor:header_color forState:UIControlStateNormal];
   [self.btnEnglish setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [self ChangeControlLanguage];
    [self btnLoginTouched:self];
    [self chnageTitleLbl];
    
    [UTILS setUserLanguageOnServer];

}
- (IBAction)btnEnglishTouched:(id)sender {

    [NSBundle setLanguage:SetEnglish];

    [GUOSettings SetLanguage:SetEnglish];
  [self.btnChinese setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  [self.btnEnglish setTitleColor:header_color forState:UIControlStateNormal];
//    [self ChangeControlLanguage];
    
    [self btnLoginTouched:self];
    [self chnageTitleLbl];
    
    [UTILS setUserLanguageOnServer];

}
- (IBAction)btnGUOTouched:(id)sender {
  
}
  
- (IBAction)btnLoginTouched:(id)sender {
    if(!self.is_came_from_sliding_menu)
    {
        UIViewController* rootController = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [(AppObj).navController pushViewController:rootController animated:YES];
        (AppObj).navController.navigationBarHidden = YES;
    }
    else
    {
        [AppObj LoadSlidingMenu:YES];

    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
