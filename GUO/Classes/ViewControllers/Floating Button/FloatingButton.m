//
//  FloatingButton.m
//  GUO Media
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "FloatingButton.h"
#import "CreatePostController.h"
@interface FloatingButton ()

@end

@implementation FloatingButton

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils SetRoundedCorner:self.btnFloating];
    if([self.screenname isEqualToString:@"message"])
    {
        [self.btnFloating setImage:[UIImage imageNamed:@"add_message"] forState:UIControlStateNormal];
    }
    
   // header_color
    self.btnFloating.backgroundColor = floating_button_bg_color;
    
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)onclick_create_post:(id)sender
{
    if([self.screenname isEqualToString:@"message"])
    {
//        NewMessageController *newmessageNav = [(AppObj).monika_storyboard  instantiateViewControllerWithIdentifier:@"NewMessageController"];
//        newmessageNav.hidesBottomBarWhenPushed = YES;
//        [[UTILS topMostController].navigationController pushViewController:newmessageNav animated:YES];
       
    }
    else
    {
        CreatePostController *createPostVC = [(AppObj).pawan_storyboard  instantiateViewControllerWithIdentifier:@"CreatePostController"];
        [[UTILS topMostControllerNormal].navigationController pushViewController:createPostVC
    animated:YES];


        [createPostVC onPostSuccess:^(PostModel *model) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UPLOAD_SUCCESS_NOTIFICATION object:model];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:UPLOAD_SUCCESS_NOTIFICATION];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
