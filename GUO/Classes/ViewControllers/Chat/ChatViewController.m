

#import "ChatViewController.h"
#import "Applozic.h"
#import "ContactsViewController.h"
#import "ALChatManager.h"
#import "GroupViewController.h"

@interface ChatViewController ()
{
    __weak IBOutlet UIView *headerView;
    
    __weak IBOutlet UILabel *headerLbl;
    ALChatManager * chatManager;
}

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property(strong, nonatomic) UIBarButtonItem *barButtonItem;
@property(strong, nonatomic) ALMessagesViewController *msgView;
@end

@implementation ChatViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    
  //  NSBundle *bundle = [NSBundle bundleWithIdentifier:@"com.applozic.framework"];
    [self.msgView.dBService getMessages:self.msgView.childGroupList];

    [self.msgView.detailChatViewController setRefreshMainView:FALSE];
    [self.msgView.mTableView reloadData];
    [self updateLocalization];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocalization) name:@"Update_messages_screen_control" object:nil];

  //  [self newRotateAnimation];
}

-(void)newRotateAnimation
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionRepeat| UIViewAnimationCurveLinear
                     animations:^{
                         self.btnNews.transform = CGAffineTransformRotate(self.btnNews.transform, M_PI );
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}



-(void)updateLocalization
{
    headerLbl.text = NSLocalizedString(@"title_message", nil);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils SetViewHeader:headerView headerLbl:headerLbl];
    UIButton *btnNewMessage = [[UIButton alloc]init];
    btnNewMessage.backgroundColor = floating_button_bg_color;
    btnNewMessage.layer.cornerRadius = 32.5;
    [btnNewMessage setImage:[UIImage imageNamed:@"add_message"] forState:UIControlStateNormal];
    [self.view addSubview:btnNewMessage];
    
    [btnNewMessage enableAutolayout];
    [btnNewMessage trailingMargin:10];
    [btnNewMessage bottomMargin:10];
    [btnNewMessage fixWidth:65];
    [btnNewMessage fixHeight:65];
    [btnNewMessage addTarget:self action:@selector(btnNewMessageClicked) forControlEvents:UIControlEventTouchUpInside];
    
    chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLICATION_ID];
    
    NSBundle * bundle = [NSBundle bundleForClass:ALMessagesViewController.class];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Applozic" bundle:bundle];
   self.msgView = (ALMessagesViewController*)[storyBoard instantiateViewControllerWithIdentifier:@"ALViewController"];
    
    //    ALNewContactsViewController *contactView = (ALNewContactsViewController *)
    //                                [storyBoard instantiateViewControllerWithIdentifier:@"ALNewContactsViewController"];
    
    [self showViewControllerInContainerView: self.msgView];
    headerLbl.text = NSLocalizedString(@"", nil);
    
     [self.btnNews addTarget:self action:@selector(btnNewsClicked) forControlEvents:UIControlEventTouchUpInside];

}

-(void)btnNewsClicked
{
    [[Toast sharedInstance]makeToast:NSLocalizedString(@"Coming Soon", nil) position:TOAST_POSITION_BOTTOM];
    
}
-(IBAction)onclick_menu:(id)sender
{
    if(!self.is_came_from_sliding_menu)
    {
         [self.frostedViewController.view endEditing:YES];
         [self.frostedViewController presentMenuViewController];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   // self.tabBarController.navigationController.navigationBarHidden = NO;
}

-(void)btnNewMessageClicked
{

    [chatManager launchContactScreenWithMessage:nil andFromViewController:self];
    
  //  [self.tabBarController.navigationController pushViewController:contactVC animated:YES];
    
}

-(IBAction)onclick_view_profile_GUO:(id)sender
{
      [(AppObj).postView showUserProfile:GUO_USERID];
}

- (IBAction)groupChatClicked:(id)sender {

    UIStoryboard* storyboard1 = [UIStoryboard storyboardWithName:@"Applozic" bundle:[NSBundle bundleForClass:ALChatViewController.class]];

    ALGroupCreationViewController *vc = (ALGroupCreationViewController *)[storyboard1 instantiateViewControllerWithIdentifier:@"ALGroupCreationViewController"];

    [[UTILS topMostControllerNormal].navigationController pushViewController:vc animated:YES];
}



-(void) showViewControllerInContainerView:(UIViewController *)viewController
{
    for(UIViewController * vc in self.childViewControllers)
    {
        [vc willMoveToParentViewController:nil];
        [vc.view removeFromSuperview];
        [vc removeFromParentViewController];
    }
    
    [self addChildViewController:viewController];
    viewController.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    [self.containerView addSubview:viewController.view];
    [viewController didMoveToParentViewController:self];
    
    NSLayoutConstraint *constraintLeading = [NSLayoutConstraint constraintWithItem : [viewController view]
                                                                         attribute : NSLayoutAttributeLeading
                                                                         relatedBy : NSLayoutRelationEqual
                                                                            toItem : self.containerView
                                                                          attribute: NSLayoutAttributeLeading
                                                                         multiplier: 1
                                                                          constant : 0];
    
    [self.containerView addConstraint:constraintLeading];
    
    NSLayoutConstraint *constraintTop = [NSLayoutConstraint constraintWithItem : [viewController view]
                                                                     attribute : NSLayoutAttributeTop
                                                                     relatedBy : NSLayoutRelationEqual
                                                                        toItem : self.containerView
                                                                      attribute: NSLayoutAttributeTop
                                                                     multiplier: 1
                                                                      constant : 0];
    
    [self.containerView addConstraint:constraintTop];
    
    NSLayoutConstraint *constraintBottom = [NSLayoutConstraint constraintWithItem : [viewController view]
                                                                        attribute : NSLayoutAttributeBottom
                                                                        relatedBy : NSLayoutRelationEqual
                                                                           toItem : self.containerView
                                                                         attribute: NSLayoutAttributeBottom
                                                                        multiplier: 1
                                                                         constant : 0];
    
    [self.containerView addConstraint:constraintBottom];
    
    NSLayoutConstraint *constraintTrailing = [NSLayoutConstraint constraintWithItem : [viewController view]
                                                                          attribute : NSLayoutAttributeTrailing
                                                                          relatedBy : NSLayoutRelationEqual
                                                                             toItem : self.containerView
                                                                           attribute: NSLayoutAttributeTrailing
                                                                          multiplier: 1
                                                                           constant : 0];
    
    [self.containerView addConstraint:constraintTrailing];
    
    [self.containerView setNeedsUpdateConstraints];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
