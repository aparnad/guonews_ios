//
//  ContactsViewController.m
//  GUO Media
//
//  Created by Pawan Ramteke on 09/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ContactsViewController.h"

@interface ContactsViewController ()
{
    
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet UILabel *headerLbl;
}
@end

@implementation ContactsViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils SetViewHeader:headerView headerLbl:headerLbl];
    [self addCustomBack];
}

#pragma mark - back button code
-(void)addCustomBack
{
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 44.0f, 30.0f)];
    [backButton setImage:[ALUtilityClass getImageFromFramworkBundle:@"ic_back1"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
}
-(void)backClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
