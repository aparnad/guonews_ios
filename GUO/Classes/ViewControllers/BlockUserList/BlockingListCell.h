//
//  BlockingListCell.h
//  GUO
//
//  Created by Parag on 6/8/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#define CellIdentifierBlockList @"CellIdentifierBlockList"


@interface BlockingListCell : UITableViewCell

  @property (weak, nonatomic) IBOutlet UIImageView *imgViewProfile;
  @property (weak, nonatomic) IBOutlet UILabel *lblTitleName;
  @property (weak, nonatomic) IBOutlet UILabel *lblSubName;
  @property (weak, nonatomic) IBOutlet UIButton *btnUnblock;
  
@end
