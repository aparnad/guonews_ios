//
//  BlockUserListViewController.m
//  GUO
//
//  Created by Parag on 6/7/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "BlockUserListViewController.h"
#import "BlockingListCell.h"

@interface BlockUserListViewController ()


@end

@implementation BlockUserListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = light_gray_bg_color;
    self.aTableView.backgroundColor = light_gray_bg_color;

    
    [Utils RemoveTableViewHeaderFooterSpace:self.aTableView];

    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];
    
    self.lblHeader.text =NSLocalizedString(@"blocking", nil);

    
    
    
    
    // Do any additional setup after loading the view.
    arrBlockUserList = [[NSMutableArray alloc]init];
    self.refreshControl=[[UIRefreshControl alloc]init];
    
    
    refreshFooter=[[YiRefreshFooter alloc] init];
    
    __weak typeof(self) weakSelf = self;
    refreshFooter.beginRefreshingBlock=^(){
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            sleep(pullDownToRefreshSleepCount);
            [weakSelf getBlockUserList];
        });
    };

    [self.refreshControl addTarget:self action:@selector(refreshList:) forControlEvents:UIControlEventValueChanged];
    
    [self.aTableView addSubview:self.refreshControl];
    [self getBlockUserList];


    
    // Do any additional setup after loading the view.
}

#pragma mark- API calling
-(void)refreshList:(UIRefreshControl *)refreshControl
{
    
    current_page = 1;
//    [UTILS ShowProgress];
    
    [self getBlockUserList];
}

-(void)getBlockUserList
{
    
    if(arrBlockUserList.count == 0)
    {
        [UTILS ShowProgress];
        current_page = 1;
    }
    NSDictionary *params = @{
                             @"get":API_BLOCK_USER_LIST,
                             @"user_id":UTILS.currentUser.userId,
                             @"limit":RecordLimit,
                             @"page":[NSString stringWithFormat:@"%d",current_page]
                             };
    
    [REMOTE_API CallPOSTWebServiceWithParam:API_BLOCK_USER_LIST params:params sBlock:^(id responseObject) {
        
        if(current_page == 1)
        {
            arrBlockUserList = [[NSMutableArray alloc]init];
        }
        if([responseObject count] >0)
        {
            current_page += 1;
        }
        
        [arrBlockUserList addObjectsFromArray:responseObject];
        [self.aTableView reloadData];
        [Utils HideProgress];
        [self.refreshControl endRefreshing];
        
        
        if([responseObject count] == 0)
        {
            [self RemovePullDownToRefresh];
        }
        else if([arrBlockUserList  count] >= [RecordLimit intValue])
        {
            [self AddPullDowntoRefresh];
        }
        
        
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        [self RemovePullDownToRefresh];
        [self.refreshControl endRefreshing];
        
    }];
    
}


#pragma mark - Tableview Delegate and Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
//{
//    return self.viewFollowers.frame.size.height;
//}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    return self.viewFollowers;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrBlockUserList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ConnectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ConnectionCell"];
    if(cell==nil)
    {
        cell=[[ConnectionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ConnectionCell"];
        
        NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"ConnectionCell" owner:self options:nil];
        cell=[menuarray objectAtIndex:0];
    }
    UserModel *model = [[UserModel alloc]init];
    model = [arrBlockUserList objectAtIndex:indexPath.row];

    [cell.imgUser setImageWithURL:[NSURL URLWithString:model.userPicture] placeholderImage:defaultUserImg];
    cell.lblName.text = [NSString stringWithFormat:@"%@",[Utils setFullname:model.userFirstname lname:model.userLastname username:model.userName]];

    cell.lblDesc.text = [NSString stringWithFormat:@"@%@",model.userName];
    
    cell.selectionStyle = NO;
    cell.btnBlock.tag = indexPath.row;
    [cell.btnBlock addTarget:self action:@selector(onclick_block:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = NO;
   NSString *status_title = NSLocalizedString(@"txt_unblock", nil);
    cell.btnBlock.layer.borderColor = gray_icon_color.CGColor;
    [cell.btnBlock setTitleColor:gray_icon_color forState:UIControlStateNormal];

    //    NSString *status_title;
//    if(![[dic objectForKey:@"is_selected"] boolValue])
//    {
//        status_title = NSLocalizedString(@"block", nil);
//        cell.btnBlock.layer.borderColor = header_color.CGColor;
//        [cell.btnBlock setTitleColor:header_color forState:UIControlStateNormal];
//
//    }
//    else
//    {
//        status_title = NSLocalizedString(@"txt_unblock", nil);
//        cell.btnBlock.layer.borderColor = gray_icon_color.CGColor;
//        [cell.btnBlock setTitleColor:gray_icon_color forState:UIControlStateNormal];
//    }
    cell.btnFollow.hidden = YES;
    cell.btnBlock.hidden = NO;
    cell.btnMore.hidden = YES;
    [cell.btnBlock setTitle:status_title forState:UIControlStateNormal];
    return cell;
}
#pragma mark- Button click
-(IBAction)onclick_block:(UIButton*)sender
{
    [CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"unblock_user_alert_msg", nil)  okHandler:^{
        UserModel *model = [arrBlockUserList objectAtIndex:sender.tag];
        NSDictionary *params = @{
                                 @"get":API_UNBLOCK_USER,
                                 @"user_id":UTILS.currentUser.userId,
                                 @"block_id":model.userId
                                 };
        
        [UTILS ShowProgress];
        [REMOTE_API CallPOSTWebServiceWithParam:API_UNBLOCK_USER params:params sBlock:^(id responseObject) {
            
            
            [Utils HideProgress];
            [arrBlockUserList removeObjectAtIndex:sender.tag];
            [self.aTableView reloadData];
            
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
            [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        }];
        
        
        [self.aTableView reloadData];
    }];
   
}
-(IBAction)onclick_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)onclick_more:(id)sender;
{
    
}
#pragma mark- Textfield delegate method
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark- Remove pull down to refresh
-(void)RemovePullDownToRefresh
{
    //Remove Pull down to refresh when category_total_count match with array
    
    [refreshFooter remove_observer];
    
    refreshFooter.scrollView=nil;
    [refreshFooter.footerView removeFromSuperview];
  //  self.aTableView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    
}
-(void)AddPullDowntoRefresh
{
    [self RemovePullDownToRefresh];
    refreshFooter.scrollView=self.aTableView;
    [refreshFooter footer];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



    
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
