//
//  BlockUserListViewController.h
//  GUO
//
//  Created by Parag on 6/7/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "BaseViewController.h"
#import "ConnectionCell.h"
#import "YiRefreshFooter.h"

@interface BlockUserListViewController : BaseViewController
{
    NSMutableArray *arrBlockUserList;
    int current_page;
    YiRefreshFooter *refreshFooter;

}
@property(strong,nonatomic) UIRefreshControl *refreshControl;
@property(strong,nonatomic) IBOutlet UITableView *aTableView;
@property(strong,nonatomic) IBOutlet UIView *headerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
-(IBAction)onclick_back:(id)sender;

@end
//MoreFollowListController
