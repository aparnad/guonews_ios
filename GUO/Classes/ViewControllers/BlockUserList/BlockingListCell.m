//
//  BlockingListCell.m
//  GUO
//
//  Created by Parag on 6/8/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "BlockingListCell.h"
#define CellIdentifierBlocking @"CellIdentifierBlocking"

@implementation BlockingListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
  self.imgViewProfile.layer.borderColor = [UIColor whiteColor].CGColor;
  self.imgViewProfile.layer.borderWidth = 4.0;
  self.imgViewProfile.clipsToBounds = YES;
  self.imgViewProfile.layer.cornerRadius = 30;
 
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
