//
//  CreatePostCollectionCell.h
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBCircularProgressBarView.h"
typedef void(^onCloseBlock)(NSIndexPath *indexPath);
@interface CreatePostCollectionCell : UICollectionViewCell
@property (nonatomic,strong)UIImageView *imgView;
@property (nonatomic,strong)NSIndexPath *indexPath;
@property (nonatomic,strong)UIButton *btnEdit;

@property (nonatomic,copy)onCloseBlock block;
@property (nonatomic,copy)onCloseBlock editBlock;

@property (nonatomic,strong)MBCircularProgressBarView *progressBar;
@property (nonatomic,strong)UILabel *lblProgress;
-(void)onDeleteImageClicked:(onCloseBlock)blk;
-(void)onEditImageClicked:(onCloseBlock)blk;

@end
