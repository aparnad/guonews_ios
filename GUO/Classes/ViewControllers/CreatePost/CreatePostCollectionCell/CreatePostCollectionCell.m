//
//  CreatePostCollectionCell.m
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "CreatePostCollectionCell.h"

@implementation CreatePostCollectionCell
@synthesize imgView;
@synthesize progressBar;
@synthesize lblProgress;
@synthesize btnEdit;
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        imgView = [[UIImageView alloc]init];
        imgView.backgroundColor = [UIColor grayColor];
        [self addSubview:imgView];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.clipsToBounds = YES;
        imgView.layer.cornerRadius = 10.0;
        imgView.layer.masksToBounds = YES;
        
        [imgView enableAutolayout];
        [imgView leadingMargin:0];
        [imgView trailingMargin:0];
        [imgView topMargin:0];
        [imgView bottomMargin:0];
        
//        progressBar = [[MBCircularProgressBarView alloc]init];
//        [self addSubview:progressBar];
//        progressBar.emptyLineWidth = 2;
//        progressBar.progressLineWidth = 5;
//        progressBar.showValueString = YES;
//        progressBar.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
//        progressBar.progressStrokeColor = [UIColor themeColor];
//        progressBar.maxValue = 100;
//        [progressBar enableAutolayout];
//        [progressBar leadingMargin:0];
//        [progressBar trailingMargin:0];
//        [progressBar topMargin:0];
//        [progressBar bottomMargin:0];
        
        lblProgress = [[UILabel alloc]init];
        lblProgress.font = [UIFont boldSystemFontOfSize:20];
        lblProgress.textColor = [UIColor whiteColor];
        [self addSubview:lblProgress];
        
        [lblProgress enableAutolayout];
        [lblProgress centerY];
        [lblProgress centerX];
        
        
        UIButton *btnClose = [[UIButton alloc]init];
        [btnClose setImage:[[UIImage imageNamed:@"ic_close"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]forState:UIControlStateNormal];
        btnClose.tintColor = [UIColor whiteColor];
        btnClose.backgroundColor = ColorRGBA(0, 0, 0, 0.8);
        [btnClose addTarget:self action:@selector(btnCloseClicked) forControlEvents:UIControlEventTouchUpInside];
        btnClose.layer.cornerRadius = 15;
        [self addSubview:btnClose];
        
        [btnClose enableAutolayout];
        [btnClose topMargin:10];
        [btnClose trailingMargin:10];
        [btnClose fixWidth:30];
        [btnClose fixHeight:30];
        
        btnEdit = [[UIButton alloc]init];
        [btnEdit setImage:[[UIImage imageNamed:@"ic_edit"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]forState:UIControlStateNormal];
        btnEdit.tintColor = [UIColor whiteColor];
        btnEdit.backgroundColor = ColorRGBA(0, 0, 0, 0.8);
        [btnEdit addTarget:self action:@selector(btnEditClicked) forControlEvents:UIControlEventTouchUpInside];
        btnEdit.layer.cornerRadius = 15;
        [self addSubview:btnEdit];
        
        [btnEdit enableAutolayout];
        [btnEdit bottomMargin:10];
        [btnEdit trailingMargin:10];
        [btnEdit fixWidth:30];
        [btnEdit fixHeight:30];
    }
    return self;
}

-(void)btnCloseClicked
{
    self.block(self.indexPath);
}

-(void)btnEditClicked
{
    self.editBlock(self.indexPath);

}

-(void)onDeleteImageClicked:(onCloseBlock)blk
{
    self.block = blk;
}

-(void)onEditImageClicked:(onCloseBlock)blk
{
    self.editBlock = blk;
}
@end
