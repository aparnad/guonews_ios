//
//  CreatePostController.h
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PostSuccessBlock)(PostModel *model);
@interface CreatePostController : UIViewController<UITextFieldDelegate>
{
    
}
@property(strong,nonatomic) IBOutlet UILabel *lblTotalLeftChar;
@property (nonatomic,copy)PostSuccessBlock block;

-(void)onPostSuccess:(PostSuccessBlock)blk;
@end
