//
//  UploadProgressView.h
//  GUO Media
//
//  Created by Pawan Ramteke on 05/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadProgressView : UIView
-(void)setProgress:(CGFloat)value;

@property (nonatomic,copy)noArgBlock blk;

-(void)onCloseClicked:(noArgBlock)block;
@end
