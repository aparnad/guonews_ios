//
//  UploadProgressView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 05/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "UploadProgressView.h"

@interface UploadProgressView()
{
    UIProgressView *progressView;
    UILabel *lblPercentage;
}
@end

@implementation UploadProgressView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
//        UIView *baseView = [[UIView alloc]init];
//        baseView.backgroundColor = [UIColor whiteColor];
//        [self addSubview:baseView];
//        baseView.layer.cornerRadius = 10;
//
//        [baseView enableAutolayout];
//        [baseView centerX];
//        [baseView centerY];
//        [baseView fixWidth:Screen_Width *0.8];
//        [baseView fixHeight:50];
        
        progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        progressView.progressTintColor = header_color;
        progressView.trackTintColor = [UIColor whiteColor];
        [[progressView layer]setMasksToBounds:TRUE];
        progressView.clipsToBounds = YES;
        
        [self addSubview:progressView];
        
        [progressView enableAutolayout];
        [progressView leadingMargin:0];
        [progressView trailingMargin:0];
        [progressView topMargin:20];
        [progressView fixHeight:5];
        
//        lblPercentage = [[UILabel alloc]init];
//        lblPercentage.textColor = [UIColor whiteColor];
//        lblPercentage.font = [UIFont fontWithName:Font_regular size:14];
//        lblPercentage.text = @"Processing...";
//        [progressView addSubview:lblPercentage];
//
//        [lblPercentage enableAutolayout];
//        [lblPercentage centerY];
//        [lblPercentage leadingMargin:10];
        
        UIButton *btnCancel = [[UIButton alloc]init];
        [btnCancel setImage:[UIImage imageNamed:@"ic_close"] forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(btnCancelClicked) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnCancel];
        
        [btnCancel enableAutolayout];
        [btnCancel leadingMargin:2];
        [btnCancel belowView:3 toView:progressView];
        [btnCancel fixHeight:40];
        [btnCancel fixWidth:40];
        
        
        
       DGActivityIndicatorView * activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
        
        
        activityIndicatorView.backgroundColor = [UIColor clearColor];
       // activityIndicatorView.frame = CGRectMake(CGRectGetMidX(self.frame)-50, Screen_Height- 100, 100, 50);
        [self addSubview:activityIndicatorView];
        
        [activityIndicatorView enableAutolayout];
        [activityIndicatorView centerX];
        [activityIndicatorView bottomMargin:80];
        [activityIndicatorView fixHeight:50];
        [activityIndicatorView fixWidth:100];
        
        [activityIndicatorView startAnimating];

    }
    
    return self;
}

-(void)setProgress:(CGFloat)value
{
    CGFloat val =  value/100.0;// value*100;
    [progressView setProgress:value animated:YES];
    lblPercentage.text = [NSString stringWithFormat:@"Processing...%d%@",(int)val,@"%"];
}

-(void)onCloseClicked:(noArgBlock)block
{
    self.blk = block;
}

-(void)btnCancelClicked
{
    self.blk();
}
@end
