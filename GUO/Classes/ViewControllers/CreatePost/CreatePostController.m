//
//  CreatePostController.m
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "CreatePostController.h"
#import "CreatePostCollectionCell.h"
#import "ASJCollectionViewFillLayout.h"
#import "MBAutoGrowingTextView.h"
#import <Applozic/QBImagePickerController.h>
#import "AWSS3.h"
#import "UploadProgressView.h"
#import "CreatePostModel.h"
#import "UIImage+fixOrientation.h"
#import "RequestProgress.h"
#import "CLImageEditor.h"
#import "UITextView+Placeholder.h"
@interface CreatePostController ()<UICollectionViewDataSource,UICollectionViewDelegate,QBImagePickerControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,CLImageEditorDelegate,UITextViewDelegate>
{
    
    __weak IBOutlet UIView *headerView;
    
    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UILabel *lblHeader;
    __weak IBOutlet UIImageView *imgViewDp;
    __weak IBOutlet UITextView *_txtView;
    __weak IBOutlet UILabel *lblName;
    __weak IBOutlet UIButton *btnPost;
    __weak IBOutlet UIView *viewBase;
   // NSMutableArray *arrAttachments;
    __weak IBOutlet UICollectionView *_collectionView;
    NSMutableArray *arrImgUploadUrls;
    NSMutableArray *arrVideoUploadUrls;
    
    NSMutableArray *arrUploadReq;
    NSMutableArray *arrThmbnailReq;
    NSMutableArray *arrAllUploadReq;
    NSMutableArray *arrRequestProgress;

    __weak IBOutlet UIView *viewSubBase;
    
    UploadProgressView *progressView;
    UIBarButtonItem *lblCount;
    __weak IBOutlet UIToolbar *keyboardToolBar;
    int count;
    
    DGActivityIndicatorView *activityIndicatorView;
    
    CGFloat totalProgress;

    __weak IBOutlet NSLayoutConstraint *collectionHtConstr;
    NSMutableArray *arrUploadTask;
    BOOL isPostWasDiscarded;
}
@end

@implementation CreatePostController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    int maxCharLimit = CREATE_POST_CHARACTER_LIMIT;
    
    if ([UTILS.currentUser.userId isEqualToString:GUO_USERID]) {
        maxCharLimit = CREATE_POST_CHARACTER_LIMIT_BOSS;
    }
    
    _txtView.delegate = self;
    _txtView.dataDetectorTypes = UIDataDetectorTypeAll;
    _lblTotalLeftChar.text = [NSString stringWithFormat:@"0/%d",maxCharLimit];

    self.view.backgroundColor = [UIColor whiteColor];
    viewSubBase.backgroundColor = [UIColor whiteColor];
  //  [Utils SetViewHeader:headerView headerLbl:lblHeader];
    [Utils SetRoundedCorner:imgViewDp];
    [Utils makeHalfRoundedButton:btnPost];
    btnPost.backgroundColor = header_color;
    [btnPost setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [imgViewDp sd_setImageWithURL:[Utils getProperContentUrl:UTILS.currentUser.userPicture] placeholderImage:defaultUserImg];
    lblName.text = UTILS.currentUser.userName;//[NSString stringWithFormat:@"%@ %@",UTILS.currentUser.userFirstname,UTILS.currentUser.userLastname];

    arrImgUploadUrls = [NSMutableArray new];
    arrVideoUploadUrls = [NSMutableArray new];
    arrRequestProgress = [NSMutableArray new];
    arrUploadTask = [NSMutableArray new];

    viewBase.layer.cornerRadius = 10;

    [self addDoneButtonToKeyboard];
    
    [self addLoader];
   
    arrUploadReq = [NSMutableArray new];
    arrThmbnailReq = [NSMutableArray new];
    
    ASJCollectionViewFillLayout *aFillLayout = [[ASJCollectionViewFillLayout alloc] init];
    aFillLayout.numberOfItemsInSide = 2;
    aFillLayout.itemSpacing = 5.0f;
    aFillLayout.itemLength = 200;
    aFillLayout.stretchesLastItems = YES;
    aFillLayout.direction = ASJCollectionViewFillLayoutVertical;
    
//    UICollectionViewFlowLayout *aFillLayout = [[UICollectionViewFlowLayout alloc]init];
//    aFillLayout.itemSize = CGSizeMake(200, 400);
    
    _collectionView.collectionViewLayout = aFillLayout;
    
    [_collectionView registerClass:[CreatePostCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    
    [btnPost setTitle:NSLocalizedString(@"post_title", nil) forState:UIControlStateNormal];

    
    lblHeader.text = NSLocalizedString(@"create_post", nil);
    [_collectionView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    _txtView.placeholder = NSLocalizedString(@"What's Happening?", nil);
    _txtView.textColor =  _txtView.tintColor = [UIColor blackColor];
    _txtView.placeholderLabel.textColor = [UIColor grayColor];
    _txtView.font = [UIFont fontWithName:Font_regular size:txtfield_font_size];
    
    [self clearTmpDirectory];
    
    [_txtView becomeFirstResponder];
}

-(void)callPostService
{
    
    //if post was discarded do nothing...
    if (isPostWasDiscarded) {
        return;
    }
    
    
    NSMutableDictionary *param = [NSMutableDictionary new];
    [param setValue:API_ADD_POST forKey:@"get"];
    [param setValue:UTILS.currentUser.userId forKey:@"user_id"];
    [param setValue:_txtView.text forKey:@"post_text"];
    
    NSMutableDictionary *mediaFiles = [NSMutableDictionary new];
    
    if (arrImgUploadUrls.count) {
        [mediaFiles setObject:arrImgUploadUrls forKey:@"photos"];
    }
    else if (arrVideoUploadUrls.count){
        [mediaFiles setObject:arrVideoUploadUrls forKey:@"video"];
    }
    
    if (mediaFiles.allValues.count) {
        [param setObject:mediaFiles forKey:@"media_files"];
    }

    [SVProgressHUD show];
    //[UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_ADD_POST params:param sBlock:^(id responseObject) {
        [Utils HideProgress];
        
        CountModel *contM = (AppObj).app_count_model;
        contM.posts = contM.posts + 1;
        (AppObj).app_count_model = contM;
        
        if (self.block) {
            self.block(responseObject);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{            
            [self clearTmpDirectory];
            [self.navigationController popViewControllerAnimated:YES];
            
            [[Toast sharedInstance]makeToast:NSLocalizedString(@"Post uploaded successfully", nil) position:TOAST_POSITION_BOTTOM];
//            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        
         [[Toast sharedInstance] makeToast:customErrorMsg position:TOAST_POSITION_BOTTOM];
    }];
    
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrUploadReq.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CreatePostCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    CreatePostModel *model = arrUploadReq[indexPath.row];
    cell.imgView.image = model.attachment;
    cell.indexPath = indexPath;
    
    cell.btnEdit.hidden = [model.type isEqualToString:@"Video"] ? YES : NO;
    [cell onDeleteImageClicked:^(NSIndexPath *indexPath) {
        if (arrThmbnailReq.count == arrUploadReq.count) {
            [arrThmbnailReq removeObjectAtIndex:indexPath.row];
        }
        [arrUploadReq removeObjectAtIndex:indexPath.row];
        [_collectionView reloadData];
    }];
    
    [cell onEditImageClicked:^(NSIndexPath *indexPath) {
        CreatePostModel *model = arrUploadReq[indexPath.row];
        [self presentImageEditor:model.attachment indexPath:indexPath];
    }];
    return cell;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    collectionHtConstr.constant = _collectionView.contentSize.height;
}

- (IBAction)btnGalleryClicked:(id)sender {
   // [self presentImageVideoPicker:YES];
    
    [UTILS showActionSheet:@"" optionsArray:@[NSLocalizedString(@"Take Photo", nil),NSLocalizedString(@"Browse Photos", nil)] completion:^(NSInteger index) {
        if (index == 0) {
            [self presentImagePickerController:UIImagePickerControllerSourceTypeCamera mediaTypes:nil];
        }
        else{
             [self presentImageVideoPicker:YES];
        }
    }];
}

- (IBAction)btnVideoClicked:(id)sender {
    //[self presentImageVideoPicker:NO];
    
    [UTILS showActionSheet:@"" optionsArray:@[NSLocalizedString(@"Record Video", nil),NSLocalizedString(@"Browse Video", nil)] completion:^(NSInteger index) {
        if (index == 0) {
             [self presentImagePickerController:UIImagePickerControllerSourceTypeCamera mediaTypes:(NSString *) kUTTypeMovie];
        }
        else{
            UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
            mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
            //mediaUI.allowsEditing = YES;
            mediaUI.delegate = self;
            
            [self presentViewController:mediaUI animated:YES completion:nil];
        }
    }];
}


- (IBAction)btnPostClicked:(id)sender {
    
    [self.view endEditing:YES];
    
    
    if (![UTILS checkIntenetShowError]) {
        return;
    }
    
    //clear previous progress
    [arrRequestProgress removeAllObjects];
    
    
//    if (arrThmbnailReq.count) {
//        for (AWSS3TransferManagerUploadRequest *request in arrThmbnailReq) {
//            [self upload:request];
//        }
//    }
    arrAllUploadReq = [NSMutableArray new];
    [arrAllUploadReq addObjectsFromArray:arrThmbnailReq];
    [arrAllUploadReq addObjectsFromArray:arrUploadReq];
    
    if (arrAllUploadReq.count) {
        [self showProgressView];
        for (CreatePostModel *model in arrAllUploadReq) {
            [self upload:model.uploadRequest];
            
        }
    }
    else if (_txtView.text.length > 0)
    {
        [self callPostService];
    }
    else{
        [[Toast sharedInstance]makeToast:NSLocalizedString(@"Nothing to post", nil) position:TOAST_POSITION_BOTTOM];
    }
    
    
}

-(void)upload:(AWSS3TransferManagerUploadRequest *)uploadRequest{
    //AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility S3TransferUtilityForKey:APP_AWS_ACCELERATION_KEY];
    
    
    
    AWSS3TransferUtilityUploadExpression *expression = [AWSS3TransferUtilityUploadExpression new];
    AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
        
        NSLog(@"ERROOROROOOOROR 2222222 => %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{

            count++;

            if ([uploadRequest.bucket containsString:@"thumb"]) {
                // return;
            }
            else{
                NSString *url = [uploadRequest.bucket stringByReplacingOccurrencesOfString:S3_bUCKET_NAME withString:@""];
                
                url = [url stringByReplacingOccurrencesOfString:@"uploads/" withString:@""];
                url = [[url stringByAppendingString:@"/"] stringByAppendingString:uploadRequest.key];
                if ([url containsString:@"photos"]) {
                    [arrImgUploadUrls addObject:url];
                }
                else{
                    [arrVideoUploadUrls addObject:url];
                }
            }

            if (arrAllUploadReq.count == count) {
                [progressView removeFromSuperview];
                [Utils HideProgress];
                btnClose.hidden = NO;
                [self callPostService];
            }
        });
    };
    
    NSString *keyName = [[[uploadRequest.bucket stringByReplacingOccurrencesOfString:S3_bUCKET_NAME withString:@""] stringByAppendingString:@"/"] stringByAppendingString:uploadRequest.key];
    
   [[transferUtility uploadFile:uploadRequest.body bucket:S3_bUCKET_NAME key:keyName contentType:@"image/jpeg" expression:expression completionHandler:completionHandler] continueWithSuccessBlock:^id _Nullable(AWSTask<AWSS3TransferUtilityUploadTask *> * _Nonnull task) {

         NSLog(@"ERROOROROOOOROR => %@",task.error);

        return nil;
    }];
    
  
    expression.progressBlock = ^(AWSS3TransferUtilityTask *task, NSProgress *progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something e.g. Update a progress bar.
            
            if (![arrUploadTask containsObject:task]) {
                  [arrUploadTask addObject:task];
            }
            
          //  NSLog(@"PROGRESS => %lf",progress.fractionCompleted);
          //  NSLog(@"TOTAL PROGRESS => %lf",totalProgress);
            totalProgress = 0;
            for (AWSS3TransferUtilityTask *taskObj in arrUploadTask) {
                totalProgress += taskObj.progress.fractionCompleted;
            }
            
            totalProgress = totalProgress/arrAllUploadReq.count;
            
            [progressView setProgress:totalProgress];

        });
    };
    
/*
    
    [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                switch (task.error.code) {
                    case AWSS3TransferManagerErrorCancelled:
                    case AWSS3TransferManagerErrorPaused:
                    {
                        
                    }
                    break;
                        
                    default:
                        NSLog(@"Upload failed: [%@]", task.error);
                        [CustomAlertView showAlert:@"" withMessage:task.error.localizedDescription];
                        [Utils HideProgress];
                        break;
                }
            } else {
                NSLog(@"Upload failed: [%@]", task.error);
                [CustomAlertView showAlert:@"" withMessage:task.error.localizedDescription];
                [Utils HideProgress];
            }
        }
        
        if (task.result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                count++;
                
                if ([uploadRequest.bucket containsString:@"thumb"]) {
                   // return;
                }
                else{
                    NSString *url = [uploadRequest.bucket stringByReplacingOccurrencesOfString:S3_bUCKET_NAME withString:@""];
                    url = [[url stringByAppendingString:@"/"] stringByAppendingString:uploadRequest.key];
                    if ([url containsString:@"photos"]) {
                        [arrImgUploadUrls addObject:url];
                    }
                    else{
                        [arrVideoUploadUrls addObject:url];
                    }
                }
                
                if (arrAllUploadReq.count == count) {
                    [progressView removeFromSuperview];
                    [Utils HideProgress];
                    btnClose.hidden = NO;
                    [self callPostService];
                }
            });
        }
        
        return nil;
    }];
    
    __weak typeof(AWSS3TransferManagerUploadRequest *) weakSelf = uploadRequest;
    weakSelf.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (uploadRequest.state) {
                case AWSS3TransferManagerRequestStateRunning:
                {
                    if (arrAllUploadReq.count) {
                        if (totalBytesExpectedToSend > 0) {
                            CGFloat progress = (float)((double) totalBytesSent / totalBytesExpectedToSend) * 100;
                            
                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uploadRequest = %@", weakSelf];
                            NSArray *filterArray = [arrAllUploadReq filteredArrayUsingPredicate:predicate];
                            
                            
                            NSInteger index = [arrAllUploadReq indexOfObject:filterArray[0]];
                            CreatePostModel *model = arrAllUploadReq[index];
                            model.progress = progress;
                            [arrAllUploadReq replaceObjectAtIndex:index withObject:model];
                            
                            CGFloat totalProgress = 0;
                            
                            for (CreatePostModel *model in arrAllUploadReq) {
                                totalProgress = totalProgress + model.progress;
                            }
                            
                            totalProgress = totalProgress/arrAllUploadReq.count;
                            NSLog(@"%f",totalProgress/100);
                            
                            //                    totalProgress = totalProgress + progress;
                            //
                            //                    CGFloat prog = (totalProgress/(100 * arrAllUploadReq.count))*100;
                            //
                            //                    NSLog(@"%f = %f",totalProgress,prog);
                            
                            [progressView setProgress:totalProgress/100];
                        }
                    }
                }
                    break;
                    
                default:
                    break;
            }
        });
    };
     
     */

    [_collectionView reloadData];
}
-(CGFloat)getTotalProgress:(CGFloat)individualRequestProgress
{
    CGFloat totalProgress = 0;
    
    RequestProgress *matchingRequest = nil;
    for (RequestProgress *request in arrRequestProgress) {
        
        if (individualRequestProgress > request.progress) {
            matchingRequest = request;
            break;
        }
    }
    
    if (matchingRequest) {
        matchingRequest.progress = individualRequestProgress;
    }
    
    for (RequestProgress *request in arrRequestProgress) {
        
        totalProgress += request.progress;
    }
    
    CGFloat progressPercent = (totalProgress/(arrRequestProgress.count*100))*100.0;
    
    return progressPercent;
}

-(void)cancelAllUploads
{
    for (CreatePostModel *model in arrUploadReq) {
        AWSS3TransferManagerUploadRequest *uploadRequest = model.uploadRequest;
        [[uploadRequest cancel] continueWithBlock:^id(AWSTask *task) {
            if (task.error) {
                NSLog(@"The cancel request failed: [%@]", task.error);
            }
            
            return nil;
        }];
    }
    
    for (CreatePostModel *model in arrThmbnailReq) {
        AWSS3TransferManagerUploadRequest *uploadRequest = model.uploadRequest;
        [[uploadRequest cancel] continueWithBlock:^id(AWSTask *task) {
            if (task.error) {
                NSLog(@"The cancel request failed: [%@]", task.error);
            }
            return nil;
        }];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [arrThmbnailReq removeAllObjects];
    [arrUploadReq removeAllObjects];
    [arrAllUploadReq removeAllObjects];
    
    [_collectionView reloadData];
    count = 0;
    _txtView.text = @"";
    [[Toast sharedInstance]makeToast:NSLocalizedString(@"Uploading cancelled.", nil) position:TOAST_POSITION_BOTTOM];
}

-(void)cancelUploadRequest:(AWSS3TransferManagerUploadRequest *)uploadRequest
{
    [[uploadRequest cancel] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            NSLog(@"The cancel request failed: [%@]", task.error);
        }
        return nil;
    }];
}

-(void) presentImageVideoPicker:(BOOL)isImagePicker
{
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = isImagePicker;
    imagePickerController.mediaType = isImagePicker ? QBImagePickerMediaTypeImage : QBImagePickerMediaTypeVideo;
    imagePickerController.showsNumberOfSelectedAssets = isImagePicker;
    imagePickerController.maximumNumberOfSelection = MAX_IMAGE_SELECTION_IN_CREATE_POST;
    [[UTILS topMostControllerNormal] presentViewController:imagePickerController animated:YES completion:NULL];
}

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    for (PHAsset *asset in assets) {
        
        PHImageManager *manager = [PHImageManager defaultManager];

        NSArray *yearMonth = [Utils getCurrentYearandMonth];

        if (asset.mediaType == PHAssetMediaTypeImage) {
            
            if (arrUploadReq.count == 1) {
                CreatePostModel *model = arrUploadReq[0];
                if ([model.uploadRequest.bucket containsString:@"videos"]) {
                    [arrUploadReq removeAllObjects];
                    [arrThmbnailReq removeAllObjects];
                }
            }
            
            PHImageRequestOptions *options = [PHImageRequestOptions new];
            options.synchronous = YES;
            [manager requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:options resultHandler:^void(UIImage *image, NSDictionary *info) {
                //NSURL *fileURL = info[@"PHImageFileURLKey"];

                NSString *extension = [info[@"PHImageFileUTIKey"] pathExtension];
                NSString *imgName = [Utils getImageName:extension];
                
                NSLog(@"image Name : %@",imgName);
                NSString *filePath = [self saveImage:image imageName:imgName];
                
                AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
                uploadRequest.body = [NSURL fileURLWithPath:filePath];
                uploadRequest.key = imgName;
                NSString *bucketPath = [NSString stringWithFormat:@"%@%@/%@/%@",S3_bUCKET_NAME,@"uploads/photos",yearMonth[0],yearMonth[1]];
                uploadRequest.bucket = bucketPath;
                uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
        
                
                CreatePostModel *model = [[CreatePostModel alloc]init];
                model.attachment = image;
                model.uploadRequest = uploadRequest;
                [arrUploadReq addObject:model];
                
               // [_collectionView reloadData];
                
                
               //Generate Request for Thumbnails
                
               UIImage *thumbImg= [Utils generatePhotoThumbnail:image];
                NSString *thumbFilePath = [self saveThumbnailImage:thumbImg imageName:imgName];
                
                AWSS3TransferManagerUploadRequest *thumbUploadRequest = [AWSS3TransferManagerUploadRequest new];
                thumbUploadRequest.body = [NSURL fileURLWithPath:thumbFilePath];
                thumbUploadRequest.key = imgName;
                NSString *thumbBucketPath = [NSString stringWithFormat:@"%@%@/%@",S3_bUCKET_NAME,@"uploads/photos",@"thumb"];
                thumbUploadRequest.bucket = thumbBucketPath;
                thumbUploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;

                CreatePostModel *thumbModel = [[CreatePostModel alloc]init];
                thumbModel.attachment = thumbImg;
                thumbModel.uploadRequest = thumbUploadRequest;
                [arrThmbnailReq addObject:thumbModel];

            }];
        }
        else{
            PHVideoRequestOptions *options = [PHVideoRequestOptions new];
            options.version = PHVideoRequestOptionsVersionOriginal;
            
            [manager requestPlayerItemForVideo:asset options:options resultHandler:^(AVPlayerItem * _Nullable playerItem, NSDictionary * _Nullable info) {
                
                NSURL *fileURL = [(AVURLAsset *)playerItem.asset URL];
                NSData *videoData = [NSData dataWithContentsOfURL:fileURL];

                 NSString *filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:yearMonth[0]] stringByAppendingPathComponent:yearMonth[1]];
                
                NSError *error = nil;
                if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error]) {
                    NSLog(@"reating 'upload' directory failed: [%@]", error);
                }
                
                NSString *extension = [fileURL pathExtension];
                NSString *videoName = [Utils getVideoName:extension];
                
                filePath = [filePath stringByAppendingPathComponent:videoName];

                [videoData writeToFile:filePath atomically:YES];

                AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
                uploadRequest.body = fileURL;//[NSURL fileURLWithPath:filePath];
                uploadRequest.key = videoName;
                NSString *bucketPath = [NSString stringWithFormat:@"%@%@/%@/%@",S3_bUCKET_NAME,@"uploads/videos",yearMonth[0],yearMonth[1]];
                uploadRequest.bucket = bucketPath;
                uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
                
                [arrUploadReq removeAllObjects];
                [arrThmbnailReq removeAllObjects];
                
                CreatePostModel *model = [[CreatePostModel alloc]init];
                //model.attachment = image;
                model.uploadRequest = uploadRequest;
                [arrUploadReq addObject:model];
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_collectionView reloadData];
                });
            }];
        }
    }
    [_collectionView reloadData];

    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)addDoneButtonToKeyboard
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    
    //UIButton *btnGallery = [UIButton alloc]in
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_down_normal"] style:UIBarButtonItemStyleDone target:self action:@selector(downKeyboard)];
    doneBtn.tintColor = [UIColor grayColor];
    doneBtn.width = 50;

    
    UIBarButtonItem *galleryBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_gallery"] style:UIBarButtonItemStylePlain target:self action:@selector(btnGalleryClicked:)];
    galleryBtn.tintColor = [UIColor grayColor];
    galleryBtn.width = 70;
    
    UIBarButtonItem *videoBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_video"] style:UIBarButtonItemStylePlain target:self action:@selector(btnVideoClicked:)];
    videoBtn.tintColor = [UIColor grayColor];
    videoBtn.width = 70;
    
    UIBarButtonItem *space1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    int maxCharLimit = CREATE_POST_CHARACTER_LIMIT;
    
    if ([UTILS.currentUser.userId isEqualToString:GUO_USERID]) {
        maxCharLimit = CREATE_POST_CHARACTER_LIMIT_BOSS;
    }

    lblCount = [[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"0/%d",maxCharLimit] style:UIBarButtonItemStylePlain target:nil action:nil];
    lblCount.tintColor = [UIColor grayColor];
    lblCount.width = 100;

    numberToolbar.items = [NSArray arrayWithObjects:space1,lblCount,galleryBtn,videoBtn,doneBtn,nil];
    [numberToolbar sizeToFit];
    _txtView.inputAccessoryView = numberToolbar;
}

-(void)downKeyboard
{
    [self.view endEditing:YES];
}
- (IBAction)upKeyboard:(id)sender {
    [_txtView becomeFirstResponder];
}

-(void)clearTmpDirectory
{
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
}

#pragma mark- Button click event
-(IBAction)onclick_viewProfile:(id)sender
{
    [(AppObj).postView showUserProfile:UTILS.currentUser.userId];
}
-(IBAction)onclick_back:(id)sender
{
    /*if (_txtView.text.length > 0 || arrUploadReq.count) {
        [CustomAlertView ShowAlertWithYesNo:NSLocalizedString(@"Discard Post", nil) withMessage:NSLocalizedString(@"Do you want to discard this post?", nil) okHandler:^{
            [self.navigationController popViewControllerAnimated:YES];
           // [self dismissViewControllerAnimated:YES completion:nil];
        }];
        return;
    }*/
    
    isPostWasDiscarded = YES;
    
    //stop all uploads
    [self stopAllUploads];
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
   // [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)stopAllUploads
{
//    AWSS3TransferUtility.default().getUploadTasks()
    
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility S3TransferUtilityForKey:APP_AWS_ACCELERATION_KEY];

    [transferUtility enumerateToAssignBlocksForUploadTask:^(AWSS3TransferUtilityUploadTask * _Nonnull uploadTask, AWSS3TransferUtilityProgressBlock  _Nullable __autoreleasing * _Nullable uploadProgressBlockReference, AWSS3TransferUtilityUploadCompletionHandlerBlock  _Nullable __autoreleasing * _Nullable completionHandlerReference) {
        
        [uploadTask cancel];
        
    } downloadTask:^(AWSS3TransferUtilityDownloadTask * _Nonnull downloadTask, AWSS3TransferUtilityProgressBlock  _Nullable __autoreleasing * _Nullable downloadProgressBlockReference, AWSS3TransferUtilityDownloadCompletionHandlerBlock  _Nullable __autoreleasing * _Nullable completionHandlerReference) {
        
    }];
    
   

}

-(void)onPostSuccess:(PostSuccessBlock)blk
{
    self.block = blk;
}

-(UIImage *)generateVideoThumbImage : (NSURL *)videoURL
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generateImg.appliesPreferredTrackTransform = YES;
    NSError *error = NULL;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    NSLog(@"error==%@, Refimage==%@", error, refImg);
    
    UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
    return FrameImage;
}

-(void)presentImagePicker
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Take Photo", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            //[CustomAlertView showAlert:@"Alert" withMessage:@"Camera not available"];
            return;
        }
        
        [self presentImagePickerController:UIImagePickerControllerSourceTypeCamera mediaTypes:nil];

    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Record Video", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self presentImagePickerController:UIImagePickerControllerSourceTypeCamera mediaTypes:(NSString *) kUTTypeMovie];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"message_dia_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    // Present action sheet.
    [self  presentViewController:actionSheet animated:YES completion:nil];
}

-(void)presentImagePickerController:(UIImagePickerControllerSourceType)sourceType mediaTypes:(NSString*)mediaTypes
{
    UIImagePickerController *pickerController = [UIImagePickerController new];
    pickerController.sourceType = sourceType;
    if (mediaTypes) {
         pickerController.mediaTypes = @[mediaTypes];
    }
   
    pickerController.delegate = self;
    [self presentViewController:pickerController animated:YES completion:^{
        pickerController.navigationBar.tintColor = [UIColor blackColor];
    }];
}

- (void)imagePickerController:(UIImagePickerController *) Picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.view endEditing:YES];
    
    NSArray *yearMonth = [Utils getCurrentYearandMonth];
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([type isEqualToString:(NSString *)kUTTypeVideo] ||
        [type isEqualToString:(NSString *)kUTTypeMovie]) { // movie != video
        
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSString *extension = [videoURL pathExtension];
        NSString *videoName = [Utils getVideoName:extension];
        
        AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
        uploadRequest.body = videoURL;//[NSURL fileURLWithPath:filePath];
        uploadRequest.key = videoName;
        NSString *bucketPath = [NSString stringWithFormat:@"%@%@/%@/%@",S3_bUCKET_NAME,@"uploads/videos",yearMonth[0],yearMonth[1]];
        uploadRequest.bucket = bucketPath;
        uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
        
        [arrUploadReq removeAllObjects];
        [arrThmbnailReq removeAllObjects];
        
        UIImage *thumbImg = [self generateVideoThumbImage:videoURL];
        
        thumbImg = [thumbImg fixOrientation];
        
        [self saveVideoThumbToDirectory:thumbImg];
        
        CreatePostModel *model = [[CreatePostModel alloc]init];
        model.attachment = thumbImg;
        model.uploadRequest = uploadRequest;
        model.type = @"Video";
        [arrUploadReq addObject:model];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_collectionView reloadData];
        });
        
        //Generate thumbnail request from VIDEO URL
        
        AWSS3TransferManagerUploadRequest *thumbUploadRequest = [AWSS3TransferManagerUploadRequest new];
        NSString *thumbName =  [[videoName stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
        
        NSString *filePath = [self saveThumbnailImage:thumbImg imageName:thumbName];
        
        thumbUploadRequest.body = [NSURL fileURLWithPath:filePath];
        thumbUploadRequest.key = thumbName;
        NSString *bucketPath1 = [NSString stringWithFormat:@"%@%@/%@",S3_bUCKET_NAME,@"uploads/photos",@"thumb"];
        thumbUploadRequest.bucket = bucketPath1;
        thumbUploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
        
        CreatePostModel *thumbModel = [[CreatePostModel alloc]init];
        thumbModel.attachment = thumbImg;
        thumbModel.uploadRequest = thumbUploadRequest;
        thumbModel.type = @"Video";
        [arrThmbnailReq addObject:thumbModel];
        // [self amazonS3Upload:videoURL];
    }
    else{
        
        if (arrUploadReq.count == 1) {
            CreatePostModel *model = arrUploadReq[0];
            if ([model.uploadRequest.bucket containsString:@"videos"]) {
                [arrUploadReq removeAllObjects];
                [arrThmbnailReq removeAllObjects];
            }
        }
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        NSString *imgName = [Utils getImageName:@"jpg"];
        
        NSString *filePath = [self saveImage:image imageName:imgName];
        
        AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
        uploadRequest.body = [NSURL fileURLWithPath:filePath];
        uploadRequest.key = imgName;
        NSString *bucketPath = [NSString stringWithFormat:@"%@%@/%@/%@",S3_bUCKET_NAME,@"uploads/photos",yearMonth[0],yearMonth[1]];
        uploadRequest.bucket = bucketPath;
        uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
        
        CreatePostModel *model = [[CreatePostModel alloc]init];
        model.attachment = image;
        model.uploadRequest = uploadRequest;
        [arrUploadReq addObject:model];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_collectionView reloadData];
        });
        
        //Generate Request for Thumbnails
        
        UIImage *img = [UIImage imageWithContentsOfFile:filePath];
        UIImage *thumbImg= [Utils generatePhotoThumbnail:img];
        
        UIImage *thImg;
        if (thumbImg.imageOrientation == UIImageOrientationRight) {
            thImg = [UIImage imageWithCGImage:[thumbImg CGImage] scale:1.0 orientation : UIImageOrientationRight];
        }
        else{
            thImg = thumbImg;
        }
        
        NSString *thumbFilePath = [self saveThumbnailImage:thImg imageName:imgName];
        
        AWSS3TransferManagerUploadRequest *thumbUploadRequest = [AWSS3TransferManagerUploadRequest new];
        thumbUploadRequest.body = [NSURL fileURLWithPath:thumbFilePath];
        thumbUploadRequest.key = imgName;
        NSString *thumbBucketPath = [NSString stringWithFormat:@"%@%@/%@",S3_bUCKET_NAME,@"uploads/photos",@"thumb"];
        thumbUploadRequest.bucket = thumbBucketPath;
        thumbUploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
        
        CreatePostModel *thumbModel = [[CreatePostModel alloc]init];
        thumbModel.attachment = thumbImg;
        thumbModel.uploadRequest = thumbUploadRequest;
        [arrThmbnailReq addObject:thumbModel];
    }
    
    [Picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(NSString *)saveImage:(UIImage *)image imageName:(NSString *)imgName
{
    NSArray *yearMonth = [Utils getCurrentYearandMonth];
    NSString *filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:yearMonth[0]] stringByAppendingPathComponent:yearMonth[1]];
    
    NSError *error = nil;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"reating 'upload' directory failed: [%@]", error);
    }
    filePath = [filePath stringByAppendingPathComponent:imgName];
    
    NSData *pngData = UIImageJPEGRepresentation(image, IMAGE_COMPRESSION);
    
    [pngData writeToFile:filePath atomically:YES];
    
    return filePath;
}

-(NSString *)saveThumbnailImage:(UIImage *)image imageName:(NSString *)imgName
{
    NSArray *yearMonth = [Utils getCurrentYearandMonth];
    NSString *filePath = [[[NSTemporaryDirectory() stringByAppendingPathComponent:yearMonth[0]] stringByAppendingPathComponent:yearMonth[1]] stringByAppendingPathComponent:@"Thumb"];
    
    NSError *error = nil;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"reating 'upload' directory failed: [%@]", error);
    }
    filePath = [filePath stringByAppendingPathComponent:imgName];
    
    NSData *pngData = UIImageJPEGRepresentation(image, 1);
    
    [pngData writeToFile:filePath atomically:YES];
    
    return filePath;
}

-(void)showProgressView
{
    btnClose.hidden = YES;
    
    progressView = [[UploadProgressView alloc] initWithFrame:self.view.frame];
    [self.navigationController.view addSubview:progressView];
    
    
    [progressView enableAutolayout];
    [progressView leadingMargin:0];
    [progressView trailingMargin:0];
    [progressView topMargin:0];
    [progressView bottomMargin:0];
    
    [progressView onCloseClicked:^{
        
        btnClose.hidden = NO;
        [progressView removeFromSuperview];
        //[self cancelAllUploads];
        [self onclick_back:nil];
        
        /*[CustomAlertView ShowAlertWithYesNo:@"" withMessage:NSLocalizedString(@"Do you want to stop uploading process?", nil) okHandler:^{
            btnClose.hidden = NO;
            [progressView removeFromSuperview];
            //[self cancelAllUploads];
            [self onclick_back:nil];
        }];*/
    }];
    
}
#pragma mark - Textview Delegate
-(void)textViewDidChange:(UITextView *)textView
{
    
    int maxCharLimit = CREATE_POST_CHARACTER_LIMIT;
    
    if ([UTILS.currentUser.userId isEqualToString:GUO_USERID]) {
        maxCharLimit = CREATE_POST_CHARACTER_LIMIT_BOSS;
    }
    
    
    NSString *temp = textView.text;
    int titleLength = [[Utils RemoveWhiteSpaceFromText:textView.text] length];
    if (titleLength >= maxCharLimit )
    {
        textView.text = [temp substringToIndex:maxCharLimit];
    }
    if (titleLength <= (maxCharLimit))
    {

        lblCount.title = [NSString stringWithFormat:@"%d/%d",titleLength,maxCharLimit];
    }

}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    int maxCharLimit = CREATE_POST_CHARACTER_LIMIT;
    
    if ([UTILS.currentUser.userId isEqualToString:GUO_USERID]) {
        maxCharLimit = CREATE_POST_CHARACTER_LIMIT_BOSS;
    }
    
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
    }
    else if([[textView text] length] > (maxCharLimit))
    {
        return NO;
    }
  
    return YES;
}

-(void)saveVideoThumbToDirectory:(UIImage *)thumbImg
{
    NSData *imageData = UIImageJPEGRepresentation(thumbImg, 1);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",@"temp_video_thumb"]];
    [imageData writeToFile:imagePath atomically:YES];
}


-(void)presentImageEditor:(UIImage *)image indexPath:(NSIndexPath *)idxPath
{
    CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:image];
    editor.delegate = self;
    editor.indexPath = idxPath;
    [self presentViewController:editor animated:YES completion:nil];
}

-(void)imageEditor:(CLImageEditor *)editor didFinishEditingWithImage:(UIImage *)image
{
    [self dismissViewControllerAnimated:editor completion:nil];
    
    CreatePostModel *model = arrUploadReq[editor.indexPath.row];
    
    model.attachment = image;
    
    [arrUploadReq replaceObjectAtIndex:editor.indexPath.row withObject:model];
    
    [_collectionView reloadData];
    
    NSData *imgData = UIImageJPEGRepresentation(image, IMAGE_COMPRESSION);
    [imgData writeToURL:model.uploadRequest.body atomically:YES];
    
    
    CreatePostModel *thumbModel = arrThmbnailReq[editor.indexPath.row];
    UIImage *thumbImg= [Utils generatePhotoThumbnail:image];
    
    NSData *thumbImgData = UIImageJPEGRepresentation(thumbImg, 1.0);
    [thumbImgData writeToURL:thumbModel.uploadRequest.body atomically:YES];

    

    //imgView.image = image;
}

-(void)addLoader
{
//    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64)];
//
//    bgView.userInteractionEnabled = NO;
//    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color size:50.0f];
//    activityIndicatorView.backgroundColor = [UIColor clearColor];
//    activityIndicatorView.frame = CGRectMake(CGRectGetMidX(bgView.frame)-50, Screen_Height- 100, 100, 50);
//    [bgView addSubview:activityIndicatorView];
//    [activityIndicatorView startAnimating];
//
//    [self.view addSubview:bgView];
}

-(void)dealloc
{
    
    [_collectionView removeObserver:self forKeyPath:@"contentSize"];

}

@end


