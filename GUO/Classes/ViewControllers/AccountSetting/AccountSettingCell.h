//
//  AccountSettingCell.h
//  GUO
//
//  Created by mac on 11/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountSettingCell : UITableViewCell
{
    
}
@property(strong,nonatomic) IBOutlet UIView *outerView;
@property(strong,nonatomic) IBOutlet UILabel *lblTitle;
@property(strong,nonatomic) IBOutlet UIButton *btnStatus;
@property(strong,nonatomic) IBOutlet UIImageView *imgIndicator,*imgDown,*imgStatusDown;
@property(strong,nonatomic) IBOutlet UITextField *txtStatus;
@property (weak, nonatomic) IBOutlet UISwitch *aSwitch;

@end
