//
//  PasswordView.m
//  GUO Media
//
//  Created by Pawan Ramteke on 08/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "PasswordView.h"
#import "TPKeyboardAvoidingScrollView.h"
@interface PasswordView()
{
    UIView *baseView;
    UITextField *txtFieldPassword;
}
@end

@implementation PasswordView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        self.alpha = 0;
        TPKeyboardAvoidingScrollView *baseScroll = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:self.bounds];
        [self addSubview:baseScroll];
        
        baseView = [[UIView alloc]initWithFrame:CGRectMake(20, CGRectGetMidY(baseScroll.frame) - 125, baseScroll.frame.size.width - 40, 250)];
        baseView.backgroundColor = header_color;//[UIColor whiteColor];
        [baseScroll addSubview:baseView];
        
        UILabel *labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, baseView.frame.size.width, 50)];
        labelTitle.font = [UIFont fontWithName:Font_regular size:18];
        labelTitle.text = NSLocalizedString(@"delete_account", nil);
        [baseView addSubview:labelTitle];
        labelTitle.textColor = [UIColor whiteColor];
        labelTitle.backgroundColor = header_color;
        labelTitle.textAlignment = NSTextAlignmentCenter;
        
        UIView *separator = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(labelTitle.frame), baseView.frame.size.width, 0.5)];
        separator.backgroundColor = [UIColor whiteColor];
        [baseView addSubview:separator];
        
        UIButton *btnClose = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(baseView.bounds)-50, 0, 50, 50)];
        [btnClose setImage:[UIImage imageNamed:@"ic_close_white"] forState:UIControlStateNormal];
        [baseView addSubview:btnClose];
        [btnClose addTarget:self action:@selector(hideBaseView) forControlEvents:UIControlEventTouchUpInside];
        
        txtFieldPassword = [[UITextField alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(labelTitle.frame)+30, baseView.frame.size.width - 40, 50)];
        txtFieldPassword.secureTextEntry = YES;
        [baseView addSubview:txtFieldPassword];
        
        UIButton *btnConfirm = [[UIButton alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(txtFieldPassword.frame)+30, baseView.frame.size.width - 40, 50)];
        [btnConfirm addTarget:self action:@selector(btnConFirmClicked) forControlEvents:UIControlEventTouchUpInside];
        [Utils SetButtonProperties:btnConfirm txt:NSLocalizedString(@"Confirm", nil)];
        [baseView addSubview:btnConfirm];
        
        [Utils SetTextFieldProperties:txtFieldPassword placeholder_txt:NSLocalizedString(@"txt_password", nil)];
        
        [self showBaseView];
    }
    return self;
}

-(void)btnConFirmClicked
{
    if (txtFieldPassword.text.length == 0) {
        [CustomAlertView showAlert:@"" withMessage:NSLocalizedString(@"Please Enter Password", nil)];
        return;
    }
    
    if (self.block) {
        self.block(txtFieldPassword.text);
    }
}

-(void)onConfirmClicked:(stringBlock)blk
{
    self.block = blk;
}

-(void)showBaseView
{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1.0;
    }];
}

-(void)hideBaseView
{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
