//
//  PasswordView.h
//  GUO Media
//
//  Created by Pawan Ramteke on 08/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PasswordView : UIView
@property (nonatomic,copy)stringBlock block;

-(void)onConfirmClicked:(stringBlock)blk;
@end
