//
//  AccountSettingViewController.m
//  GUO
//
//  Created by Parag on 6/7/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "AccountSettingViewController.h"
#import "BlockUserListViewController.h"
#import "UIView+DropShadow.h"
#import "PasswordView.h"

@interface AccountSettingViewController ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lblWhoCanSee;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePswd;
@property (weak, nonatomic) IBOutlet UIButton *btnWhoCanSee;
@property (weak, nonatomic) IBOutlet UIButton *btnBlocking;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteAccount;
@property (weak, nonatomic) IBOutlet UIView *viewWhoCanSee;
@property (strong, nonatomic)  UIAlertController *whoCanViewActionSheet;
@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;


@end

@implementation AccountSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = light_gray_bg_color;
    self.aTableView.backgroundColor = light_gray_bg_color;

    if([UTILS.currentUser.userPrivacyMessage isEqualToString:@"public"])
    {
            statusStr = NSLocalizedString(@"who_to_follow_everyone", nil);
    }
    else
    {
        statusStr = NSLocalizedString(@"who_to_follow_follow", nil);
    }
    
    [Utils RemoveTableViewHeaderFooterSpace:self.aTableView];

    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];
    
    self.lblHeader.text =NSLocalizedString(@"account_setting", nil);

    optionsArr = @[NSLocalizedString(@"txt_change_pass", nil),NSLocalizedString(@"txt_who_can_see", nil),NSLocalizedString(@"blocking", nil),NSLocalizedString(@"delete_account", nil),NSLocalizedString(@"title_notification", nil)];
    
    
    // Do any additional setup after loading the view.
}
#pragma mark - Tableview Delegate and Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 8;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerVw= [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 8)];
    headerVw.backgroundColor = [UIColor clearColor];
    return headerVw;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if(indexPath.row==1)
   {
       return 85;
   }
    return 62;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return optionsArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AccountSettingCell *cell=[tableView dequeueReusableCellWithIdentifier:@"AccountSettingCell"];
    if(cell==nil)
    {
        cell=[[AccountSettingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AccountSettingCell"];
        
        NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"AccountSettingCell" owner:self options:nil];
        cell=[menuarray objectAtIndex:0];
    }
  
    cell.lblTitle.text = optionsArr[indexPath.row];
    if(indexPath.row==1)
    {
        cell.aSwitch.hidden = YES;
        cell.outerView.frame = CGRectMake(cell.outerView.frame.origin.x, cell.outerView.frame.origin.y, Screen_Width - (cell.outerView.frame.origin.x*2), 73);
        cell.txtStatus.hidden = NO;
        cell.btnStatus.hidden = NO;
        cell.imgStatusDown.hidden = NO;

        cell.imgIndicator.hidden = YES;
        cell.lblTitle.frame = CGRectMake(cell.lblTitle.frame.origin.x, cell.lblTitle.frame.origin.y-8, cell.lblTitle.frame.size.width ,cell.lblTitle.frame.size.height);

        cell.txtStatus.text = statusStr;
        [cell.btnStatus addTarget:self action:@selector(onclick_Who_can_see:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {

        cell.aSwitch.hidden = indexPath.row == optionsArr.count - 1 ? NO : YES;
        cell.outerView.frame = CGRectMake(cell.outerView.frame.origin.x, cell.outerView.frame.origin.y, Screen_Width - (cell.outerView.frame.origin.x*2), 50);
        cell.txtStatus.hidden = YES;
        cell.btnStatus.hidden = YES;
        cell.imgStatusDown.hidden = YES;
        cell.imgIndicator.hidden = indexPath.row == optionsArr.count - 1 ? YES : NO;
        
        cell.aSwitch.on = [UTILS.currentUser.notificationActivated isEqualToString:@"1"] ? YES : NO;
    }
    cell.outerView.layer.cornerRadius = 5;
    cell.outerView.layer.masksToBounds = YES;

    [Utils dropShadow:cell.outerView];
    cell.selectionStyle = NO;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0) // Chnage pwd
    {
        self.resetpwdVC = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"ResetPasswordController"];
        [self.navigationController pushViewController:self.resetpwdVC animated:YES];

    }
    else if(indexPath.row == 2) // blocking
    {
        self.blockVC = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"BlockUserListViewController"];
        [self.navigationController pushViewController:self.blockVC animated:YES];

    }
    else if(indexPath.row == 3) // delete account
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [CustomAlertView ShowAlertWithYesNo:NSLocalizedString(@"delete_account", nil) withMessage:NSLocalizedString(@"msg_delete_account", nil) okHandler:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showPasswordView];
                });
            }];
        });
    }
}

-(void)showPasswordView
{
    PasswordView *view = [[PasswordView alloc]initWithFrame:self.view.bounds];
    [self.navigationController.view addSubview:view];
    
    [view enableAutolayout];
    [view leadingMargin:0];
    [view trailingMargin:0];
    [view topMargin:0];
    [view bottomMargin:0];
    
    [view onConfirmClicked:^(NSString *value) {
        [self deleteAccountService:value];
    }];
}

-(void)deleteAccountService:(NSString *)password
{
    NSDictionary *param = @{
        @"get":API_DELETE_USER,
        @"user_id": UTILS.currentUser.userId,
        @"username":UTILS.currentUser.userName,
        @"password":password
        };
    
    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_DELETE_USER params:param sBlock:^(id responseObject) {
        [Utils HideProgress];
        [self setLandingAsRoot];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
}

-(void)setLandingAsRoot{
    
    UTILS.currentUser = nil;
    [GUOSettings setIsLogin:nil];
    
    [Utils removeProfilePicPath];

    LandingViewController *navVC = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
    navVC.is_came_from_sliding_menu = false;
    
    (AppObj).navController=[[UINavigationController alloc] initWithRootViewController:navVC];
    (AppObj).navController.navigationBarHidden=YES;
    (AppObj).window.rootViewController = (AppObj).navController;
}

#pragma mark- Button click
-(IBAction)onclick_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onclick_Who_can_see:(UIButton*)sender
{
    __block NSDictionary *params;
    
    

    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"action_sheet_select_one", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"message_dialog_cancel", nil).capitalizedString style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"who_to_follow_everyone", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
            params = @{
                        @"get":API_WHO_CAN_SEND_MSG,
                        @"user_id":UTILS.currentUser.userId,
                        @"user_privacy_message":Select_Everyone
                        
                        };
            
            [UTILS ShowProgress];
            [REMOTE_API CallPOSTWebServiceWithParam:API_WHO_CAN_SEND_MSG params:params sBlock:^(id responseObject) {
                [CustomAlertView showAlert:@"" withMessage:responseObject];
                UserModel *model = UTILS.currentUser;
                model.userPrivacyMessage = @"public";
            [Utils HideProgress];
            
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
            [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        }];

        statusStr = NSLocalizedString(@"who_to_follow_everyone", nil);

        [self.aTableView reloadData];
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"who_to_follow_follow", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        params = @{
                   @"get":API_WHO_CAN_SEND_MSG,
                   @"user_id":UTILS.currentUser.userId,
                   @"user_privacy_message":Select_Follow
                   
                   };
        
        [UTILS ShowProgress];
        [REMOTE_API CallPOSTWebServiceWithParam:API_WHO_CAN_SEND_MSG params:params sBlock:^(id responseObject) {
            [CustomAlertView showAlert:@"" withMessage:responseObject];
            UserModel *model = UTILS.currentUser;
            model.userPrivacyMessage = @"follow";
            [Utils HideProgress];
            
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
            [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        }];

        
        statusStr = NSLocalizedString(@"who_to_follow_follow", nil);
        
        [self.aTableView reloadData];
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];

}
#pragma mark: display views

-(void)displayBlockingView{
//  UIViewController* vc = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"BlockUserListViewController"];
//  [(AppObj).navController pushViewController:vc animated:YES];
    DashboardController *CommonDocumentControllerNav = [(AppObj).monika_storyboard  instantiateViewControllerWithIdentifier:@"BlockUserListViewController"];
    [self.navigationController pushViewController:CommonDocumentControllerNav animated:YES];
  
}





@end
