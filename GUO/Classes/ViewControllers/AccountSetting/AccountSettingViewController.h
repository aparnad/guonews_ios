//
//  AccountSettingViewController.h
//  GUO
//
//  Created by Parag on 6/7/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "BaseViewController.h"
#import "AccountSettingCell.h"
#import "ResetPasswordController.h"
#import "BlockUserListViewController.h"
#import "SlideMenuVC.h"
#import "LoginVC.h"

@interface AccountSettingViewController : BaseViewController
{
    NSArray *optionsArr;
    NSString *statusStr;
}
@property(strong,nonatomic) IBOutlet UITableView *aTableView;
@property(strong,nonatomic) IBOutlet UIView *headerView;
;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property(strong,nonatomic) NSString *header_str;
@property(strong,nonatomic) ResetPasswordController *resetpwdVC;
@property(strong,nonatomic) BlockUserListViewController *blockVC;
-(IBAction)onclick_back:(id)sender;
@end
