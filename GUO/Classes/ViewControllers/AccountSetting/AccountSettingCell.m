//
//  AccountSettingCell.m
//  GUO
//
//  Created by mac on 11/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "AccountSettingCell.h"

@implementation AccountSettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lblTitle setTextColor:[UIColor blackColor]];
  
    [self.lblTitle setFont:[UIFont fontWithName:Font_Medium size:17.0f]];
    self.outerView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = light_gray_bg_color;
    self.outerView.frame = CGRectMake(self.outerView.frame.origin.x, self.outerView.frame.origin.y, Screen_Width - (self.outerView.frame.origin.x*2), self.outerView.frame.size.height);
    
    self.txtStatus.layer.borderWidth = 1;
    self.txtStatus.layer.borderColor = medium_gray_bg_color.CGColor;
    self.txtStatus.layer.masksToBounds = YES;
    
    UIView *paddingView;
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.txtStatus.leftView = paddingView;
    self.txtStatus.leftViewMode = UITextFieldViewModeAlways;
    
   // self.aSwitch.onTintColor = [header_color colorWithAlphaComponent:0.6];
   // self.aSwitch.thumbTintColor = header_color;
    
    [self.aSwitch addTarget:self action:@selector(onSwitchChange:) forControlEvents:UIControlEventValueChanged];
}

-(void)onSwitchChange:(UISwitch *)aSwitch
{
   // aSwitch.on = !aSwitch.on;
    [self apiNotificationActivation:aSwitch.on ? @"1" : @"0"];
}

-(void)apiNotificationActivation:(NSString *)status
{
    NSDictionary *param = @{
                            @"get":API_NOTIFICATION_ACTIVATE,
                            @"me_id":UTILS.currentUser.userId,
                            @"activation_status": status
                            };
    
    [UTILS ShowProgress];

    [REMOTE_API CallPOSTWebServiceWithParam:API_NOTIFICATION_ACTIVATE params:param sBlock:^(id responseObject) {
        if (responseObject) {
            UserModel *model = UTILS.currentUser;
            model.notificationActivated = status;
            UTILS.currentUser = model;
            [GUOSettings setIsLogin:model];
        }
        
        [Utils HideProgress];

        
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
