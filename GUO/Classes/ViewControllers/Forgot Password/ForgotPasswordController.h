//
//  ForgotPasswordController.h
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordController : UIViewController
{
    
}
@property(strong,nonatomic) IBOutlet TPKeyboardAvoidingScrollView *aScrollView;
@property(strong,nonatomic) IBOutlet UITextField *txtEmail;
@property(strong,nonatomic) IBOutlet UIButton *btnSendLink,*btnBackToLogin;

-(IBAction)onclick_back_to_login:(id)sender;
-(IBAction)onclick_send_link:(id)sender;

@property(strong,nonatomic) IBOutlet UILabel *lblSlogan;

@end
