//
//  ForgotPasswordController.m
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ForgotPasswordController.h"

@interface ForgotPasswordController ()

@end

@implementation ForgotPasswordController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialization];

    // Do any additional setup after loading the view from its nib.
}
#pragma mark - Initialization of Data
-(void)initialization
{
    self.view.backgroundColor = view_BGColor;
    
    [Utils SetTextFieldProperties:self.txtEmail placeholder_txt:NSLocalizedString(@"edt_email", nil)];
    
    [Utils SetButtonProperties:self.btnSendLink txt:NSLocalizedString(@"send_link", nil)];
    
    self.lblSlogan.text = NSLocalizedString(@"guo_msg", nil);
    [self.btnBackToLogin setTitle:NSLocalizedString(@"txt_login_now", nil) forState:UIControlStateNormal];
    self.lblSlogan.font = [UIFont fontWithName:Font_Medium size:slogan_font_size];

}
#pragma mark - Button Click
-(IBAction)onclick_back_to_login:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)onclick_send_link:(id)sender
{
//    :get=forget_password&amp;email=cghagre@tiuconsulting.com&amp;query=1234
    if(![self validation])
    {
        //        DashboardController *CommonDocumentControllerNav = [(AppObj).monika_storyboard  instantiateViewControllerWithIdentifier:@"DashboardController"];
        //        [self.navigationController pushViewController:CommonDocumentControllerNav animated:YES];
        
        
        NSDictionary *params = @{
                                 @"get":API_FORGOT_PASSWORD,
                                 @"email":[Utils RemoveWhiteSpaceFromText:self.txtEmail.text]
                                 };
        
        [UTILS ShowProgress];
        [REMOTE_API CallPOSTWebServiceWithParam:API_FORGOT_PASSWORD params:params sBlock:^(id responseObject) {
            
            self.txtEmail.text = nil;
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:NSLocalizedString(@"Alert", nil)
                                         message:NSLocalizedString(@"Pwd sent on email", nil)
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"Ok", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            
                                            [self onclick_back_to_login:self];
                                        }];
            
            
            
            //Add your buttons to alert controller
            
            [alert addAction:yesButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];

            
            [Utils HideProgress];
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
            [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        }];
        
        
    }
}
#pragma mark- validation function
-(BOOL)validation
{
    NSString *str=@"";
    
    if([Utils RemoveWhiteSpaceFromText:self.txtEmail.text].length==0)
    {
        str=[NSString stringWithFormat:@"%@\n%@",str,NSLocalizedString(@"Please Enter Email", nil)];
    }
    else  if(![Utils EmailVerification:[Utils RemoveWhiteSpaceFromText:self.txtEmail.text]])
    {
        str=[NSString stringWithFormat:@"%@\n%@",str,NSLocalizedString(@"Please Enter Valid Email", nil)];
    }
    
    if([str isEqualToString:@""])
    {
        return false;
    }
    else
    {
        
        if([str hasPrefix:@"\n"])
        {
            str=[str substringFromIndex:1];
        }
        [Utils ShowAlert:str];
        
        return true;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
