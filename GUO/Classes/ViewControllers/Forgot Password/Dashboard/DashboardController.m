//
//  DashboardController.m
//  GUO
//
//  Created by mac on 05/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "DashboardController.h"
#import "MenuViewController.h"
#import "GlobalSearchController.h"
#import "ALChatManager.h"
#import <Applozic/Applozic.h>



@interface DashboardController (){
  MenuViewController *menuVC;
}
@end



@implementation DashboardController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
   // [[UIMenuController sharedMenuController] update];
    
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];
    self.view.backgroundColor = view_BGColor;
    
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake([UITabBar appearance].frame.origin.x,[UITabBar appearance].frame.origin.y, [[UIScreen mainScreen] bounds].size.width/4, 56)];
    
    UIImageView *border = [[UIImageView alloc]initWithFrame:CGRectMake(view1.frame.origin.x,view1.frame.size.height-6, [[UIScreen mainScreen] bounds].size.width/4, 6)];
    border.backgroundColor = [UIColor whiteColor];
    [view1 addSubview:border];
    UIImage *img=[self ChangeViewToImage:view1];
    [[UITabBar appearance] setSelectionIndicatorImage:img];

    [self.view addGestureRecognizer:[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureRecognized:)]];

    self.delegate = self;
    
    [self updateTokenAPI];
    if (![ALUserDefaultsHandler isLoggedIn]) {
        [self loginAplozic];
    }
    
    [self getNotificaitonCountService];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(newNotification:) name:NEW_PUSH_NOTIFICATION object:nil];
}



-(void)getNotificaitonCountService
{
    NSDictionary *param = @{
                            @"get":API_NOTIFICATION_UNREAD_COUNT,
                            @"to_user_id":UTILS.currentUser.userId
                            };
    [REMOTE_API CallPOSTWebServiceWithParam:API_NOTIFICATION_UNREAD_COUNT params:param sBlock:^(id responseObject) {
        
        if ([responseObject intValue] > 0) {
            UITabBarItem *tab_bar = [[self.viewControllers objectAtIndex:2] tabBarItem];
            tab_bar.badgeValue = [NSString stringWithFormat:@"%@",responseObject];
        }
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
    }];
    
   
}

-(void)setChatCount
{
    if([self.viewControllers count] > 1){
        ALUserService * alUserService = [[ALUserService alloc] init];
        NSNumber * totalUnreadCount = [alUserService getTotalUnreadCount];
        
        if([totalUnreadCount integerValue] > 0){
            
            UITabBarItem *tab_bar = [[self.viewControllers objectAtIndex:1] tabBarItem];
            tab_bar.badgeValue = [NSString stringWithFormat:@"%ld",(long)[totalUnreadCount integerValue]];
        }
        else
        {
            UITabBarItem *tab_bar = [[self.viewControllers objectAtIndex:1] tabBarItem];
            tab_bar.badgeValue = 0;

        }
    }
}

-(void)newNotification:(NSNotification *)notification
{
    if (self.selectedIndex != 2) {
        [self getNotificaitonCountService];
    }
}


-(void)loginAplozic
{
    if (UTILS.currentUser.userId) {
        
        ALUser *alUser = [[ALUser alloc] initWithUserId:UTILS.currentUser.userId password:UTILS.currentUser.userName email:UTILS.currentUser.userEmail andDisplayName:UTILS.currentUser.userName];
        
        [alUser setAuthenticationTypeId:APPLOZIC];
        [ALUserDefaultsHandler setUserAuthenticationTypeId:APPLOZIC];

        //Saving the details
        [ALUserDefaultsHandler setUserId:alUser.userId];
        [ALUserDefaultsHandler setEmailId:alUser.email];
        [ALUserDefaultsHandler setDisplayName:alUser.displayName];
        [[NSUserDefaults standardUserDefaults] setValue:UTILS.currentUser.userPicture forKey:@"picture"];

        
        //Registering or Loging in the User
        ALChatManager * chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLICATION_ID];
        [chatManager registerUserWithCompletion:alUser withHandler:^(ALRegistrationResponse *rResponse, NSError *error) {
            
            if (!error)
            {
                //Applozic registration successful
            }
            else
            {
                NSLog(@"Error in Applozic registration : %@",error.description);
            }
        }];
    }
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [AppObj GetCountAPI:UTILS.currentUser.userId is_show_progress:true];
    
    //GSSSSSSSSSS
    UTILS.searchText = nil;
    UTILS.apiToCall = nil;
    UTILS.userIdOfVisitedProfile = nil;
    UTILS.isFromGlobalSearch = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setChatCount)
                                                 name:NEW_MESSAGE_NOTIFICATION
                                               object:nil];
    
    [self setChatCount];
    

}
-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NEW_MESSAGE_NOTIFICATION object:nil];
}
#pragma mark- Menu swipe gesture
- (void)swipeGestureRecognized:(UISwipeGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    if(self.frostedViewController.swipeGestureEnabled)
    {
        [self.frostedViewController presentMenuViewController];
    }
}

-(UIImage * ) ChangeViewToImage : (UIView *) viewForImage
{
    UIGraphicsBeginImageContext(viewForImage.bounds.size);
    [viewForImage.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
#pragma mark- Tapbar delegate

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if (tabBarController.selectedIndex && tabBarController.selectedIndex != 2) {
        [self getNotificaitonCountService];
    }
    
}
-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if (viewController == [tabBarController.viewControllers objectAtIndex:3])
    {
        [self pushToGlobalSearchController];
        return NO;
    }
    return YES;

}

-(void)pushToGlobalSearchController
{
    UTILS.isFromGlobalSearch = YES;
    GlobalSearchController *globalSearchVC = [(AppObj).pawan_storyboard  instantiateViewControllerWithIdentifier:@"GlobalSearchController"];
    [self.navigationController pushViewController:globalSearchVC animated:YES];
}

#pragma mark- Button click
-(void)closeDrawer{
    
    [UIView animateWithDuration:0.3 animations:^{
      [menuVC.view setFrame:CGRectMake(-640, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
  }
  
-(IBAction)onclick_logout:(id)sender
{
    UIViewController* rootController = [(AppObj).monika_storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    
    (AppObj).navController=[[UINavigationController alloc] initWithRootViewController:rootController];
    (AppObj).navController.navigationBarHidden=YES;
    (AppObj).window.rootViewController = (AppObj).navController;
}

#pragma mark - Update Push Token
-(void)updateTokenAPI
{
    
    if ([GUOSettings getDeviceToken].length > 0) {
        
        NSDictionary *params = @{
                                 @"get":API_UPDATE_PUSH_TOKEN,
                                 @"user_id":UTILS.currentUser.userId,
                                 @"device_token":[GUOSettings getDeviceToken],
                                 @"device_type":DEVICE_TYPE
                                 };
        
        [REMOTE_API CallPOSTWebServiceWithParam:API_UPDATE_PUSH_TOKEN params:params sBlock:^(id responseObject) {
            
        } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
            [Utils HideProgress];
            // [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
        }];
    }
    
}


@end
