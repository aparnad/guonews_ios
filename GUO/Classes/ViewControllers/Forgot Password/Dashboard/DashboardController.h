//
//  DashboardController.h
//  GUO
//
//  Created by mac on 05/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostController.h"
#import "NotificationController.h"
#import "SearchController.h"

#import "ConnectionController.h"
#import "MessageController.h"

@interface DashboardController : UITabBarController<UITabBarDelegate,UITabBarControllerDelegate>
{
    
}
@property(strong,nonatomic) IBOutlet UIView *headerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property(strong,nonatomic) IBOutlet UITabBarController *tabBarController;

-(IBAction)onclick_menu:(id)sender;
-(IBAction)onclick_logout:(id)sender;

@property(strong,nonatomic) IBOutlet UIViewController *aPostcontroller,*aSearchController,*aNotificationController,*aConnectionController,*aMessageController;
@property(strong,nonatomic) IBOutlet UINavigationController *NavPostcontroller,*NavSearchController,*NavNotificationController,*NavConnectionController,*NavMessageController;
  
  -(void)closeDrawer;
@property BOOL from_login_screen;

@end
