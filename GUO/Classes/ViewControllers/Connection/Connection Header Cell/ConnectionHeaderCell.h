//
//  ConnectionHeaderCell.h
//  GUO
//
//  Created by mac on 07/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConnectionHeaderCell : UICollectionViewCell
{
    
}
@property(strong,nonatomic) IBOutlet UIImageView *imgUser;
@property(strong,nonatomic) IBOutlet UIButton *btnUser;
@end
