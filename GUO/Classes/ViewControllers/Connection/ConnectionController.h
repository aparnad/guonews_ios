//
//  ConnectionController.h
//  GUO
//
//  Created by mac on 05/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectionCell.h"
#import "ConnectionHeaderCell.h"
#import "MoreFollowListController.h"
#import "CommonDocumentController.h"

@interface ConnectionController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSArray *arrUserImg;
    NSMutableArray *ArrMsg;
}
@property(strong,nonatomic) IBOutlet UITableView *aTableView;
@property(strong,nonatomic) IBOutlet UIView *headerView,*viewFollowers;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader,*lblYouFollow,*lblWhoToFollow;
@property(strong,nonatomic) IBOutlet UIButton *btnYouFollow,*btnWhoToFollow,*btnMore;

-(IBAction)onclick_menu:(id)sender;
-(IBAction)onclick_more:(id)sender;


@property(strong,nonatomic) IBOutlet UICollectionView *aCollectionView;
-(void)UpdateLocalisationOnControl;

-(IBAction)onclick_view_profile_GUO:(id)sender;
@property(strong,nonatomic) CommonDocumentController *commonDocNav;
@property(strong,nonatomic) IBOutlet UIButton *btn_header_userimg;
@end
