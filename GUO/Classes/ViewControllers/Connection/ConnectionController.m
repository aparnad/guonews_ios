//
//  ConnectionController.m
//  GUO
//
//  Created by mac on 05/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ConnectionController.h"

@interface ConnectionController ()

@end

@implementation ConnectionController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = light_gray_bg_color;
    self.aTableView.backgroundColor = light_gray_bg_color;
    self.viewFollowers.backgroundColor = light_gray_bg_color;
    self.aCollectionView.backgroundColor = light_gray_bg_color;
    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];

    [Utils RemoveTableViewHeaderFooterSpace:self.aTableView];

    arrUserImg = @[@"7",@"2",@"9",@"10"];
    ArrMsg = [[NSMutableArray alloc]init];
    [ArrMsg addObject:@{@"Name":@"Ronan",
                        @"profileName":@"RonanJocky",
                        @"ImgName":@"1",
                        @"is_selected":@"0"
                        }];
    [ArrMsg addObject:@{@"Name":@"Brayden",
                        @"profileName":@"Braydenhugo",
                        @"ImgName":@"2",
                        @"is_selected":@"0"
                        }];
    [ArrMsg addObject:@{@"Name":@"Hugo",
                        @"profileName":@"hugoSima",
                        @"ImgName":@"3",
                        @"is_selected":@"0"
                        }];
    [ArrMsg addObject:@{@"Name":@"Diego",
                        @"profileName":@"JuoDiego",
                        @"ImgName":@"4",
                        @"is_selected":@"0"
                        }];
    [ArrMsg addObject:@{@"Name":@"Antonio",
                        @"profileName":@"Rs_Antonio",
                        @"ImgName":@"5",
                        @"is_selected":@"0"
                        }];
    [ArrMsg addObject:@{@"Name":@"Marco",
                        @"profileName":@"MarcoRosina",
                        @"ImgName":@"6",
                        @"is_selected":@"0"
                        }];
    [ArrMsg addObject:@{@"Name":@"Steffan",
                        @"profileName":@"SteffanBill",
                        @"ImgName":@"7",
                        @"is_selected":@"0"
                        }];
    [ArrMsg addObject:@{@"Name":@"Devin",
                        @"profileName":@"Devin25Antonio",
                        @"ImgName":@"8",
                        @"is_selected":@"0"
                        }];
    [ArrMsg addObject:@{@"Name":@"Ethan",
                        @"profileName":@"EthanDevin",
                        @"ImgName":@"9",
                        @"is_selected":@"0"
                        }];
    [ArrMsg addObject:@{@"Name":@"Aaron",
                        @"profileName":@"DiegoAaron",
                        @"ImgName":@"10",
                        @"is_selected":@"0"
                        }];

    [self.aCollectionView registerNib:[UINib nibWithNibName:@"ConnectionHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"ConnectionHeaderCell"];
    [self.aCollectionView reloadData];
    
    [self.view addGestureRecognizer:[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureRecognized:)]];
    
   
    
    [self.btnYouFollow.titleLabel setFont:[UIFont fontWithName:Font_Medium size:You_follow_font_size]];
    [self.btnWhoToFollow.titleLabel setFont:[UIFont fontWithName:Font_Medium size:You_follow_font_size]];
    [self.btnMore.titleLabel setFont:[UIFont fontWithName:Font_regular size:more_font_size]];
    [self.btnMore setTitleColor:header_color forState:UIControlStateNormal];
    [Utils SetRoundedCorner:self.btn_header_userimg];
   // self.btn_header_userimg.layer.borderWidth = header_user_img_border;
    //self.btn_header_userimg.layer.borderColor = [UIColor whiteColor].CGColor;

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Update_connection_screen_control" object:nil];//remove single noti
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocalization) name:@"Update_connection_screen_control" object:nil];
    
    
}
-(void)updateLocalization
{
    self.lblHeader.text = NSLocalizedString(@"title_connection", nil);
    [self.btnYouFollow setTitle:NSLocalizedString(@"you_follow", nil) forState:UIControlStateNormal];
    [self.btnWhoToFollow setTitle:NSLocalizedString(@"who_to_follow", nil) forState:UIControlStateNormal];
    [self.btnMore setTitle:NSLocalizedString(@"more", nil) forState:UIControlStateNormal];
    [self.aTableView reloadData];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateLocalization];
}
#pragma mark- Menu swipe gesture
- (void)swipeGestureRecognized:(UISwipeGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    if(self.frostedViewController.swipeGestureEnabled)
    {
        [self.frostedViewController presentMenuViewController];
    }
}

#pragma mark - Number of items in collection view
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrUserImg.count;
}

#pragma mark - Load value in collectionView
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ConnectionHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"ConnectionHeaderCell" forIndexPath:indexPath];
    
    cell.imgUser.image = [UIImage imageNamed:[arrUserImg objectAtIndex:indexPath.row]];


    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cell_size = 55;
    return CGSizeMake(cell_size, cell_size);
}
- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
                        layout:(UICollectionViewLayout *) collectionViewLayout
        insetForSectionAtIndex:(NSInteger) section {
    
    return UIEdgeInsetsMake(10, 10, 10, 10); // top, left, bottom, right
}

//- (CGFloat)collectionView:(UICollectionView *) collectionView
//                   layout:(UICollectionViewLayout *) collectionViewLayout
//minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
//    return 0;
//}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //[(AppObj).nav_home pushViewController:[[lawyer_screen alloc] init] animated:YES];
    
}
    

#pragma mark - Tableview Delegate and Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return self.viewFollowers.frame.size.height;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.viewFollowers;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrMsg.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ConnectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ConnectionCell"];
    if(cell==nil)
    {
        cell=[[ConnectionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ConnectionCell"];
        
        NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"ConnectionCell" owner:self options:nil];
        cell=[menuarray objectAtIndex:0];
    }
    NSDictionary *dic= [ArrMsg objectAtIndex:indexPath.row];
    cell.imgUser.image = [UIImage imageNamed:[dic objectForKey:@"ImgName"]];
    cell.lblName.text = [dic objectForKey:@"Name"];
    cell.lblDesc.text = [NSString stringWithFormat:@"@%@",[dic objectForKey:@"profileName"]];
    cell.btnFollow.tag = indexPath.row;
    [cell.btnFollow addTarget:self action:@selector(onclick_follow:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *status_title = NSLocalizedString(@"btn_unfollow", nil);
    if(![[dic objectForKey:@"is_selected"] boolValue])
    {
        status_title = NSLocalizedString(@"btn_follow", nil);
    }
    [cell.btnFollow setTitle:status_title forState:UIControlStateNormal];
    cell.selectionStyle = NO;

    [cell.btnMore addTarget:self action:@selector(btn_more_option:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
#pragma mark- Button click
-(IBAction)btn_more_option:(UIButton*)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"message_dialog_cancel", nil).capitalizedString style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"block_connection", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
-(IBAction)onclick_follow:(UIButton*)sender
{
    NSMutableDictionary *aDic = [[NSMutableDictionary alloc]initWithDictionary:[ArrMsg objectAtIndex:sender.tag]];
    BOOL get_is_selected_val = [[aDic objectForKey:@"is_selected"] boolValue];
    [aDic setObject:[NSString stringWithFormat:@"%d",!get_is_selected_val] forKey:@"is_selected"];
    [ArrMsg replaceObjectAtIndex:sender.tag withObject:aDic];
    [self.aTableView reloadData];
}
-(IBAction)onclick_menu:(id)sender
{
    [self.frostedViewController.view endEditing:YES];
    [self.frostedViewController presentMenuViewController];
}
-(IBAction)onclick_more:(id)sender;
{
    MoreFollowListController *messageNav = [(AppObj).pawan_storyboard  instantiateViewControllerWithIdentifier:@"MoreFollowListController"];
    messageNav.header_str = NSLocalizedString(@"txt_following", nil);
    messageNav.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:messageNav animated:YES];

}
-(IBAction)onclick_view_profile_GUO:(id)sender
{
   [(AppObj).postView showUserProfile:GUO_USERID];
    
}

#pragma mark- Textfield delegate method
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
