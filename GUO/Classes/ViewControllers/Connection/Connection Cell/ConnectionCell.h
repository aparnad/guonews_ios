//
//  ConnectionCell.h
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FollowModel.h"
#import "FollowModel.h"

typedef void(^followBlock)(FollowModel* f_model);


@interface ConnectionCell : UITableViewCell
{
    
}
@property(strong,nonatomic) IBOutlet UIImageView *imgUser;
@property(strong,nonatomic) IBOutlet UILabel *lblName,*lblDesc,*LblSeparator;
@property(strong,nonatomic) IBOutlet UIButton *btnFollow,*btnMore,*btnBlock,*btnUserImg;
-(void)follow_unfollow_API:(NSString*)API_name model:(FollowModel*)model;

@property(nonatomic,copy) followBlock latestModel;
-(void)updateCommentBlock:(followBlock)blk;

@end
