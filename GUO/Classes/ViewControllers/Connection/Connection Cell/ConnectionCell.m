//
//  ConnectionCell.m
//  GUO
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "ConnectionCell.h"

@implementation ConnectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.LblSeparator.backgroundColor = tbl_separator_color;
    

    [Utils SetRoundedCorner:self.imgUser];
    
    [Utils makeHalfRoundedButton:self.btnFollow];
    self.btnFollow.backgroundColor = header_color;
    self.btnFollow.layer.cornerRadius = 15;
    [self.btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnFollow.layer.masksToBounds = YES;
    self.btnFollow.titleLabel.font = [UIFont fontWithName:Font_Medium size:14];

    
    
    [Utils makeHalfRoundedButton:self.btnBlock];
    [self.btnBlock setTitleColor:header_color forState:UIControlStateNormal];
    self.btnBlock.layer.masksToBounds = YES;

    [self.btnFollow setTitle:NSLocalizedString(@"btn_follow", nil) forState:UIControlStateNormal];

    self.lblName.font = [UIFont fontWithName:Font_Medium size:userfull_name_font_size];
    self.lblDesc.font = [UIFont fontWithName:Font_regular size:username_font_size];

    [self.btnMore setImage:[Utils setTintColorToUIImage:self.btnMore.imageView.image color:header_color] forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)follow_unfollow_API:(NSString*)API_name model:(FollowModel*)model
{
//    if (self.commentBlock) {
//        self.commentBlock(self.indexPath);
//    }
    NSDictionary *params = @{
                             @"get":API_name,
                             @"user_id":UTILS.currentUser.userId,
                             @"following_id":model.userId
                             };

    [UTILS ShowProgress];
    [REMOTE_API CallPOSTWebServiceWithParam:API_name params:params sBlock:^(id responseObject) {
        model.isFollowing=!model.isFollowing;
        if (self.latestModel) {
            self.latestModel(model);
        }
        
        CountModel *model = (AppObj).app_count_model;
        if ([API_name isEqualToString:API_FOLLOW]) {
            model.followings = model.followings + 1;
        }
        else{
            model.followings = model.followings - 1;
        }
        (AppObj).app_count_model = model;
        [Utils HideProgress];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [Utils HideProgress];
       
        
        [CustomAlertView showAlert:@"" withMessage:customErrorMsg];
    }];
    
}
-(void)updateCommentBlock:(followBlock)blk
{
    self.latestModel = blk;
}

@end
