//
//  BroadcastCell.m
//  GUO
//
//  Created by Parag on 6/8/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "BroadcastCell.h"

@implementation BroadcastCell

- (void)awakeFromNib {
    [super awakeFromNib];
  [self.contentView bringSubviewToFront:_playBtn];
    // Initialization code
    self.lblTime.textColor= gray_icon_color;
    self.lblTitle.font = [UIFont fontWithName:Font_Medium size:userfull_name_font_size];
    self.lblTime.font = [UIFont fontWithName:Font_Medium size:username_font_size];
    self.lblSeparator.backgroundColor=tbl_separator_color;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnPlayTouched:(id)sender {
}
  
@end
