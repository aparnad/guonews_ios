//
//  BroadcastViewController.h
//  GUO
//
//  Created by Parag on 6/7/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "BaseViewController.h"

@interface BroadcastViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *broadcastArr;
}
@property(strong,nonatomic) IBOutlet UITableView *aTableView;
@property(strong,nonatomic) IBOutlet UIView *headerView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property(strong,nonatomic) NSString *headerStr;

-(IBAction)onclick_back:(id)sender;

@end
