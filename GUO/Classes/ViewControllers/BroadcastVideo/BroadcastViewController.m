//
//  BroadcastViewController.m
//  GUO
//
//  Created by Parag on 6/7/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "BroadcastViewController.h"
#import "BroadcastCell.h"

@interface BroadcastViewController ()


@end

@implementation BroadcastViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = light_gray_bg_color;
    self.aTableView.backgroundColor = light_gray_bg_color;

    
    [Utils RemoveTableViewHeaderFooterSpace:self.aTableView];

    [Utils SetViewHeader:self.headerView headerLbl:self.lblHeader];
    
    self.lblHeader.text =self.headerStr;

    broadcastArr = [[NSMutableArray alloc]init];
    [broadcastArr addObject:@{@"Name":@"CNBC Video",
                              @"time":@"3 days ago"}];
    [broadcastArr addObject:@{@"Name":@"ABPC Video",
                              @"time":@"10 days ago"}];
    [broadcastArr addObject:@{@"Name":@"CDDC Video",
                              @"time":@"14 days ago"}];
    [broadcastArr addObject:@{@"Name":@"JES Video",
                              @"time":@"23 days ago"}];
  
    self.aTableView.delegate =self;
    self.aTableView.dataSource =self;
    
    // Do any additional setup after loading the view.
}
#pragma mark - Tableview Delegate and Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return broadcastArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BroadcastCell *cell=[tableView dequeueReusableCellWithIdentifier:@"BroadcastCell"];
    if(cell==nil)
    {
        cell=[[BroadcastCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BroadcastCell"];
        
        NSArray *menuarray=[[NSBundle mainBundle]loadNibNamed:@"BroadcastCell" owner:self options:nil];
        cell=[menuarray objectAtIndex:0];
    }
    
    NSDictionary *dic= [broadcastArr objectAtIndex:indexPath.row];
    
    cell.lblTitle.text = [dic objectForKey:@"Name"];
    cell.lblTime.text = [dic objectForKey:@"time"];

    return cell;
}
#pragma mark- Button click


-(IBAction)onclick_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)onclick_more:(id)sender;
{
    
}
#pragma mark- Textfield delegate method
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
