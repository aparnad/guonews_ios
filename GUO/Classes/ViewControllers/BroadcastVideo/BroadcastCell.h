//
//  BroadcastCell.h
//  GUO
//
//  Created by Parag on 6/8/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#define CellIdentifierBroadcast @"CellIdentifierBroadcast"

@interface BroadcastCell : UITableViewCell
{
    
}@property (weak, nonatomic) IBOutlet UILabel *lblTitle,*lblSeparator;
  @property (weak, nonatomic) IBOutlet UILabel *lblTime;
  @property (weak, nonatomic) IBOutlet UIButton *playBtn;
  
@end
