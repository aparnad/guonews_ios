//
//  Utils.m
//  GUO
//
//  Created by mac on 04/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "Utils.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "IDMPhotoBrowser.h"
#import "UIImage+FX.h"
static Utils *viewManager = nil;

@implementation Utils

+ (Utils *)getSharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        viewManager = [[self alloc] init];
    });
    return viewManager;
}

+(void)LoadWebViewURL:(UIWebView *)aWebview URL_Str:(NSString*)URL_Str
{
    NSURL *websiteUrl = [NSURL URLWithString:URL_Str];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [aWebview loadRequest:urlRequest];
    aWebview.scalesPageToFit = true;
}
+(void)RemoveTableViewHeaderFooterSpace:(UITableView*)aTableView
{
    //Set TableView properties
    aTableView.backgroundColor = [UIColor clearColor];
    aTableView.sectionHeaderHeight = 0.0;
    aTableView.sectionFooterHeight = 0.0;
    aTableView.tableHeaderView=[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, aTableView.bounds.size.width, 0.01f)];
    aTableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, aTableView.bounds.size.width, 0.01f)];
    
}
+(void)SetViewHeader:(UIView *)headerview headerLbl:(UILabel*)headerLbl
{
    headerview.backgroundColor = header_color;
    headerLbl.textColor = header_lbl_text_color;
    headerLbl.font = [UIFont fontWithName:Font_Medium size:header_lbl_font_size];
}
#pragma mark- Show Static Alert
+(void)ShowAlert:(NSString*)str
{
    [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Alert", nil) message:str delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil] show];
}
#pragma mark- remove white space
+(NSString*)RemoveWhiteSpaceFromText:(NSString*)str
{
    return [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+(void)SetTextFieldProperties:(UITextField*)txtfield placeholder_txt:(NSString*)placeholder_str
{
    txtfield.textColor = textfield_text_color;
    txtfield.placeholder = placeholder_str;
    [txtfield setFont:[UIFont fontWithName:Font_regular size:textfield_font_size]];
    txtfield.backgroundColor = textfield_bg_color;
    txtfield.layer.borderColor = textfield_border_color.CGColor;
    txtfield.layer.borderWidth = textfield_border_size;
    txtfield.layer.cornerRadius = textfield_corner_radius;
    txtfield.layer.masksToBounds = YES;
    txtfield.tintColor = textfield_text_color;
    
    UIView *paddingView;
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, textfield_left_padding, 20)];
    txtfield.leftView = paddingView;
    txtfield.leftViewMode = UITextFieldViewModeAlways;
    
    [txtfield setValue:textfield_text_color
            forKeyPath:@"_placeholderLabel.textColor"];
}
+(void)SetTextViewProperties:(LPlaceholderTextView*)txtview placeholder_txt:(NSString*)placeholder_str
{
    txtview.textColor = gray_icon_color;
    txtview.placeholderText = placeholder_str;
    txtview.backgroundColor = [UIColor clearColor];
    txtview.layer.borderColor = medium_gray_bg_color.CGColor;
    txtview.layer.borderWidth = textfield_border_size;
    txtview.layer.cornerRadius = textfield_corner_radius;
    txtview.layer.masksToBounds = YES;
    [txtview setFont:[UIFont fontWithName:Font_regular size:textfield_font_size]];

    [txtview setValue:gray_icon_color
            forKeyPath:@"_placeholderLabel.textColor"];

    txtview.tintColor = gray_icon_color;
    
}
+(void)SetButtonProperties:(UIButton*)btn txt:(NSString*)txt
{
    [btn setTitle:txt forState:UIControlStateNormal];
    [btn setTitleColor:header_color forState:UIControlStateNormal];
    
    btn.titleLabel.font = [UIFont fontWithName:Font_Medium size:button_font_size];
    btn.backgroundColor = [UIColor clearColor];
//    btn.layer.borderColor = textfield_border_color.CGColor;
//    btn.layer.borderWidth = textfield_border_size;
    btn.layer.cornerRadius = textfield_corner_radius;
    btn.layer.masksToBounds = YES;
    
    btn.backgroundColor = button_bg_color;
    
}
+(BOOL)EmailVerification:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

+(void)DrawLine:(CGRect)aFrame view:(UIView*)baseView color:(UIColor*)aColor
{
    
    CALayer *border = [CALayer layer];
    border.frame =aFrame;
    border.backgroundColor=aColor.CGColor;
    
    [baseView.layer addSublayer:border];
}
+(void)SetRoundedCorner:(UIView*)aView
{
    aView.layer.cornerRadius = aView.frame.size.height/2;
    aView.layer.masksToBounds = YES;
   // aView.layer.borderColor = header_color.CGColor;
    //aView.layer.borderWidth = textfield_border_size;
    aView.layer.masksToBounds = YES;

}

+(void)SetRoundedCorner:(UIView*)aView widthHeight:(CGFloat)widthHeight
{
    aView.layer.cornerRadius = widthHeight/2;
    aView.layer.masksToBounds = YES;
    //aView.layer.borderColor = header_color.CGColor;
   // aView.layer.borderWidth = textfield_border_size;
    aView.layer.masksToBounds = YES;
    
}
+(UIImage*)setTintColorToUIImage:(UIImage*)image color:(UIColor*)color
{
    UIImage *newImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIGraphicsBeginImageContextWithOptions(image.size, NO, newImage.scale);
    [color set];
    [newImage drawInRect:CGRectMake(0, 0, image.size.width, newImage.size.height)];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(void)SearchContainerDesign:(UITextField *)TxtSearch ContainerView:(UIView*)SearchContainerView
{
    UIImageView *envelopeView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 0, 25, 25)];
    envelopeView.image = [UIImage imageNamed:@"ic_people_search"];
//    envelopeView.image = [Utils setTintColorToUIImage:envelopeView.image color:tbl_separator_color];
    envelopeView.alpha = 1;
    envelopeView.clipsToBounds = YES;
    envelopeView.contentMode = UIViewContentModeScaleAspectFit;
    UIView *test=  [[UIView alloc]initWithFrame:CGRectMake(8, 0, 40, 25)];
    [test addSubview:envelopeView];
    [TxtSearch.leftView setFrame:envelopeView.frame];
    TxtSearch.leftView =test;
    TxtSearch.autocorrectionType = UITextAutocorrectionTypeNo;
    TxtSearch.leftViewMode = UITextFieldViewModeAlways;
    TxtSearch.tintColor = [UIColor blackColor];
    TxtSearch.textColor = [UIColor darkGrayColor];
    TxtSearch.layer.borderWidth = textfield_border_size;
    TxtSearch.layer.borderColor = tbl_separator_color.CGColor;
    [TxtSearch setValue:[UIColor darkGrayColor]
            forKeyPath:@"_placeholderLabel.textColor"];
    TxtSearch.layer.cornerRadius = TxtSearch.frame.size.height/2;
    TxtSearch.layer.masksToBounds = YES;
    TxtSearch.backgroundColor = [UIColor whiteColor];
    [TxtSearch setFont:[UIFont fontWithName:Font_regular size:search_textfield_font_size]];

    //    SearchContainerView.frame=CGRectMake(SearchContainerView.frame.origin.x, SearchContainerView.frame.origin.y, Screen_Width , 46);
    //
    //    TxtSearch.frame=CGRectMake(side_space, 0, Screen_Width - (side_space*2), SearchContainerView.frame.size.height);
    //    [Utils DrawLine:CGRectMake(TxtSearch.frame.origin.x, CGRectGetMaxY(TxtSearch.frame)-2, TxtSearch.frame.size.width, 2) view:SearchContainerView color:headerBgColor];
    //
    //    //    SearchContainerView.clipsToBounds = YES;
    //    SearchContainerView.backgroundColor = [UIColor clearColor];
    //    
}
+(void)makeHalfRoundedButton:(UIButton*)btn
{
    btn.layer.cornerRadius = btn.frame.size.height/2;
    btn.layer.borderWidth = textfield_border_size;
    btn.layer.borderColor = [UIColor blackColor].CGColor;
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:Font_Medium size:button_font_size - 3];

}

-(void)showImagePickerSheetWithPath:(ImageSelectionWithPathBlock)block
{
    self.imgBlkWithPath = block;
    [self presentImagePicker];
}

- (void)showImagePickerSheet:(ImageSelectionBlock)block
{
    self.blk = block;
    [self presentImagePicker];
}

-(void)presentGalleryPicker:(ImageSelectionBlock)block sorceType:(UIImagePickerControllerSourceType)sourceType
{
    self.blk = block;
    [self presentImagePickerController: sourceType];
}

-(void)presentCameraPicker:(ImageSelectionBlock)block
{
    self.blk = block;
    [self presentImagePickerController: UIImagePickerControllerSourceTypeCamera];
}

-(void)presentVideoPicker:(VideoSelectionBlock)block
{
    self.videoBlk = block;
    UIImagePickerController *pickerController = [UIImagePickerController new];
    pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerController.mediaTypes = @[(NSString*)kUTTypeMovie];
    pickerController.delegate = self;
    [[self topMostController] presentViewController:pickerController animated:YES completion:^{
        pickerController.navigationBar.tintColor = [UIColor blackColor];
    }];
}

-(void)presentImagePicker
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Take Photo", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            //[CustomAlertView showAlert:@"Alert" withMessage:@"Camera not available"];
            return;
        }
        
        [self presentImagePickerController: UIImagePickerControllerSourceTypeCamera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Choose From Gallery", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self presentImagePickerController: UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"message_dialog_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    // Present action sheet.
    [[self topMostController]  presentViewController:actionSheet animated:YES completion:nil];
}

-(void)presentImagePickerController:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *pickerController = [UIImagePickerController new];
    pickerController.sourceType = sourceType;
    pickerController.delegate = self;
    [[self topMostController] presentViewController:pickerController animated:YES completion:^{
        pickerController.navigationBar.tintColor = [UIColor blackColor];
    }];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    if ([info[UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
        if (self.videoBlk) {
            self.videoBlk(info[UIImagePickerControllerMediaURL]);
        }
    }
    else{
        UIImage *image = [self resizeImage:info[UIImagePickerControllerOriginalImage] withWidth:200 withHeight:200];
        NSData *imgData= UIImageJPEGRepresentation(image,0.4 /*compressionQuality*/);
        image=[UIImage imageWithData:imgData];
        
        if (self.blk) {
            self.blk(image);
        }
        
        if (self.imgBlkWithPath) {
            self.imgBlkWithPath(image , [self saveImage:image withImageName:@"commentReplyImg"]);
        }
    }
    [[self topMostController] dismissViewControllerAnimated:YES completion:nil];
}

//saving an image

- (NSString *)saveImage:(UIImage*)image withImageName:(NSString*)imageName {
    
    NSData *imageData = UIImagePNGRepresentation(image);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
    NSLog(@"image saved");
    
    return fullPath;
    
}


- (UIViewController *) topMostController
{
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (true)
    {
        if (topViewController.presentedViewController) {
            topViewController = topViewController.presentedViewController;
        } else if ([topViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *nav = (UINavigationController *)topViewController;
            topViewController = nav.topViewController;
        } else if ([topViewController isKindOfClass:[UITabBarController class]]) {
            UITabBarController *tab = (UITabBarController *)topViewController;
            topViewController = tab.selectedViewController;
        } else {
            break;
        }
    }
    
    if ([topViewController isKindOfClass:[REFrostedViewController class]]) {
        id tab =  ((UINavigationController*) ((REFrostedViewController*) topViewController).contentViewController).viewControllers.firstObject;

        if ([tab isKindOfClass:[UITabBarController class]]) {
            topViewController =  ((UITabBarController*)tab).selectedViewController;

            if ([topViewController isKindOfClass:[UINavigationController class]]) {
                topViewController =  ((UINavigationController*)topViewController).viewControllers.firstObject;
            }
        }
    }
    
    return topViewController;
}

- (UIViewController *) topMostControllerNormal
{
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (true)
    {
        if (topViewController.presentedViewController) {
            topViewController = topViewController.presentedViewController;
        } else if ([topViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *nav = (UINavigationController *)topViewController;
            topViewController = nav.topViewController;
        } else if ([topViewController isKindOfClass:[UITabBarController class]]) {
            UITabBarController *tab = (UITabBarController *)topViewController;
            topViewController = tab.selectedViewController;
        } else {
            break;
        }
    }
    
   
    return topViewController;
}

- (UIImage*)resizeImage:(UIImage*)image withWidth:(CGFloat)width withHeight:(CGFloat)height
{
    CGSize newSize = CGSizeMake(width, height);
    CGFloat widthRatio = newSize.width/image.size.width;
    CGFloat heightRatio = newSize.height/image.size.height;
    if(widthRatio > heightRatio)
    {
        newSize=CGSizeMake(image.size.width*heightRatio,image.size.height*heightRatio);
    }
    else
    {
        newSize=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio);
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
-(void)ShowProgress
{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
   // [SVProgressHUD showWithStatus:Please_Wait];
    [SVProgressHUD  show];
    
//    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter,KLCPopupVerticalLayoutCenter);
//    self.activityIndicator1 = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color];
//    self.activityIndicator1.frame = CGRectMake((Screen_Width/2), (Screen_Height/2), 100, 100);
//
//
//    [self.activityIndicator1 startAnimating];
//    KLCPopup *popup = [KLCPopup popupWithContentView:self.activityIndicator1
//                                            showType:KLCPopupShowTypeBounceIn
//                                         dismissType:KLCPopupDismissTypeNone
//                                            maskType:KLCPopupMaskTypeNone
//                            dismissOnBackgroundTouch:NO
//                               dismissOnContentTouch:NO
//                       ];
//
//    [popup showWithLayout:layout];
    
}
+(void)HideProgress
{
    [SVProgressHUD dismiss];
//    [KLCPopup dismissAllPopups];
}
#pragma mark- newtwork reachability
+(BOOL)NetworkRechability
{
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        return false;
    }
    else
    {
        return true;
    }
}
+(void)dropShadow:(UIView *)aView
{
    aView.layer.shadowRadius  = 1.5f;
    aView.layer.shadowColor   = [UIColor colorWithRed:150.0/255.f green:152/255.f blue:154/255.f alpha:0.5].CGColor;
    aView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    aView.layer.shadowOpacity = 0.9f;
    aView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(aView.bounds, shadowInsets)];
    aView.layer.shadowPath    = shadowPath.CGPath;
    
}

-(void)showActionSheet:(NSString *)title optionsArray:(NSArray *)arrData completion:(void(^)(NSInteger index))completion
{
     UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (NSString *str in arrData) {
        [actionSheet addAction:[UIAlertAction actionWithTitle:str style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSInteger index = [actionSheet.actions indexOfObject:action];
            [[self topMostController] dismissViewControllerAnimated:YES completion:^{
            }];
            completion(index);
        }]];
    }
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"message_dialog_cancel", nil).capitalizedString style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [[self topMostController] dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [[self topMostController] presentViewController:actionSheet animated:YES completion:nil];


}
+(NSString*)setFullname:(NSString*)fname lname:(NSString*)lname
{
    if([Utils RemoveWhiteSpaceFromText:fname].length>0 && [Utils RemoveWhiteSpaceFromText:lname].length>0)
    {
        return [NSString stringWithFormat:@"%@ %@",[Utils RemoveWhiteSpaceFromText:fname],[Utils RemoveWhiteSpaceFromText:lname]];
    }
    else
    {
        return [NSString stringWithFormat:@"%@",[Utils RemoveWhiteSpaceFromText:fname]];

    }
}

+(NSString*)setFullname:(NSString*)fname lname:(NSString*)lname  username:(NSString*)username
{
//    if([Utils RemoveWhiteSpaceFromText:fname].length>0 && [Utils RemoveWhiteSpaceFromText:lname].length>0)
//    {
//        return [NSString stringWithFormat:@"%@ %@",[Utils RemoveWhiteSpaceFromText:fname],[Utils RemoveWhiteSpaceFromText:lname]];
//    }
//    else
//    {
//        return [NSString stringWithFormat:@"%@",[Utils RemoveWhiteSpaceFromText:fname]];
//
//    }
    
       return username?[NSString stringWithFormat:@"@%@",username]:@"";

}




+(NSURL *)getProperContentUrl:(NSString *)strUrl
{
    NSURL *url;
    if ([strUrl containsString:@"http"]) {
        url = [NSURL URLWithString:strUrl];
    }
    else{
        strUrl = [NSString stringWithFormat:@"%@%@",HTTPImgURL,strUrl];
        url = [NSURL URLWithString:strUrl];
    }
    
    return url;
}

+(NSURL *)getThumbUrl:(NSString *)strUrl
{
    strUrl = [strUrl lastPathComponent];
    //guomedia_
    if (strUrl && [strUrl hasPrefix:@"guomedia_"]) {
        strUrl = [[strUrl stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
    }
    
    strUrl = [NSString stringWithFormat:@"%@%@",HTTPImgThumbURL,strUrl];
    
    NSURL *url = [NSURL URLWithString:strUrl];

    return url;
}

+(NSArray *)getCurrentYearandMonth
{
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components
    
    return @[@(components.year).stringValue,[NSString stringWithFormat:@"%02ld",(long)components.month]];
}

+(NSString *)getImageName:(NSString *)extension
{
    return [NSString stringWithFormat:@"img_%d_%u.%@",(int)[[NSDate date] timeIntervalSince1970],arc4random() % 99999,extension];
}

+(NSString *)getVideoName:(NSString *)extension
{
    return [NSString stringWithFormat:@"vid_%d_%u.%@",(int)[[NSDate date] timeIntervalSince1970],arc4random() % 99999,extension];

}

+(NSString *)getFormattedDate:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [dateFormatter dateFromString:date];
    
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    NSString *strDate;
    
    NSString *time = [dateFormatter stringFromDate:dateFromString];

    NSTimeInterval secondsBetween = [dateFromString timeIntervalSinceDate:[NSDate date]];
    
    int numberOfDays = secondsBetween / 86400;
    if (numberOfDays == 0) {
        strDate = @"Today";
    }
    else{
        [dateFormatter setDateFormat:@"dd-MMM-YYYY"];
        strDate = [dateFormatter stringFromDate:dateFromString];
    }

    return [NSString stringWithFormat:@"%@ at %@",strDate,time];
}

+(NSString *)GetOnlyDate:(NSString *)dateStr
{
    NSDateFormatter *dateformater=[[NSDateFormatter alloc]init];
    dateformater.dateFormat=@"yyyy-MM-dd HH:mm:ss";
    
    NSDate *date = [dateformater dateFromString:dateStr];
    dateformater.dateFormat=@"dd/MM/YY hh:mm a";
    if([dateformater stringFromDate:date]!=nil)
    {
        return [dateformater stringFromDate:date];
    }
    else
    {
        return @"";
    }
}

-(void)showImageViewer:(NSArray *)arrPhotoModels
{
    NSMutableArray *urlsArr = [NSMutableArray new];
    
    for (Photos *model in arrPhotoModels) {
        IDMPhoto *photo = [IDMPhoto photoWithURL:[Utils getProperContentUrl:model.source]];
        photo.likes = model.likes;
        photo.comments = model.comments;
        [urlsArr addObject:photo];
        
        
    }
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:urlsArr];
    browser.displayActionButton = YES;
    browser.displayCounterLabel = YES;
    browser.autoHideInterface = NO;
    [[self topMostController] presentViewController:browser animated:YES completion:nil];
}

-(void)showMultiImageViewer:(NSArray *)arrPhotoModels currentPageIndex:(NSInteger)index
{
    NSMutableArray *urlsArr = [NSMutableArray new];
    
    for (Photos *photoModel in arrPhotoModels) {
        IDMPhoto *photo = [IDMPhoto photoWithURL:[Utils getProperContentUrl:photoModel.source]];
        [urlsArr addObject:photo];
    }
    [self presentPhotoBrowserWithUrls:urlsArr currentPageIndex:index];
}

-(void)presentPhotoBrowserWithUrls:(NSArray *)arrURLs currentPageIndex:(NSInteger)index
{
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:arrURLs];
    browser.displayActionButton = YES;
    browser.displayCounterLabel = YES;
    browser.autoHideInterface = NO;
    [[self topMostController] presentViewController:browser animated:YES completion:nil];
    [browser setInitialPageIndex:index];
    
}

-(void)showImageViewerFromUserProfile:(NSArray *)arrPhotoModels source:(NSString*)source
{
    NSMutableArray *urlsArr = [NSMutableArray new];
    int selected_idx = 0;
    for(int i = 0;i<arrPhotoModels.count;i++)
    {
        NSDictionary *dic=[arrPhotoModels objectAtIndex:i];
        if([[dic objectForKey:@"key"] isEqualToString:@"photo"])
        {
            Photos *model = [dic objectForKey:@"model"];
            if([model.source isEqualToString:source])
            {
                selected_idx = i;
            }
            IDMPhoto *photo = [IDMPhoto photoWithURL:[Utils getProperContentUrl:model.source]];
            [urlsArr addObject:photo];

        }
    }
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:urlsArr];
    browser.displayActionButton = YES;
    browser.displayCounterLabel = YES;
    browser.autoHideInterface = NO;
  //  browser.selected_idx =selected_idx;
    [[self topMostController] presentViewController:browser animated:YES completion:nil];
    [browser setInitialPageIndex:selected_idx];

}

#pragma mark- Custom popup
- (void)showCustomPopup:(UIView *)view
{
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter,KLCPopupVerticalLayoutCenter);
    popup = [KLCPopup popupWithContentView:view
                                  showType:KLCPopupShowTypeBounceIn
                               dismissType:KLCPopupDismissTypeBounceOut
                                  maskType:KLCPopupMaskTypeDimmed
                  dismissOnBackgroundTouch:YES
                     dismissOnContentTouch:NO
             ];
    
    [popup showWithLayout:layout];
    
}

-(void)addFloating:(UIView*)aView tab_height:(int)tab_height screen_name:(NSString*)screenname
{
    [self.btnFloatView removeFromParentViewController];

    self.btnFloatView = [[FloatingButton alloc]initWithNibName:@"FloatingButton" bundle:nil];
    self.btnFloatView.screenname=screenname;

    self.btnFloatView.view.backgroundColor= [UIColor clearColor];
    
    int margin = 0;
    
    if ([[UIScreen mainScreen] nativeBounds].size.height >= 2436) {
        margin = 34;
    }
    
    self.btnFloatView.view.frame = CGRectMake(Screen_Width - self.btnFloatView.view.frame.size.width - 10, Screen_Height - 50 - margin - self.btnFloatView.view.frame.size.height - 10 - 0, self.btnFloatView.view.frame.size.width, self.btnFloatView.view.frame.size.width);
    //    self.btnFloatView.view.layer.zPosition = -1;
    //    [aView bringSubviewToFront:self.btnFloatView.view];
    [aView addSubview:self.btnFloatView.view];
}

-(void)sharePostToSocial:(NSURL *)url
{
    NSArray* dataToShare = @[url];
    UIActivityViewController* activityViewController =[[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypeAirDrop];
    [[self topMostController] presentViewController:activityViewController animated:YES completion:^{}];
}
-(void)copyLink:(NSString*)post_id
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = [NSString stringWithFormat:@"%@%@",COPY_BASE_URL,post_id];
    [[Toast sharedInstance]makeToast:NSLocalizedString(@"Link Copied", nil) position:TOAST_POSITION_BOTTOM];
}

-(void)showUserProfile:(NSString*)user_id
{
    if (![UTILS checkIntenetShowError]) {
        return;
    }
  
    UserProfileController *userProfileVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"UserProfileController"];
    userProfileVC.other_user_id = user_id;
    [[UTILS topMostController].navigationController pushViewController:userProfileVC animated:YES];
    
}

-(void)showUserProfileSingleController:(NSString*)user_id
{
    if (![UTILS checkIntenetShowError]) {
        return;
    }
    
    UserProfileController *userProfileVC = [(AppObj).pawan_storyboard instantiateViewControllerWithIdentifier:@"UserProfileController"];
    userProfileVC.other_user_id = user_id;
    [[UTILS topMostControllerNormal].navigationController pushViewController:userProfileVC animated:YES];
    
}
+(void)setProfilePic:(UIImage*)newImage pic_name:(NSString*)pic_name
{
    NSData *imageData = UIImagePNGRepresentation(newImage);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",pic_name]];
    
    //    NSLog(@"pre writing to file");
    if (![imageData writeToFile:imagePath atomically:NO])
    {
        //        NSLog(@"Failed to cache image data to disk");
    }
    else
    {
        //        NSLog(@"the cachedImagedPath is %@",imagePath);
    }
}
+(UIImage*)getProfilePic:(NSString*)pic_name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",pic_name]];
    
    NSData *pngData = [NSData dataWithContentsOfFile:imagePath];
    if(pngData == nil)
    {
        return nil;
    }
    UIImage *image = [UIImage imageWithData:pngData];
    return image;
}
+(void)removeProfilePicPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *profilePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",p_name]];
    
    NSString *wallPath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",w_name]];
    
    [fileManager removeItemAtPath:profilePath error:nil];
    [fileManager removeItemAtPath:wallPath error:nil];
    
}

+ (UIImage *)generatePhotoThumbnail:(UIImage *)image
{
    
    
    UIImage *croppedImg = [image imageScaledToFitSize:CGSizeMake(400, 200)];
    
  //  NSData *data = UIImageJPEGRepresentation(croppedImg, 0.7);
   // croppedImg = [UIImage imageWithData:data];
    //NSData *data = UIImageJPEGRepresentation(croppedImg, 0.2);
    //croppedImg = [UIImage imageWithData:data];
    return croppedImg;
    
    // Create a thumbnail version of the image for the event object.
    CGSize size = image.size;
    CGSize croppedSize;
    CGFloat WidthRatio = 400.0;
    CGFloat HeightRatio = 250.0;
    CGFloat offsetX = 0.0;
    CGFloat offsetY = 0.0;
    
    // check the size of the image, we want to make it
    // a square with sides the size of the smallest dimension
    if (size.width > size.height) {
        offsetX = (size.height - size.width);
        croppedSize = CGSizeMake(size.height, size.height);
    } else {
        offsetY = (size.width - size.height);
        croppedSize = CGSizeMake(size.width, size.width);
    }
    
    // Crop the image before resize
    CGRect clippedRect = CGRectMake(offsetX * -1, offsetY * -1, croppedSize.width, croppedSize.height);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], clippedRect);
    // Done cropping
    
    // Resize the image
    CGRect rect = CGRectMake(0.0, 0.0, WidthRatio, HeightRatio);
    
    UIGraphicsBeginImageContext(rect.size);
    [[UIImage imageWithCGImage:imageRef] drawInRect:rect];
    UIImage *thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return thumbnail;
}

+(UIImage *) imageWithSize:(UIImage *)image size:(CGSize)size
{
    CGRect scaledImageRect = CGRectZero;
    
    CGFloat aspectWidth = size.width / image.size.width;
    CGFloat aspectHeight = size.height / image.size.height;
    CGFloat aspectRatio = MIN(aspectWidth, aspectHeight);
    
    scaledImageRect.size.width = image.size.width * aspectRatio;
    scaledImageRect.size.height = image.size.height * aspectRatio;
    scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0;
    scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0;
    
    UIGraphicsBeginImageContextWithOptions(size, false, 0);
    
    [image drawInRect:scaledImageRect];
    
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}


+(NSString*)suffixNumber:(NSString*)number
{
    double value = [number doubleValue];
    NSUInteger index = 0;
    NSArray *suffixArray = @[@"", @"K", @"M", @"B", @"T", @"P", @"E"];
    
    while ((value/1000) >= 1){
        value = value/1000;
        index++;
    }
    
    //3 line of code below for round doubles to 1 digit
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setMaximumFractionDigits:2];
    NSString *valueWith1Digit = [fmt stringFromNumber:[NSNumber numberWithFloat:value]];
    
    NSString *svalue = [NSString stringWithFormat:@"%@%@",valueWith1Digit, [suffixArray objectAtIndex:index]];
    return svalue;
}

-(void)setUserLanguageOnServer
{
    
    
    NSString *lan = @"en_us";
    
    if([[GUOSettings GetLanguage] isEqualToString:SetChinese])
    {
        lan = @"zh_cn";
    }
    
//    NSDictionary *params = @{
//                             @"get":API_SET_LANGUAGE,
//                             @"lan":lan
//
//                             };
    
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    [params setValue:API_SET_LANGUAGE forKey:@"get"];
    [params setValue:lan forKey:@"lan"];
    if (UTILS.currentUser.userId.length > 0) {
        [params setValue:UTILS.currentUser.userId forKey:@"user_id"];
    }

    [REMOTE_API CallPOSTWebServiceWithParam:API_SET_LANGUAGE params:params sBlock:^(id responseObject) {
        
        if (UTILS.currentUser.userId.length > 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Update_notification_screen_list" object:nil userInfo:nil];

        }

    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
      
    }];
}


- (CGFloat)getFlexibleHeight:(CGFloat)height
{
    /*CGFloat flexibleheight;
    flexibleheight = height * ([[UIScreen mainScreen] bounds].size.width/CONST_PAGE_WIDTH);
    return flexibleheight;
     */
    
    return height;
}

#pragma mark - Network Connection
-(BOOL)connectedToInternet
{
    
    BOOL status = NO;
    
    Reachability *_reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable) {
        // not reachable
        status = NO;
    } else if (remoteHostStatus == ReachableViaWiFi) {
        // reachable via Wifi
        status = YES;
    } else if (remoteHostStatus == ReachableViaWWAN) {
        // reachable via WWAN
        status = YES;
    }
    
    return status;
}

-(BOOL)checkIntenetShowError
{
    BOOL status = [self connectedToInternet];
    NSString *errorMsg = NSLocalizedString(@"No Internet Connectivity", nil);

    if (!status) {
        //[CustomAlertView showAlert:@"" withMessage:errorMsg];
        [[Toast sharedInstance]makeToast:errorMsg position:TOAST_POSITION_BOTTOM];

    }
    return status;
}

#pragma mark - Sound Play
-(void)playSoundOnEndRefresh
{
   /* NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"refresh" ofType:@"mp3"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL,
                                     &self.pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);*/
    
    
    /*NSString *path = [[NSBundle mainBundle] pathForResource:@"refresh" ofType:@"mp3"];
    NSError *error = nil;
    NSURL *url = [NSURL fileURLWithPath:path];
 AVAudioPlayer *myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    myAudioPlayer.numberOfLoops = 1; //infinite loop
    [myAudioPlayer play];
     */
    
    AudioServicesPlaySystemSound (1003); // SMSReceived (see SystemSoundID below)


    
    
}

#pragma mark- Show Network Indicator
//-(KLCPopup *)ShowNetworkIndicator
//{
//    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter,KLCPopupVerticalLayoutCenter);
//    self.activityIndicator = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeThreeDots tintColor:header_color];
//    self.activityIndicator.frame = CGRectMake((Screen_Width/2), (Screen_Height/2), 100, 100);
//
//    
//    [self.activityIndicator startAnimating];
//    KLCPopup *popup = [KLCPopup popupWithContentView:self.activityIndicator
//                                            showType:KLCPopupShowTypeBounceIn
//                                         dismissType:KLCPopupDismissTypeBounceOutToBottom
//                                            maskType:KLCPopupMaskTypeNone
//                            dismissOnBackgroundTouch:NO
//                               dismissOnContentTouch:NO
//                       ];
//    
//    [popup showWithLayout:layout];
//    return popup;
//}
@end
