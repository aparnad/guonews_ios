//
//  ShareHandler.h
//  GUO Media
//
//  Created by Pawan Ramteke on 06/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareHandler : NSObject
+(void)shareExtension;
@end
