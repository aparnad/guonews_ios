//
//  CustomAlertView.h
//  TourApp
//
//  Created by IOS_DEV on 23/02/17.
//  Algro Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIView

+ (void)showAlert:(NSString*)title withMessage:(NSString*)message;
+ (void)ShowAlert:(NSString*)title withMessage:(NSString*)msg tapHandler:(void(^)(void))handler;
+ (void)ShowAlertWithCancel:(NSString*)title withMessage:(NSString*)msg okHandler:(void(^)(void))handler;

+ (void)ShowAlertWithYesNo:(NSString*)title withMessage:(NSString*)msg okHandler:(void(^)(void))handler;

+(void)loginAlertForGuest:(void(^)(void))handler;

+(void)showNoNetworkAvailableAlert;
@end
