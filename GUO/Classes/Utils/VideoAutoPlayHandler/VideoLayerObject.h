//
//  VideoLayerObject.h
//  VideoAutoplayDemo
//
//  Created by Pawan Ramteke on 07/09/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
@interface VideoLayerObject : NSObject
@property (nonatomic,strong)AVPlayerLayer *layer;
@property (nonatomic,assign)BOOL used;

-(instancetype)init;
@end

@interface VideoLayers : NSObject
@property (nonatomic,strong)NSMutableArray *layers;
@end
