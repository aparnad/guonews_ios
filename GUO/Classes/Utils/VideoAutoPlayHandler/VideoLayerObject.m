//
//  VideoLayerObject.m
//  VideoAutoplayDemo
//
//  Created by Pawan Ramteke on 07/09/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "VideoLayerObject.h"

@implementation VideoLayerObject
-(instancetype)init
{
    self = [super init];
    if (self) {
        self.layer = [[AVPlayerLayer alloc]init];
        self.layer.backgroundColor = [UIColor clearColor].CGColor;
        self.layer.videoGravity = AVLayerVideoGravityResize;
    }
    return self;
}


@end

@implementation VideoLayers
-(instancetype)init
{
    self = [super init];
    if (self) {
        
        self.layers = [NSMutableArray arrayWithObject:[VideoLayerObject new]];
        
    }
    return self;
}

-(AVPlayerLayer *)getLayerForParentLayer:(CALayer *)parentLayer
{
    for (VideoLayerObject *videoObject in self.layers) {
        if (videoObject.layer.superlayer == parentLayer) {
            return videoObject.layer;
        }
    }
    
    return [self getFreeVideoLayer];
}

-(AVPlayerLayer *)getFreeVideoLayer
{
    for (VideoLayerObject *videoObject in self.layers) {
        if (videoObject.used == NO) {
            videoObject.used = YES;
            return videoObject.layer;
        }
    }
    return self.layers[0];
}

-(void)freeLayer:(AVPlayerLayer *)layerToFree
{
    for (VideoLayerObject *videoObject in self.layers) {
        if (videoObject.layer == layerToFree) {
            videoObject.used = NO;
            videoObject.layer.player = nil;
            if (videoObject.layer.superlayer != nil) {
                [videoObject.layer removeFromSuperlayer];
            }
            break;
        }
    }
}

@end
