//
//  VideoContainer.m
//  VideoAutoplayDemo
//
//  Created by Pawan Ramteke on 07/09/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "VideoContainer.h"
#import "VideoPlayerController.h"
@interface VideoContainer ()
{
    
}
@end


@implementation VideoContainer

-(id)initWithPlayer:(AVPlayer *)aPlayer item:(AVPlayerItem *)item url:(NSString *)aUrl
{
    self = [super init];
    if (self) {
        self.player = aPlayer;
        self.playerItem = item;
        self.url = aUrl;
        self.isPlayOn = NO;
    }
    return self;
}

-(void)setPlayOn:(BOOL)playOn
{
    NSLog(@"%@",[NSNumber numberWithBool:playOn]);
    self.isPlayOn = playOn;
    //self.player.isMuted = [VideoPlayerController getSharedInstance].mu
    self.playerItem.preferredPeakBitRate = [VideoPlayerController getSharedInstance].preferredPeakBitRate;
    if (self.isPlayOn && self.playerItem.status == AVPlayerItemStatusReadyToPlay) {
        [self.player play];
    }
    else{
        [self.player pause];
    }
    
}
@end
