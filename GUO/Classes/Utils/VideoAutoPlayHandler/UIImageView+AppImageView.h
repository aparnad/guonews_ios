//
//  UIImageView+AppImageView.h
//  VideoAutoplayDemo
//
//  Created by Pawan Ramteke on 08/09/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (AppImageView)
@property (nonatomic,strong)NSString *imageURL;
@end
