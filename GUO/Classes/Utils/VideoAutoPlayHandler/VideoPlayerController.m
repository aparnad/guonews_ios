//
//  VideoPlayerController.m
//  VideoAutoplayDemo
//
//  Created by Pawan Ramteke on 07/09/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "VideoPlayerController.h"
#import "VideoLayerObject.h"
#import "VideoContainer.h"


@interface VideoPlayerController()<NSCacheDelegate>
{
    CGFloat minimumLayerHeightToPlay;
    // Mute unmute video
    BOOL mute;
    int playerViewControllerKVOContext;
    //video url for currently playing video
    NSString *videoURL;
    /**
     Stores video url as key and true as value when player item associated to the url
     is being observed for its status change.
     Helps in removing observers for player items that are not being played.
     */
    NSMutableDictionary *observingURLs;
    // Cache of player and player item
    NSCache *videoCache;
    VideoLayers *videoLayers;
    // Current AVPlapyerLayer that is playing video
    AVPlayerLayer *currentLayer;
    PostsTableCell *postCell;
    ThanksAmericaCell *taPostCell;
}
@end

static VideoPlayerController *sharedVideoPlayer = nil;

@implementation VideoPlayerController

+ (VideoPlayerController *)getSharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedVideoPlayer = [[self alloc] init];
    });
    return sharedVideoPlayer;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        minimumLayerHeightToPlay = 60;
        mute = NO;
        _preferredPeakBitRate = 1000000;
        playerViewControllerKVOContext = 0;
        videoLayers = [[VideoLayers alloc]init];
        videoCache = [[NSCache alloc]init];
        observingURLs = [NSMutableDictionary new];
    }
    return self;
}

-(void)setupVideoFor:(NSString *)url postCell:(PostsTableCell *)cell
{
    if ([videoCache objectForKey:url] != nil) {
        return;
    }
    
    NSURL *URL = [NSURL URLWithString:url];
    if (!URL) {
        return;
    }
    
    postCell = cell;
    AVURLAsset *asset = [AVURLAsset assetWithURL:URL];
    NSArray *requestedKeys =                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           @[@"playable"];
    
    [asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:^{
        
        NSError *error;
        AVKeyValueStatus status = [asset statusOfValueForKey:@"playable" error:&error];
        switch (status) {
            case AVKeyValueStatusLoaded:
                break;
            case AVKeyValueStatusFailed:AVKeyValueStatusCancelled:
                return;
            default:
                break;
        }
        
        AVPlayer *player = [[AVPlayer alloc]init];
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
        dispatch_async(dispatch_get_main_queue(), ^{
            VideoContainer *videoContainer = [[VideoContainer alloc]initWithPlayer:player item:item url:url];
            [videoCache setObject:videoContainer forKey:url];
            [videoContainer.player replaceCurrentItemWithPlayerItem:videoContainer.playerItem];
            
            if (videoURL == url && currentLayer) {
                [self playVideoWithLayer:currentLayer url:url];
            }
        });
        
    }];
}


-(void)playVideoWithLayer:(AVPlayerLayer *)layer url:(NSString *)url
{
    videoURL = url;
    currentLayer = layer;
    
    VideoContainer *videoContainer = [videoCache objectForKey:url];
    if (videoContainer) {
        layer.player = videoContainer.player;
        videoContainer.playOn = YES;
        [self addObservers:url videoContainer:videoContainer];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (videoContainer.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
                videoContainer.playOn = YES;
            }
        });
    }
}

-(void)pauseVideoforLayer:(AVPlayerLayer *)layer url:(NSString *)url
{
    videoURL = nil;
    currentLayer = nil;
    VideoContainer *videoContainer = [videoCache objectForKey:url];
    if (videoContainer) {
        videoContainer.playOn = NO;
        [self removeObserverFor:url];
    }
}

-(void)removeLayerFor:(PostsTableCell *)cell
{
    if (cell.videoURL) {
        [self removeFromSuperLayer:cell.videoLayer url:cell.videoURL];
    }
}

-(void)removeFromSuperLayer:(AVPlayerLayer *)layer url:(NSString *)url
{
    videoURL = nil;
    currentLayer = nil;
    VideoContainer *videoContainer = [videoCache objectForKey:url];
    if (videoContainer) {
        videoContainer.playOn = NO;
        [self removeObserverFor:url];
    }
    layer.player = nil;
}

-(void)pauseRemoveLayer:(AVPlayerLayer *)layer url:(NSString *)url layerHeight:(CGFloat)layerHeight
{
    [self pauseVideoforLayer:layer url:url];
}


-(void)playerDidFinishPlaying:(NSNotification *)note
{
    AVPlayerItem *playerItem = note.object;
    AVPlayer *currentPlayer = [self currentVideoContainer].player;
    if (playerItem && currentPlayer) {
        return;
    }
    
    AVPlayerItem *currentItem = currentPlayer.currentItem;
    if (currentItem && currentItem == playerItem) {
        [currentPlayer seekToTime:kCMTimeZero];
        [currentPlayer play];
    }
}

-(VideoContainer *)currentVideoContainer
{
    if (videoURL) {
        VideoContainer *videoContainer = [videoCache objectForKey:videoURL];
        return videoContainer;
    }
    return nil;
}


-(void)addObservers:(NSString *)url videoContainer:(VideoContainer *)videoContainer
{
    if ([observingURLs[url] boolValue] == NO || observingURLs[url] == nil) {
        [videoContainer.player.currentItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:&playerViewControllerKVOContext];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:videoContainer.player.currentItem];
        [observingURLs setObject:[NSNumber numberWithBool:YES] forKey:url];
    }
    
}

-(void)removeObserverFor:(NSString *)url
{
    VideoContainer *videoContainer = [videoCache objectForKey:url];
    AVPlayerItem *currentItem = videoContainer.player.currentItem;
    
    if (videoContainer && currentItem && [[observingURLs objectForKey:url] boolValue] == YES) {
        @try {
            [currentItem removeObserver:self forKeyPath:@"status" context:&playerViewControllerKVOContext];
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:currentItem];
        [observingURLs setObject:[NSNumber numberWithBool:NO] forKey:url];
    }
}

-(void)pausePlayeVideosFor:(UITableView *)tableView appEnteredFromBackground:(BOOL)appEnteredFromBackground
{
    NSArray *visisbleCells = [tableView visibleCells];
    PostsTableCell *videoCellContainer;
    CGFloat maxHeight = 0;
    
    for (UITableViewCell *cellView in visisbleCells) {
        PostsTableCell *containerCell = (PostsTableCell *)cellView;
        NSString *videoCellURL = containerCell.videoURL;
        if (!videoCellURL) {
            continue;
        }
        CGFloat height = [containerCell visibleVideoHeight];
        if (maxHeight < height) {
            maxHeight = height;
            videoCellContainer = containerCell;
        }
        [self pauseRemoveLayer:containerCell.videoLayer url:videoCellURL layerHeight:height];
    }
    
    if (!videoCellContainer.videoURL) {
        return;
    }
    
    CGFloat minCellLayerHeight = videoCellContainer.videoLayer.bounds.size.height * 0.5;
    
    CGFloat minimumVideoLayerVisibleHeight = MAX(minCellLayerHeight, minimumLayerHeightToPlay);
    
    if (maxHeight > minimumVideoLayerVisibleHeight) {
        if (appEnteredFromBackground) {
            [self setupVideoFor:videoCellContainer.videoURL postCell:postCell];
        }
        
        [self playVideoWithLayer:videoCellContainer.videoLayer url:videoCellContainer.videoURL];
    }
}

// Set observing urls false when objects are removed from cache

-(void)cache:(NSCache *)cache willEvictObject:(id)obj
{
    VideoContainer *videoObject = obj;
    
    [observingURLs setObject:[NSNumber numberWithBool:NO] forKey:videoObject.url];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (context == &playerViewControllerKVOContext) {
        
    }
    else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        return;
    }
    
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerItemStatus newStatusAsNumber = [change[NSKeyValueChangeNewKey] intValue];
        if (newStatusAsNumber && newStatusAsNumber == AVPlayerItemStatusReadyToPlay) {
            AVPlayerItem *item = object;
            
            VideoContainer *currentVideoContainer = [self currentVideoContainer];
            
            AVPlayerItem *currentItem = currentVideoContainer.player.currentItem;
            if (!currentItem) {
                return;
            }
            
            if (item == currentItem && currentVideoContainer.isPlayOn == YES) {
                currentVideoContainer.playOn = YES;
            }
        }
        else{
            newStatusAsNumber = AVPlayerItemStatusUnknown;
        }
    }
}



@end
