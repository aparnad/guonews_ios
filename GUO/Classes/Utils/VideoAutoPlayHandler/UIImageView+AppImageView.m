//
//  UIImageView+AppImageView.m
//  VideoAutoplayDemo
//
//  Created by Pawan Ramteke on 08/09/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "UIImageView+AppImageView.h"
#import <objc/runtime.h>

static NSCache *imageCache = nil;
static char STRING_KEY;
@implementation UIImageView (AppImageView)
@dynamic imageURL;


-(void)setImageURL:(NSString *)imageURL
{
    if (!imageURL) {
        objc_setAssociatedObject(self, &STRING_KEY, imageURL, OBJC_ASSOCIATION_RETAIN);
        self.image = nil;
        return;
    }
    
    objc_setAssociatedObject(self, &STRING_KEY, imageURL, OBJC_ASSOCIATION_RETAIN);
    
    UIImage *image = [imageCache objectForKey:@(imageURL.hash).stringValue];
    if (image) {
        self.image = image;
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
        if (!image) {
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [imageCache setObject:image forKey:imageURL];
            
            CATransition *animation = [[CATransition alloc]init];
            animation.type = kCATransitionFade;
            animation.duration = 0.3;
            [self.layer addAnimation:animation forKey:@"transition"];
            self.image = image;
        });
    });

}

-(NSString *)imageURL
{
    return (NSString *)objc_getAssociatedObject(self, &STRING_KEY);
}

@end
