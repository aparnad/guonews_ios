//
//  VideoContainer.h
//  VideoAutoplayDemo
//
//  Created by Pawan Ramteke on 07/09/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VideoContainer : NSObject
@property (nonatomic,strong) NSString *url;
@property (nonatomic,assign) BOOL playOn;
@property (nonatomic,assign) BOOL isPlayOn;
@property (nonatomic,strong) AVPlayer *player;
@property (nonatomic,strong) AVPlayerItem *playerItem;

-(id)initWithPlayer:(AVPlayer *)aPlayer item:(AVPlayerItem *)item url:(NSString *)aUrl;
@end
