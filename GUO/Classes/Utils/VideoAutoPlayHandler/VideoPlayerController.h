//
//  VideoPlayerController.h
//  VideoAutoplayDemo
//
//  Created by Pawan Ramteke on 07/09/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "PostsTableCell.h"
@class ThanksAmericaCell;
@protocol AutoPlayVideoLayerContainer
@property (nonatomic,strong)NSString *videoURL;
@property (nonatomic,strong)AVPlayerLayer *videoLayer;
-(CGFloat)visibleVideoHeight;
@end


@interface VideoPlayerController : NSObject
@property (nonatomic,assign) double preferredPeakBitRate;

+ (VideoPlayerController *)getSharedInstance;
-(void)pausePlayeVideosFor:(UITableView *)tableView appEnteredFromBackground:(BOOL)appEnteredFromBackground;
//-(void)setupVideoFor:(NSString *)url;
-(void)setupVideoFor:(NSString *)url postCell:(PostsTableCell *)cell;
//-(void)removeLayerFor:(AutoPlayVideoLayerContainer *)cell;
-(void)removeFromSuperLayer:(AVPlayerLayer *)layer url:(NSString *)url;
-(void)pauseVideoforLayer:(AVPlayerLayer *)layer url:(NSString *)url;
-(void)pausePlayeVideosForTA:(UICollectionView *)collectionView appEnteredFromBackground:(BOOL)appEnteredFromBackground;
-(void)playVideoWithLayer:(AVPlayerLayer *)layer url:(NSString *)url;
-(void)pauseRemoveLayer:(AVPlayerLayer *)layer url:(NSString *)url layerHeight:(CGFloat)layerHeight;
@end


