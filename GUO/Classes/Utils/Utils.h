//
//  Utils.h
//  GUO
//
//  Created by mac on 04/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LPlaceholderTextView.h"
#import "UserModel.h"
#import "UIImageView+AFNetworking.h"
#import "DGActivityIndicatorView.h"

typedef void(^ImageSelectionBlock)(UIImage *image);
typedef void(^VideoSelectionBlock)(NSString *path);
typedef void(^ImageSelectionWithPathBlock)(UIImage *image,NSString *imgPath);

@interface Utils : NSObject<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    KLCPopup *popup;

}
@property(nonatomic,copy)ImageSelectionBlock blk;
@property(nonatomic,copy)VideoSelectionBlock videoBlk;
@property(nonatomic,copy)ImageSelectionWithPathBlock imgBlkWithPath;



@property(nonatomic,strong) UserModel *currentUser;
@property(nonatomic,strong) NSString *userIdOfVisitedProfile;
@property(nonatomic,strong) NSString *apiToCall;
@property(nonatomic,strong) NSString *searchType;
@property(nonatomic,strong) NSString *searchText;
@property(nonatomic,assign) BOOL isFromGlobalSearch;
@property(nonatomic,strong) NSDictionary *apnsDictionary;


@property(strong,nonatomic)DGActivityIndicatorView *activityIndicator1;


-(void)showImagePickerSheet:(ImageSelectionBlock)block;
-(void)presentGalleryPicker:(ImageSelectionBlock)block sorceType:(UIImagePickerControllerSourceType)sourceType;
-(void)presentCameraPicker:(ImageSelectionBlock)block;
-(void)presentVideoPicker:(VideoSelectionBlock)block;
-(void)showImagePickerSheetWithPath:(ImageSelectionWithPathBlock)block;

+(void)SetTextFieldProperties:(UITextField*)txtfield placeholder_txt:(NSString*)placeholder_str;
+(void)SetButtonProperties:(UIButton*)btn txt:(NSString*)txt;
+(BOOL)EmailVerification:(NSString *)emailStr;
+(void)DrawLine:(CGRect)aFrame view:(UIView*)baseView color:(UIColor*)aColor;
+(void)SetRoundedCorner:(UIView*)aView;
+(UIImage*)setTintColorToUIImage:(UIImage*)image color:(UIColor*)color;
+(void)RemoveTableViewHeaderFooterSpace:(UITableView*)aTableView;
+(void)SetViewHeader:(UIView *)headerview headerLbl:(UILabel*)headerLbl;
+(void)SearchContainerDesign:(UITextField *)TxtSearch ContainerView:(UIView*)SearchContainerView;
+(void)makeHalfRoundedButton:(UIButton*)btn;
+(void)SetTextViewProperties:(LPlaceholderTextView*)txtview placeholder_txt:(NSString*)placeholder_str;
+(void)LoadWebViewURL:(UIWebView *)aWebview URL_Str:(NSString*)URL_Str;
+(void)ShowAlert:(NSString*)str;
#pragma mark- remove white space
+(NSString*)RemoveWhiteSpaceFromText:(NSString*)str;
+ (Utils *)getSharedInstance;
-(void)ShowProgress;
+(void)HideProgress;
+(BOOL)NetworkRechability;
+(void)dropShadow:(UIView *)aView;

- (UIViewController *) topMostController;

-(void)showActionSheet:(NSString *)title optionsArray:(NSArray *)arrData completion:(void(^)(NSInteger index))completion;

+(NSString*)setFullname:(NSString*)fname lname:(NSString*)lname;
+(NSArray *)getCurrentYearandMonth;
+(NSString *)getImageName:(NSString *)extension;
+(NSString *)getVideoName:(NSString *)extension;
+(NSURL *)getProperContentUrl:(NSString *)strUrl;
+(NSString *)getFormattedDate:(NSString *)date;
+(NSString *)GetOnlyDate:(NSString *)dateStr;

-(void)showImageViewer:(NSArray *)arrPhotoModels;
-(void)showMultiImageViewer:(NSArray *)arrPhotoModels currentPageIndex:(NSInteger)index;
-(void)openUserProfile:(NSString*)userid;
-(void)showImageViewerFromUserProfile:(NSArray *)arrPhotoModels source:(NSString*)source;


- (void)showCustomPopup:(UIView *)view;
-(void)addFloating:(UIView*)aView tab_height:(int)tab_height screen_name:(NSString*)screenname;

@property(strong,nonatomic) FloatingButton *btnFloatView;

-(void)sharePostToSocial:(NSURL *)url;
-(void)copyLink:(NSString*)post_id;
+(NSString*)setFullname:(NSString*)fname lname:(NSString*)lname  username:(NSString*)username;
-(void)showUserProfile:(NSString*)user_id;

+(void)setProfilePic:(UIImage*)newImage pic_name:(NSString*)pic_name;
+(UIImage*)getProfilePic:(NSString*)pic_name;
+(void)removeProfilePicPath;
+ (UIImage *)generatePhotoThumbnail:(UIImage *)image;
+(NSURL *)getThumbUrl:(NSString *)strUrl;
+(NSString*)suffixNumber:(NSString*)number;
-(void)setUserLanguageOnServer;
- (UIViewController *) topMostControllerNormal;
- (CGFloat)getFlexibleHeight:(CGFloat)height;
+(void)SetRoundedCorner:(UIView*)aView widthHeight:(CGFloat)widthHeight;

-(BOOL)connectedToInternet;
-(BOOL)checkIntenetShowError;
-(void)showUserProfileSingleController:(NSString*)user_id;
-(void)playSoundOnEndRefresh;

- (NSString *)saveImage:(UIImage*)image withImageName:(NSString*)imageName;
//-(KLCPopup *)ShowNetworkIndicator;

@end
