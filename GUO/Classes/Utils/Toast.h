//
//  Toast.h
//  ROBS
//
//  Created by Pawan Ramteke on 28/06/18.
//  Copyright © 2018 Deepti W. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    TOAST_POSITION_TOP,
    TOAST_POSITION_CENTER,
    TOAST_POSITION_BOTTOM,
} position;

@interface Toast : UIView

+(Toast *)sharedInstance;
-(void)makeToast:(NSString *)message position:(position)position;

@end
