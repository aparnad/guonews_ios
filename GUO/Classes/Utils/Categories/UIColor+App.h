//
//  UIColor+App.h
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (App)
+(UIColor *)themeColor;

+(UIColor *)controllerBGColor;
@end
