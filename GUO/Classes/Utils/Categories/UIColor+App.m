//
//  UIColor+App.m
//  GUO
//
//  Created by Pawan Ramteke on 10/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "UIColor+App.h"

@implementation UIColor (App)
+(UIColor *)themeColor
{
    return ColorRGB(229, 32, 39);
}

+(UIColor *)controllerBGColor
{
    return ColorRGB(236, 236, 236);

}
@end
