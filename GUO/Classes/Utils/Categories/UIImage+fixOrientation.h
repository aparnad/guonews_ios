//
//  UIImage+fixOrientation.h
//  GUO Media
//
//  Created by Amol Hirkane on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
