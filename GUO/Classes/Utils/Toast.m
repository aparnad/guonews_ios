//
//  Toast.m
//  ROBS
//
//  Created by Pawan Ramteke on 28/06/18.
//  Copyright © 2018 Deepti W. All rights reserved.
//

#import "Toast.h"
#import "UIView+Autolayout.h"

@implementation Toast

+(Toast *)sharedInstance{
    Toast *sharedInstance=[[Toast alloc]initWithFrame:[UIScreen mainScreen].bounds];
    sharedInstance.userInteractionEnabled = NO;
    return sharedInstance;
}
-(void)makeToast:(NSString *)message position:(position)position
{
    [self createToast:message position:position];
    
    self.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 delay:1.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
}

-(void)createToast:(NSString *)message position:(position)position
{
    [[UTILS topMostControllerNormal].view addSubview:self];
    self.backgroundColor = header_color;
    
    [self enableAutolayout];
    [self centerX];
    [self fixWidth:Screen_Width *0.9];
    
    if (position == TOAST_POSITION_TOP) {
        [self topMargin:50];
    }
    else if (position == TOAST_POSITION_CENTER){
        [self centerY];
    }
    else{
        [self bottomMargin:70];
    }
    
    UILabel *lblMessage = [[UILabel alloc]init];
    lblMessage.font = [UIFont systemFontOfSize:18];
    lblMessage.text = message;
    lblMessage.textColor = [UIColor whiteColor];
    lblMessage.numberOfLines = 0;
    lblMessage.textAlignment = NSTextAlignmentCenter;
    [self addSubview:lblMessage];
    
    [lblMessage enableAutolayout];
    [lblMessage leadingMargin:10];
    [lblMessage topMargin:10];
    [lblMessage bottomMargin:10];
    [lblMessage trailingMargin:10];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.layer.cornerRadius = self.frame.size.height/2;
}

@end
