//
//  CustomAlertView.m
//  TourApp
//
//  Created by IOS_DEV on 23/02/17.
//  Algro Inc. All rights reserved.
//

#import "CustomAlertView.h"


@implementation CustomAlertView

+ (void)showAlert:(NSString*)title withMessage:(NSString*)message
{
    if(message!=nil)
    {
        
        //Check for error message condition
        
        NSString *errorMsg = NSLocalizedString(@"No Internet Connectivity", nil);

        
        if ([message isEqualToString:errorMsg]) {
           // [[Toast sharedInstance] makeToast:message position:TOAST_POSITION_BOTTOM];

            //do nothing dont show any error message

        }
        else{
        
            UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [UTILS.topMostController presentViewController:alertController animated:YES completion:nil];
        }
    }
}

+ (void)ShowAlert:(NSString*)title withMessage:(NSString*)msg tapHandler:(void(^)(void))handler
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler)
        {
            handler();
        }
    }];
    [alertController addAction:ok];
    
    [UTILS.topMostController presentViewController:alertController animated:YES completion:nil];
}

+ (void)ShowAlertWithCancel:(NSString*)title withMessage:(NSString*)msg okHandler:(void(^)(void))handler
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler)
        {
            handler();
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"message_dialog_cancel", nil)  style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    [UTILS.topMostController presentViewController:alertController animated:YES completion:nil];
}

+ (void)ShowAlertWithYesNo:(NSString*)title withMessage:(NSString*)msg okHandler:(void(^)(void))handler
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"message_dialog_yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler){
            handler();
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"message_dialog_no", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    [UTILS.topMostController presentViewController:alertController animated:YES completion:nil];
}

+(void)loginAlertForGuest:(void(^)(void))handler
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please Login to Continue" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler) {
            handler();
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Later" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    [UTILS.topMostController presentViewController:alertController animated:YES completion:nil];
}

+(void)showNoNetworkAvailableAlert
{
    [self showAlert:@"" withMessage:@"Please check internet connection and try again."];
}

@end
