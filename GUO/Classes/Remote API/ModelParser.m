
#import "ModelParser.h"
#import "DataModels.h"

@implementation ModelParser

+(id)parseModelWithAPIType:(NSString *)apiURL responseData:(NSDictionary*)responseData
{
//    if ([apiURL isEqualToString:API_SIGNUP]) {
//        return [responseData valueForKey:@"content"];
//    }
//    else
    if ([apiURL isEqualToString:API_LOGIN] || [apiURL isEqualToString:API_SIGNUP] || [apiURL isEqualToString:API_GET_USER_PROFILE] || [apiURL isEqualToString:API_EDIT_PROFILE_FIELD] || [apiURL isEqualToString:API_EDIT_PROFILE_PIC]) {
        return [self parseLoginData:responseData];
    }
    else if ([apiURL isEqualToString:API_FOLLOWINGS_LIST] || [apiURL isEqualToString:API_FOLLOWERS_LIST] || [apiURL isEqualToString:API_GLOBAL_SEARCH_PEOPLE]) {
        return [self parseFollowList:responseData];
    }
    else if ([apiURL isEqualToString:API_GET_COUNT]) {
        return [self parseCountData:responseData];
    }
    else if ([apiURL isEqualToString:API_BLOCK_USER_LIST]) {
        return [self parseBlockUserList:responseData];
    }
    else if ([apiURL isEqualToString:API_GET_POST] || [apiURL isEqualToString:API_SEARCH] || [apiURL isEqualToString:API_LIKED_POST]) {
        return [self parsePostList:responseData];
    }
    
    else if ([apiURL isEqualToString:API_ADD_POST]  || [apiURL isEqualToString:API_POST_DETAILS_WITH_POST_MODEL]){
        return [self parseAddPostData:responseData];
    }
    
    else if ([apiURL isEqualToString:API_POST_DETAILS]){
        return [self parsePostDetailsData:responseData];

    }
    else if ([apiURL isEqualToString:API_GET_NOTIFICATION]){
        return [self parseNotificationList:responseData];
        
    }
    
    else if ([apiURL isEqualToString:API_COMMENT_IDENTIFICATION]){
        return [self parseCommentReplyData:responseData];
    }

    else if ([apiURL isEqualToString:API_SHARE_POST]){
        return [self parseSharePostData:responseData];
    }
    
    else if ([apiURL isEqualToString:API_NOTIFICATION_UNREAD_COUNT])
    {
        return responseData[@"unread_count"];
    }
    else if ([apiURL isEqualToString:API_TRANSLATE_TEXT])
    {
        return responseData;
    }
    
    return nil;
}

+(id)parseLoginData:(NSDictionary *)responseData
{
   // [GUOSettings setLoginResponse:responseData[@"Restaurant"]];
    UserModel *loginModel = [UserModel modelObjectWithDictionary:responseData];
    return loginModel;
}
+(id)parseNotificationList:(NSDictionary *)responseData
{
    NSMutableArray *arrToSend = [NSMutableArray new];
    for (NSDictionary *dict in responseData) {
        NotificationModel *model = [NotificationModel modelObjectWithDictionary:dict];
        [arrToSend addObject:model];
    }
    return arrToSend;
}
+(id)parsePostList:(NSDictionary *)responseData
{
    
    NSMutableArray *arrToSend = [NSMutableArray new];
    for (NSDictionary *dict in responseData) {
       
        PostModel *model = [PostModel modelObjectWithDictionary:dict];
        [arrToSend addObject:model];
    }
    return arrToSend;
}
+(id)parseBlockUserList:(NSDictionary *)responseData
{
    NSMutableArray *arrToSend = [NSMutableArray new];
    for (NSDictionary *dict in responseData) {
        UserModel *model = [UserModel modelObjectWithDictionary:dict];
        [arrToSend addObject:model];
    }
    return arrToSend;
}
+(id)parseFollowList:(NSDictionary *)responseData
{
    NSMutableArray *arrToSend = [NSMutableArray new];
    for (NSDictionary *dict in responseData) {
        FollowModel *model = [FollowModel modelObjectWithDictionary:dict];
        [arrToSend addObject:model];
    }
    return arrToSend;
}
+(id)parseCountData:(NSDictionary *)responseData
{
    CountModel *countModel = [CountModel modelObjectWithDictionary:responseData];
    return countModel;
}

+(PostModel *)parseAddPostData:(NSDictionary *)responseData
{
    PostModel *model = [PostModel modelObjectWithDictionary:responseData];
    return model;
}

+(PostDetailsModel *)parsePostDetailsData:(NSDictionary *)responseData
{
    PostDetailsModel *model = [PostDetailsModel modelObjectWithDictionary:responseData];
    return model;
}

+(CommentReplies *)parseCommentReplyData:(NSDictionary *)responseData
{
    CommentReplies *model = [CommentReplies modelObjectWithDictionary:responseData];
    return model;
}

+(PostModel *)parseSharePostData:(NSDictionary *)responseData
{
    PostModel *model = [PostModel modelObjectWithDictionary:responseData];
    return model;
}

@end
