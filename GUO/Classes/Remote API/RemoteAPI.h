//
//  RemoteAPI.h
//  GUO Media
//
//  Created by Pawan Ramteke on 16/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "CallbackTypes.h"

//Network error code
#define NO_INTERNET_CONNECTION  -1009

//Server Error code
#define UNAUTHORIZED_ACCESS     401

@interface RemoteAPI : NSObject

@property (nonatomic,strong)AFHTTPSessionManager *sessionManager;
@property (nonatomic,strong)AFHTTPRequestOperationManager *operationManager;


- (void)CallGETWebServiceWithParam:(NSString*)apiUrl params:(NSDictionary*)params sBlock:(SUCCESSBLOCK)sBlock fBlock:(FAILUREBLOCK)fBlock;

-(void)CallPOSTWebServiceWithParam:(NSString*)apiUrl  params:(NSDictionary*)params sBlock:(SUCCESSBLOCK)sBlock   fBlock:(FAILUREBLOCK)fBlock;
-(void)testAPI;
@end
