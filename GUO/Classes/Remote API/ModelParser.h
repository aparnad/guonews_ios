

#import <Foundation/Foundation.h>

@interface ModelParser : NSObject

+(id)parseModelWithAPIType:(NSString *)apiURL responseData:(NSDictionary*)responseData;


@end
