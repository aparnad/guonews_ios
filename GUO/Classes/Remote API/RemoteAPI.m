//
//  RemoteAPI.m
//  GUO Media
//
//  Created by Pawan Ramteke on 16/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import "RemoteAPI.h"
#import "config.h"

#import "ModelParser.h"
#import "AFNetworking.h"



@implementation RemoteAPI


-(id)init
{
    self = [super init];
    if (self){
        
        
        self.operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        
        self.operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        //[self.operationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-//Type"];
        //[self.operationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];


        
        
        
        
        self.sessionManager = [[AFHTTPSessionManager alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        
       // [self.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //[self.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
    }
    return self;
}

-(void)testAPI
{
    
   /* NSDictionary *param = @{
                            @"get":@"login",
                            @"username":@"chetanghagre1",
                            @"password":@"chetan2006",
                            @"query":@"1234"
                            };
    
    self.operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [self.operationManager POST:@"api.php" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
    
         NSString* newStr = [NSString stringWithUTF8String:[responseObject bytes]];

         NSLog(@"%@",newStr);
        // sBlock([ModelParser parseModelWithAPIType:apiUrl responseData:responseObject]);
         
     } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
         
         NSLog(@"%@",error);
         //handle all other error seperately
        // [self handleErrors:operation error:error fBlock:fBlock];
     }];
    */
    
}

/*-(void)CallPOSTWebServiceWithParam:(NSString *)apiUrl params:(NSDictionary *)params sBlock:(SUCCESSBLOCK)sBlock fBlock:(FAILUREBLOCK)fBlock
{
    
    NSMutableDictionary *finalParam = [NSMutableDictionary dictionaryWithDictionary:params];
    [finalParam setValue:API_QUERY_KEY forKey:@"query"];
    
   
    
    NSError *error;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:finalParam options:0 error:&error];
    
    
    NSURL *URL = [NSURL URLWithString:BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPBody:postData];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 60.0;
    sessionConfig.timeoutIntervalForResource = 60.0;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error)
                                  {
                                      // ...
                                      
                                      dispatch_sync(dispatch_get_main_queue(), ^{
                                          
                                          
                                          if (error) {
                                              [self handleErrors:task error:error fBlock:fBlock];
                                              return ;
                                          }
                                          
                                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                                          NSLog(@"%@",json);
                                          if (![json[@"status"] isEqualToString:@"SUCCESS"]) {
                                              
                                              fBlock(json[@"message"],0);
                                              return;
                                          }
                                         
                                         
                                              if ([apiUrl isEqualToString:API_FORGOT_PASSWORD] || [apiUrl isEqualToString:API_CHANGE_PASSWORD] || [apiUrl isEqualToString:API_WHO_CAN_SEND_MSG] || [apiUrl isEqualToString:API_UNBLOCK_USER] || [apiUrl isEqualToString:API_FOLLOW] || [apiUrl isEqualToString:API_UNFOLLOW] || [apiUrl isEqualToString:API_SHARE_POST] || [apiUrl isEqualToString:API_COMMENT_IDENTIFICATION] || [apiUrl isEqualToString:API_DELETE_POST]) {
                                                  sBlock(json[@"message"]);
                                              }
                                              else{
                                                  sBlock([ModelParser parseModelWithAPIType:apiUrl responseData:json[@"response"]]);
                                                  
                                              }
                                     
                                      });
                                      
                                  }];
    
    [task resume];
    
}*/


-(void)CallPOSTWebServiceWithParam:(NSString *)apiUrl params:(NSDictionary *)params sBlock:(SUCCESSBLOCK)sBlock fBlock:(FAILUREBLOCK)fBlock
    {
        
        NSMutableDictionary *finalParam = [NSMutableDictionary dictionaryWithDictionary:params];
        [finalParam setValue:API_QUERY_KEY forKey:@"query"];
        
    //    if([[GUOSettings GetLanguage] isEqualToString:SetEnglish])
    //    {
    //         [finalParam setValue:@"en_us" forKey:@"lan"];
    //    }
    //    else  if([[GUOSettings GetLanguage] isEqualToString:SetChinese])
    //    {
    //        [finalParam setValue:@"zh_cn" forKey:@"lan"];
    //    }
      
        self.operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        [self.operationManager POST:@"api.php" parameters:finalParam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSLog(@"LoginfinalParam==%@",finalParam);
             NSLog(@"operationManager==%@",_operationManager);

             
             NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            // NSString* newStr = [NSString stringWithUTF8String:[responseObject bytes]];
             
             if (([json[@"status"] caseInsensitiveCompare:@"SUCCESS"] != NSOrderedSame)) {
                 
                 fBlock(json[@"message"],0);
                 return;
             }
             
             if ([apiUrl isEqualToString:API_FORGOT_PASSWORD] || [apiUrl isEqualToString:API_CHANGE_PASSWORD] || [apiUrl isEqualToString:API_WHO_CAN_SEND_MSG] || [apiUrl isEqualToString:API_UNBLOCK_USER] || [apiUrl isEqualToString:API_FOLLOW] || [apiUrl isEqualToString:API_UNFOLLOW] || [apiUrl isEqualToString:API_COMMENT_IDENTIFICATION] || [apiUrl isEqualToString:API_DELETE_POST] || [apiUrl isEqualToString:API_VERIFIED_PROFILE] || [apiUrl isEqualToString:API_UNLIKE_COMMENT] || [apiUrl isEqualToString:API_LIKE_COMMENT] || [apiUrl isEqualToString:API_LOGOUT] || [apiUrl isEqualToString:API_UPDATE_PUSH_TOKEN]  || [apiUrl isEqualToString:API_SET_LANGUAGE] || [apiUrl isEqualToString:API_MUTE_POST] || [apiUrl isEqualToString:API_SUPPORT] || [apiUrl isEqualToString:API_DELETE_COMMENT] || [apiUrl isEqualToString:API_NOTIFICATION_ACTIVATE]) {
                 sBlock(json[@"message"]);
             }
             else{
                 
                 if ([json[@"response"] isEqual:[NSNull  null]]) {
                     sBlock(nil);
                 }
                 else{
                     sBlock([ModelParser parseModelWithAPIType:apiUrl responseData:json[@"response"]]);
                 }
                 
             }
             
           //  NSLog(@"%@",newStr);
             // sBlock([ModelParser parseModelWithAPIType:apiUrl responseData:responseObject]);
             
         } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
             
             NSLog(@"%@",error);
             [self handleErrors:nil error:error fBlock:fBlock];
         }];

        
    }

- (void)handleErrors:(NSURLSessionDataTask *)operation  error:(NSError*)error     fBlock:(FAILUREBLOCK)fBlock
{
    //check first network errors like internet connection
//    NSString *errorMsg = NSLocalizedString(@"NoInternet", nil);
    NSString *errorMsg = NSLocalizedString(@"No Internet Connectivity", nil);

    
    NSInteger errorCode = 0;
    if(error.code == NO_INTERNET_CONNECTION)
    {
       // errorMsg =  error.userInfo[NSLocalizedDescriptionKey];
        errorCode = NO_INTERNET_CONNECTION;
    }
    else
    {
//        //Some other error occured from server
//        switch (error.code)
//        {
//            case UNAUTHORIZED_ACCESS:
//            {
//                errorMsg = @"Your internet connection is slow. Please try again.";
//                errorCode = UNAUTHORIZED_ACCESS;
//            }
//                break;
//
////            default:
////            {
////                errorMsg = @"Internal Server Error";
////            }
//                break;
//        }
    }
    fBlock(errorMsg,errorCode);
}


@end

