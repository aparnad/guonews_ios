//
//  TranslatePopUp.h
//  GUO Media
//
//  Created by Pawan Ramteke on 16/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^commentBlock)(PostModel *model);

@interface TranslatePopUp : UIView

-(instancetype)initWithFrame:(CGRect)frame model:(PostModel *)postModel translatedText:(NSString *)text;

@property (nonatomic,assign)BOOL isComment;
@property (nonatomic,copy)commentBlock updatedPost;
-(void)updatePostModel:(commentBlock)blk;
@end
