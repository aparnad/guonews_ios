//
//  AppDelegate.h
//  GUO
//
//  Created by mac on 04/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KLCPopup.h"
#import "REFrostedViewController.h"
#import "SlideMenuVC.h"
#import "DashboardController.h"
#import "CountModel.h"
#import "PostController.h"
#import "FloatingButton.h"
#import <UserNotifications/UserNotifications.h> 

@interface AppDelegate : UIResponder <UIApplicationDelegate,REFrostedViewControllerDelegate,UNUserNotificationCenterDelegate>
{

}
@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic) UINavigationController *navController;
@property (strong,nonatomic) UIStoryboard *monika_storyboard;
@property (strong,nonatomic) UIStoryboard *pawan_storyboard;
@property (strong,nonatomic) CountModel *app_count_model;

  -(void)displayLandingVC;

@property(strong,nonatomic) REFrostedViewController *frostedViewController;
-(void)LoadSlidingMenu:(BOOL)Anim_val;
-(void)loadNewsSlidingMenu:(BOOL)Anim_val;

@property NSIndexPath *currIndexPath;
@property PostController *postView;
@property NSString *golbalSearchScreenOpen;
@property BOOL is_first_time_add_observer,is_first_time_add_observer_people;
@property(strong,nonatomic,nonnull) UIImage *selectedPopupImg;

-(void)GetCountAPI:(NSString*)userId is_show_progress:(BOOL)is_show_progress;

@end

