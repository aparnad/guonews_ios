//
//  WebViewController.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 08/02/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {
    
    var webUrl:String?
    
    var webView: WKWebView!
    
    lazy var activityIndicator: UIActivityIndicatorView = {
      let  activityIndicator = UIActivityIndicatorView()
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        return activityIndicator
    }()

    
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    
    private func loadWebView(){
        guard let urlString = webUrl else { return }
        guard let url = URL(string: urlString) else { return }
        let urlRequest = URLRequest(url: url)
        webView.load(urlRequest)
    }
    
    private func setNavigationBar(){
        let revealIcon = UIImage(named:"back_Img")?.withRenderingMode(.alwaysOriginal)
        let backButtonItem = UIBarButtonItem(image: revealIcon, style: .plain, target: self, action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem = backButtonItem
    }
    
    @objc private func backButtonTapped(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false

        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadWebView()
        self.setNavigationBar()
        
        self.view.addSubview(activityIndicator)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      //  activityIndicator.stopAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false

        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
     //   activityIndicator.startAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
       // activityIndicator.stopAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    //    Toast

    }
}
