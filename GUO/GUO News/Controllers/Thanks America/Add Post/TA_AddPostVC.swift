//
//  PopOverVC.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 17/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol TA_AddPostVCDelegate: class {
    func fetchResponse(toStart:Int)
}

class TA_AddPostVC: UIViewController{

    @IBOutlet var addPostView: UIView!
    @IBOutlet var addPostViewBottomHeight: NSLayoutConstraint!
    
    @IBOutlet var uploadView: UIView!
    @IBOutlet var uploadViewHeight: NSLayoutConstraint!
    @IBOutlet var uploadViewTopConstant: NSLayoutConstraint!
    
    @IBOutlet var contentTextView: UITextView!
    var selectedType:Int = 0
    
    var selectedImageData:Data?
    var videoUrl:URL?
    
    var imageFileName:String?
    
    let imageView = UIImageView()
    
    let closeButton:UIButton = {
       let button = UIButton()
        let closeImage:UIImage = UIImage(named:"ic_close")?.withRenderingMode(.alwaysTemplate) ?? #imageLiteral(resourceName: "DELETEIOSX")
        button.setImage(closeImage, for: .normal)
        button.tintColor = .white
        button.backgroundColor = UIColor(white: 0, alpha: 0.8)
        button.layer.cornerRadius = 15
        return button
    }()
    
    weak var delegate:TA_AddPostVCDelegate?
    
    fileprivate func observeKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardShow) , name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardHide) , name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func keyboardShow(notification:NSNotification){
        let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        if let keyboardHeight = keyboardSize?.cgRectValue.size.height {
            //let tabBarHeight:CGFloat = self.tabBarController?.tabBar.frame.size.height ?? 49
               self.addPostViewBottomHeight.constant = keyboardHeight
               self.addPostView.layoutIfNeeded()
        }
    }
    
    @objc func keyboardHide(){
            self.addPostViewBottomHeight.constant = 0
            self.addPostView.layoutIfNeeded()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if (touch.view == self.view) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uploadViewHeight.isActive = false
        uploadView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        uploadViewTopConstant.constant = 0
        contentTextView.text =  "What's Happening?".localized

        observeKeyboardNotifications()
    }
    
    //MARK:UPLOAD BUTTON ACTION

    @IBAction func uploadImageAction(_ sender: Any) {
        
        selectedType = 0
        
        let alertController = UIAlertController(title: "", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Take Photo".localized, style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.presentImagePickerController(.camera, mediaType: "")
                
            } else {
                print("Camera is not available")
            }
        }
        
        let secondAction: UIAlertAction = UIAlertAction(title: "Browse Photos".localized, style: .default) { action -> Void in
            self.presentImagePickerController(.photoLibrary, mediaType: "")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: {(alert: UIAlertAction!) in
        })
        // add actions
        alertController.addAction(firstAction)
        alertController.addAction(secondAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func uploadVideoAction(_ sender: Any) {
        
        selectedType = 1
        
        let alertController = UIAlertController(title: "", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Record Video".localized, style: .default) { action -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.presentImagePickerController(.camera, mediaType: kUTTypeMovie as String)
            
            } else {
                print("Camera is not available")
            }
           
        }
        
        let secondAction: UIAlertAction = UIAlertAction(title: "Browse Video".localized, style: .default) { action -> Void in
            self.presentImagePickerController(.photoLibrary, mediaType: kUTTypeMovie as String)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: {(alert: UIAlertAction!) in
        })
        // add actions
        alertController.addAction(firstAction)
        alertController.addAction(secondAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func dismissAddPostView(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func postMessage(_ sender: Any) {
        contentTextView.resignFirstResponder()
        
        if contentTextView.text == "What's Happening?".localized || contentTextView.text == "" || contentTextView.text.trimmingCharacters(in: .whitespaces) == "" {
            Toast.sharedInstance()?.make("Please enter text".localized, position: TOAST_POSITION_BOTTOM)
            return
        }
       
        
        if selectedType == 0 {
            if selectedImageData == nil  {
                Toast.sharedInstance()?.make("Please upload image/video".localized, position: TOAST_POSITION_BOTTOM)
                return
            }
            let params:[String:Any] = [
                "content":contentTextView.text!,
                ]
            guard let imageData = selectedImageData else { return }
           Utils().showProgress()
            MediaUploadService().uploadImageToServer(imageData: imageData, params: params, completion: { (result,statusCode) in
                

                if (result){
                    print("StatusCode:\(statusCode)")
                    if statusCode == 200{
                        self.delegate?.fetchResponse(toStart: 0)
                    } else if statusCode == 401 {
                        CustomTabVC().addUserSession(completion: {result in
                            if (result){
                                MediaUploadService().uploadImageToServer(imageData: imageData, params: params, completion:{(result,statusCode) in
                                    if (result){
                                        if statusCode == 200{
                                            self.delegate?.fetchResponse(toStart: 0)
                                        }
                                    } else {
                                        DispatchQueue.main.async {
                                            Utils.hideProgress()
                                            

                                           Toast.sharedInstance()?.make("Unable to post. Please retry".localized, position: TOAST_POSITION_BOTTOM)
                                        }
                                    }
                                })
                            } else {
                                DispatchQueue.main.async {
                                    Utils.hideProgress()
                                    
                                   
                                    

                                    Toast.sharedInstance()?.make("Unable to post. Please retry".localized, position: TOAST_POSITION_BOTTOM)
                                }
                            }
                        })
                        
                    } else if statusCode == 500 {
                        
                        DispatchQueue.main.async {
                            Utils.hideProgress()
                            
                            Toast.sharedInstance()?.make("Something went wrong. Please try again later", position: TOAST_POSITION_BOTTOM)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                    Utils.hideProgress()
                        

                       Toast.sharedInstance()?.make("Unable to post16. Please retry".localized, position: TOAST_POSITION_BOTTOM)
                    }
                }
            })
    
        } else {
            if videoUrl == nil  {
                Toast.sharedInstance()?.make("Please upload image/video".localized, position: TOAST_POSITION_BOTTOM)
                return
            }
            let params:[String:Any] = [
                "content":contentTextView.text!,
                "isVideo": 1
                ]
            guard let videoUrl = videoUrl else { return }
            Utils().showProgress()
            MediaUploadService().uploadVideoToServer(videoPath: videoUrl, params: params, completion: { (result,statusCode) in
                if (result){
                    if statusCode == 200{
                        self.delegate?.fetchResponse(toStart: 0)
                    } else if statusCode == 401 {
                        CustomTabVC().addUserSession(completion: {result in
                            if (result){
                                MediaUploadService().uploadVideoToServer(videoPath: videoUrl, params: params, completion: {(result,statusCode) in
                                    if (result){
                                        if statusCode == 200{
                                            self.delegate?.fetchResponse(toStart: 0)
                                        }  else {
                                            DispatchQueue.main.async {
                                                Utils.hideProgress()
                                                Toast.sharedInstance()?.make("Unable to post. Please retry".localized, position: TOAST_POSITION_BOTTOM)
                                            }
                                        }
                                    }
                                })
                            } else {
                                DispatchQueue.main.async {
                                    Utils.hideProgress()
                                    Toast.sharedInstance()?.make("Unable to post. Please retry".localized, position: TOAST_POSITION_BOTTOM)
                                }
                            }
                        })
                    
                    } else if statusCode == 500 {
                        DispatchQueue.main.async {
                            Utils.hideProgress()
                            Toast.sharedInstance()?.make("Something went wrong. Please try again later", position: TOAST_POSITION_BOTTOM)
                        }
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        Utils.hideProgress()
                        Toast.sharedInstance()?.make("Unable to post. Please retry".localized, position: TOAST_POSITION_BOTTOM)
                    }
                    
                }
            })
        }
        self.dismiss(animated: true, completion: nil)

    }
    
    
    
}

extension TA_AddPostVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func presentImagePickerController(_ sourceType: UIImagePickerController.SourceType,mediaType:String) {
        let pickerController = UIImagePickerController()
        pickerController.sourceType = sourceType
        pickerController.videoMaximumDuration = 5

        if (mediaType).isEmpty == false {
            pickerController.mediaTypes = [mediaType];
        } else {
            pickerController.allowsEditing = true
        }
        pickerController.delegate = self
        pickerController.modalPresentationStyle = .overFullScreen
        self.present(pickerController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let type = info[UIImagePickerController.InfoKey.mediaType] as? String
        if type == kUTTypeMovie as String {
            if let selectedVideoUrl:URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL){
                
                self.createVideoUploadRequest(selectedVideoUrl)
                videoUrl = selectedVideoUrl
                

            }
        } else {
            if let selectedImage = info[.editedImage] as? UIImage {
                addImageViewToUploadView(selectedImage: selectedImage)
                selectedImageData = selectedImage.jpegData(compressionQuality: 1)
            }
        }
      
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func addImageViewToUploadView(selectedImage:UIImage){
        uploadViewHeight.isActive = true
        uploadView.heightAnchor.constraint(equalToConstant: 0).isActive = false
        uploadViewTopConstant.constant = 20
        imageView.image = selectedImage
        uploadView.addSubview(imageView)
        imageView.anchorToTop(uploadView.topAnchor, left: uploadView.leftAnchor, bottom: uploadView.bottomAnchor, right: uploadView.rightAnchor)
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        
        uploadView.addSubview(closeButton)
        _ = closeButton.anchor(uploadView.topAnchor, left: nil, bottom: nil, right: uploadView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 30, heightConstant: 30)
        closeButton.addTarget(self, action: #selector(hideUploadView), for: .touchUpInside)
    }
    
    @objc private func hideUploadView(){
        uploadView.subviews.forEach { $0.removeFromSuperview() }
        uploadViewHeight.isActive = false
        uploadView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        uploadViewTopConstant.constant = 0
        if  selectedImageData != nil {
            selectedImageData = nil
        }
        if videoUrl != nil {
            videoUrl = nil
        }
    }
    
    private func createVideoUploadRequest(_ videoURL: URL?) {
        var thumbImg: UIImage? = self.generateVideoThumbImage(videoURL)
        thumbImg = thumbImg?.fixOrientation()
        imageView.image = thumbImg
        self.addImageViewToUploadView(selectedImage: thumbImg!)
    }
    
    private func generateVideoThumbImage(_ videoURL: URL?) -> UIImage? {
        var asset: AVURLAsset? = nil
        if let anURL = videoURL {
            asset = AVURLAsset(url: anURL, options: nil)
        }
        var generateImg: AVAssetImageGenerator? = nil
        if let anAsset = asset {
            generateImg = AVAssetImageGenerator(asset: anAsset)
        }
        generateImg?.appliesPreferredTrackTransform = true
        let error: Error? = nil
        let time: CMTime = CMTimeMake(value: 1, timescale: 1)
        let refImg = try? generateImg?.copyCGImage(at: time, actualTime: nil)
        if let anError = error, let anImg = refImg {
            print("error==\(anError), Refimage==\(String(describing: anImg))")
        }
        var frameImage: UIImage? = nil
        if let anImg = refImg {
            frameImage = UIImage(cgImage: anImg!)
        }
        return frameImage
    }
  
}

//MARK:TEXTVIEW DELEGATE
extension TA_AddPostVC:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "What's Happening?".localized {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
        textView.text = "What's Happening?".localized
        textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 1000    // 10 Limit Value
    }
    
}

