//
//  Thanks_AmericaVC.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 22/02/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import IDMPhotoBrowser
import AVKit

class Thanks_AmericaVC: UIViewController,TA_AddPostVCDelegate {
    @IBOutlet var thanksAmericaTableView: UITableView!
    var heightAtIndexPath = NSMutableDictionary()
    
    @IBOutlet var alertLabel: UILabel!
    @IBOutlet var noDataView: UIView!
    
    
    
    let cellId = "TACell"
    
    var thanksAmericaResponse:ThanksAmericaFeed?
    var refreshFooter:YiRefreshFooter?
    var startValue:Int = 0
    
    var isDataFetched:Bool = false
    
    var isFromPlayer:Bool = false
   
      func fetchResponse(toStart:Int){
        startValue = toStart
        if toStart == 0{
            Utils().showProgress()
            alertLabel.text = ""
        }
        
        
        let apiUrl =  Constants().thanksAmericaUrl + "?toStart=" + String(toStart)
        //print("TAURL:\(apiUrl)")
        ConnectionAPI().fetchGenericData(urlString: apiUrl, sBlock: { (response: ThanksAmericaFeed) in
            Utils.hideProgress()
            
            if toStart == 0 {
                self.thanksAmericaResponse = response
            } else {
                for post in response.thanksAmericaFeed {
                    self.thanksAmericaResponse?.thanksAmericaFeed.append(post)
                }
            }
            if self.thanksAmericaResponse?.thanksAmericaFeed.count ?? 0 >= 10 {
                self.addPullDowntoRefresh()
            }
            if response.thanksAmericaFeed.count < 10 {
                self.removePullDownToRefresh()
            }
            //print("Response:\(self.thanksAmericaResponse)")
            self.reloadView(isDataFetched: true, toStart: toStart)
            
        }, fBlock: { customErrorMsg, errorCode in
            Utils.hideProgress()
            self.removePullDownToRefresh()

            self.reloadView(isDataFetched: false, toStart: toStart)
        })
    }
    
    private func reloadView(isDataFetched:Bool,toStart:Int){
        self.isDataFetched = isDataFetched
        
        if isDataFetched {
            self.thanksAmericaTableView.reloadData()
            if  toStart == 0 {
                if (thanksAmericaResponse?.thanksAmericaFeed.count ?? 0) > 0  {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.thanksAmericaTableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }
            }
        } else {
            self.alertLabel.text = "Unable to retrieve data, please retry after sometime".localized
            self.thanksAmericaTableView.reloadData()
        }
    }
    
    //MARK:REFRESH FOOTER
    
    private func setUpRefreshFooter(){
        refreshFooter = YiRefreshFooter.init()
        refreshFooter!.beginRefreshingBlock = {
            DispatchQueue.main.async {
                self.fetchResponse(toStart: self.startValue)
            }
            self.startValue += 10
        }
    }
    
    private func removePullDownToRefresh() {
        if let refreshFooter = refreshFooter {
            refreshFooter.remove_observer()
            refreshFooter.scrollView = nil
            refreshFooter.footerView?.removeFromSuperview()
        }
    }
    
    private func addPullDowntoRefresh() {
        removePullDownToRefresh()
        if let refreshFooter = refreshFooter {
            refreshFooter.scrollView = thanksAmericaTableView
            refreshFooter.footer()
        }
    }
    
    //MARK:SET UP VIEWS
    private func setNotifications(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Update_news_screen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocalisation), name: NSNotification.Name(rawValue: "Update_news_screen"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appEnteredFromBackground), name: UIApplication.willEnterForegroundNotification
            , object: nil)
    }
    
    private func setNavigationBar(){
        
        let navButton:UIButton = {
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: 0, y: 0, width: view.frame.width - 32, height: 44)
            button.setImage(#imageLiteral(resourceName: "Namaste"), for: .disabled)
            button.setTitle("Thanks America".localized, for: .normal)
            button.titleLabel?.font = Constants().navigationTitleFont
            button.contentHorizontalAlignment = .left
            button.semanticContentAttribute = .forceRightToLeft
            button.isEnabled = false
            button.imageEdgeInsets = UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 0)
            return button
        }()
        
        navigationItem.titleView = navButton
        
        let revealIcon = UIImage(named:"menu")?.withRenderingMode(.alwaysOriginal)
        let revealBarButtonItem = UIBarButtonItem(image: revealIcon, style: .plain, target: self, action: #selector(revealSlidingMenu))
        navigationItem.leftBarButtonItem = revealBarButtonItem
    }
    
    @objc func revealSlidingMenu(){
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    @objc func updateLocalisation(){
        setNavigationBar()
        self.alertLabel.text = "Unable to retrieve data, please retry after sometime".localized

        self.thanksAmericaTableView.reloadData()
    }
    
    //MARK:VIEWCONTROLLER LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationBar()
        self.setNotifications()
        self.setUpRefreshFooter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !(isFromPlayer){
            removePullDownToRefresh()
            self.fetchResponse(toStart: 0)
        } else {
            isFromPlayer = !isFromPlayer
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("IsFromPlayer:\(isFromPlayer)")
        if !(isFromPlayer){
        if let visibleCells = thanksAmericaTableView.visibleCells as? [ThanksAmericaCell]{
            for cell in visibleCells{
             if let videoUrl = cell.videoURL{
                ASVideoPlayerController.sharedVideoPlayer.pauseVideo(forLayer: cell.videoLayer, url: videoUrl)

                    ASVideoPlayerController.sharedVideoPlayer.removeFromSuperLayer(layer: cell.videoLayer, url:videoUrl )
                }
            }
            }
        }
    }
  
    @IBAction func showAddPostView(_ sender: Any) {
        let presentedViewController = UIStoryboard.init(name: "News", bundle: nil).instantiateViewController(withIdentifier: "TA_ADDPOST") as! TA_AddPostVC
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen;
        presentedViewController.delegate = self
        presentedViewController.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.present(presentedViewController, animated: true, completion: {
            if let visibleCells = self.thanksAmericaTableView.visibleCells as? [ThanksAmericaCell]{
                for cell in visibleCells{
                    if cell.videoURL != nil{
                        cell.pauseVideo()
                       /* if let time = cell.getPlayerCurrentTime() {
                            self.playVideo(with: URL(string: videoUrl), time: time)
                        } else {
                            self.playVideo(with: URL(string: videoUrl), time: .zero)
                        } */
                    }
                }
            }

        })
    }
    
}

//MARK: UITABLEVIEW DELEGATE AND DATASOURCE
extension Thanks_AmericaVC:UITableViewDelegate,UITableViewDataSource{
    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        if (isDataFetched) {
            thanksAmericaTableView.backgroundView = nil
            if thanksAmericaResponse?.thanksAmericaFeed.isEmpty == true{
                thanksAmericaTableView.backgroundView = noDataView
                self.alertLabel.text = "Unable to retrieve data, please retry after sometime".localized
                return 0
            }
            return 1
        } else {
            if startValue == 0 {
                thanksAmericaTableView.backgroundView = noDataView
                return 0
            } else {
                thanksAmericaTableView.backgroundView = nil
                return 1
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return thanksAmericaResponse?.thanksAmericaFeed.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = thanksAmericaTableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ThanksAmericaCell
        cell.thanksAmericaPost = thanksAmericaResponse?.thanksAmericaFeed[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? ThanksAmericaCell, let videoUrl = videoCell.videoURL{
            ASVideoPlayerController.sharedVideoPlayer.removeFromSuperLayer(layer: videoCell.videoLayer, url:videoUrl )
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = heightAtIndexPath.object(forKey: indexPath) as? NSNumber {
            return CGFloat(height.floatValue)
        } else {
            return UITableView.automaticDimension
        }
    }
  
}

//MARK: PLAYER PROPERTIES
extension Thanks_AmericaVC{
    

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            scrollView.contentOffset = CGPoint.zero
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.playPauseVideos()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.playPauseVideos()
        }
    }
    
    func playPauseVideos(){
        if let visibleCells = thanksAmericaTableView.visibleCells as? [ThanksAmericaCell]{
            for cell in visibleCells{
                cell.playButton.isHidden = true
            }
        }
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView:thanksAmericaTableView, appEnteredFromBackground: false)
    }
    
    
    @objc func appEnteredFromBackground() {
        if let visibleCells = thanksAmericaTableView.visibleCells as? [ThanksAmericaCell]{
            for cell in visibleCells{
                if cell.videoLayer.isHidden == false {
                    cell.playButton.isHidden = false
                }
            }
        }
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView:thanksAmericaTableView, appEnteredFromBackground: false)
    }
    
    
    func playVideo(with videoUrl: URL?, time currentTime: CMTime) {
        var player: AVPlayer? = nil

        if let videoUrl = videoUrl {
            player = AVPlayer(url: videoUrl)
        }
        
       
        
        let playerViewController = AVPlayerViewController()
        
        playerViewController.player = player
        playerViewController.player?.currentItem?.seek(to: currentTime.value > 0 ? currentTime : .zero)
        playerViewController.player?.rate = 2
        player?.play()
        self.present(playerViewController, animated: true)
    }
}

//MARK: TA_CELL DELEGATE
extension Thanks_AmericaVC:TACellDelegate{
    func thanksAmericaCellDidTapPlay(_ sender: ThanksAmericaCell) {
            isFromPlayer = true
            sender.pauseVideo()
        if let videoUrl = sender.videoURL {
            if let time = sender.getPlayerCurrentTime() {
                playVideo(with: URL(string: videoUrl), time: time)
            } else {
                playVideo(with: URL(string: videoUrl), time: .zero)
            }
        }
    }
    
    func thanksAmericaCellDidTapShare(_ sender: ThanksAmericaCell) {
        
        var guoMediaUrl = URL(string:"https://www.guo.media/")
        if let id = sender.postId {
          
            let urlString = Constants().taSharePostUrl + String(id)
            guoMediaUrl = URL(string: urlString)
        }
        let dataToShare = [guoMediaUrl]
        let activityViewController = UIActivityViewController.init(activityItems: dataToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func thanksAmericaCellDidTapImage(_ sender: ThanksAmericaCell) {

        if let imageUrl = sender.imageURL{
            let url = URL(string: imageUrl)
            let browser = IDMPhotoBrowser.init(photoURLs: [url as Any])
           // print("TAIMAGEURL====\(browser,sender.imageURL)")

            if let browser = browser {
                self.present(browser, animated: true, completion: nil)
            }
        }
        
       
        
        if let videoUrl = sender.videoURL {
            isFromPlayer = true
            sender.pauseVideo()
            if let time = sender.getPlayerCurrentTime() {
                playVideo(with: URL(string: videoUrl), time: time)
            } else {
                playVideo(with: URL(string: videoUrl), time: .zero)
            }
        }
    }
}
