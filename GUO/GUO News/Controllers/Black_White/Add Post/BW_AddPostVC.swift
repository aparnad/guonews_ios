//
//  BW_AddPostVC.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 18/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Toast_Swift

protocol BW_AddPostVCDelegate: class {
    func fetchResponse(toStart:Int)
    func showToastMessage(message:String)
}

class BW_AddPostVC: UIViewController {

    @IBOutlet var addPostView: UIView!
    @IBOutlet var addPostViewBottomHeight: NSLayoutConstraint!
    @IBOutlet var contentTextView: UITextView!
    
    var selectedIndex:Int = 0
    
    weak var delegate:BW_AddPostVCDelegate?
    
    fileprivate func observeKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardShow) , name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardHide) , name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    
    @objc func keyboardShow(notification:NSNotification){
        let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        if let keyboardHeight = keyboardSize?.cgRectValue.size.height {
            self.addPostViewBottomHeight.constant = keyboardHeight
            self.addPostView.layoutIfNeeded()
        }
    }
    
    @objc func keyboardHide(){
        self.addPostViewBottomHeight.constant = 0
        self.addPostView.layoutIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     contentTextView.text =  "What's Happening?".localized
        observeKeyboardNotifications()
    }
    
   
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if (touch.view == self.view) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func dismissAddPostView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showToastMessage(statusCode:Int, message:String){
        if statusCode == 500 {
            self.delegate?.showToastMessage(message: message)
        } else if statusCode == -1009 {
            self.delegate?.showToastMessage(message: "No network connection found. Please retry".localized)
        } else {
            self.delegate?.showToastMessage(message: "Unable to post. Please retry".localized)
        }
    }
    
    @IBAction func postMessage(_ sender: Any) {
        contentTextView.resignFirstResponder()
        if contentTextView.text == "What's Happening?".localized || contentTextView.text == "" || contentTextView.text.trimmingCharacters(in: .whitespaces) == "" {
            Toast.sharedInstance()?.make("Please enter text".localized, position: TOAST_POSITION_BOTTOM)
            return
        }
        let postUrl = Constants().bwPostUrl
        let paramsDict: [String: Any] = ["content": contentTextView.text,
                                         "type":selectedIndex]
        
    
        Utils().showProgress()
        ConnectionAPI().callNewsWebServiceGet(apiName: "BW", apiUrl: postUrl, params: paramsDict, sBlock: { (data,statusCode) in
            
            if statusCode == 200 {
                self.delegate?.fetchResponse(toStart: 0)
            }
            
        }, fBlock: {customErrorMsg, statusCode in
            if statusCode == 401 {
                CustomTabVC().addUserSession(completion:{result in
                    if (result){
                        ConnectionAPI().callNewsWebServiceGet(apiName: "BW", apiUrl: postUrl, params: paramsDict, sBlock: {(data,statusCode) in
                            if statusCode == 200 {
                                self.delegate?.fetchResponse(toStart: 0)
                            }
                        },fBlock: {customErrorMsg, errorCode in
                            self.showToastMessage(statusCode: statusCode, message: customErrorMsg ?? "Unable to post. Please retry".localized)
                        })
                    } else {
                        Utils.hideProgress()
                       self.showToastMessage(statusCode: 500, message: "Unable to post. Please retry".localized)
                    }
                })
             
            } else {
                Utils.hideProgress()

                self.showToastMessage(statusCode: statusCode, message: customErrorMsg ?? "Unable to post. Please retry".localized)
            }
        })
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension BW_AddPostVC:UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "What's Happening?".localized {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "What's Happening?".localized
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 1000    // 10 Limit Value
    }
    
   
    
}
