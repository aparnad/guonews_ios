//
//  Black_WhiteVC.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 02/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Toast_Swift

class Black_WhiteVC: UIViewController,BW_AddPostVCDelegate {
   

    @IBOutlet var bwSegmentedControl: UISegmentedControl!
    
    @IBOutlet var bwTableView: UITableView!
    var heightAtIndexPath = NSMutableDictionary()
    
    @IBOutlet var noDataView: UIView!
    
    @IBOutlet var alertLabel: UILabel!
    
    let cellId = "BLACK_WHITE"
    let xibId = "BlackWhiteCell"
    var selectedColor:UIColor = .black
    
    var blackWhiteFeed:BlackWhiteFeed?
    var refreshFooter:YiRefreshFooter?
    var startValue:Int = 0
    var isDataFetched:Bool = false
    
    //MARK: FETCH RESPONSE
    
    func fetchResponse(toStart:Int){
        startValue = toStart
        if toStart == 0{
            Utils().showProgress()
            alertLabel.text = ""
            //            self.blackWhiteFeed?.blackFeed.removeAll()
            //            self.blackWhiteFeed?.whiteFeed.removeAll()
        }
        
        
        var apiUrl = selectedColor == .black ? Constants().blackUrl : Constants().whiteUrl
        apiUrl = apiUrl + "&toStart=" + String(toStart)
        
        ConnectionAPI().fetchGenericData(urlString: apiUrl, sBlock: { (response: BlackWhiteFeed) in
            Utils.hideProgress()
            if toStart == 0 {
                self.blackWhiteFeed = response
            } else {
                for post in response.blackFeed {
                    self.blackWhiteFeed?.blackFeed.append(post)
                }
                for post in response.whiteFeed {
                    self.blackWhiteFeed?.whiteFeed.append(post)
                }
            }
            
            if self.selectedColor == .black {
                if self.blackWhiteFeed?.blackFeed.count ?? 0 >= 10 {
                    self.addPullDowntoRefresh()
                }
                if response.blackFeed.count < 10 {
                    self.removePullDownToRefresh()
                }
                
            } else {
                if self.blackWhiteFeed?.whiteFeed.count ?? 0 >= 10 {
                    self.addPullDowntoRefresh()
                }
                if response.whiteFeed.count < 10 {
                    self.removePullDownToRefresh()
                }
                
            }
            
            //print("Response:\(self.blackWhiteFeed)")
            self.reloadView(isDataFetched: true, toStart: toStart)
            
        }, fBlock: { customErrorMsg, errorCode in
            Utils.hideProgress()
            self.removePullDownToRefresh()
            
            self.reloadView(isDataFetched: false, toStart: toStart)
            
        })
    }
    
    private func reloadView(isDataFetched:Bool,toStart:Int){
        self.isDataFetched = isDataFetched
        if isDataFetched {
            self.bwTableView.reloadData()
            
            if  toStart == 0 {
                if (blackWhiteFeed?.blackFeed.count ?? 0) > 0 || (blackWhiteFeed?.whiteFeed.count ?? 0) > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.bwTableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }
            }
        } else {
            self.alertLabel.text = "Unable to retrieve data, please retry after sometime".localized
            self.bwTableView.reloadData()
        }
        
    }
    
    //MARK:REFRESH FOOTER
    
    private func setUpRefreshFooter(){
        refreshFooter = YiRefreshFooter.init()
        refreshFooter!.beginRefreshingBlock = {
            DispatchQueue.main.async {
                self.fetchResponse(toStart: self.startValue)
            }
            self.startValue += 10
        }
    }
    
    private func removePullDownToRefresh() {
        if let refreshFooter = refreshFooter {
            refreshFooter.remove_observer()
            refreshFooter.scrollView = nil
            refreshFooter.footerView?.removeFromSuperview()
        }
    }
    
    private func addPullDowntoRefresh() {
        removePullDownToRefresh()
        if let refreshFooter = refreshFooter {
            refreshFooter.scrollView = bwTableView
            refreshFooter.footer()
        }
    }
    //MARK: SETUP VIEW
    
    private func setNotifications(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Update_news_screen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocalisation), name: NSNotification.Name(rawValue: "Update_news_screen"), object: nil)
    }
    
    private func setNavigationBar(){
        
        let titleLabel:UILabel = {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.size.height))
            // let label = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: view.frame.size.height))
            label.text = "Black & White".localized
            label.font = Constants().navigationTitleFont
            label.textColor = .white
            return label
        }()
        navigationItem.titleView = titleLabel
        
        let revealIcon = UIImage(named:"menu")?.withRenderingMode(.alwaysOriginal)
        let revealBarButtonItem = UIBarButtonItem(image: revealIcon, style: .plain, target: self, action: #selector(revealSlidingMenu))
        navigationItem.leftBarButtonItem = revealBarButtonItem
    }
    
    @objc func revealSlidingMenu(){
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    @objc func updateLocalisation(){
        setNavigationBar()
        bwSegmentedControl.setTitle("Black".localized, forSegmentAt: 0)
        bwSegmentedControl.setTitle("White".localized, forSegmentAt: 1)
        self.alertLabel.text = "Unable to retrieve data, please retry after sometime".localized
        
        
        self.bwTableView.reloadData()
    }
    
    //MARK:VIEWCONTROLLER LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNotifications()
        
        setNavigationBar()
        self.setUpRefreshFooter()
        /*  if let layout = bwCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
         layout.estimatedItemSize = CGSize (width: view.frame.width - 20, height: 10)
         } */
        
        bwTableView.register(UINib(nibName: "BWCell", bundle: nil), forCellReuseIdentifier: xibId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchResponse(toStart: 0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removePullDownToRefresh()
    }
    
    @IBAction func bwSegmentedControlAction(_ sender: Any) {
        selectedColor = (bwSegmentedControl.selectedSegmentIndex == 0) ? .black : .white
        self.view.backgroundColor = selectedColor == .black ? .white : .black
        bwSegmentedControl.tintColor = selectedColor == .black ? .black : .white
        bwSegmentedControl.backgroundColor = selectedColor == .black ? .white : .black
        removePullDownToRefresh()
        bwTableView.reloadData()
        fetchResponse(toStart: 0)
    }
    
    @IBAction func showAddPostView(_ sender: Any) {
        let presentedViewController = UIStoryboard.init(name: "News", bundle: nil).instantiateViewController(withIdentifier: "BW_ADDPOST") as! BW_AddPostVC
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen;
        presentedViewController.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        presentedViewController.selectedIndex = selectedColor == .black ? 0 : 1
        presentedViewController.delegate = self
        self.present(presentedViewController, animated: true, completion: nil)
    }
    
    func showToastMessage(message: String) {
        var style = ToastStyle()
        style.backgroundColor = UIColor(red: 19/255.0, green: 66/255.0, blue: 142/255.0, alpha: 1.0)
        style.messageFont = UIFont.systemFont(ofSize: 18)
        style.messageColor = UIColor.white
        style.messageAlignment = .center
        ToastManager.shared.style = style
        self.view.makeToast(message, duration: 3.0, position: .bottom, style: style)
    }
    
}

//MARK: TABLEVIEW

extension Black_WhiteVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (isDataFetched) {
            bwTableView.backgroundView = nil
            if selectedColor == .black {
                if blackWhiteFeed?.blackFeed.isEmpty == true{
                    self.alertLabel.text = "Unable to retrieve data, please retry after sometime".localized
                    bwTableView.backgroundView = noDataView
                    return 0
                }
            } else {
                if blackWhiteFeed?.whiteFeed.isEmpty == true{
                    self.alertLabel.text = "Unable to retrieve data, please retry after sometime".localized
                    bwTableView.backgroundView = noDataView
                    return 0
                }
            }
            return 1
        } else {
            if startValue == 0 {
                bwTableView.backgroundView = noDataView
                return 0
            } else {
                bwTableView.backgroundView = nil
                return 1
            }
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedColor == .black ? (blackWhiteFeed?.blackFeed.count ?? 0) : (blackWhiteFeed?.whiteFeed.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = bwTableView.dequeueReusableCell(withIdentifier: xibId) as! BWCell
        if selectedColor == .black {
            cell.backgroundColor = .black
            cell.postTextView.textColor = .white
            cell.bottomView.layer.borderColor = UIColor.black.cgColor
            cell.bottomView.layer.borderWidth = 1
            cell.seperatorView.backgroundColor = .white
        } else {
            cell.backgroundColor = .white
            cell.postTextView.textColor = .black
            cell.bottomView.layer.borderColor = UIColor.white.cgColor
            cell.seperatorView.backgroundColor = .black
        }
        
        cell.black_whitePost = selectedColor == .black ? blackWhiteFeed?.blackFeed[indexPath.row] :blackWhiteFeed?.whiteFeed[indexPath.row]
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = heightAtIndexPath.object(forKey: indexPath) as? NSNumber {
            return CGFloat(height.floatValue)
        } else {
            return UITableView.automaticDimension
        }
    }
    
}




extension Black_WhiteVC: Black_WhiteCellDelegate {
   
    
    func bwCellDidTapShare(_ sender: BWCell) {
        
        var guoMediaUrl = URL(string:"https://www.guo.media/")
        if let id = sender.postId {
           let urlString = Constants().bwSharePostUrl + String(id)
            guoMediaUrl = URL(string: urlString)
        }
        
        let dataToShare = [guoMediaUrl]
        let activityViewController = UIActivityViewController.init(activityItems: dataToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop]
        self.present(activityViewController, animated: true, completion: nil)
    }
}


