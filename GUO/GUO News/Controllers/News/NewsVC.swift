//
//  NewsVC.swift
//  GUO Media
//
//  Created by apple on 1/3/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import AVKit

class NewsVC: UIViewController {

    @IBOutlet var categoryCollectionView: UICollectionView!
    @IBOutlet var newsTableView: UITableView!
    
    var languageBarButtonItem: UIBarButtonItem?
    var changeLanguagePopup: ChangeLanguagePopup?

    var isDataFetched:Bool = false
    
    let categoryCellId = "CATEGORY"
    let slideCellId = "SLIDE"
    let newsCellId = "NEWS"
    
    var selectedCategoryCollectionIndex: Int = 0
    var categoryArray:[String] = [String]()
    var categoryIdArray:[String] = [String]()
    
    var heightAtIndexPath = NSMutableDictionary()
    
    var newsResponse:NewsFeed?
    
    var timer:Timer?
    
    var refreshFooter:YiRefreshFooter?
    
    var id:String = "wd"
    var newsCount:Int = 0
    var isFromWebView:Bool = false
    
    var isFetchedLatestNews: Bool = true
    
    var alertLabel:String = ""
    
    var language: String {
        get {
            if let languageStr = UserDefaults.standard.value(forKey: "AppleLanguages1") as? String {
                return (languageStr == "zh-Hant") ? "c":"e"
            } else {
                return "e"
            }
        }
    }
    
    var currentLanguage :String = "e"
    
     var startValue: Int = 0
   
    //MARK: FETCH NEWS
    
    func callNewsAPI (newsTypeId: String, toStart: Int) {
        newsCount = toStart; id = newsTypeId; startValue = toStart
        alertLabel = ""
        
        if toStart == 0 {
            Utils().showProgress()
            self.isFetchedLatestNews = true
            SlideTableViewCell().stopTimer()
        }
      
       
        let newsAPI: String = Constants().newsUrl + "newsTypeId=" + newsTypeId + "&language=" + self.language + "&toStart=" + String(toStart)
        
        ConnectionAPI().fetchGenericData(urlString: newsAPI, sBlock: { (response: NewsFeed) in
        Utils.hideProgress()
            
            if toStart == 0 {
                //print("NewsResponse:\(response)")
                self.newsResponse = response
                if response.latestNewsHeadlines.count == 0 {
                    self.isFetchedLatestNews = false
                }
            } else {
                for news in response.latestNewsHeadlines {
                    self.newsResponse?.latestNewsHeadlines.append(news)
                }
            }
            if self.newsResponse?.latestNewsHeadlines.count ?? 0 >= 10 {
                self.addPullDowntoRefresh()
            }
            
            if response.latestNewsHeadlines.count < 10 {
                self.removePullDownToRefresh()
            }
            
            if self.isFetchedLatestNews == false {
                if toStart == 0 {
                    for news in response.topHeadlines {
                        self.newsResponse?.latestNewsHeadlines.append(news)
                    }
                    if self.newsResponse?.latestNewsHeadlines.count ?? 0 > 0 {
                        self.newsResponse?.latestNewsHeadlines.removeFirst()
                    } else {
                      //  self.alertLabel = "Unable to retrieve data, please retry after sometime".localized

                    }

                } else {
                    for news in response.topHeadlines {
                        self.newsResponse?.latestNewsHeadlines.append(news)
                    }
                }
                
                if self.newsResponse?.latestNewsHeadlines.count ?? 0 >= 9 {
                    self.addPullDowntoRefresh()
                }
                
                if response.topHeadlines.count < 10 {
                    self.removePullDownToRefresh()
                }
                self.reloadView(isDataFetched: true, toStart: toStart, responseCount: response.topHeadlines.count)
            } else {
                self.reloadView(isDataFetched: true, toStart: toStart, responseCount: response.latestNewsHeadlines.count)
            }
            
            
           
            
        }, fBlock: { customErrorMsg, errorCode in
            Utils.hideProgress()
            self.reloadView(isDataFetched: false, toStart: toStart, responseCount: 0)
        })
    }
    
    private func reloadView(isDataFetched:Bool,toStart:Int,responseCount:Int) {

        self.isDataFetched = isDataFetched
        if (isDataFetched) {

            if (responseCount != 0 || toStart == 0){
                self.newsTableView.reloadData()
            }

            if toStart == 0 {
                if (newsResponse?.latestNewsHeadlines.count ?? 0) > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.newsTableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }
            }
          
            if (newsResponse?.topHeadlines.count == 0 && newsResponse?.latestNewsHeadlines.count == 0) {
                alertLabel = "Unable to retrieve data, please retry after sometime".localized
            }
        } else {
            
            if toStart == 0 {
                self.removePullDownToRefresh()
                self.newsResponse?.topHeadlines.removeAll()
                self.newsResponse?.latestNewsHeadlines.removeAll()

            }
            
            print("BUSINESSSCOUNTFALSE==\(toStart)")
            alertLabel = "Unable to retrieve data, please retry after sometime".localized
            self.removePullDownToRefresh()
            self.newsTableView.reloadData()
        }
        
        
    
    }
    

    
    //MARK: SETUP VIEW
    private func setUpViews(){
        let tabBarHeight:CGFloat = self.tabBarController?.tabBar.frame.size.height ?? 68
        
        if #available(iOS 11.0, *) {
            newsTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: tabBarHeight).isActive = true
        } else {
            newsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: tabBarHeight).isActive = true
        }
   
        isFromWebView = false
    }
    
    private func setNotifications(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Update_news_screen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocalisation), name: NSNotification.Name(rawValue: "Update_news_screen"), object: nil)
    }
    
    private func setNavigationBar(){
        let titleLabel:UILabel = {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.size.height))
            label.text = "News".localized
            label.font = Constants().navigationTitleFont
            label.textColor = .white
            return label
        }()
        navigationItem.titleView = titleLabel
        
        let revealIcon = UIImage(named:"menu")?.withRenderingMode(.alwaysOriginal)
        let revealBarButtonItem = UIBarButtonItem(image: revealIcon, style: .plain, target: self, action: #selector(revealSlidingMenu))
        navigationItem.leftBarButtonItem = revealBarButtonItem
        
        let languageIcon = UIImage(named: "ic_change_language")?.withRenderingMode(.alwaysTemplate)
        languageBarButtonItem = UIBarButtonItem(image: languageIcon, style: .plain, target: self, action: #selector(changeLanguagePopUp))
        languageBarButtonItem?.tintColor = language == "e" ? .white : .black
       // navigationItem.rightBarButtonItem = languageBarButtonItem
    }
    
    @objc func changeLanguagePopUp() {
        changeLanguagePopup = (UIStoryboard.init(name: "Main_Monika", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChangeLanguagePopup") as? ChangeLanguagePopup)!
        
        
        Utils().showCustomPopup(changeLanguagePopup!.view)
    }
    
    @objc func revealSlidingMenu() {
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    @objc func updateLocalisation(){
        setNavigationBar()
        
        categoryArray = ["WORLD".localized,"US".localized,"CHINA".localized,"EU".localized,"OTHERS".localized,"POLITICS".localized,"FINANCE".localized,"BREAKING NEWS".localized,"TECHNOLOGY".localized,"BUSINESS".localized,"ENTERTAINMENT".localized,"SPORTS".localized]
        categoryIdArray = ["wd","us","cn","eu","or","pt","fm","br","ty","bn","et","sp"]

        let categoryId = categoryIdArray[selectedCategoryCollectionIndex]
        
        callNewsAPI(newsTypeId: categoryId, toStart: 0)
        
       /* if let languageStr = UserDefaults.standard.value(forKey: "AppleLanguages1") as? String {
            if currentLanguage != languageStr {
                  callNewsAPI(newsTypeId: categoryId, toStart: 0)
                currentLanguage = languageStr
            } else {
               // callNewsAPI(newsTypeId: categoryId, toStart: newsCount)
            }
        } else {
              callNewsAPI(newsTypeId: categoryId, toStart: 0)
        } */

        categoryCollectionView.reloadData()
    }
    
    //MARK:REFRESH FOOTER
    
    private func removePullDownToRefresh() {
        if let refreshFooter = refreshFooter {
            refreshFooter.remove_observer()
            refreshFooter.scrollView = nil
            refreshFooter.footerView?.removeFromSuperview()
        }
    }
    
    private func addPullDowntoRefresh() {
        removePullDownToRefresh()
        if let refreshFooter = refreshFooter {
            refreshFooter.scrollView = newsTableView
            refreshFooter.footer()
        }
    }


    //MARK:VIEWCONTROLLER LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpViews()
        self.setNavigationBar()
        self.setNotifications()
        
        refreshFooter = YiRefreshFooter.init()
        refreshFooter!.beginRefreshingBlock = {
            DispatchQueue.main.async {
                self.callNewsAPI(newsTypeId:self.id , toStart:self.newsCount)
            }
            self.newsCount += 10
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !(isFromWebView){
             updateLocalisation()
        } else {
            isFromWebView = !isFromWebView
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !(isFromWebView){
            removePullDownToRefresh()
        }
    }
    
}

//MARK:COLLECTION VIEW
extension NewsVC:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: categoryCellId, for: indexPath)
        let categoryName:UILabel = cell.contentView.viewWithTag(1) as! UILabel
        categoryName.text = categoryArray[indexPath.row]
    
        categoryName.font = language == "e" ? Constants().categoryLabelFont : Constants().categoryLabelCnFont
        
        if let underlineView:UIView = cell.contentView.viewWithTag(2) {
            underlineView.backgroundColor = (indexPath.item == selectedCategoryCollectionIndex) ? .white : .clear
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.categoryCollectionView.frame.width/4.0, height: self.categoryCollectionView.frame.height/3)
        
    }
    
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategoryCollectionIndex = indexPath.item
        self.categoryCollectionView.reloadData()
        let categoryId = categoryIdArray[indexPath.item]
        alertLabel = ""
        //newsTableView.reloadData()
        callNewsAPI(newsTypeId: categoryId,toStart: 0)
    }
}

//MARK:TABLEVIEW
extension NewsVC:UITableViewDelegate,UITableViewDataSource,NewsTableCellDelegate,SlideTableViewCellDelegate {
   
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        
        if !(isDataFetched){
            if newsCount == 0 {
                return 1
            } else {
                return (newsResponse?.latestNewsHeadlines.count ?? 0 ) + 1
            }
            
        }
    /*    if newsResponse?.latestNewsHeadlines.count != 0 && newsResponse?.topHeadlines.count == 0{
            self.newsTableView.setEmptyMessage("Unable to retrieve data, please retry after sometime".localized)
            return 1
        } else if newsResponse?.latestNewsHeadlines.count == 0 && newsResponse?.topHeadlines.count != 0{
            self.newsTableView.setEmptyMessage("Unable to retrieve data, please retry after sometime".localized)
            return 1
        } else {
            self.newsTableView.backgroundView = nil
        }*/
        if newsResponse?.latestNewsHeadlines.count != 0 && newsResponse?.topHeadlines.count == 0{
            self.newsTableView.setEmptyMessage("Unable to retrieve data, please retry after sometime".localized)
            return 1
        } else if newsResponse?.latestNewsHeadlines.count == 0 && newsResponse?.topHeadlines.count != 0{
            self.newsTableView.setEmptyMessage("Unable to retrieve data, please retry after sometime".localized)
            return 1
        } else if newsResponse?.latestNewsHeadlines.count == 0 && newsResponse?.topHeadlines.count == 0 {
            self.newsTableView.setEmptyMessage("Unable to retrieve data, please retry after sometime".localized)
            return 1
        } else {
            self.newsTableView.backgroundView = nil
        }
        let headlineCount = (newsResponse?.latestNewsHeadlines.count ?? 0) + 1
        return headlineCount
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = newsTableView.dequeueReusableCell(withIdentifier: slideCellId, for: indexPath) as! SlideTableViewCell
            cell.newsResponse = newsResponse
            cell.delegate = self
            
            
            cell.slideCollectionView.reloadData()
           
            if newsCount == 0 || self.newsResponse?.topHeadlines.isEmpty == true || self.newsResponse?.topHeadlines == nil {
                cell.alertLabel = self.alertLabel
    
                cell.slideCollectionView.reloadData()
            }
            
            
            cell.pageControl.numberOfPages = (self.newsResponse?.topHeadlines.count) ?? 0
            
            if cell.pageControl.numberOfPages > 1 {
                cell.pageControl.isHidden = false
                cell.nextButton.isHidden = false
                //print("NewsCount:\(newsCount)")
                if startValue == 0 {
                    cell.pageControl.currentPage = 0
                    let indexPath = IndexPath(item:0 , section: 0)
                    cell.slideCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
                }
                cell.startTimer()
            } else {
                cell.pageControl.isHidden = true
            }
            
            
            return cell
        }
        
        let cell = newsTableView.dequeueReusableCell(withIdentifier: newsCellId, for: indexPath) as! NewsTableCell
        
        if newsResponse?.latestNewsHeadlines.count ?? 0 > indexPath.row {
            cell.newsHeadline = newsResponse?.latestNewsHeadlines[indexPath.row - 1]
            cell.delegate = self
        }
   
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            return
        }
       var isExpanded = newsResponse?.latestNewsHeadlines[indexPath.row - 1].isExpanded
        isExpanded = !isExpanded!
        newsResponse?.latestNewsHeadlines[indexPath.row - 1].isExpanded = isExpanded!
        newsTableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UIScreen.main.bounds.height * 0.3
        }
        print("Count:",newsResponse?.latestNewsHeadlines.count,"Indexpath:",indexPath)
        /*if ((newsResponse?.latestNewsHeadlines[indexPath.row - 1].isExpanded)!) {
            return UITableView.automaticDimension
        } else {
        //  return UIScreen.main.bounds.height/7
            return UITableView.automaticDimension
        } */
        
        return UITableView.automaticDimension
    }
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UIScreen.main.bounds.height * 0.3
        }
        

      /*  if let boolVal = newsResponse?.latestNewsHeadlines[indexPath.row - 1].isExpanded {
            if boolVal {
                if let height = heightAtIndexPath.object(forKey: indexPath) as? NSNumber {
                    return CGFloat(height.floatValue)
                } else {
                    return UITableView.automaticDimension
                }
            } else {
                return UITableView.automaticDimension
              // return UIScreen.main.bounds.height/7

            }
           
        } else {
            return UITableView.automaticDimension
            //return UIScreen.main.bounds.height/7

        } */
        
        return UITableView.automaticDimension

    }
    
    func newsTableCellDidTapMore(_ sender: NewsTableCell) {
        
        
        guard let tappedIndexPath = newsTableView.indexPath(for: sender) else { return }
        if let url = sender.webUrl {
            let webVC = WebViewController()
            webVC.webUrl = url
            isFromWebView = true
            self.navigationController?.pushViewController(webVC, animated: true)
        }
    }
    
    func newsShareButtonAction(_ sender: NewsTableCell) {
        
        
        let dataToShare = [sender.webUrl]
        let activityViewController = UIActivityViewController.init(activityItems: dataToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop]
        self.present(activityViewController, animated: true, completion: nil)
    }
    func didTapSlideCell(sender: SlideTableViewCell, webUrl: String) {
        let webVC = WebViewController()
        webVC.webUrl = webUrl
        isFromWebView = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    
}


