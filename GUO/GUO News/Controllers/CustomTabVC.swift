//
//  CustomTabVC.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 02/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class CustomTabVC: UITabBarController {
    
    let userDefaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor : UIColor.white], for: .selected)
        
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor : UIColor.white], for: .normal)
       
        // ADD SWIPE GESTURE
        
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(revealSlidingMenu))
        self.view.addGestureRecognizer(swipeGesture)
        
        //Localisation Notification
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Update_news_screen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocalisation), name: NSNotification.Name(rawValue: "Update_news_screen"), object: nil)
        
        Utils().checkIntenetShowError()
        
        let isNewsFirstTime:Bool = UserDefaults.standard.bool(forKey: "IsNewsFIrstTime")
        if !(isNewsFirstTime){
            UserDefaults.standard.set(true, forKey: "IsNewsFIrstTime")
            self.addUserSession(completion:{_ in })
        }
        
        
    }
    
    @objc func revealSlidingMenu(){
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    @objc func updateLocalisation(){
        tabBar.items?[0].title = "News".localized
        tabBar.items?[1].title = "Black & White".localized
        tabBar.items?[2].title = "Thanks America".localized
    }
  
    
    //used to add session in server when logged in or TA addpost class returns 401 or Black and White add post returns 401
    
    func addUserSession(completion:@escaping ((Bool) -> Void)){
        var profileImageUrl:String = String()
        guard let userId:String = userDefaults.value(forKey: "com.applozic.userdefault.USER_ID") as? String else { return }
        
        if let profileImage = userDefaults.value(forKey: "picture"){
            profileImageUrl = profileImage as! String
            print("ProfileImage==\(profileImageUrl)")

        } else {
            profileImageUrl = "blank_profile_male.jpg"

        }
      
        guard let userName:String = userDefaults.value(forKey: "com.applozic.userdefault.DISPLAY_NAME") as? String else { return }
        guard let deviceKey:String = userDefaults.value(forKey: "com.applozic.userdefault.DEVICE_KEY_STRING") as? String else { return }
        
        let url = Constants().addUserSessionUrl
        
        let jsonObj:[String:String] = [
            "userId":userId,
            "profileImageUrl":profileImageUrl,
            "userName":userName,
            "deviceKey":deviceKey
        ]
        if (!JSONSerialization.isValidJSONObject(jsonObj)) {
            print("is not a valid json object")
            return
        }
        print("JsonObj:\(jsonObj)")
        guard  let postData = try? JSONSerialization.data(withJSONObject: jsonObj, options: JSONSerialization.WritingOptions.prettyPrinted) else { return }
        
        var request:URLRequest = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = postData
        
        let queue = DispatchQueue.global(qos: .userInteractive)
        queue.async { () -> Void in
            let task  = URLSession.shared.dataTask(with: request, completionHandler: { (responseData, urlResponse, error) -> Void in
                
                if let response = urlResponse as? HTTPURLResponse{
                    let statusCode = response.statusCode
                    print("STATUSCODE:\(statusCode)")
                }
                
                if let error:Error = error {
                    print("addSessionError",error.localizedDescription)
                    completion(false)
                    return
                }
                completion(true)
                do {
                    guard let responseData:Data = responseData else { return }
                    let jsonObject = try JSONSerialization.jsonObject(with: responseData, options:.mutableContainers)
                 //   print("Json:\(jsonObject)")
                } catch {
                    
                }
            })
            
            task.resume()
        }
    
    }
    
}
