//
//  ConnectionAPI.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 30/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ConnectionAPI: NSObject {
    
    let deviceKey = { () -> String in
        var deviceKey = String()
        if UserDefaults.standard.value(forKey: "com.applozic.userdefault.DISPLAY_NAME") != nil {
            deviceKey = UserDefaults.standard.value(forKey: "com.applozic.userdefault.DISPLAY_NAME") as! String
        }
        return deviceKey
    }()
    
    let userInfoId = { () -> String in
        var id = String()
        if UserDefaults.standard.value(forKey: "com.applozic.userdefault.USER_ID") != nil {
            id = UserDefaults.standard.value(forKey: "com.applozic.userdefault.USER_ID") as! String
        }
        return id
    }()
    
    
    func fetchGenericData<T: Decodable>(urlString: String, sBlock: @escaping (T) -> (), fBlock: @escaping FAILUREBLOCK) {
        
        let configuration:URLSessionConfiguration = URLSessionConfiguration.default
        let manager = AFHTTPSessionManager(sessionConfiguration: configuration)
        manager.responseSerializer = AFHTTPResponseSerializer.init()
        
        manager.get(urlString, parameters: nil, success: {
            (task,responseObject) in
            
            let response = task.response as! HTTPURLResponse
            let statusCode = response.statusCode
            if statusCode != 200 {
                return
            }
            
            guard let responseData:Data = responseObject as? Data else { return }
            
            do {
                let obj = try JSONDecoder().decode(T.self, from: responseData)
                sBlock(obj)
            } catch let jsonErr {
                print("Failed to decode json:", jsonErr)
            }
            
        }, failure:{(task, error) in
            print("error")
            self.handleErrors(nil, error: error, fBlock: fBlock)
        } )
    }
    
    func callNewsWebServiceGet(apiName:String, apiUrl:String?,params:[String:Any], sBlock: @escaping ((Data,Int) -> Void), fBlock: @escaping FAILUREBLOCK) {
        let configuration:URLSessionConfiguration = URLSessionConfiguration.default
        let manager = AFHTTPSessionManager(sessionConfiguration: configuration)
        manager.responseSerializer = AFHTTPResponseSerializer.init()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if apiName == "BW"{
            var userId:String = String()
            var deviceKey:String = String()
            userId = UserDefaults.standard.value(forKey: "com.applozic.userdefault.USER_ID") as! String
            deviceKey = UserDefaults.standard.value(forKey: "com.applozic.userdefault.DEVICE_KEY_STRING") as! String
            manager.requestSerializer.setValue(deviceKey, forHTTPHeaderField: "deviceKey")
            manager.requestSerializer.setValue(userId, forHTTPHeaderField: "userInfoId")
        }
        
        manager.post(apiUrl!, parameters: params, success: {
            (task,responseObject) in
            let response = task.response as! HTTPURLResponse
            let statusCode = response.statusCode
            print(statusCode)
            
            guard let responseData:Data = responseObject as? Data else { return }
            sBlock(responseData,statusCode)
        }, failure: {(task, error) in
            var statusCode  = -1009
            
            if let response = task?.response as? HTTPURLResponse {
                statusCode = response.statusCode
                if statusCode == 401 {
                    fBlock("Failure",statusCode)
                    return
                }
                
                if apiName == "BW"{
                    let nserror = error as NSError
                    if let errordata = nserror.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data {
 
                        do {
                            guard let jsonObject:[String:Any] = try JSONSerialization.jsonObject(with: errordata, options:.mutableContainers) as? [String : Any] else { fBlock("Unable to post. Please retry".localized,statusCode); return }
                            
                            if let errorDict:NSMutableDictionary = (jsonObject["Error"] as! NSArray)[0] as? NSMutableDictionary {
                                if let _:String = errorDict["Code"] as? String,let message:String = (errorDict["Message"] as! NSArray)[0] as? String{
                                    fBlock(message,statusCode)
                                } else {
                                    fBlock("Unable to post. Please retry".localized,statusCode)
                                }
                            } else {
                                fBlock("Unable to post. Please retry".localized,statusCode)
                            }
                        } catch {
                            
                        }
                    }
                } else {
                    fBlock("Unable to post. Please retry",statusCode)
                }
            } else {
                fBlock("Failure",statusCode)
            }
            print(statusCode)
            
        } )
   
    }
    
    func handleErrors(_ operation: URLSessionDataTask?, error: Error?, fBlock: FAILUREBLOCK) {
        //check first network errors like internet connection
        //    NSString *errorMsg = NSLocalizedString(@"NoInternet", nil);
        let errorMsg = NSLocalizedString("No Internet Connectivity", comment: "")
    
        var errorCode: Int = 0
        if (error as NSError?)?.code == -1009 {
            // errorMsg =  error.userInfo[NSLocalizedDescriptionKey];
            errorCode = -1009
        } else {
            //
        }
        
        fBlock(errorMsg, errorCode)
    }
}

