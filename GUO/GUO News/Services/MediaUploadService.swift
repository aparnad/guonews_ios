//
//  MediaUploadService.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 13/02/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class MediaUploadService: NSObject {
    //e1c4057e-d25b-4af5-913d-0dc5eb988bfd
    //121238
    
    var deviceKey = { () -> String in
        var deviceKey = String()
        if UserDefaults.standard.value(forKey: "com.applozic.userdefault.DEVICE_KEY_STRING") != nil {
            deviceKey = UserDefaults.standard.value(forKey: "com.applozic.userdefault.DEVICE_KEY_STRING") as! String
        }
        return deviceKey
    }()
    
    let userInfoId = { () -> String in
        var id = String()
        if UserDefaults.standard.value(forKey: "com.applozic.userdefault.USER_ID") != nil {
            id = UserDefaults.standard.value(forKey: "com.applozic.userdefault.USER_ID") as! String
        }
        return id
    }()
    
    let boundary = { () -> String in
        return "Boundary-\(NSUUID().uuidString)"
    }()
    
    
    func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: NSData, boundary: String,isVideo:Bool) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        var filename = ""
        var mimetype = ""
        if isVideo == true {
            filename = "upload.mov"
            mimetype = "video/mov"
        } else {
            filename = "user-profile.jpg"
            
            mimetype = "image/jpg"
        }
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    private func createRequest() -> URLRequest{
        let url = Constants().taPostUrl
        var request = URLRequest(url: URL(string: url)!, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        
        request.httpMethod = "POST"
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.setValue(userInfoId, forHTTPHeaderField: "userInfoId")
        request.setValue(deviceKey, forHTTPHeaderField: "deviceKey")
        
        return request
    }
    
 
    
    func uploadImageToServer(imageData : Data, params:[String:Any], completion: @escaping ((Bool,Int) -> Void)){
        
    
        var request = createRequest()
        
        request.httpBody = createBodyWithParameters(parameters: params, filePathKey: "file", imageDataKey: imageData as NSData, boundary: boundary,isVideo: false) as Data
        
        let task = URLSession.shared.dataTask(with: request,
                completionHandler: {(data, response, error) -> Void in
                    
                    if let data = data {
                        let responseString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                       
                        if let response = response as? HTTPURLResponse{
                            let statusCode:Int = response.statusCode
                            print("CODE:\(statusCode)")
                            if statusCode == 200 {
                                completion(true,200)
                            } else if statusCode == 500 {
                                completion(true,500)
                            } else if statusCode == 401 {
                                completion(true,401)
                            }
                        }
                        
                        print("****** response data = \(responseString!)")
                    } else if let error = error {
                       completion(false,500)
                        print("ErrorImage",error)
                    }
        })
        task.resume()
       
    }
    
    
    func uploadVideoToServer (videoPath :URL,params:[String:Any], completion: @escaping ((Bool,Int) -> Void)) {
        
        var selectedVideoData: Data?
        do {
            selectedVideoData = try Data(contentsOf: videoPath, options: Data.ReadingOptions.alwaysMapped)
        } catch _ {
            selectedVideoData = nil
            return
        }
       
        var request = createRequest()
        
        guard let videoData = selectedVideoData else { return }
        
        
        let byteCount = videoData.count
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
        bcf.countStyle = .file
        let videoLength = bcf.string(fromByteCount: Int64(byteCount))
        print("VideoPathString==\(videoLength)")
        
        request.httpBody = createBodyWithParameters(parameters:params, filePathKey: "file", imageDataKey: videoData as NSData, boundary: self.boundary,isVideo: true) as Data
    
        let task =  URLSession.shared.dataTask(with: request,
                 completionHandler: {(data, response, error) -> Void in
                    
                    if let data = data {
                        let responseString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                        if let response = response as? HTTPURLResponse{
                            let statusCode = response.statusCode
                            print("CODE:\(statusCode)")
                            if statusCode == 200 {
                                completion(true,200)
                            } else if statusCode == 500 {
                                completion(true,500)
                            } else {
                                completion(true,401)
                            }
                        }
                        print("****** response data = \(responseString!)")
                    } else if let error = error {
                        completion(false,500)
                        print(error)
                    }
        })
        task.resume()
        
    }
}
extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
