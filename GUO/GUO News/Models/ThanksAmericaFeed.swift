//
//  ThanksAmericaFeed.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 08/02/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

struct ThanksAmericaFeed {
    var thanksAmericaFeed:[Post]
    
    enum RootKeys:String,CodingKey {
        case thanksAmericaFeed = "thanksOfAmericaObject"
    }
    
    struct Post:Decodable {
        let postId:Int?
        let publishedDate:String?
        let content:String?
       
        let imageUrl:String?
        let videoUrl:String?
        let isVideo:Int?
        
        let userInfoId:UserInfo?
        
        struct UserInfo:Decodable {
            let userId: Int?
            let profileImageUrl:String?
            let userName:String?
        }
        
        private enum CodingKeys:String,CodingKey {
            case postId = "id"
            case publishedDate = "publishedAtDate"
            case content
            case userInfoId
            case imageUrl = "image"
            case videoUrl = "video"
            case isVideo
        }
    }
}

extension ThanksAmericaFeed:Decodable {
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: RootKeys.self)
        
        var taUnkeyedContainer = try container.nestedUnkeyedContainer(forKey: .thanksAmericaFeed)
        var allTAFeed:[Post] = []
        
        while !taUnkeyedContainer.isAtEnd {
            let taPost = try taUnkeyedContainer.decode(Post.self)
            allTAFeed += [taPost]
        }
        thanksAmericaFeed = allTAFeed
    }
}
