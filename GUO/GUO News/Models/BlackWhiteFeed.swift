//
//  BlackWhiteFeed.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 05/02/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

struct BlackWhiteFeed {
    
    var blackFeed:[Post]
    var whiteFeed:[Post]
    
    enum RootKeys:String,CodingKey {
        case black = "blackObjects"
        case white = "whiteObjects"
    }
    
    
    struct Post:Decodable {
        let postId:Int?
        let content:String?
        let createdDate:String?
        let userInfoId:UserInfo?
        
        struct UserInfo:Decodable {
            let userId: Int?
            let profileImageUrl:String?
            let userName:String?
        }
        
        private enum CodingKeys:String,CodingKey {
            case postId = "id"
            case content
            case userInfoId 
            case createdDate
            
        }
    }
    
    
    
    
    
}



extension BlackWhiteFeed:Decodable {
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: RootKeys.self)
        
        var blackUnkeyedContainer = try container.nestedUnkeyedContainer(forKey: .black)
        var allBlackFeed:[Post] = []
        
        while !blackUnkeyedContainer.isAtEnd {
            let blackPost = try blackUnkeyedContainer.decode(Post.self)
            allBlackFeed += [blackPost]
        }
        blackFeed = allBlackFeed
        
        var whiteUnkeyedContainer = try container.nestedUnkeyedContainer(forKey: .white)
        var allWhiteFeed:[Post] = []
        
        while !whiteUnkeyedContainer.isAtEnd {
            let whitePost = try whiteUnkeyedContainer.decode(Post.self)
            allWhiteFeed += [whitePost]
        }
        whiteFeed = allWhiteFeed
  
    }
}


