//
//  NewsModel.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 30/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

struct NewsFeed {
    
    let topHeadlines:[Headline]
    var latestNewsHeadlines:[LatestNewsHeadline]
    
    enum RootKeys:String,CodingKey {
        case topHeadlines = "topHeadLineNewsObject"
        case latestNewsHeadlines = "everyThingNewsObject"
    }
    
    struct  LatestNewsHeadline:Decodable {
        let title:String?
        let description:String?
        var imageUrl:String?
        var isExpanded:Bool = false
        
        private enum CodingKeys:String,CodingKey {
            case title
            case description
            case imageUrl = "urlToImage"
        }
    }
    
    struct Headline:Decodable {
        let title:String?
        let description:String?
        var imageUrl:String?
        
        private enum CodingKeys:String,CodingKey {
            case title
            case description
            case imageUrl = "urlToImage"
        }
    }
   
}



extension NewsFeed:Decodable {
    init(from decoder: Decoder) throws {
       
        let container = try decoder.container(keyedBy: RootKeys.self)
        
        var topHeadlinesUnkeyedContainer = try container.nestedUnkeyedContainer(forKey: .topHeadlines)
        var allTopHeadlines:[Headline] = []
        
        while !topHeadlinesUnkeyedContainer.isAtEnd {
            let topHeadline = try topHeadlinesUnkeyedContainer.decode(Headline.self)
            allTopHeadlines += [topHeadline]
        }
        topHeadlines = allTopHeadlines
        
        var latestNewsUnkeyedContainer = try container.nestedUnkeyedContainer(forKey: .latestNewsHeadlines)
        var allLatestNewsHeadlines:[LatestNewsHeadline] = []
        
        while !latestNewsUnkeyedContainer.isAtEnd {
            let latestNewsHeadline = try latestNewsUnkeyedContainer.decode(LatestNewsHeadline.self)
            allLatestNewsHeadlines += [latestNewsHeadline]
        }
        latestNewsHeadlines = allLatestNewsHeadlines
    }
}


