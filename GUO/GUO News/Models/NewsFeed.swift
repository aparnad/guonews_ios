//
//  NewsModel.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 30/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

struct NewsFeed {
    
    var topHeadlines:[Headline]
    var latestNewsHeadlines:[Headline]
    
    enum RootKeys:String,CodingKey {
        case topHeadlines = "topHeadLineNewsObject"
        case latestNewsHeadlines = "everyThingNewsObject"
    }
    
    struct  Headline:Decodable {
        let title:String?
        let description:String?
        let url:String?
        let author:String?
        var imageUrl:String?
        var isExpanded:Bool = false
        
        private enum CodingKeys:String,CodingKey {
            case title
            case description
            case url
            case author
            case imageUrl = "urlToImage"
        }
    }
    
   /* struct Headline:Decodable {
        let title:String?
        let description:String?
        var imageUrl:String?
        let url:String?
        
        private enum CodingKeys:String,CodingKey {
            case title
            case description
            case url
            case imageUrl = "urlToImage"
        }
    } */
   
}



extension NewsFeed:Decodable {
    init(from decoder: Decoder) throws {
       
        let container = try decoder.container(keyedBy: RootKeys.self)
        
        var topHeadlinesUnkeyedContainer = try container.nestedUnkeyedContainer(forKey: .topHeadlines)
        var allTopHeadlines:[Headline] = []
        
        while !topHeadlinesUnkeyedContainer.isAtEnd {
            let topHeadline = try topHeadlinesUnkeyedContainer.decode(Headline.self)
            allTopHeadlines += [topHeadline]
        }
        topHeadlines = allTopHeadlines
        
        var latestNewsUnkeyedContainer = try container.nestedUnkeyedContainer(forKey: .latestNewsHeadlines)
        var allLatestNewsHeadlines:[Headline] = []
        
        while !latestNewsUnkeyedContainer.isAtEnd {
            let latestNewsHeadline = try latestNewsUnkeyedContainer.decode(Headline.self)
            allLatestNewsHeadlines += [latestNewsHeadline]
        }
        latestNewsHeadlines = allLatestNewsHeadlines
    }
}


