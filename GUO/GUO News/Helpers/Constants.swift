//
//  Constants.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 22/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit


let localHost = "https://news-services.guo.media/guo-news-services/api/"

//let localHost = "http://52.23.163.20:8080/guo-news-services/api/"

class Constants: NSObject {
    
    // let taBaseUrl = "http://52.23.163.20:8080/guo-news-services"
    // let taSharePostUrl =  "http://52.23.163.20:8080/guo-news-ui/#/thanksAmericaDetails/"
    // let bwSharePostUrl =  "http://52.23.163.20:8080/guo-news-ui/#/blackAndWhiteDetails/"
    
    let bwSharePostUrl = "https://news.guo.media/#/blackAndWhiteDetails/"
    let taSharePostUrl = "https://news.guo.media/#/thanksAmericaDetails/"
    let taBaseUrl = "https://news-services.guo.media/guo-news-services"
    
    let navigationTitleFont = UIFont.systemFont(ofSize: 18, weight: .semibold)
    
    //News Page
    let categoryLabelFont = UIFont(name: "Gill Sans", size: UIScreen.main.bounds.width/35)
    let categoryLabelCnFont = UIFont(name: "Gill Sans", size: UIScreen.main.bounds.width/28)

    let slideHeadlineLabelFont = UIFont(name: "GillSans-SemiBold", size: UIScreen.main.bounds.width/21)
    let headlineTitlelabelFont = UIFont(name: "GillSans-SemiBold", size: UIScreen.main.bounds.width/21)
    let headlinesSubTitleLabelFont = UIFont(name: "GillSans", size: UIScreen.main.bounds.width/23)
    
    //URL
 
   
    let newsUrl =  localHost +  "jpa/getNewsByNewsTypeAndLanguage?"
    
    let bwUrl =  localHost + "jpa/getAllBlackAndWhite"
   
    let blackUrl = localHost + "jpa/getAllBlackAndWhite?type=0"
    let whiteUrl = localHost + "jpa/getAllBlackAndWhite?type=1"
    
    let thanksAmericaUrl =  localHost + "jpa/getAllThanksOfAmerica"
    
    let bwPostUrl = localHost + "secure/addBlackAndWhite"
    
    let taPostUrl = localHost + "secure/addThanksAmerica"
    
    let addUserSessionUrl =  localHost + "jpa/addUserSession"
}
