//
//  SlideTableViewCell.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 25/03/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol SlideTableViewCellDelegate : class {
    func didTapSlideCell(sender:SlideTableViewCell, webUrl:String)
}

class SlideTableViewCell:UITableViewCell {
    var newsResponse:NewsFeed?
    var timer:Timer?
    let slideCellId = "SLIDE"
    var alertLabel: String = ""
    
    weak var delegate:SlideTableViewCellDelegate?
    
    @IBOutlet var slideCollectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var previousButton: UIButton!
    
    override func awakeFromNib() {
        slideCollectionView.delegate = self
        slideCollectionView.dataSource = self
        
    }
    
    
    
    //MARK: TIMER
    
     func startTimer () {
        if self.timer == nil {
            self.timer =  Timer.scheduledTimer(
                timeInterval: TimeInterval(3.0),
                target      : self,
                selector    : #selector(self.nextButton(_:)),
                userInfo    : nil,
                repeats     : true)
        }
    }
    
     func stopTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    //MARK: PAGE CONTROL
    
    private func moveButtonsOffPage(){
        previousButton.isHidden = pageControl.currentPage == 0 ? true : false
        nextButton.isHidden = pageControl.currentPage == newsResponse?.topHeadlines.count ? true : false
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
            let pageNumber = Int(targetContentOffset.pointee.x/contentView.frame.size.width)
            pageControl.currentPage = pageNumber
            moveButtonsOffPage()
    }
    
    @IBAction func nextButton(_ sender: Any) {
        
        if pageControl.currentPage == (newsResponse?.topHeadlines.count)! - 1 {
            
            let indexPath = IndexPath(item:0 , section: 0)
            pageControl.currentPage = 0
            slideCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        } else {
            let indexPath = IndexPath(item:pageControl.currentPage + 1 , section: 0)
           
            pageControl.currentPage = pageControl.currentPage + 1
            slideCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
        moveButtonsOffPage()
    }
    
    @IBAction func previousButton(_ sender: Any) {
        let indexPath = IndexPath(item:pageControl.currentPage - 1 , section: 0)
        pageControl.currentPage = pageControl.currentPage - 1
        slideCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        moveButtonsOffPage()
    }
    
    @IBAction func pageControlAction(_ sender: Any) {
        let page: Int? = (sender as AnyObject).currentPage
        var frame: CGRect = self.slideCollectionView.frame
        frame.origin.x = frame.size.width * CGFloat(page ?? 0)
        frame.origin.y = 0
        self.slideCollectionView.scrollRectToVisible(frame, animated: true)
    }
}


extension SlideTableViewCell:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if newsResponse?.topHeadlines == nil || newsResponse?.topHeadlines.isEmpty == true {
            stopTimer()
            previousButton.isHidden = true
            nextButton.isHidden = true
          //  self.slideCollectionView.setEmptyMessage(alertLabel)
            if (newsResponse?.topHeadlines == nil || newsResponse?.topHeadlines.isEmpty == true) && (newsResponse?.latestNewsHeadlines.isEmpty == false) {
                self.slideCollectionView.setEmptyMessage(alertLabel)

            }
            return 0
        }  else {
            slideCollectionView.backgroundView = nil
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return (newsResponse?.topHeadlines.count ?? 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = slideCollectionView.dequeueReusableCell(withReuseIdentifier: slideCellId, for: indexPath) as! SlideCell
            cell.newsHeadline = newsResponse?.topHeadlines[indexPath.item]
            return cell
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize (width: slideCollectionView.frame.width, height: UIScreen.main.bounds.height * 0.3) //slidecollectionview height is not calculated yet. So, used default height used for slidecollection view
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        if collectionView == slideCollectionView {
            let cell = slideCollectionView.cellForItem(at: indexPath) as! SlideCell
            
            if cell.slideHeadlineLabel.text == "" {
                return
            }
            if let webUrl = cell.webUrl{
                delegate?.didTapSlideCell(sender: self, webUrl: webUrl)
            }
        }
    }
}

