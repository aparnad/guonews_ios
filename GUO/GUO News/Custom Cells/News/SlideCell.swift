//
//  SlideCell.swift
//  GUO Media
//
//  Created by apple on 1/10/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import SDWebImage

class SlideCell: UICollectionViewCell {
    @IBOutlet var slideImageView: UIImageView!
    @IBOutlet var slideHeadlineLabel: UILabel!
    
    var webUrl:String?
    
    var newsHeadline:NewsFeed.Headline? {
        didSet {
            guard let newsHeadline = newsHeadline else { return }
            
            if var title = newsHeadline.title {
                
                if  title.count > 10 {
                    title =  title.replacingOccurrences(of: "�", with: "")
            //slideHeadlineLabel.text = title
                  slideHeadlineLabel.text = title.htmlToString
                } else {
                    var description : String = String(newsHeadline.description!.prefix(50))
                    description =  description.replacingOccurrences(of: "�", with: "")
                   // slideHeadlineLabel.text = description
                   slideHeadlineLabel.text = description.htmlToString

                }
               // slideHeadlineLabel.text = title == "NULL" ? "" : title
            }
            
            if var imageUrl = newsHeadline.imageUrl {
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                //slideImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "NoImageAvailable"))
                slideImageView?.sd_setImage(with: URL(string: imageUrl)) { (image, error, cache, urls) in
                    if (error != nil) {
                        self.slideImageView.image = UIImage(named: "slideNoImage")
                    } else {
                        self.slideImageView.image = image
                    }
                }
            }
            
            if let url = newsHeadline.url{
                webUrl = url
            }
              
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.slideImageView?.image = UIImage(named: "default-postImg")
    }
    
    
    override func awakeFromNib() {
    slideHeadlineLabel.font = Constants().slideHeadlineLabelFont
    }
}
