//
//  NewsTableCell.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 11/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import SDWebImage


protocol NewsTableCellDelegate : class {
    func newsTableCellDidTapMore(_ sender: NewsTableCell)
    func newsShareButtonAction(_ sender: NewsTableCell)
    
}

class NewsTableCell: UITableViewCell {
    @IBOutlet var headlinesImageView: UIImageView!
    @IBOutlet var headlinesTitleLabel: UILabel!
    @IBOutlet var headlinesSubTitleLabel: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet var moreButton: UIButton!
    
    weak var delegate: NewsTableCellDelegate?
    
    var webUrl:String?
    
    let minHeight: CGFloat = UIScreen.main.bounds.height/7
    
    var newsHeadline:NewsFeed.Headline? {
        didSet {
            guard let newsHeadline = newsHeadline else { return }
            
           moreButton.isEnabled = newsHeadline.isExpanded
            
            if var title = newsHeadline.title, var description = newsHeadline.description {
                
                if  title.count > 10 {
                    title =  title.replacingOccurrences(of: "�", with: "")
                    //headlinesTitleLabel.text = title
                    headlinesTitleLabel.text = title.htmlToString
                    
                } else {
                    description = String(description.prefix(50))
                    description =  description.replacingOccurrences(of: "�", with: "")
                   // headlinesTitleLabel.text = description
                    headlinesTitleLabel.text = description.htmlToString

                }
             //   headlinesTitleLabel.text = title == "NULL" ? "" : title
            } else {
                headlinesTitleLabel.text = ""
            }
            
            if var description = newsHeadline.description, var author = newsHeadline.author {
                description = description == "NULL" ? "" : description
                description =  description.replacingOccurrences(of: "�", with: "")
                author =  author.replacingOccurrences(of: "By", with: "")
                
                if author == "" || author == "NULL"  || (author.trimmingCharacters(in: .whitespaces) == "") {
                    author = ""
                } else {
                    author =  ("\n" + "By".localized + " " +  author)
                }
                
                author =  author.replacingOccurrences(of: "�", with: "")
                 headlinesSubTitleLabel.text = newsHeadline.isExpanded ? (description + author) : author
            } else {
                headlinesSubTitleLabel.text = ""
            }
            
            if var imageUrl = newsHeadline.imageUrl {
                //print("imageUrl==\(imageUrl)")
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                //print("imageUrl==\(imageUrl)")

               // headlinesImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "NoImageAvailable"))
                headlinesImageView?.sd_setImage(with: URL(string: imageUrl)) { (image, error, cache, urls) in
                    if (error != nil) {
                        self.headlinesImageView.image = UIImage(named: "everythingNoImage")
                    } else {
                        self.headlinesImageView.image = image
                    }
                }
            } 
            
            if let url = newsHeadline.url {
                webUrl = url
            }
            
            if var author = newsHeadline.author {
                 author =  author.replacingOccurrences(of: "By", with: "")
                
                if author == "" || author == "NULL"  || (author.trimmingCharacters(in: .whitespaces) == "") {
                    author = ""
                } else {
                    author =  ("\n" + "By".localized + " " +  author)
                }
               
                author =  author.replacingOccurrences(of: "�", with: "")
               // author = author == "NULL" ? "" : "By".localized + " " + author
                if newsHeadline.isExpanded {
                    moreButton.setTitle("View More".localized, for: .normal)
                    moreButton.titleLabel?.font = Constants().headlinesSubTitleLabelFont

                } else {
                    moreButton.setTitle("", for: .normal)
                   /* moreButton.setTitle(author, for: .disabled)
                    moreButton.setTitleColor(.darkGray, for: .disabled)
                    moreButton.backgroundColor = UIColor.red

                    moreButton.titleLabel?.font = Constants().headlinesSubTitleLabelFont */
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.headlinesImageView?.image = UIImage(named: "default-postImg")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        headlinesTitleLabel.font = Constants().headlineTitlelabelFont
        headlinesSubTitleLabel.font = Constants().headlinesSubTitleLabelFont
        let height = minHeight - 10
        headlinesImageView.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
        override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
            let size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
            return CGSize(width: size.width, height: max(size.height, minHeight))
        }
    
    @IBAction func moreTapped(_ sender: Any) {
        delegate?.newsTableCellDidTapMore(self)
    }
    
    @IBAction func shareButtonAction(_ sender: Any) {
        delegate?.newsShareButtonAction(self)
    }
    
}
