//
//  TACell.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 22/02/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol TACellDelegate : class {
    func thanksAmericaCellDidTapPlay(_ sender: ThanksAmericaCell)
    func thanksAmericaCellDidTapShare(_ sender: ThanksAmericaCell)
    func thanksAmericaCellDidTapImage(_ sender: ThanksAmericaCell)
}

class ThanksAmericaCell: UITableViewCell,ASAutoPlayVideoLayerContainer {
    @IBOutlet var mainView: UIView!
    @IBOutlet var mediaContainerView: UIView!
    
    @IBOutlet var postLabel: UILabel!
    @IBOutlet var profileUserNameLabel: UILabel!
    @IBOutlet var timeStampLabel: UILabel!
    
    @IBOutlet var postImageView: UIImageView!
    @IBOutlet var playButton: UIButton!
    
    weak var delegate:TACellDelegate?
    
    let baseUrl = "http://52.23.163.20:8080/guo-news-services"
    
    var videoURL: String?
    var imageURL:String?
    
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(playButton.frame, from: playButton)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
                return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }
    
    func pauseVideo() {
        ASVideoPlayerController.sharedVideoPlayer.pauseVideo(forLayer: videoLayer, url: videoURL!)
        videoLayer.player?.currentItem?.seek(to: .zero)
        if let imageView = playButton.imageView {
            playButton.setImage(UIImage(named: "ic_play_video"), for: .normal)
            playButton.bringSubviewToFront(imageView)
        }
    }
    
    
    func getPlayerCurrentTime() -> CMTime? {
        return (videoLayer.player?.currentItem?.currentTime())
    }
    
    
    var thanksAmericaPost:ThanksAmericaFeed.Post? {
        didSet{
            guard let thanksAmericaPost = thanksAmericaPost else { return }
            
            
            if let profileUserName = thanksAmericaPost.userInfoId?.userName {
                profileUserNameLabel.text = profileUserName
            }
            
            if let publishedDate = thanksAmericaPost.publishedDate {
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "dd/MM/yyyy"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "yyyy/MM/dd"
                
                if let date = dateFormatterGet.date(from: publishedDate) {
                    timeStampLabel.text = dateFormatterPrint.string(from: date)
                } else {
                    timeStampLabel.text = publishedDate
                }
            }
            
            if let content = thanksAmericaPost.content {
                postLabel.text = content
            }
            if let isVideo = thanksAmericaPost.isVideo {
                postImageView.isHidden = isVideo == 1 ? true : false
                playButton.isHidden = isVideo == 0 ? true : false
            }
            if let imageUrl = thanksAmericaPost.imageUrl{
                imageURL = baseUrl + imageUrl
                
                postImageView.sd_setImage(with: URL(string: imageURL!), placeholderImage: UIImage(named: "NoImageAvailable"))
            }
            
            if let videoUrl = thanksAmericaPost.videoUrl {
                videoURL = baseUrl + videoUrl
                //videoURL = "http://jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v"
                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL!)
                videoLayer.isHidden = videoURL == nil
                playButton.setImage(nil, for: .normal)
            }
            
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let width: CGFloat = playButton.frame.width
        let height: CGFloat = playButton.frame.height
        videoLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)
    }
    
    override func prepareForReuse() {
        postImageView.imageURL = nil
        super.prepareForReuse()
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        postImageView.layer.cornerRadius = 5
        postImageView.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
        postImageView.clipsToBounds = true
        postImageView.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
        postImageView.layer.borderWidth = 0.5
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(self.imageTapped(gestureRecgonizer:)))
        postImageView.addGestureRecognizer(tapGesture)
        
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resize
        playButton.layer.addSublayer(videoLayer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func playVideo(_ sender: Any) {
        delegate?.thanksAmericaCellDidTapPlay(self)
    }
    
    @IBAction func sharePost(_ sender: Any) {
        self.delegate?.thanksAmericaCellDidTapShare(self)
    }
    
    @objc func imageTapped(gestureRecgonizer: UITapGestureRecognizer) {
        self.delegate?.thanksAmericaCellDidTapImage(self)
    }
    
}
