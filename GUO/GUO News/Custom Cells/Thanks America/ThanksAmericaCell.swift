//
//  TACell.swift
//  GUO Media
//
//  Created by Easyway_Mac2 on 22/02/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import SDWebImage
import FLAnimatedImage

protocol TACellDelegate : class {
    func thanksAmericaCellDidTapPlay(_ sender: ThanksAmericaCell)
    func thanksAmericaCellDidTapShare(_ sender: ThanksAmericaCell)
    func thanksAmericaCellDidTapImage(_ sender: ThanksAmericaCell)
}

class ThanksAmericaCell: UITableViewCell,ASAutoPlayVideoLayerContainer {
    @IBOutlet var mainView: UIView!
    @IBOutlet var mediaContainerView: UIView!
    @IBOutlet var postLabel: UILabel!
    @IBOutlet var profileUserNameLabel: UILabel!
    @IBOutlet var timeStampLabel: UILabel!
   // @IBOutlet var postImageView: UIImageView!
    @IBOutlet var postImageView: FLAnimatedImageView!

    //FLAnimatedImageView!
    @IBOutlet var profileButton: UIButton!
    @IBOutlet var playButton: UIButton!
    
    
    weak var delegate:TACellDelegate?
    
   
   
    
    var videoURL: String?
    var imageURL:String?
    var postId:Int?
    
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(postImageView.frame, from: postImageView)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
                return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }
    
    func pauseVideo() {
        ASVideoPlayerController.sharedVideoPlayer.pauseVideo(forLayer: videoLayer, url: videoURL!)
        videoLayer.player?.currentItem?.seek(to: .zero)
        playButton.isHidden = false
    }
    
    
    func getPlayerCurrentTime() -> CMTime? {
        return (videoLayer.player?.currentItem?.currentTime())
    }
    
    
    var thanksAmericaPost:ThanksAmericaFeed.Post? {
        didSet{
            guard let thanksAmericaPost = thanksAmericaPost else { return }
            
            if let profileUserName = thanksAmericaPost.userInfoId?.userName {
                profileUserNameLabel.text = profileUserName
            }
            
            if let publishedDate = thanksAmericaPost.publishedDate {
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "dd/MM/yyyy"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "yyyy/MM/dd"
                
                if let date = dateFormatterGet.date(from: publishedDate) {
                    timeStampLabel.text = dateFormatterPrint.string(from: date)
                } else {
                    timeStampLabel.text = publishedDate
                }
            }
            
            if let content = thanksAmericaPost.content {
                postLabel.text = content
            }
            if let isVideo = thanksAmericaPost.isVideo {
                videoLayer.isHidden = isVideo == 1 ? false : true
                videoLayer.backgroundColor = UIColor.black.cgColor

                //playButton.isHidden = isVideo == 0 ? true : false
            }
            if let imageUrl = thanksAmericaPost.imageUrl{
                imageURL = Constants().taBaseUrl + imageUrl
                imageURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
                if (imageURL?.contains(".gif"))!{
                    print("gifimage==\(imageURL)")
                    let url = URL(string: imageURL!)!


                    
                   /* let imageData = try? Data(contentsOf: url)
                    let imageData3 = FLAnimatedImage(animatedGIFData: imageData)
                        self.postImageView.animatedImage = imageData3*/
                    
                    
                    DispatchQueue.global().async { [weak self] in
                        do {
                            let gifData = try Data(contentsOf: url)
                            DispatchQueue.main.async {
                                self?.postImageView.animatedImage = FLAnimatedImage(gifData: gifData)
                            }
                        } catch {
                            print("GIFERROR==",error)
                        }
                    }
                    
                    
                   /* let urlString = "https://upload.wikimedia.org/wikipedia/commons/2/2c/Rotating_earth_%28large%29.gif"
                    let url = URL(string: urlString)!
                    let imageData = try? Data(contentsOf: url)
                    let imageData3 = FLAnimatedImage(animatedGIFData: imageData)
                    image3.animatedImage = imageData3*/

                }
                else  { postImageView?.sd_setImage(with: URL(string: imageURL!)) { (image, error, cache, urls) in
                    if (error != nil) {
                        //self.postImageView.image = UIImage(named: "NoImageAvailable")
                        self.postImageView.image = UIImage(named: "slideNoImage")

                    } else {
                        self.postImageView.image = image
                    }
                }
            }
               // postImageView.sd_setImage(with: URL(string: imageURL!), placeholderImage: UIImage(named: "NoImageAvailable"))
            } else {
                imageURL = nil
            }
            
            
            if let videoUrl = thanksAmericaPost.videoUrl {
                videoURL =  Constants().taBaseUrl + videoUrl
                videoURL = videoURL?.replacingOccurrences(of: " ", with: "%20")
                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL!)
                if videoLayer.backgroundColor == UIColor.black.cgColor {
                    playButton.isHidden = false
                } else {
                    playButton.isHidden = true
                }
            } else {
                videoURL = nil
            }
            
            
            if let id = thanksAmericaPost.postId{
                postId = id
            }
            
            if var profileImageUrl = thanksAmericaPost.userInfoId?.profileImageUrl {
                profileImageUrl = "https://d57iplyuvntm7.cloudfront.net/uploads/" + profileImageUrl
                profileButton.sd_setImage(with: URL(string: profileImageUrl), for: .normal, completed: nil)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let width: CGFloat = postImageView.frame.width
        let height: CGFloat = postImageView.frame.height
        videoLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.postImageView?.image = UIImage(named: "default-postImg")
        playButton.isHidden = true
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        postImageView.layer.cornerRadius = 5
        postImageView.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
        postImageView.clipsToBounds = true
        postImageView.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
        postImageView.layer.borderWidth = 0.5
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(self.imageTapped(gestureRecgonizer:)))
        postImageView.addGestureRecognizer(tapGesture)
        
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resize
        postImageView.layer.addSublayer(videoLayer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func playVideo(_ sender: Any) {
        delegate?.thanksAmericaCellDidTapPlay(self)
    }
    
    @IBAction func sharePost(_ sender: Any) {
        self.delegate?.thanksAmericaCellDidTapShare(self)
    }
    
    @objc func imageTapped(gestureRecgonizer: UITapGestureRecognizer) {
        self.delegate?.thanksAmericaCellDidTapImage(self)
    }
    
}
