//
//  BWCell.swift
//  GUO Media
//
//  Created by apple on 5/13/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import SDWebImage

protocol Black_WhiteCellDelegate : class {
    func bwCellDidTapShare(_ sender: BWCell)
}
class BWCell: UITableViewCell {
    
    @IBOutlet var bottomView: UIView!
    @IBOutlet var postTextView: UITextView!
    @IBOutlet var profileButton: UIButton!
    @IBOutlet var profileUserNameLabel: UILabel!
    @IBOutlet var seperatorView: UIView!
    @IBOutlet var timeStampLabel: UILabel!
    
    
    weak var delegate: Black_WhiteCellDelegate?
    
    var postId:Int?
    
    var black_whitePost:BlackWhiteFeed.Post? {
        didSet{
            guard let black_WhitePost = black_whitePost else { return }
            
            if let profileUserName = black_WhitePost.userInfoId?.userName {
                profileUserNameLabel.text = profileUserName
            }
            
            if let timeStamp = black_WhitePost.createdDate {
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "dd/MM/yyyy"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "yyyy-MM-dd"
                
                if let date = dateFormatterGet.date(from: timeStamp) {
                    timeStampLabel.text = dateFormatterPrint.string(from: date)
                } else {
                    timeStampLabel.text = timeStamp
                }
                
            }
            
            if let message = black_WhitePost.content {
                postTextView.text = message
            }
            
            if let id = black_WhitePost.postId{
                postId = id
            }
            
            if var profileImageUrl = black_WhitePost.userInfoId?.profileImageUrl {
                profileImageUrl = "https://d57iplyuvntm7.cloudfront.net/uploads/" + profileImageUrl
                profileButton.sd_setImage(with: URL(string: profileImageUrl), for: .normal, completed: nil)
            }
            
        }
    }
    
    override func layoutSubviews() {
        postTextView.centerVertically()
    }
    
    override func awakeFromNib() {
        backgroundColor = .black
        
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        let size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
        return CGSize(width: size.width, height: max(size.height, 170.0))
    }
    
    
    
    @IBAction func sharePost(_ sender: UIButton) {
        self.delegate?.bwCellDidTapShare(self)
    }
    
}
